<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function index()
    {
        /*$lang = $this->input->post(COL_LANGUAGEID);
        $keyword = $this->input->post("Keyword");

        $this->db->select(TBL_TRANSLATION.".*, ".TBL_LANGUAGE.".".COL_LANGUAGENAME." as ".COL_LANGUAGENAME);
        $this->db->join(TBL_LANGUAGE,TBL_LANGUAGE.'.'.COL_LANGUAGEID." = ".TBL_TRANSLATION.".".COL_LANGUAGEID,"inner");

        if(!empty($keyword)) {
            $this->db->like(TBL_TRANSLATION.'.'.COL_WORD, $keyword);
            if(!empty($lang)) $this->db->where(TBL_LANGUAGE.'.'.COL_LANGUAGEID, $lang);
        }
        else {
            $this->db->where(TBL_LANGUAGE.'.'.COL_LANGUAGEID, -999);
        }

        $this->db->order_by(COL_WORD, 'asc');
        $data['res'] = $this->db->get(TBL_TRANSLATION)->result_array();

        $this->load->view('home/index', $data);*/
        //redirect('user/dashboard');
        if(IsLogin()) {
            //redirect('user/dashboard');
        }
        if(!IsLogin()) {
            //redirect('user/login');
        }
        $data['title'] = 'Beranda';
        /*$this->db->select('*, (SELECT COUNT(*) FROM mkeluarga kk WHERE kk.KdDasawisma=mdasawisma.KdDasawisma) AS COUNT_KK');
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        $this->db->order_by(COL_NMKELOMPOK, 'asc');
        $data['res'] = $this->db->get(TBL_MDASAWISMA)->result_array();

        $this->load->view('home/index', $data);*/
        $this->load->view('home/index');
        $eplandb = $this->load->database("eplan", true);
        $eplandb->select('*, CONCAT('.COL_KD_URUSAN.',\'|\','.COL_KD_BIDANG.',\'|\','.COL_KD_UNIT.',\'|\','.COL_KD_SUB.') AS ID, CONCAT('.COL_KD_URUSAN.',\'.\','.COL_KD_BIDANG.',\'.\','.COL_KD_UNIT.',\'.\','.COL_KD_SUB.',\' \',Nm_Sub_Unit) AS Text');
        $data['res'] = $eplandb->get("ref_sub_unit")->result_array();
    }

    public function status_opd() {
        $data['title'] = 'Status OPD';

        $filter = array(
            COL_KD_URUSAN=>$this->input->get(COL_KD_URUSAN),
            COL_KD_BIDANG=>$this->input->get(COL_KD_BIDANG),
            COL_KD_UNIT=>$this->input->get(COL_KD_UNIT),
            COL_KD_SUB=>$this->input->get(COL_KD_SUB)
        );
        $data['filter_opd'] = $filter;
        $data['opd'] = array();
        if(!empty($_GET)) {
            $eplandb = $this->load->database("eplan", true);
            $eplandb->select('*, CONCAT('.COL_KD_URUSAN.',\'|\','.COL_KD_BIDANG.',\'|\','.COL_KD_UNIT.',\'|\','.COL_KD_SUB.') AS ID, CONCAT('.COL_KD_URUSAN.',\'.\','.COL_KD_BIDANG.',\'.\','.COL_KD_UNIT.',\'.\','.COL_KD_SUB.',\' \',Nm_Sub_Unit) AS Text');
            if(!empty($filter[COL_KD_URUSAN])) $eplandb->where(COL_KD_URUSAN, $filter[COL_KD_URUSAN]);
            if(!empty($filter[COL_KD_BIDANG])) $eplandb->where(COL_KD_BIDANG, $filter[COL_KD_BIDANG]);
            if(!empty($filter[COL_KD_UNIT])) $eplandb->where(COL_KD_UNIT, $filter[COL_KD_UNIT]);
            if(!empty($filter[COL_KD_SUB])) $eplandb->where(COL_KD_SUB, $filter[COL_KD_SUB]);
            $eplandb->order_by(COL_KD_URUSAN, 'asc');
            $eplandb->order_by(COL_KD_BIDANG, 'asc');
            $eplandb->order_by(COL_KD_UNIT, 'asc');
            $eplandb->order_by(COL_KD_SUB, 'asc');
            $data['opd'] = $eplandb->get("ref_sub_unit")->result_array();
        }

        $this->load->view('home/status_opd', $data);
    }

    function _404() {
        $this->load->view('home/error');
    }
}
