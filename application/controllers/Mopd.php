<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 10:57
 */
class Mopd extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEBAPPEDA && GetLoggedUser()[COL_ROLEID] != ROLEKADIS)) {
            redirect('user/dashboard');
        }
    }

    function tujuan() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Tujuan OPD';
        $this->db->select('*,'.TBL_SAKIP_MOPD_TUJUAN.'.'.COL_UNIQ.' as ID');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORSASARAN,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SUB, $strOPD[3]);
        }
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->group_by(array(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUANOPD, TBL_SAKIP_MOPD_TUJUAN.".".COL_NM_TUJUANOPD));
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_TUJUAN)->result_array();
        $this->load->view('mopd/tujuan', $data);
    }

    function tujuan_add() {
        $data['title'] = "Tujuan OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/tujuan');

            $kdMisi = $this->input->post(COL_KD_MISI);
            $kdTujuan = $this->input->post(COL_KD_TUJUAN);
            $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
            $kdSasaran = $this->input->post(COL_KD_SASARAN);
            $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
            $dataInsert = [];

            $detNo = $this->input->post("NoDet");
            $detDesc = $this->input->post("KetDet");
            $det = [];

            try {
                $this->db->trans_begin();
                for($i=0; $i<count($kdMisi); $i++) {
                    $dataInsert[] = array(
                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $kdMisi[$i],
                        COL_KD_TUJUAN => $kdTujuan[$i],
                        COL_KD_INDIKATORTUJUAN => $kdIkTujuan[$i],
                        COL_KD_SASARAN => $kdSasaran[$i],
                        COL_KD_INDIKATORSASARAN => $kdIkSasaran[$i],
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_NM_TUJUANOPD => $this->input->post(COL_NM_TUJUANOPD)
                    );
                    for($d = 0; $d<count($detNo); $d++) {
                        $det[] = array(
                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $kdMisi[$i],
                            COL_KD_TUJUAN => $kdTujuan[$i],
                            COL_KD_INDIKATORTUJUAN => $kdIkTujuan[$i],
                            COL_KD_SASARAN => $kdSasaran[$i],
                            COL_KD_INDIKATORSASARAN => $kdIkSasaran[$i],
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $detNo[$d],
                            COL_NM_INDIKATORTUJUANOPD => $detDesc[$d]
                        );
                    }
                }
                if(count($dataInsert) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MOPD_TUJUAN, $dataInsert);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                if(count($det) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MOPD_IKTUJUAN, $det);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                $this->db->trans_commit();
                echo json_encode($resp);

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error__message'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            if(!empty($rpemda)) {
                $data['data'] = array(
                    COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                    "DefPeriod" => $rpemda[COL_KD_TAHUN_FROM]." s.d ".$rpemda[COL_KD_TAHUN_TO]." : ".$rpemda[COL_NM_PEJABAT]
                );
            }

            $this->load->view('mopd/tujuan_form',$data);
        }
    }

    function tujuan_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            ->select(TBL_SAKIP_MOPD_TUJUAN.'.*,'.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.','.TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.','.TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN.','.TBL_SAKIP_MPMD_SASARAN.'.'.COL_NM_SASARAN.','.TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_NM_INDIKATORSASARAN)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI,"inner")
            ->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORTUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SASARAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_INDIKATORSASARAN,"inner")
            ->where(TBL_SAKIP_MOPD_TUJUAN.'.'.COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SUB, $strOPD[3]);
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MOPD_TUJUAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Tujuan OPD";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/tujuan');
            $cond = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
            );
            $data = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                /*COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),*/
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_NM_TUJUANOPD => $this->input->post(COL_NM_TUJUANOPD)
            );

            try {
                $this->db->trans_begin();
                if(!$this->db->where($cond)->update(TBL_SAKIP_MOPD_TUJUAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }

                $detNo = $this->input->post("NoDet");
                $detKet = $this->input->post("KetDet");
                $arrDet = [];

                /* update / delete */
                $det = $this->db
                    //->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN.",".COL_KD_SASARAN.",".COL_KD_INDIKATORSASARAN)
                    ->where(COL_KD_URUSAN, $data[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $data[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $data[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $data[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $data[COL_KD_PEMDA])
                    /*->where(COL_KD_MISI, $data[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $data[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $data[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $data[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $data[COL_KD_INDIKATORSASARAN])*/
                    ->where(COL_KD_TUJUANOPD, $data[COL_KD_TUJUANOPD])
                    ->group_by(array(COL_KD_URUSAN,COL_KD_BIDANG,COL_KD_UNIT,COL_KD_SUB,COL_KD_PEMDA,COL_KD_TUJUANOPD,COL_KD_INDIKATORTUJUANOPD))
                    ->get(TBL_SAKIP_MOPD_IKTUJUAN)
                    ->result_array();
                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),

                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        /*COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN],
                        COL_KD_SASARAN=>$d[COL_KD_SASARAN],
                        COL_KD_INDIKATORSASARAN=>$d[COL_KD_INDIKATORSASARAN],*/
                        COL_KD_TUJUANOPD=>$d[COL_KD_TUJUANOPD],
                        COL_KD_INDIKATORTUJUANOPD=>$d[COL_KD_INDIKATORTUJUANOPD]
                    );
                    if(!empty($detNo) && in_array($d[COL_KD_INDIKATORTUJUANOPD], $detNo)) {
                        $res = $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MOPD_IKTUJUAN, array(
                                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                /*COL_KD_MISI => $this->input->post(COL_KD_MISI),
                                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),*/
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),

                                COL_KD_INDIKATORTUJUANOPD => $d[COL_KD_INDIKATORTUJUANOPD],
                                COL_NM_INDIKATORTUJUANOPD => $detKet[array_search($d[COL_KD_INDIKATORTUJUANOPD], $detNo)]
                            ));
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    else {
                        $res = $this->db->delete(TBL_SAKIP_MOPD_IKTUJUAN, $arrCond);
                        if(!$res) {
                            throw new Exception("Database error: ".$this->db->error());
                        }
                    }
                    $detUpdated[] = $d[COL_KD_INDIKATORTUJUANOPD];
                }
                /* update / delete */

                /* insert */
                $kdMisi = $this->input->post(COL_KD_MISI);
                $kdTujuan = $this->input->post(COL_KD_TUJUAN);
                $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
                $kdSasaran = $this->input->post(COL_KD_SASARAN);
                $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
                for($m=0; $m<count($kdMisi); $m++) {
                    for($i = 0; $i<count($detNo); $i++) {
                        if(!in_array($detNo[$i], $detUpdated)) {
                            $arrDet[] = array(
                                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $kdMisi[$m],
                                COL_KD_TUJUAN => $kdTujuan[$m],
                                COL_KD_INDIKATORTUJUAN => $kdIkTujuan[$m],
                                COL_KD_SASARAN => $kdSasaran[$m],
                                COL_KD_INDIKATORSASARAN => $kdIkSasaran[$m],
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),

                                COL_KD_INDIKATORTUJUANOPD => $detNo[$i],
                                COL_NM_INDIKATORTUJUANOPD => $detKet[$i]
                            );
                        }
                    }
                }

                if(count($arrDet) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MOPD_IKTUJUAN, $arrDet);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                /* insert */

                $this->db->trans_commit();
                echo json_encode($resp);

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('mopd/tujuan_form',$data);
        }
    }

    function tujuan_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            try {
                $this->db->trans_begin();
                $rtujuan = $this->db->where(COL_UNIQ, $datum)->get(TBL_SAKIP_MOPD_TUJUAN)->row_array();
                $arrCond = array(
                    COL_KD_URUSAN => $rtujuan[COL_KD_URUSAN],
                    COL_KD_BIDANG => $rtujuan[COL_KD_BIDANG],
                    COL_KD_UNIT => $rtujuan[COL_KD_UNIT],
                    COL_KD_SUB => $rtujuan[COL_KD_SUB],

                    COL_KD_PEMDA=>$rtujuan[COL_KD_PEMDA],
                    /*COL_KD_MISI=>$rtujuan[COL_KD_MISI],
                    COL_KD_TUJUAN=>$rtujuan[COL_KD_TUJUAN],
                    COL_KD_INDIKATORTUJUAN=>$rtujuan[COL_KD_INDIKATORTUJUAN],
                    COL_KD_SASARAN=>$rtujuan[COL_KD_SASARAN],
                    COL_KD_INDIKATORSASARAN=>$rtujuan[COL_KD_INDIKATORSASARAN],*/
                    COL_KD_TUJUANOPD=>$rtujuan[COL_KD_TUJUANOPD]
                );
                $this->db->delete(TBL_SAKIP_MOPD_IKTUJUAN, $arrCond);
                $this->db->delete(TBL_SAKIP_MOPD_TUJUAN, /*array(COL_UNIQ => $datum)*/$arrCond);
                $deleted++;
                $this->db->trans_commit();

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }

        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function sasaran() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Sasaran OPD';
        $this->db->select('*,'.TBL_SAKIP_MOPD_SASARAN.'.'.COL_UNIQ.' as ID');
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN,"inner");
        $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN,"inner");
        $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN. " AND ".
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUANOPD
            ,"inner");
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SUB, $strOPD[3]);
        }
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc');
        //$this->db->group_by(array(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA, TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD));
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_SASARAN)->result_array();
        $this->load->view('mopd/sasaran', $data);
    }

    function sasaran_add() {
        $data['title'] = "Sasaran OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/sasaran');

            /*$kdMisi = $this->input->post(COL_KD_MISI);
            $kdTujuan = $this->input->post(COL_KD_TUJUAN);
            $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
            $kdSasaran = $this->input->post(COL_KD_SASARAN);
            $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
            $dataInsert = [];

            $detNo = $this->input->post("NoDet");
            $detDesc = $this->input->post("KetDet");
            $detFormula = $this->input->post("FormulaDet");
            $detSumber = $this->input->post("SumberDet");
            $detPIC = $this->input->post("PICDet");
            $det = [];

            try {
                $this->db->trans_begin();
                for($i=0; $i<count($kdMisi); $i++) {
                    $dataInsert[] = array(
                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $kdMisi[$i],
                        COL_KD_TUJUAN => $kdTujuan[$i],
                        COL_KD_INDIKATORTUJUAN => $kdIkTujuan[$i],
                        COL_KD_SASARAN => $kdSasaran[$i],
                        COL_KD_INDIKATORSASARAN => $kdIkSasaran[$i],
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                        COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                        COL_NM_SASARANOPD => $this->input->post(COL_NM_SASARANOPD)
                    );
                    for($d = 0; $d<count($detNo); $d++) {
                        $det[] = array(
                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $kdMisi[$i],
                            COL_KD_TUJUAN => $kdTujuan[$i],
                            COL_KD_INDIKATORTUJUAN => $kdIkTujuan[$i],
                            COL_KD_SASARAN => $kdSasaran[$i],
                            COL_KD_INDIKATORSASARAN => $kdIkSasaran[$i],
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),

                            COL_KD_INDIKATORSASARANOPD => $detNo[$d],
                            COL_NM_INDIKATORSASARANOPD => $detDesc[$d],
                            COL_NM_FORMULA => $detFormula[$d],
                            COL_NM_SUMBERDATA => $detSumber[$d],
                            COL_NM_PENANGGUNGJAWAB => $detPIC[$d]
                        );
                    }
                }
                if(count($dataInsert) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MOPD_SASARAN, $dataInsert);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                if(count($det) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MOPD_IKSASARAN, $det);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                $this->db->trans_commit();
                echo json_encode($resp);

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }*/
            $data = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_NM_SASARANOPD => $this->input->post(COL_NM_SASARANOPD)
            );
            try {
                $this->db->trans_begin();
                if(!$this->db->insert(TBL_SAKIP_MOPD_SASARAN, $data)){
                    throw new Exception("Database error: ".$this->db->error());
                }

                $detNo = $this->input->post("NoDet");
                $detDesc = $this->input->post("KetDet");
                $detFormula = $this->input->post("FormulaDet");
                $detSumber = $this->input->post("SumberDet");
                $detPIC = $this->input->post("PICDet");
                $det = [];
                for($i = 0; $i<count($detNo); $i++) {
                    $det[] = array(
                        COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                        COL_KD_MISI => $this->input->post(COL_KD_MISI),
                        COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                        COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                        COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                        COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                        COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                        COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                        COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                        COL_KD_SUB => $this->input->post(COL_KD_SUB),
                        COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                        COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                        COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                        COL_KD_INDIKATORSASARANOPD => $detNo[$i],
                        COL_NM_INDIKATORSASARANOPD => $detDesc[$i],
                        COL_NM_FORMULA => $detFormula[$i],
                        COL_NM_SUMBERDATA => $detSumber[$i],
                        COL_NM_PENANGGUNGJAWAB => $detPIC[$i]
                    );
                }
                if(count($det) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MOPD_IKSASARAN, $det);
                    if(!$res) {
                        throw new Exception("Database error: ".$this->db->error());
                    }
                }
                $this->db->trans_commit();
                echo json_encode($resp);

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
            $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
            $this->db->order_by(COL_KD_TAHUN_FROM, 'desc');
            $rpemda = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();

            if(!empty($rpemda)) {
                $data['data'] = array(
                    COL_KD_PEMDA => $rpemda[COL_KD_PEMDA],
                    "DefPeriod" => $rpemda[COL_KD_TAHUN_FROM]." s.d ".$rpemda[COL_KD_TAHUN_TO]." : ".$rpemda[COL_NM_PEJABAT]
                );
            }

            $this->load->view('mopd/sasaran_form',$data);
        }
    }

    function sasaran_edit($id) {
        $ruser = GetLoggedUser();
        $this->db
            ->select(TBL_SAKIP_MOPD_SASARAN.'.*,'.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.','.TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.','.TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN.','.TBL_SAKIP_MPMD_SASARAN.'.'.COL_NM_SASARAN.','.TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_NM_INDIKATORSASARAN.','.TBL_SAKIP_MOPD_TUJUAN.'.'.COL_NM_TUJUANOPD.','.TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUANOPD)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI,"inner")
            ->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN,"inner")
            ->join(TBL_SAKIP_MOPD_TUJUAN,
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKTUJUAN,
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUANOPD
                ,"inner")
            ->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_UNIQ, $id);

        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SUB, $strOPD[3]);
        }

        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MOPD_SASARAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sasaran OPD";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/sasaran');
            /*$cond = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
            );*/
            $data = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),

                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),

                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_NM_SASARANOPD => $this->input->post(COL_NM_SASARANOPD)
            );

            try {
                $this->db->trans_begin();
                $detNo = $this->input->post("NoDet");
                $detKet = $this->input->post("KetDet");
                $detFormula = $this->input->post("FormulaDet");
                $detSumber = $this->input->post("SumberDet");
                $detPIC = $this->input->post("PICDet");
                $arrDet = [];

                $det = $this->db
                    //->select(COL_KD_PEMDA.",".COL_KD_MISI.",".COL_KD_TUJUAN.",".COL_KD_INDIKATORTUJUAN.",".COL_KD_SASARAN.",".COL_KD_INDIKATORSASARAN)
                    ->where(COL_KD_URUSAN, $rdata[COL_KD_URUSAN])
                    ->where(COL_KD_BIDANG, $rdata[COL_KD_BIDANG])
                    ->where(COL_KD_UNIT, $rdata[COL_KD_UNIT])
                    ->where(COL_KD_SUB, $rdata[COL_KD_SUB])

                    ->where(COL_KD_PEMDA, $rdata[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $rdata[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $rdata[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $rdata[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $rdata[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $rdata[COL_KD_INDIKATORSASARAN])
                    ->where(COL_KD_TUJUANOPD, $rdata[COL_KD_TUJUANOPD])
                    ->where(COL_KD_INDIKATORTUJUANOPD, $rdata[COL_KD_INDIKATORTUJUANOPD])
                    ->where(COL_KD_SASARANOPD, $rdata[COL_KD_SASARANOPD])
                    ->get(TBL_SAKIP_MOPD_IKSASARAN)
                    ->result_array();

                if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MOPD_SASARAN, $data)){
                    throw new Exception("Database error :".$this->db->error());
                }

                $detUpdated = [];
                foreach($det as $d) {
                    $arrCond = array(
                        COL_KD_URUSAN => $d[COL_KD_URUSAN],
                        COL_KD_BIDANG => $d[COL_KD_BIDANG],
                        COL_KD_UNIT => $d[COL_KD_UNIT],
                        COL_KD_SUB => $d[COL_KD_SUB],

                        COL_KD_PEMDA=>$d[COL_KD_PEMDA],
                        COL_KD_MISI=>$d[COL_KD_MISI],
                        COL_KD_TUJUAN=>$d[COL_KD_TUJUAN],
                        COL_KD_INDIKATORTUJUAN=>$d[COL_KD_INDIKATORTUJUAN],
                        COL_KD_SASARAN=>$d[COL_KD_SASARAN],
                        COL_KD_INDIKATORSASARAN=>$d[COL_KD_INDIKATORSASARAN],
                        COL_KD_TUJUANOPD=>$d[COL_KD_TUJUANOPD],
                        COL_KD_INDIKATORTUJUANOPD=>$d[COL_KD_INDIKATORTUJUANOPD],
                        COL_KD_SASARANOPD=>$d[COL_KD_SASARANOPD],
                        COL_KD_INDIKATORSASARANOPD=>$d[COL_KD_INDIKATORSASARANOPD]
                    );
                    if(!empty($detNo) && in_array($d[COL_KD_INDIKATORSASARANOPD], $detNo)) {
                        $res = $this->db
                            ->where($arrCond)
                            ->update(TBL_SAKIP_MOPD_IKSASARAN, array(
                                /*COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),*/

                                COL_KD_INDIKATORSASARANOPD => $d[COL_KD_INDIKATORSASARANOPD],
                                COL_NM_INDIKATORSASARANOPD => $detKet[array_search($d[COL_KD_INDIKATORSASARANOPD], $detNo)],
                                COL_NM_FORMULA => $detFormula[array_search($d[COL_KD_INDIKATORSASARANOPD], $detNo)],
                                COL_NM_SUMBERDATA => $detSumber[array_search($d[COL_KD_INDIKATORSASARANOPD], $detNo)],
                                COL_NM_PENANGGUNGJAWAB => $detPIC[array_search($d[COL_KD_INDIKATORSASARANOPD], $detNo)]
                            ));
                        if(!$res) {
                            throw new Exception("Database error : ".$this->db->error());
                        }
                    }
                    else {
                        $res = $this->db->delete(TBL_SAKIP_MOPD_IKSASARAN, $arrCond);
                        if(!$res) {
                            throw new Exception("Database error :".$this->db->error());
                        }
                    }
                    $detUpdated[] = $d[COL_KD_INDIKATORSASARANOPD];
                }

                $kdMisi = $this->input->post(COL_KD_MISI);
                $kdTujuan = $this->input->post(COL_KD_TUJUAN);
                $kdIkTujuan = $this->input->post(COL_KD_INDIKATORTUJUAN);
                $kdSasaran = $this->input->post(COL_KD_SASARAN);
                $kdIkSasaran = $this->input->post(COL_KD_INDIKATORSASARAN);
                /*for($m=0; $m<count($kdMisi); $m++) {
                    for($i = 0; $i<count($detNo); $i++) {
                        if(!in_array($detNo[$i], $detUpdated)) {
                            $arrDet[] = array(
                                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                                COL_KD_SUB => $this->input->post(COL_KD_SUB),

                                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                                COL_KD_MISI => $kdMisi[$m],
                                COL_KD_TUJUAN => $kdTujuan[$m],
                                COL_KD_INDIKATORTUJUAN => $kdIkTujuan[$m],
                                COL_KD_SASARAN => $kdSasaran[$m],
                                COL_KD_INDIKATORSASARAN => $kdIkSasaran[$m],
                                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),

                                COL_KD_INDIKATORSASARANOPD => $detNo[$i],
                                COL_NM_INDIKATORSASARANOPD => $detKet[$i],
                                COL_NM_FORMULA => $detFormula[$i],
                                COL_NM_SUMBERDATA => $detSumber[$i],
                                COL_NM_PENANGGUNGJAWAB => $detPIC[$i]
                            );
                        }
                    }
                }*/
                for($i = 0; $i<count($detNo); $i++) {
                    if(!in_array($detNo[$i], $detUpdated)) {
                        $arrDet[] = array(
                            COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                            COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                            COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                            COL_KD_SUB => $this->input->post(COL_KD_SUB),

                            COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                            COL_KD_MISI => $kdMisi,
                            COL_KD_TUJUAN => $kdTujuan,
                            COL_KD_INDIKATORTUJUAN => $kdIkTujuan,
                            COL_KD_SASARAN => $kdSasaran,
                            COL_KD_INDIKATORSASARAN => $kdIkSasaran,
                            COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                            COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                            COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),

                            COL_KD_INDIKATORSASARANOPD => $detNo[$i],
                            COL_NM_INDIKATORSASARANOPD => $detKet[$i],
                            COL_NM_FORMULA => $detFormula[$i],
                            COL_NM_SUMBERDATA => $detSumber[$i],
                            COL_NM_PENANGGUNGJAWAB => $detPIC[$i]
                        );
                    }
                }
                if(count($arrDet) > 0) {
                    $res = $this->db->insert_batch(TBL_SAKIP_MOPD_IKSASARAN, $arrDet);
                    if(!$res) {
                        throw new Exception("Database error :".$this->db->error());
                    }
                }

                $this->db->trans_commit();
                echo json_encode($resp);

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }else{
            $this->load->view('mopd/sasaran_form',$data);
        }
    }

    function sasaran_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->trans_begin();
            try {
                $rsasaran = $this->db->where(COL_UNIQ, $datum)->get(TBL_SAKIP_MOPD_SASARAN)->row_array();
                $arrCond = array(
                    COL_KD_URUSAN => $rsasaran[COL_KD_URUSAN],
                    COL_KD_BIDANG => $rsasaran[COL_KD_BIDANG],
                    COL_KD_UNIT => $rsasaran[COL_KD_UNIT],
                    COL_KD_SUB => $rsasaran[COL_KD_SUB],

                    COL_KD_PEMDA=>$rsasaran[COL_KD_PEMDA],
                    /*COL_KD_MISI=>$rsasaran[COL_KD_MISI],
                    COL_KD_TUJUAN=>$rsasaran[COL_KD_TUJUAN],
                    COL_KD_INDIKATORTUJUAN=>$rsasaran[COL_KD_INDIKATORTUJUAN],
                    COL_KD_SASARAN=>$rsasaran[COL_KD_SASARAN],
                    COL_KD_INDIKATORSASARAN=>$rsasaran[COL_KD_INDIKATORSASARAN],*/
                    COL_KD_TUJUANOPD=>$rsasaran[COL_KD_TUJUANOPD],
                    COL_KD_INDIKATORTUJUANOPD=>$rsasaran[COL_KD_INDIKATORTUJUANOPD],
                    COL_KD_SASARANOPD=>$rsasaran[COL_KD_SASARANOPD]
                );
                $this->db->delete(TBL_SAKIP_MOPD_IKSASARAN, $arrCond);
                $this->db->delete(TBL_SAKIP_MOPD_SASARAN, array(COL_UNIQ => $datum));
                $deleted++;
                $this->db->trans_commit();

            } catch (Exception $e) {
                $this->db->trans_rollback();
                $resp['error'] = $e->getMessage();
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function bid() {

        $ruser = GetLoggedUser();
        $data['title'] = 'Bidang OPD';
        $this->db->select('*,'.TBL_SAKIP_MBID.'.'.COL_UNIQ.' as ID');
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_SUB, $strOPD[3]);
        }
        $this->db->order_by(COL_KD_URUSAN, 'asc');
        $this->db->order_by(COL_KD_BIDANG, 'asc');
        $this->db->order_by(COL_KD_UNIT, 'asc');
        $this->db->order_by(COL_KD_SUB, 'asc');
        $this->db->order_by(COL_KD_BID, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MBID)->result_array();
        $this->load->view('mopd/bid', $data);
    }

    function bid_add() {
        $data['title'] = "Bidang OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/bid');
            $data = array(
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_NM_BID => $this->input->post(COL_NM_BID),
                COL_NM_KABID => $this->input->post(COL_NM_KABID)
            );
            if(!$this->db->insert(TBL_SAKIP_MBID, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mopd/bid_form',$data);
        }
    }

    function bid_edit($id) {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_SUB, $strOPD[3]);
        }
        $rdata = $data['data'] = $this->db->where(COL_UNIQ, $id)->get(TBL_SAKIP_MBID)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Bidang OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){

            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/bid');
            $data = array(
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_NM_BID => $this->input->post(COL_NM_BID),
                COL_NM_KABID => $this->input->post(COL_NM_KABID)
            );
            if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MBID, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mopd/bid_form',$data);
        }
    }

    function bid_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MBID, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function subbid() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Sub Bidang OPD';
        $this->db->select('*,'.TBL_SAKIP_MSUBBID.'.'.COL_UNIQ.' as ID');
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID.".".COL_KD_BID
            ,"inner");
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MSUBBID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MSUBBID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MSUBBID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MSUBBID.".".COL_KD_SUB, $strOPD[3]);
        }
        $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_SUBBID, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MSUBBID)->result_array();
        $this->load->view('mopd/subbid', $data);
    }

    function subbid_add() {
        $data['title'] = "Sub Bidang OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/subbid');
            $data = array(
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),
                COL_NM_SUBBID => $this->input->post(COL_NM_SUBBID),
                COL_NM_KASUBBID => $this->input->post(COL_NM_KASUBBID)
            );
            if(!$this->db->insert(TBL_SAKIP_MSUBBID, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mopd/subbid_form',$data);
        }
    }

    function subbid_edit($id) {
        $ruser = GetLoggedUser();
        $this->db->select(TBL_SAKIP_MSUBBID.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MBID.".".COL_KD_SUB, $strOPD[3]);
        }

        $this->db->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID.".".COL_KD_BID
                ,"inner")
            ->where(TBL_SAKIP_MSUBBID.".".COL_UNIQ, $id);
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MSUBBID)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Sub Bidang OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/subbid');
            $data = array(
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),
                COL_NM_SUBBID => $this->input->post(COL_NM_SUBBID),
                COL_NM_KASUBBID => $this->input->post(COL_NM_KASUBBID)
            );
            if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MSUBBID, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mopd/subbid_form',$data);
        }
    }

    function subbid_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MSUBBID, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function file($id) {
        $ruser = GetLoggedUser();
        $data['title'] = 'Dokumen OPD';
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_FILE.".".COL_KD_PEMDA,"inner");
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_SUB, $strOPD[3]);
        }
        $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_TYPE, $id);
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_FILE.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_FILE.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_FILE.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_MOPD_FILE.".".COL_KD_SUB, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_MOPD_FILE)->result_array();
        $data['id'] = $id;
        $this->load->view('mopd/file', $data);
    }

    function file_add($id) {
        $ruser = GetLoggedUser();
        $data['title'] = "Arsip OPD";
        $data['edit'] = FALSE;
        $data['tipe'] = $id;

        if(!empty($_POST)){
            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/file/'.$id);
            $data = array(
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_TYPE => $this->input->post(COL_KD_TYPE),
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_NM_KETERANGAN => $this->input->post(COL_NM_KETERANGAN),
                COL_CREATE_BY => $ruser[COL_USERNAME],
                COL_CREATE_DATE => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 5120000;
            $config['max_width']  = 1600;
            $config['max_height']  = 1600;
            $config['overwrite'] = FALSE;

            $this->load->library('upload', $config);
            if (!empty($_FILES["userfile"]["name"])) {
                if (!$this->upload->do_upload()) {
                    $resp['error'] = $this->upload->display_errors();
                    $resp['success'] = 0;
                    echo json_encode($resp);
                    return;
                }

                $dataupload = $this->upload->data();
                if (!empty($dataupload) && $dataupload['file_name']) {
                    $data[COL_NM_FILE] = $dataupload['file_name'];
                }
            }

            if(!$this->db->insert(TBL_SAKIP_MOPD_FILE, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mopd/file_form',$data);
        }
    }

    function file_edit($id) {
        $ruser = GetLoggedUser();
        $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_MOPD_FILE.".".COL_KD_SUB, $strOPD[3]);
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_MOPD_FILE)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Arsip OPD";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('mopd/file/'.$rdata[COL_KD_TYPE]);
            $data = array(
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_TYPE => $this->input->post(COL_KD_TYPE),
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_NM_KETERANGAN => $this->input->post(COL_NM_KETERANGAN),
                COL_EDIT_BY => $ruser[COL_USERNAME],
                COL_EDIT_DATE => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 5120000;
            $config['max_width']  = 1600;
            $config['max_height']  = 1600;
            $config['overwrite'] = FALSE;

            $this->load->library('upload', $config);
            if (!empty($_FILES["userfile"]["name"])) {
                if (!$this->upload->do_upload()) {
                    $resp['error'] = $this->upload->display_errors();
                    $resp['success'] = 0;
                    echo json_encode($resp);
                    return;
                }

                $dataupload = $this->upload->data();
                if (!empty($dataupload) && $dataupload['file_name']) {
                    $data[COL_NM_FILE] = $dataupload['file_name'];
                }
            }

            if(!$this->db->where(COL_UNIQ, $id)->update(TBL_SAKIP_MOPD_FILE, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('mopd/file_form',$data);
        }
    }

    function file_del(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_SAKIP_MOPD_FILE, array(COL_UNIQ => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}