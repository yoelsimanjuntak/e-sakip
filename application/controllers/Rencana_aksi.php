<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 09/07/2019
 * Time: 13:50
 */
class Rencana_aksi extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEBAPPEDA && GetLoggedUser()[COL_ROLEID] != ROLEKADIS && GetLoggedUser()[COL_ROLEID] != ROLEKABID  && GetLoggedUser()[COL_ROLEID] != ROLEKASUBBID)) {
            redirect('user/dashboard');
        }
    }

    function program() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Rencana Aksi - Program';
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID) {
            redirect('user/dashboard');
        }

        $this->db->select(
            TBL_SAKIP_DPA_PROGRAM.'.*,'.
            TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.
            TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.
            TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.
            TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.', '.
            TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.', '.
            TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN.', '.
            TBL_SAKIP_MPMD_SASARAN.'.'.COL_NM_SASARAN.', '.
            TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_NM_INDIKATORSASARAN.', '.
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_NM_TUJUANOPD.', '.
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUANOPD.', '.
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD.', '.
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD.', '.
            TBL_SAKIP_DPA_PROGRAM.".".COL_UNIQ.",".
            TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN.",".
            TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG.",".
            TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT.",".
            TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB.",".
            TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID.",".
            TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TAHUN." as Period,".
            TBL_SAKIP_MBID.".".COL_NM_BID.",".
            TBL_SAKIP_DPA_PROGRAM.".".COL_NM_PROGRAMOPD.",".
            "(
                select count(*) from sakip_dpa_program_sasaran
                WHERE `sakip_dpa_program_sasaran`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_program_sasaran`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_program_sasaran`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_program_sasaran`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_program_sasaran`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`
                AND `sakip_dpa_program_sasaran`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_program_sasaran`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_program_sasaran`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_program_sasaran`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_program_sasaran`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_program_sasaran`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
            ) as count_sasaran,".
            "(
                select count(*) from sakip_dpa_program_indikator
                WHERE `sakip_dpa_program_indikator`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_program_indikator`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_program_indikator`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_program_indikator`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_program_indikator`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`
                AND `sakip_dpa_program_indikator`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_program_indikator`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_program_indikator`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_program_indikator`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_program_indikator`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_program_indikator`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_program_indikator`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_program_indikator`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
                and (`sakip_dpa_program_indikator`.`Target_TW1` is null or `sakip_dpa_program_indikator`.`Target_TW2` is null or `sakip_dpa_program_indikator`.`Target_TW3` is null or `sakip_dpa_program_indikator`.`Target_TW4` is null)
            ) as count_os_sasaran,
            (
                select count(*) from sakip_dpa_program_indikator
                WHERE `sakip_dpa_program_indikator`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                AND `sakip_dpa_program_indikator`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                AND `sakip_dpa_program_indikator`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                AND `sakip_dpa_program_indikator`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                AND `sakip_dpa_program_indikator`.`Kd_Bid` = `sakip_dpa_program`.`Kd_Bid`
                AND `sakip_dpa_program_indikator`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                AND `sakip_dpa_program_indikator`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                AND `sakip_dpa_program_indikator`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_program_indikator`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_program_indikator`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_program_indikator`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                AND `sakip_dpa_program_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_program_indikator`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                AND `sakip_dpa_program_indikator`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
                and (`sakip_dpa_program_indikator`.`Kinerja_TW1` is null or `sakip_dpa_program_indikator`.`Kinerja_TW2` is null or `sakip_dpa_program_indikator`.`Kinerja_TW3` is null or `sakip_dpa_program_indikator`.`Kinerja_TW4` is null)
            ) as count_os_realisasi"
        );
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID
            ,"inner");
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI,"inner");
        $this->db->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN,"inner");
            $this->db->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN,"inner");
            $this->db->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN,"inner");
            $this->db->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN,"inner");
            $this->db->join(TBL_SAKIP_MOPD_TUJUAN,
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD
                ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD
                ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD
                ,"inner");
            $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                ,"inner");

        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) {
                $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID, $strOPD[4]);
            }
        }
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_DPA_PROGRAM)->result_array();
        $this->load->view('rencana_aksi/program', $data);
    }

    function program_edit($id) {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID) {
            redirect('user/dashboard');
        }

        $this->db
            ->select(TBL_SAKIP_DPA_PROGRAM.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                ,"inner")
            ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) {
                $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID, $strOPD[4]);
            }
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Rencana Aksi - Program";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('rencana-aksi/program');

            $post = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                COL_TARGET_TW1 => $this->input->post(COL_TARGET_TW1),
                COL_TARGET_TW2 => $this->input->post(COL_TARGET_TW2),
                COL_TARGET_TW3 => $this->input->post(COL_TARGET_TW3),
                COL_TARGET_TW4 => $this->input->post(COL_TARGET_TW4)
            );

            $success = true;
            $this->db->trans_begin();
            for($i=0; $i<count($post[COL_KD_INDIKATORPROGRAMOPD]); $i++) {
                $cond = array(
                    COL_KD_PEMDA => $post[COL_KD_PEMDA][$i],
                    COL_KD_MISI => $post[COL_KD_MISI][$i],
                    COL_KD_TUJUAN => $post[COL_KD_TUJUAN][$i],
                    COL_KD_INDIKATORTUJUAN => $post[COL_KD_INDIKATORTUJUAN][$i],
                    COL_KD_SASARAN => $post[COL_KD_SASARAN][$i],
                    COL_KD_INDIKATORSASARAN => $post[COL_KD_INDIKATORSASARAN][$i],
                    COL_KD_TUJUANOPD => $post[COL_KD_TUJUANOPD][$i],
                    COL_KD_INDIKATORTUJUANOPD => $post[COL_KD_INDIKATORTUJUANOPD][$i],
                    COL_KD_SASARANOPD => $post[COL_KD_SASARANOPD][$i],
                    COL_KD_INDIKATORSASARANOPD => $post[COL_KD_INDIKATORSASARANOPD][$i],
                    COL_KD_TAHUN => $post[COL_KD_TAHUN][$i],
                    COL_KD_URUSAN => $post[COL_KD_URUSAN][$i],
                    COL_KD_BIDANG => $post[COL_KD_BIDANG][$i],
                    COL_KD_UNIT => $post[COL_KD_UNIT][$i],
                    COL_KD_SUB => $post[COL_KD_SUB][$i],
                    COL_KD_BID => $post[COL_KD_BID][$i],
                    COL_KD_PROGRAMOPD => $post[COL_KD_PROGRAMOPD][$i],
                    COL_KD_SASARANPROGRAMOPD => $post[COL_KD_SASARANPROGRAMOPD][$i],
                    COL_KD_INDIKATORPROGRAMOPD => $post[COL_KD_INDIKATORPROGRAMOPD][$i]
                );
                $res = $this->db
                    ->where($cond)
                    ->update(TBL_SAKIP_DPA_PROGRAM_INDIKATOR, array(
                        COL_TARGET_TW1 => toNum($post[COL_TARGET_TW1][$i]),
                        COL_TARGET_TW2 => toNum($post[COL_TARGET_TW2][$i]),
                        COL_TARGET_TW3 => toNum($post[COL_TARGET_TW3][$i]),
                        COL_TARGET_TW4 => toNum($post[COL_TARGET_TW4][$i])
                    ));
                if(!$res) {
                    $this->db->trans_rollback();
                    $success = false;
                    $resp['error'] = "Gagal mengupdate Indikator Program No. ".$post[COL_KD_SASARANPROGRAMOPD][$i].".".$post[COL_KD_INDIKATORPROGRAMOPD][$i];
                    $resp['success'] = 0;
                    break;
                }
            }

            if($success) $this->db->trans_commit();
            echo json_encode($resp);

        }else{
            $this->load->view('rencana_aksi/program_form',$data);
        }
    }

    function program_realisasi($id) {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID) {
            redirect('user/dashboard');
        }

        $this->db
            ->select(TBL_SAKIP_DPA_PROGRAM.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MOPD_IKSASARAN,
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                ,"inner")
            ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) {
                $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID, $strOPD[4]);
            }
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_DPA_PROGRAM)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Realisasi Program";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('rencana-aksi/program');

            $post = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_INDIKATORPROGRAMOPD => $this->input->post(COL_KD_INDIKATORPROGRAMOPD),
                COL_KINERJA_TW1 => $this->input->post(COL_KINERJA_TW1),
                COL_KINERJA_TW2 => $this->input->post(COL_KINERJA_TW2),
                COL_KINERJA_TW3 => $this->input->post(COL_KINERJA_TW3),
                COL_KINERJA_TW4 => $this->input->post(COL_KINERJA_TW4)
            );

            $success = true;
            $this->db->trans_begin();
            for($i=0; $i<count($post[COL_KD_SASARANPROGRAMOPD]); $i++) {
                $cond = array(
                    COL_KD_PEMDA => $post[COL_KD_PEMDA][$i],
                    COL_KD_MISI => $post[COL_KD_MISI][$i],
                    COL_KD_TUJUAN => $post[COL_KD_TUJUAN][$i],
                    COL_KD_INDIKATORTUJUAN => $post[COL_KD_INDIKATORTUJUAN][$i],
                    COL_KD_SASARAN => $post[COL_KD_SASARAN][$i],
                    COL_KD_INDIKATORSASARAN => $post[COL_KD_INDIKATORSASARAN][$i],
                    COL_KD_TUJUANOPD => $post[COL_KD_TUJUANOPD][$i],
                    COL_KD_INDIKATORTUJUANOPD => $post[COL_KD_INDIKATORTUJUANOPD][$i],
                    COL_KD_SASARANOPD => $post[COL_KD_SASARANOPD][$i],
                    COL_KD_INDIKATORSASARANOPD => $post[COL_KD_INDIKATORSASARANOPD][$i],
                    COL_KD_TAHUN => $post[COL_KD_TAHUN][$i],
                    COL_KD_URUSAN => $post[COL_KD_URUSAN][$i],
                    COL_KD_BIDANG => $post[COL_KD_BIDANG][$i],
                    COL_KD_UNIT => $post[COL_KD_UNIT][$i],
                    COL_KD_SUB => $post[COL_KD_SUB][$i],
                    COL_KD_BID => $post[COL_KD_BID][$i],
                    COL_KD_PROGRAMOPD => $post[COL_KD_PROGRAMOPD][$i],
                    COL_KD_SASARANPROGRAMOPD => $post[COL_KD_SASARANPROGRAMOPD][$i],
                    COL_KD_INDIKATORPROGRAMOPD => $post[COL_KD_INDIKATORPROGRAMOPD][$i]
                );
                $res = $this->db
                    ->where($cond)
                    ->update(TBL_SAKIP_DPA_PROGRAM_INDIKATOR, array(
                        COL_KINERJA_TW1 => toNum($post[COL_KINERJA_TW1][$i]),
                        COL_KINERJA_TW2 => toNum($post[COL_KINERJA_TW2][$i]),
                        COL_KINERJA_TW3 => toNum($post[COL_KINERJA_TW3][$i]),
                        COL_KINERJA_TW4 => toNum($post[COL_KINERJA_TW4][$i])
                    ));
                if(!$res) {
                    $this->db->trans_rollback();
                    $success = false;
                    $resp['error'] = "Gagal mengupdate Indikator Program No. ".$post[COL_KD_SASARANPROGRAMOPD][$i].".".$post[COL_KD_INDIKATORPROGRAMOPD][$i];
                    $resp['success'] = 0;
                    break;
                }
            }

            if($success) $this->db->trans_commit();
            echo json_encode($resp);

        }else{
            $this->load->view('rencana_aksi/program_realisasi',$data);
        }
    }

    function kegiatan() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Rencana Aksi - Kegiatan';
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID && $ruser[COL_ROLEID] != ROLEKASUBBID) {
            redirect('user/dashboard');
        }

        $this->db->select(
            TBL_SAKIP_DPA_KEGIATAN.'.*,'.
            TBL_SAKIP_MPMD_MISI.'.'.COL_NM_MISI.','.
            TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.
            TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.
            TBL_SAKIP_MPEMDA.'.'.COL_NM_PEJABAT.', '.
            TBL_SAKIP_MPMD_TUJUAN.'.'.COL_NM_TUJUAN.', '.
            TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUAN.', '.
            TBL_SAKIP_MPMD_SASARAN.'.'.COL_NM_SASARAN.', '.
            TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_NM_INDIKATORSASARAN.', '.
            TBL_SAKIP_MOPD_TUJUAN.'.'.COL_NM_TUJUANOPD.', '.
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_NM_INDIKATORTUJUANOPD.', '.
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_NM_SASARANOPD.', '.
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_NM_INDIKATORSASARANOPD.', '.
            TBL_SAKIP_DPA_KEGIATAN.".".COL_UNIQ.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUBBID.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TAHUN." as Period,".
            TBL_SAKIP_MBID.".".COL_NM_BID.",".
            TBL_SAKIP_MSUBBID.".".COL_NM_SUBBID.",".
            TBL_SAKIP_DPA_PROGRAM.".".COL_NM_PROGRAMOPD.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_NM_KEGIATANOPD.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_BUDGET_TW1.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_BUDGET_TW2.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_BUDGET_TW3.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_BUDGET_TW4.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_ANGGARAN_TW1.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_ANGGARAN_TW2.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_ANGGARAN_TW3.",".
            TBL_SAKIP_DPA_KEGIATAN.".".COL_ANGGARAN_TW4.",".
            "(
                select count(*) from sakip_dpa_kegiatan_sasaran
                WHERE `sakip_dpa_kegiatan_sasaran`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                AND `sakip_dpa_kegiatan_sasaran`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
            ) as count_sasaran,".
            "(
                select count(*) from sakip_dpa_kegiatan_indikator
                WHERE `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                and (`sakip_dpa_kegiatan_indikator`.`Target_TW1` is null
                        or `sakip_dpa_kegiatan_indikator`.`Target_TW2` is null
                        or `sakip_dpa_kegiatan_indikator`.`Target_TW3` is null
                        or `sakip_dpa_kegiatan_indikator`.`Target_TW4` is null
                        /*or `sakip_dpa_kegiatan_indikator`.`Budget_TW1` is null
                        or `sakip_dpa_kegiatan_indikator`.`Budget_TW2` is null
                        or `sakip_dpa_kegiatan_indikator`.`Budget_TW3` is null
                        or `sakip_dpa_kegiatan_indikator`.`Budget_TW4` is null*/
                )
            ) as count_os_sasaran,".
            "(
                select count(*) from sakip_dpa_kegiatan_indikator
                WHERE `sakip_dpa_kegiatan_indikator`.`Kd_Urusan` = `sakip_dpa_kegiatan`.`Kd_Urusan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bidang` = `sakip_dpa_kegiatan`.`Kd_Bidang`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Unit` = `sakip_dpa_kegiatan`.`Kd_Unit`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sub` = `sakip_dpa_kegiatan`.`Kd_Sub`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Bid` = `sakip_dpa_kegiatan`.`Kd_Bid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Subbid` = `sakip_dpa_kegiatan`.`Kd_Subbid`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Pemda` = `sakip_dpa_kegiatan`.`Kd_Pemda`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Misi` = `sakip_dpa_kegiatan`.`Kd_Misi`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tujuan` = `sakip_dpa_kegiatan`.`Kd_Tujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuan` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Sasaran` = `sakip_dpa_kegiatan`.`Kd_Sasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaran` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_TujuanOPD` = `sakip_dpa_kegiatan`.`Kd_TujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_ProgramOPD` = `sakip_dpa_kegiatan`.`Kd_ProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_SasaranProgramOPD` = `sakip_dpa_kegiatan`.`Kd_SasaranProgramOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_KegiatanOPD` = `sakip_dpa_kegiatan`.`Kd_KegiatanOPD`
                AND `sakip_dpa_kegiatan_indikator`.`Kd_Tahun` = `sakip_dpa_kegiatan`.`Kd_Tahun`
                and (`sakip_dpa_kegiatan_indikator`.`Kinerja_TW1` is null
                        or `sakip_dpa_kegiatan_indikator`.`Kinerja_TW2` is null
                        or `sakip_dpa_kegiatan_indikator`.`Kinerja_TW3` is null
                        or `sakip_dpa_kegiatan_indikator`.`Kinerja_TW4` is null
                        /*or `sakip_dpa_kegiatan_indikator`.`Anggaran_TW1` is null
                        or `sakip_dpa_kegiatan_indikator`.`Anggaran_TW2` is null
                        or `sakip_dpa_kegiatan_indikator`.`Anggaran_TW3` is null
                        or `sakip_dpa_kegiatan_indikator`.`Anggaran_TW4` is null*/
                )
            ) as count_os_realisasi"
        );
        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA,"inner");
        $this->db->join(TBL_SAKIP_MBID,
            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID
            ,"inner");
        $this->db->join(TBL_SAKIP_MSUBBID,
            TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUBBID
            ,"inner");
        $this->db->join(TBL_SAKIP_DPA_PROGRAM,
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".

            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_MISI." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
            TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TAHUN
            ,"inner");
        $this->db->join(TBL_SAKIP_MPMD_MISI,TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI,"inner")
            ->join(TBL_SAKIP_MPMD_TUJUAN,TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKTUJUAN,TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN,"inner")
            ->join(TBL_SAKIP_MPMD_SASARAN,TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN,"inner")
            ->join(TBL_SAKIP_MPMD_IKSASARAN,TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN,"inner")
            ->join(TBL_SAKIP_MOPD_TUJUAN,
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_MOPD_TUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD
                ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKTUJUAN,
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_SASARAN,
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD
            ,"inner");
        $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD." AND ".
            TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
            ,"inner");

        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) {
                $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID, $strOPD[4]);
            }
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TAHUN, 'desc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID, 'asc');
        $this->db->order_by(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM, 'desc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_MISI, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARAN, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD, 'asc');
        $this->db->order_by(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_KEGIATANOPD, 'asc');
        $data['res'] = $this->db->get(TBL_SAKIP_DPA_KEGIATAN)->result_array();
        $this->load->view('rencana_aksi/kegiatan', $data);
    }

    function kegiatan_edit($id) {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID && $ruser[COL_ROLEID] != ROLEKASUBBID) {
            redirect('user/dashboard');
        }
        $this->db
            ->select(TBL_SAKIP_DPA_KEGIATAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_NM_PROGRAMOPD)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MSUBBID,
                TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUBBID
                ,"inner")
            ->join(TBL_SAKIP_DPA_PROGRAM,
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TAHUN
                ,"inner")
            ->join(TBL_SAKIP_DPA_PROGRAM_SASARAN,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TAHUN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD
                ,"inner")
            ->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) {
                $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID, $strOPD[4]);
            }
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Rencana Aksi - Kegiatan";;
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('rencana-aksi/kegiatan');

            $post = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),
                COL_KD_INDIKATORKEGIATANOPD => $this->input->post(COL_KD_INDIKATORKEGIATANOPD),
                COL_TARGET_TW1 => $this->input->post(COL_TARGET_TW1),
                COL_TARGET_TW2 => $this->input->post(COL_TARGET_TW2),
                COL_TARGET_TW3 => $this->input->post(COL_TARGET_TW3),
                COL_TARGET_TW4 => $this->input->post(COL_TARGET_TW4),
                COL_BUDGET_TW1 => $this->input->post(COL_BUDGET_TW1),
                COL_BUDGET_TW2 => $this->input->post(COL_BUDGET_TW2),
                COL_BUDGET_TW3 => $this->input->post(COL_BUDGET_TW3),
                COL_BUDGET_TW4 => $this->input->post(COL_BUDGET_TW4)
            );

            $success = true;
            $this->db->trans_begin();
            $cond1 = array(
                COL_KD_PEMDA => $post[COL_KD_PEMDA],
                COL_KD_MISI => $post[COL_KD_MISI],
                COL_KD_TUJUAN => $post[COL_KD_TUJUAN],
                COL_KD_INDIKATORTUJUAN => $post[COL_KD_INDIKATORTUJUAN],
                COL_KD_SASARAN => $post[COL_KD_SASARAN],
                COL_KD_INDIKATORSASARAN => $post[COL_KD_INDIKATORSASARAN],
                COL_KD_TUJUANOPD => $post[COL_KD_TUJUANOPD],
                COL_KD_INDIKATORTUJUANOPD => $post[COL_KD_INDIKATORTUJUANOPD],
                COL_KD_SASARANOPD => $post[COL_KD_SASARANOPD],
                COL_KD_INDIKATORSASARANOPD => $post[COL_KD_INDIKATORSASARANOPD],
                COL_KD_TAHUN => $post[COL_KD_TAHUN],
                COL_KD_URUSAN => $post[COL_KD_URUSAN],
                COL_KD_BIDANG => $post[COL_KD_BIDANG],
                COL_KD_UNIT => $post[COL_KD_UNIT],
                COL_KD_SUB => $post[COL_KD_SUB],
                COL_KD_BID => $post[COL_KD_BID],
                COL_KD_SUBBID => $post[COL_KD_SUBBID],
                COL_KD_PROGRAMOPD => $post[COL_KD_PROGRAMOPD],
                COL_KD_SASARANPROGRAMOPD => $post[COL_KD_SASARANPROGRAMOPD],
                COL_KD_KEGIATANOPD => $post[COL_KD_KEGIATANOPD]
            );
            $res1 = $this->db
                ->where($cond1)
                ->update(TBL_SAKIP_DPA_KEGIATAN, array(
                    COL_BUDGET_TW1 => toNum($post[COL_BUDGET_TW1]),
                    COL_BUDGET_TW2 => toNum($post[COL_BUDGET_TW2]),
                    COL_BUDGET_TW3 => toNum($post[COL_BUDGET_TW3]),
                    COL_BUDGET_TW4 => toNum($post[COL_BUDGET_TW4])
                ));
            if(!$res1) {
                $this->db->trans_rollback();
                $success = false;
                $resp['error'] = "Gagal mengupdate Kegiatan.";
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }

            for($i=0; $i<count($post[COL_KD_SASARANKEGIATANOPD]); $i++) {

                $cond2 = array(
                    COL_KD_PEMDA => $post[COL_KD_PEMDA],
                    COL_KD_MISI => $post[COL_KD_MISI],
                    COL_KD_TUJUAN => $post[COL_KD_TUJUAN],
                    COL_KD_INDIKATORTUJUAN => $post[COL_KD_INDIKATORTUJUAN],
                    COL_KD_SASARAN => $post[COL_KD_SASARAN],
                    COL_KD_INDIKATORSASARAN => $post[COL_KD_INDIKATORSASARAN],
                    COL_KD_TUJUANOPD => $post[COL_KD_TUJUANOPD],
                    COL_KD_INDIKATORTUJUANOPD => $post[COL_KD_INDIKATORTUJUANOPD],
                    COL_KD_SASARANOPD => $post[COL_KD_SASARANOPD],
                    COL_KD_INDIKATORSASARANOPD => $post[COL_KD_INDIKATORSASARANOPD],
                    COL_KD_TAHUN => $post[COL_KD_TAHUN],
                    COL_KD_URUSAN => $post[COL_KD_URUSAN],
                    COL_KD_BIDANG => $post[COL_KD_BIDANG],
                    COL_KD_UNIT => $post[COL_KD_UNIT],
                    COL_KD_SUB => $post[COL_KD_SUB],
                    COL_KD_BID => $post[COL_KD_BID],
                    COL_KD_SUBBID => $post[COL_KD_SUBBID],
                    COL_KD_PROGRAMOPD => $post[COL_KD_PROGRAMOPD],
                    COL_KD_SASARANPROGRAMOPD => $post[COL_KD_SASARANPROGRAMOPD],
                    COL_KD_KEGIATANOPD => $post[COL_KD_KEGIATANOPD],
                    COL_KD_SASARANKEGIATANOPD => $post[COL_KD_SASARANKEGIATANOPD][$i],
                    COL_KD_INDIKATORKEGIATANOPD => $post[COL_KD_INDIKATORKEGIATANOPD][$i]
                );
                $res2 = $this->db
                    ->where($cond2)
                    ->update(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR, array(
                        COL_TARGET_TW1 => toNum($post[COL_TARGET_TW1][$i]),
                        COL_TARGET_TW2 => toNum($post[COL_TARGET_TW2][$i]),
                        COL_TARGET_TW3 => toNum($post[COL_TARGET_TW3][$i]),
                        COL_TARGET_TW4 => toNum($post[COL_TARGET_TW4][$i])
                    ));
                if(!$res2) {
                    $this->db->trans_rollback();
                    $success = false;
                    $resp['error'] = "Gagal mengupdate Indikator Kegiatan No. ".$post[COL_KD_INDIKATORKEGIATANOPD][$i];
                    $resp['success'] = 0;
                    break;
                }
            }

            if($success) $this->db->trans_commit();
            echo json_encode($resp);

        } else {
            $this->load->view('rencana_aksi/kegiatan_form',$data);
        }
    }

    function kegiatan_realisasi($id) {
        $ruser = GetLoggedUser();
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA && $ruser[COL_ROLEID] != ROLEKADIS && $ruser[COL_ROLEID] != ROLEKABID && $ruser[COL_ROLEID] != ROLEKASUBBID) {
            redirect('user/dashboard');
        }
        $this->db
            ->select(TBL_SAKIP_DPA_KEGIATAN.'.*,'.TBL_SAKIP_MBID.'.'.COL_NM_BID.','.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_FROM.','.TBL_SAKIP_MPEMDA.'.'.COL_KD_TAHUN_TO.','.TBL_SAKIP_DPA_PROGRAM.'.'.COL_NM_PROGRAMOPD)
            ->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA,"inner")
            ->join(TBL_SAKIP_MBID,
                TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID
                ,"inner")
            ->join(TBL_SAKIP_MSUBBID,
                TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID." AND ".
                TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUBBID
                ,"inner")
            ->join(TBL_SAKIP_DPA_PROGRAM,
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TAHUN
                ,"inner")
            ->join(TBL_SAKIP_DPA_PROGRAM_SASARAN,
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID." AND ".

                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PEMDA." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_MISI." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_TAHUN." AND ".
                TBL_SAKIP_DPA_PROGRAM_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD
                ,"inner")
            ->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_UNIQ, $id);
        if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEBAPPEDA) {
            $strOPD = explode('.', $ruser[COL_COMPANYID]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_URUSAN, $strOPD[0]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BIDANG, $strOPD[1]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_UNIT, $strOPD[2]);
            $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUB, $strOPD[3]);
            if($ruser[COL_ROLEID] == ROLEKABID) {
                $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID, $strOPD[4]);
            }
            if($ruser[COL_ROLEID] == ROLEKASUBBID) {
                $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_BID, $strOPD[4]);
                $this->db->where(TBL_SAKIP_DPA_KEGIATAN.".".COL_KD_SUBBID, $strOPD[5]);
            }
        }
        $rdata = $data['data'] = $this->db->get(TBL_SAKIP_DPA_KEGIATAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = "Realisasi Kegiatan";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('rencana-aksi/kegiatan');

            $post = array(
                COL_KD_PEMDA => $this->input->post(COL_KD_PEMDA),
                COL_KD_MISI => $this->input->post(COL_KD_MISI),
                COL_KD_TUJUAN => $this->input->post(COL_KD_TUJUAN),
                COL_KD_INDIKATORTUJUAN => $this->input->post(COL_KD_INDIKATORTUJUAN),
                COL_KD_SASARAN => $this->input->post(COL_KD_SASARAN),
                COL_KD_INDIKATORSASARAN => $this->input->post(COL_KD_INDIKATORSASARAN),
                COL_KD_TUJUANOPD => $this->input->post(COL_KD_TUJUANOPD),
                COL_KD_INDIKATORTUJUANOPD => $this->input->post(COL_KD_INDIKATORTUJUANOPD),
                COL_KD_SASARANOPD => $this->input->post(COL_KD_SASARANOPD),
                COL_KD_INDIKATORSASARANOPD => $this->input->post(COL_KD_INDIKATORSASARANOPD),
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_KD_URUSAN => $this->input->post(COL_KD_URUSAN),
                COL_KD_BIDANG => $this->input->post(COL_KD_BIDANG),
                COL_KD_UNIT => $this->input->post(COL_KD_UNIT),
                COL_KD_SUB => $this->input->post(COL_KD_SUB),
                COL_KD_BID => $this->input->post(COL_KD_BID),
                COL_KD_SUBBID => $this->input->post(COL_KD_SUBBID),
                COL_KD_PROGRAMOPD => $this->input->post(COL_KD_PROGRAMOPD),
                COL_KD_SASARANPROGRAMOPD => $this->input->post(COL_KD_SASARANPROGRAMOPD),
                COL_KD_KEGIATANOPD => $this->input->post(COL_KD_KEGIATANOPD),
                COL_KD_SASARANKEGIATANOPD => $this->input->post(COL_KD_SASARANKEGIATANOPD),
                COL_KD_INDIKATORKEGIATANOPD => $this->input->post(COL_KD_INDIKATORKEGIATANOPD),
                COL_KINERJA_TW1 => $this->input->post(COL_KINERJA_TW1),
                COL_KINERJA_TW2 => $this->input->post(COL_KINERJA_TW2),
                COL_KINERJA_TW3 => $this->input->post(COL_KINERJA_TW3),
                COL_KINERJA_TW4 => $this->input->post(COL_KINERJA_TW4),
                COL_ANGGARAN_TW1 => $this->input->post(COL_ANGGARAN_TW1),
                COL_ANGGARAN_TW2 => $this->input->post(COL_ANGGARAN_TW2),
                COL_ANGGARAN_TW3 => $this->input->post(COL_ANGGARAN_TW3),
                COL_ANGGARAN_TW4 => $this->input->post(COL_ANGGARAN_TW4)
            );

            $success = true;
            $this->db->trans_begin();

            $cond1 = array(
                COL_KD_PEMDA => $post[COL_KD_PEMDA],
                COL_KD_MISI => $post[COL_KD_MISI],
                COL_KD_TUJUAN => $post[COL_KD_TUJUAN],
                COL_KD_INDIKATORTUJUAN => $post[COL_KD_INDIKATORTUJUAN],
                COL_KD_SASARAN => $post[COL_KD_SASARAN],
                COL_KD_INDIKATORSASARAN => $post[COL_KD_INDIKATORSASARAN],
                COL_KD_TUJUANOPD => $post[COL_KD_TUJUANOPD],
                COL_KD_INDIKATORTUJUANOPD => $post[COL_KD_INDIKATORTUJUANOPD],
                COL_KD_SASARANOPD => $post[COL_KD_SASARANOPD],
                COL_KD_INDIKATORSASARANOPD => $post[COL_KD_INDIKATORSASARANOPD],
                COL_KD_TAHUN => $post[COL_KD_TAHUN],
                COL_KD_URUSAN => $post[COL_KD_URUSAN],
                COL_KD_BIDANG => $post[COL_KD_BIDANG],
                COL_KD_UNIT => $post[COL_KD_UNIT],
                COL_KD_SUB => $post[COL_KD_SUB],
                COL_KD_BID => $post[COL_KD_BID],
                COL_KD_SUBBID => $post[COL_KD_SUBBID],
                COL_KD_PROGRAMOPD => $post[COL_KD_PROGRAMOPD],
                COL_KD_SASARANPROGRAMOPD => $post[COL_KD_SASARANPROGRAMOPD],
                COL_KD_KEGIATANOPD => $post[COL_KD_KEGIATANOPD]
            );
            $res1 = $this->db
                ->where($cond1)
                ->update(TBL_SAKIP_DPA_KEGIATAN, array(
                    COL_ANGGARAN_TW1 => toNum($post[COL_ANGGARAN_TW1]),
                    COL_ANGGARAN_TW2 => toNum($post[COL_ANGGARAN_TW2]),
                    COL_ANGGARAN_TW3 => toNum($post[COL_ANGGARAN_TW3]),
                    COL_ANGGARAN_TW4 => toNum($post[COL_ANGGARAN_TW4])
                ));
            if(!$res1) {
                $this->db->trans_rollback();
                $success = false;
                $resp['error'] = "Gagal mengupdate Kegiatan.";
                $resp['success'] = 0;
                echo json_encode($resp);
                return;
            }

            for($i=0; $i<count($post[COL_KD_INDIKATORKEGIATANOPD]); $i++) {
                $cond = array(
                    COL_KD_PEMDA => $post[COL_KD_PEMDA],
                    COL_KD_MISI => $post[COL_KD_MISI],
                    COL_KD_TUJUAN => $post[COL_KD_TUJUAN],
                    COL_KD_INDIKATORTUJUAN => $post[COL_KD_INDIKATORTUJUAN],
                    COL_KD_SASARAN => $post[COL_KD_SASARAN],
                    COL_KD_INDIKATORSASARAN => $post[COL_KD_INDIKATORSASARAN],
                    COL_KD_TUJUANOPD => $post[COL_KD_TUJUANOPD],
                    COL_KD_INDIKATORTUJUANOPD => $post[COL_KD_INDIKATORTUJUANOPD],
                    COL_KD_SASARANOPD => $post[COL_KD_SASARANOPD],
                    COL_KD_INDIKATORSASARANOPD => $post[COL_KD_INDIKATORSASARANOPD],
                    COL_KD_TAHUN => $post[COL_KD_TAHUN],
                    COL_KD_URUSAN => $post[COL_KD_URUSAN],
                    COL_KD_BIDANG => $post[COL_KD_BIDANG],
                    COL_KD_UNIT => $post[COL_KD_UNIT],
                    COL_KD_SUB => $post[COL_KD_SUB],
                    COL_KD_BID => $post[COL_KD_BID],
                    COL_KD_SUBBID => $post[COL_KD_SUBBID],
                    COL_KD_PROGRAMOPD => $post[COL_KD_PROGRAMOPD],
                    COL_KD_SASARANPROGRAMOPD => $post[COL_KD_SASARANPROGRAMOPD],
                    COL_KD_KEGIATANOPD => $post[COL_KD_KEGIATANOPD],
                    COL_KD_SASARANKEGIATANOPD => $post[COL_KD_SASARANKEGIATANOPD][$i],
                    COL_KD_INDIKATORKEGIATANOPD => $post[COL_KD_INDIKATORKEGIATANOPD][$i]
                );
                $res = $this->db
                    ->where($cond)
                    ->update(TBL_SAKIP_DPA_KEGIATAN_INDIKATOR, array(
                        COL_KINERJA_TW1 => toNum($post[COL_KINERJA_TW1][$i]),
                        COL_KINERJA_TW2 => toNum($post[COL_KINERJA_TW2][$i]),
                        COL_KINERJA_TW3 => toNum($post[COL_KINERJA_TW3][$i]),
                        COL_KINERJA_TW4 => toNum($post[COL_KINERJA_TW4][$i])
                    ));
                if(!$res) {
                    $this->db->trans_rollback();
                    $success = false;
                    $resp['error'] = "Gagal mengupdate Indikator Kegiatan No. ".$post[COL_KD_SASARANKEGIATANOPD][$i].".".$post[COL_KD_INDIKATORKEGIATANOPD][$i];
                    $resp['success'] = 0;
                    break;
                }
            }

            if($success) $this->db->trans_commit();
            echo json_encode($resp);

        } else {
            $this->load->view('rencana_aksi/kegiatan_realisasi',$data);
        }
    }
}