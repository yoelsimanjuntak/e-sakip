<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 00:35
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        $i+1,
        //$d["Text"]
        '<input type="hidden" name="ID" value="'.$d["ID"].'" /><input type="hidden" name="Text" value="'.$d["Text"].'" />'.$d["Text"],

    );
    $i++;
}
$data = json_encode($res);
?>
<style>
    table.table-picker > tbody > tr > td {
        cursor: pointer;
    }
</style>
<form id="dataform" method="post" action="#">
    <input type="hidden" name="selID" />
    <input type="hidden" name="selText" />
    <table id="browseGrid" class="table table-bordered table-hover table-picker" width="100%" cellspacing="0">

    </table>
</form>

<script>
    $(document).ready(function () {
        var dataTable = $('#browseGrid').dataTable({
            //"sDom": "Rlfrtip",
            "aaData": <?=$data?>,
            //"bJQueryUI": true,
            "aaSorting" : [[0,'asc']],
            //"scrollY" : 400,
            //"scrollX": "200%",
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 100, -1], [10, 100, "Semua"]],
            //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
            "aoColumns": [
                {"sTitle": "#","sWidth":"2%"},
                {"sTitle": "Description","sWidth":"90%"}
            ],
            "createdRow": function (row, data, index) {
                //var elBrowseTable = $("#browseGrid>tbody");
                $(row).dblclick(function () {
                    var elID = $(row).find("[name=ID][type=hidden]").first();
                    var elText = $(row).find("[name=Text][type=hidden]").first();

                    $("[name=selID][type=hidden]").val(elID.val()).change();
                    $("[name=selText][type=hidden]").val(elText.val()).change();
                    $(row).closest(".modal").find("button[data-dismiss=modal]").click();
                    $(row).closest(".modal-body").empty();
                });
            }
        });
    });
</script>
<div class="clearfix"></div>