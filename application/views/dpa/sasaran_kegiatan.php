<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 08/07/2019
 * Time: 23:57
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $nmSub = "";
    $eplandb = $this->load->database("eplan", true);
    $eplandb->where(COL_KD_URUSAN, $d[COL_KD_URUSAN]);
    $eplandb->where(COL_KD_BIDANG, $d[COL_KD_BIDANG]);
    $eplandb->where(COL_KD_UNIT, $d[COL_KD_UNIT]);
    $eplandb->where(COL_KD_SUB, $d[COL_KD_SUB]);
    $subunit = $eplandb->get("ref_sub_unit")->row_array();
    if($subunit) {
        $nmSub = $subunit["Nm_Sub_Unit"];
    }

    $nmProg = $d[COL_NM_PROGRAMOPD];
    $nmKeg = $d[COL_NM_KEGIATANOPD];

    $htmlIKU_ = "";
    $htmlIKU_ .= "<table>";
    $htmlIKU_ .= "<tr>";
    $htmlIKU_ .= "<td style='text-align: right; font-weight: bold'>Tujuan</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_TUJUANOPD]."</td>";
    $htmlIKU_ .= "</tr>";
    $htmlIKU_ .= "<tr>";
    $htmlIKU_ .= "<td style='text-align: right; font-weight: bold'>Ik. Tujuan</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_INDIKATORTUJUANOPD]."</td>";
    $htmlIKU_ .= "</tr>";
    $htmlIKU_ .= "<tr>";
    $htmlIKU_ .= "<td style='text-align: right; font-weight: bold'>Sasaran</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_SASARANOPD]."</td>";
    $htmlIKU_ .= "</tr>";
    $htmlIKU_ .= "<tr>";
    $htmlIKU_ .= "<td style='text-align: right; font-weight: bold'>Ik. Sasaran</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_INDIKATORSASARANOPD]."</td>";
    $htmlIKU_ .= "</tr>";
    $htmlIKU_ .= "</table>";

    $htmlIKU = "";
    $htmlIKU .= "<table>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Misi</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_NM_MISI]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Tujuan</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_NM_TUJUAN]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Ik. Tujuan</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN].".".$d[COL_NM_INDIKATORTUJUAN]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Sasaran</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN].".".$d[COL_KD_SASARAN].".".$d[COL_NM_SASARAN]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "<tr>";
    $htmlIKU .= "<td style='text-align: right; font-weight: bold'>Ik. Sasaran</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN].".".$d[COL_KD_SASARAN].".".$d[COL_KD_INDIKATORSASARAN].".".$d[COL_NM_INDIKATORSASARAN]."</td>";
    $htmlIKU .= "</tr>";
    $htmlIKU .= "</table>";

    $htmlBidang = "";
    $htmlBidang .= "<table>";
    $htmlBidang .= "<tr>";
    $htmlBidang .= "<td style='text-align: right; font-weight: bold'>OPD</td><td style='padding: 0px 3px'>:</td><td>".$nmSub."</td>";
    $htmlBidang .= "</tr>";
    $htmlBidang .= "<tr>";
    $htmlBidang .= "<td style='text-align: right; font-weight: bold'>Bidang</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_BID]."</td>";
    $htmlBidang .= "</tr>";
    $htmlBidang .= "<tr>";
    $htmlBidang .= "<td style='text-align: right; font-weight: bold'>Sub Bidang</td><td style='padding: 0px 3px'>:</td><td>".$d[COL_NM_SUBBID]."</td>";
    $htmlBidang .= "</tr>";
    $htmlBidang .= "</table>";

    $htmlProg = "";
    $htmlProg .= "<table>";
    $htmlProg .= "<tr>";
    $htmlProg .= "<td style='text-align: right; font-weight: bold'>Program</td><td style='padding: 0px 3px'>:</td><td>".$nmProg."</td>";
    $htmlProg .= "</tr>";
    $htmlProg .= "<tr>";
    $htmlProg .= "<td style='text-align: right; font-weight: bold'>Kegiatan</td><td style='padding: 0px 3px'>:</td><td>".$nmKeg."</td>";
    $htmlProg .= "</tr>";
    $htmlProg .= "</table>";

    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d["ID"] . '" />',
        anchor('dpa/sasaran-kegiatan-edit/'.$d["ID"],$d[COL_NM_SASARANKEGIATANOPD]),
        $htmlProg,
        $htmlBidang,
        $htmlIKU,
        $htmlIKU_,
        $d[COL_KD_TAHUN],

        //number_format($d[COL_BUDGET], 0)
    );
    $i++;
}
$data = json_encode($res);
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                DPA - Sasaran Kegiatan
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('dpa/sasaran-kegiatan-del','<i class="fa fa-trash-o"></i> Hapus',array('class'=>'cekboxaction btn btn-sm btn-danger','confirm'=>'Apa anda yakin?'))
            ?>
            <?=anchor('dpa/sasaran-kegiatan-add','<i class="fa fa-plus"></i> Data Baru',array('class'=>'btn btn-sm btn-primary'))
            ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover" style="white-space: nowrap">

                    </table>
                </form>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : '40vh',
                //"scrollX": "200%",
                "scrollX": true,
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "order": [[ 1, "asc" ]],
                "columnDefs": [
                    //{ className: "text-right", "targets": [8] }
                ],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":10,bSortable:false},
                    {"sTitle": "Sasaran"},
                    {"sTitle": "Program / Kegiatan"},
                    {"sTitle": "Unit"},
                    {"sTitle": "IKU Kabupaten"},
                    {"sTitle": "IKU OPD"},
                    {"sTitle": "Tahun"}
                ]/*,
                 'columnDefs': [
                 {
                 "targets": 7,
                 "className": "text-right"
                 }
                 ]*/
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')
?>