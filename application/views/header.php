
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? 'E-SAKIP | '.$title : SITENAME?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/adminlte/plugins/modernizr/modernizr.js"></script>

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bootstrap/css/bootstrap.min.css">
    <!-- font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">

    <link href="<?=base_url()?>assets/css/my.css" rel="stylesheet" type="text/css" />
    <!--<link href="--><?//=base_url()?><!--assets/tbs/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />-->

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/plugins/iCheck/all.css">

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/plugins/select2/select2.min.css">

    <!-- Bootstrap select -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-select.css">-->

    <!-- datatable css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>

    <!-- datatable buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

    <!-- WYSIHTML5 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/plugins/datepicker/datepicker3.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- Preloader Style -->
<style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url()?>assets/preloader/images/loader-128x/Preloader_4.gif) center no-repeat #fff;
    }
    .nowrap {
        white-space: nowrap !important;
    }
    .autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
    .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: visible; }
    .autocomplete-selected { background: #F0F0F0; }
    .autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
    .autocomplete-group { padding: 2px 5px; }
    .autocomplete-group strong { display: block; border-bottom: 1px solid #000; }
</style>
<!-- /.preloader style -->

<!-- Preloader Script -->
<script>
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
        $(".no-auto").attr("autocomplete", "off");
    });
</script>
<!-- /.preloader script -->

<body class="skin-green-light fixed sidebar-mini sidebar-collapse">
<!-- preloader -->
<div class="se-pre-con"></div>
<!-- /.preloader -->

<div class="wrapper">
    <?php
    $ruser = GetLoggedUser();
    $displayname = $ruser ? $ruser[COL_USERNAME] : "Guest";
    $displayfullname = $ruser ? $ruser[COL_NAME] : "Guest";
    $displaypicture = MY_IMAGEURL.'user.jpg';
    $displayrole = "";
    if($ruser) {
        if($ruser[COL_ROLEID] == ROLECOMPANY) {
            $displaypicture = $ruser[COL_FILENAME] ? MY_UPLOADURL.$ruser[COL_FILENAME] : MY_IMAGEURL.'company-icon.jpg';
        } else {
            $displaypicture = $ruser[COL_IMAGEFILENAME] ? MY_UPLOADURL.$ruser[COL_IMAGEFILENAME] : MY_IMAGEURL.'user.jpg';
        }

        $rrole = $this->db->where(COL_ROLEID, $ruser[COL_ROLEID])->get(TBL_SAKIP_ROLES)->row_array();
        if($rrole) {
            $displayrole = $rrole[COL_ROLENAME];
        }
    }
    ?>
    <header class="main-header">

        <!-- Logo -->
        <a href="<?=site_url()?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
                <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 30px" alt="Logo">
            </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                E-<b>SAKIP</b>
            </span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?=$displaypicture?>" class="user-image" alt="Your Profile Image">
                            <span class="hidden-xs"><?=$displayfullname?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?=$displaypicture?>" class="img-circle" alt="User Image">

                                <p>
                                    <?=$displayname?>
                                    <small><?=$displayrole?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= site_url("user/changepassword") ?>" class="btn btn-info"><i class="fa fa-gear"></i> Ubah Password</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= site_url("user/logout") ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Keluar</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?=$displaypicture?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?=$displayname?></p>
                    <a href="#"><?=date("d M Y")?></a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MENU UTAMA</li>
                <li class="treeview">
                    <a href="<?=site_url('user/dashboard')?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <?php
                if($ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Master Data</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('master/uom')?>"><i class="fa fa-angle-right"></i>Satuan</a></li>
                            <li><a href="<?=site_url('master/sumberdana')?>"><i class="fa fa-angle-right"></i>Sumber Dana</a></li>
                        </ul>
                    </li>
                <?php
                }
                if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Pemerintah Daerah</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('mpemda/period')?>"><i class="fa fa-angle-right"></i>Periode, Visi & Misi</a></li>
                            <li><a href="<?=site_url('mpemda/tujuan')?>"><i class="fa fa-angle-right"></i>Tujuan</a></li>
                            <li><a href="<?=site_url('mpemda/sasaran')?>"><i class="fa fa-angle-right"></i>Sasaran</a></li>
                        </ul>
                    </li>
                    <?php
                }
                if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>OPD</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('mopd/tujuan')?>"><i class="fa fa-angle-right"></i>Tujuan</a></li>
                            <li><a href="<?=site_url('mopd/sasaran')?>"><i class="fa fa-angle-right"></i>Sasaran</a></li>
                            <li><a href="<?=site_url('mopd/bid')?>"><i class="fa fa-angle-right"></i>Bidang</a></li>
                            <li><a href="<?=site_url('mopd/subbid')?>"><i class="fa fa-angle-right"></i>Sub Bidang</a></li>
                        </ul>
                    </li>
                    <?php
                }
                if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Bidang</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('mbid/sasaran')?>"><i class="fa fa-angle-right"></i>Sasaran Bidang</a></li>
                            <li><a href="<?=site_url('mbid/program')?>"><i class="fa fa-angle-right"></i>Program</a></li>
                            <li><a href="<?=site_url('mbid/sasaran-program')?>"><i class="fa fa-angle-right"></i>Sasaran Program</a></li>

                        </ul>
                    </li>
                    <?php
                }
                if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID || $ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Sub Bidang</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('msubbid/sasaran')?>"><i class="fa fa-angle-right"></i>Sasaran Sub Bidang</a></li>
                            <li><a href="<?=site_url('msubbid/kegiatan')?>"><i class="fa fa-angle-right"></i>Kegiatan</a></li>
                            <li><a href="<?=site_url('msubbid/sasaran-kegiatan')?>"><i class="fa fa-angle-right"></i>Sasaran Kegiatan</a></li>
                        </ul>
                    </li>
                    <?php
                }
                if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKEUANGAN || $ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>DPA</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('dpa/program')?>"><i class="fa fa-angle-right"></i>Program</a></li>
                            <li><a href="<?=site_url('dpa/sasaran-program')?>"><i class="fa fa-angle-right"></i>Sasaran Program</a></li>
                            <li><a href="<?=site_url('dpa/kegiatan')?>"><i class="fa fa-angle-right"></i>Kegiatan</a></li>
                            <li><a href="<?=site_url('dpa/sasaran-kegiatan')?>"><i class="fa fa-angle-right"></i>Sasaran Kegiatan</a></li>
                        </ul>
                    </li>
                <?php
                }
                if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID || $ruser[COL_ROLEID] == ROLEKASUBBID || $ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Rencana Aksi</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('rencana-aksi/program')?>"><i class="fa fa-angle-right"></i>Program</a></li>
                            <li><a href="<?=site_url('rencana-aksi/kegiatan')?>"><i class="fa fa-angle-right"></i>Kegiatan</a></li>
                        </ul>
                    </li>
                <?php
                }
                ?>
                <?php
                if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEKEUANGAN) {
                    ?>
                    <li class="header">LAPORAN</li>
                    <?php
                    if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEADMIN) {
                        ?>
                        <li class="treeview">
                            <a href="<?= site_url('report/tc27') ?>">
                                <i class="fa fa-book"></i> <span>Resume Renstra</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i> <span>IKU</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=site_url('report/iku-kabupaten')?>"><i class="fa fa-angle-right"></i>IKU Kabupaten</a></li>
                                <li><a href="<?=site_url('report/iku')?>"><i class="fa fa-angle-right"></i>IKU OPD</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="<?=site_url('report/renja')?>">
                                <i class="fa fa-book"></i> <span>Resume Renja</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="<?=site_url('report/rka')?>">
                                <i class="fa fa-book"></i> <span>RKA</span>
                            </a>
                        </li>
                    <?php
                    }
                    ?>
                        <li class="treeview">
                            <a href="<?=site_url('report/dpa')?>">
                                <i class="fa fa-book"></i> <span>DPA</span>
                            </a>
                        </li>
                    <?php
                    if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i> <span>Pohon Kinerja</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=site_url('report/pohonkinerja-kabupaten')?>"><i class="fa fa-angle-right"></i>Pohon Kinerja Kabupaten</a></li>
                                <li><a href="<?=site_url('report/pohonkinerja')?>"><i class="fa fa-angle-right"></i>Pohon Kinerja OPD</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="<?=site_url('report/cascading')?>">
                                <i class="fa fa-book"></i> <span>Cascading</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="<?=site_url('report/perjanjiankerja')?>">
                                <i class="fa fa-book"></i> <span>Perjanjian Kinerja</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="<?=site_url('report/rencana-aksi')?>">
                                <i class="fa fa-book"></i> <span>Rencana Aksi</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="<?=site_url('report/evaluasi')?>">
                                <i class="fa fa-book"></i> <span>Evaluasi Kinerja</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i> <span>Laporan Kinerja / LAKIP</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                            </a>
                            <ul class="treeview-menu">
                                <!--<li><a href="<?=site_url('report/lakip-kinerja')?>"><i class="fa fa-angle-right"></i>Capaian Kinerja</a></li>-->
                                <li><a href="<?=site_url('report/lakip-kinerja/1')?>"><i class="fa fa-angle-right"></i>Perbandingan Target Kinerja</a></li>
                                <li><a href="<?=site_url('report/lakip-kinerja/2')?>"><i class="fa fa-angle-right"></i>Perbandingan Realisasi Kinerja</a></li>
                                <li><a href="<?=site_url('report/lakip-kinerja/3')?>"><i class="fa fa-angle-right"></i>Perbandingan Tingkat Capaian</a></li>
                                <li><a href="<?=site_url('report/lakip-anggaran')?>"><i class="fa fa-angle-right"></i>Realisasi Anggaran</a></li>
                            </ul>
                        </li>
                        <?php
                        if($ruser[COL_ROLEID] == ROLEBAPPEDA || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEADMIN) {
                            ?>
                            <li class="header">DOKUMEN</li>
                            <li class="treeview">
                                <a href="<?=site_url('mopd/file/RPJMD')?>">
                                    <i class="fa fa-file"></i> <span>RPJMD</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="<?=site_url('mopd/file/RENSTRA')?>">
                                    <i class="fa fa-file"></i> <span>Rencana Strategis</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="<?=site_url('mopd/file/RENJA')?>">
                                    <i class="fa fa-file"></i> <span>Rencana Kerja</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="<?=site_url('mopd/file/IKU')?>">
                                    <i class="fa fa-file"></i> <span>IKU Kabupaten</span>
                                </a>
                            </li>
                            <li class="treeview">
                                <a href="<?=site_url('mopd/file/LAKIP')?>">
                                    <i class="fa fa-file"></i> <span>Laporan Kinerja / LAKIP</span>
                                </a>
                            </li>
                        <?php
                        }
                    }
                    ?>
                    <!--<li class="treeview">
                        <a href="<?=site_url('report/lakip')?>">
                            <i class="fa fa-book"></i> <span>Laporan Kinerja</span>
                        </a>
                    </li>-->
                <?php
                }
                ?>
                <li class="header">LAIN-LAIN</li>
                <?php
                if($ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Pengguna</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('user/index')?>"><i class="fa fa-angle-right"></i> Data</a></li>
                            <li><a href="<?=site_url('user/add')?>"><i class="fa fa-angle-right"></i> Tambah Pengguna</a></li>
                        </ul>
                    </li>
                <?php
                }
                ?>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-circle-o"></i> <span>Akun</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <!--<li><a href="<?=site_url('user/profile')?>"><i class="fa fa-circle-o"></i> Profil</a></li>-->
                        <li><a href="<?=site_url('user/changepassword')?>"><i class="fa fa-angle-right"></i> Ubah Password</a></li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">