<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/14/2019
 * Time: 10:27 PM
 */
$this->load->view('header');
?>
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('master/sumberdana')?>"> Sumber Dana</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Kode</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="<?=COL_KD_SUMBERDANA?>" value="<?= $edit ? $data[COL_KD_SUMBERDANA] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Nama</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="<?=COL_NM_SUMBERDANA?>" value="<?= $edit ? $data[COL_NM_SUMBERDANA] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('master/sumberdana')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>

                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $("#main-form").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        $(".modal-body", $("#alertDialog")).html(a.responseText);
                        $("#alertDialog").modal('show');
                        //alert(a.responseText);
                    }
                });
                return false;
            }
        });
    </script>
<?php $this->load->view('footer') ?>