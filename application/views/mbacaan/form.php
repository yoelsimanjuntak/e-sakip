<?=form_open(current_url(),array('role'=>'form','id'=>'deviceForm','class'=>'form-horizontal'))?>
<div style="display: none" class="alert alert-danger errorBox">
    <i class="fa fa-ban"></i>
    <span class="errorMsg"></span>
</div>
<?php
if($this->input->get('success') == 1){
    ?>
    <div class="alert alert-success">
        <i class="fa fa-check"></i>
        <span class="">Data disimpan</span>
    </div>
<?php
}
?>
<div class="form-group">
    <label class="control-label col-sm-3">Nama Kategori</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="<?=COL_NMKATEGORI?>" value="<?= $edit ? $data[COL_NMKATEGORI] : ""?>" required>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12" style="text-align: right">
        <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
    </div>
</div>
<?=form_close()?>
<?php $this->load->view('loadjs') ?>
<script type="text/javascript">
    $("#deviceForm").validate({
        submitHandler : function(form){
            $(form).find('btn').attr('disabled',true);
            $(form).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success : function(data){
                    $(form).find('btn').attr('disabled',false);
                    if(data.error != 0){
                        $('.errorBox').show().find('.errorMsg').text(data.error);
                    }else{
                        window.location.href = data.redirect;
                    }
                },
                error : function(a,b,c){
                    alert('Response Error');
                }
            });
            return false;
        }
    });
</script>