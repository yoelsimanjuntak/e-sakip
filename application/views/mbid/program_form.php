<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 19:45
 */
$this->load->view('header') ?>
<?php
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mbid/program')?>">Program Bidang OPD</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSub = "";
                                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                    if($edit) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                        $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                        $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                        $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }

                                    ?>
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[0]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[1]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[2]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[3]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-periode" value="<?= $edit ? $data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]." : ".$data[COL_NM_PEJABAT] : (!empty($data["DefPeriod"])?$data["DefPeriod"]:"")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= !empty($data[COL_KD_PEMDA])?$data[COL_KD_PEMDA]:"" ?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-period" data-toggle="modal" data-target="#browsePeriod" data-toggle="tooltip" data-placement="top" title="Pilih Periode" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="control-label col-sm-2">Misi</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-misi" value="<?= $edit ? $data[COL_KD_MISI].". ".$data[COL_NM_MISI] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-misi" data-toggle="modal" data-target="#browseMisi" data-toggle="tooltip" data-placement="top" title="Pilih Misi" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tujuan</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-tujuan" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN]." ".$data[COL_NM_TUJUAN] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-tujuan" data-toggle="modal" data-target="#browseTujuan" data-toggle="tooltip" data-placement="top" title="Pilih Tujuan" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Tujuan</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-iktujuan" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN]." ".$data[COL_NM_INDIKATORTUJUAN] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-iktujuan" data-toggle="modal" data-target="#browseIKTujuan" data-toggle="tooltip" data-placement="top" title="Pilih Indikator Tujuan" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Sasaran</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-sasaran" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN]." ".$data[COL_NM_SASARAN] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-sasaran" data-toggle="modal" data-target="#browseSasaran" data-toggle="tooltip" data-placement="top" title="Pilih Sasaran" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Sasaran</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-iksasaran" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN]." ".$data[COL_NM_INDIKATORSASARAN] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-iksasaran" data-toggle="modal" data-target="#browseIKSasaran" data-toggle="tooltip" data-placement="top" title="Pilih Indikator Sasaran" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <label class="control-label col-sm-2">IKU Kabupaten</label>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#browseIKU" data-toggle="tooltip" data-placement="top" title="Pilih IKU">
                                    Pilih ...
                                </button>
                                <div class="div-iku no-padding" style="margin-top: 10px">
                                    <table class="table table-bordered" style="outline-style: dashed;">
                                        <tr>
                                            <td style="width: 150px" class="control-label">Misi</td>
                                            <td style="width: 10px">:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required >
                                                <span class="text-misi"><?= $edit ? $data[COL_KD_MISI].". ".$data[COL_NM_MISI] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Tujuan</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required >
                                                <span class="text-tujuan"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].". ".$data[COL_NM_TUJUAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Tujuan</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required >
                                                <span class="text-iktujuan"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].". ".$data[COL_NM_INDIKATORTUJUAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required >
                                                <span class="text-sasaran"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].". ".$data[COL_NM_SASARAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required >
                                                <span class="text-iksasaran"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].". ".$data[COL_NM_INDIKATORSASARAN] : ""?></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">IKU OPD</label>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#browseIKUOPD" data-toggle="tooltip" data-placement="top" title="Pilih IKU OPD">
                                    Pilih ...
                                </button>
                                <div class="div-iku no-padding" style="margin-top: 10px">
                                    <table class="table table-bordered" style="outline-style: dashed;">
                                        <tr>
                                            <td style="width: 150px" class="control-label">Tujuan</td>
                                            <td style="width: 10px">:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required >
                                                <span class="text-tujuan-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].". ".$data[COL_NM_TUJUANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Tujuan</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required >
                                                <span class="text-iktujuan-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].". ".$data[COL_NM_INDIKATORTUJUANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_SASARANOPD?>" value="<?= $edit ? $data[COL_KD_SASARANOPD] : ""?>" required >
                                                <span class="text-sasaran-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].".".$data[COL_KD_SASARANOPD].". ".$data[COL_NM_SASARANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORSASARANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARANOPD] : ""?>" required >
                                                <span class="text-iksasaran-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].".".$data[COL_KD_SASARANOPD].".".$data[COL_KD_INDIKATORSASARANOPD].". ".$data[COL_NM_INDIKATORSASARANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="control-label col-sm-2">Tujuan OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-tujuanopd" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].".".$data[COL_KD_TUJUANOPD]." ".$data[COL_NM_TUJUANOPD] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-tujuanopd" data-toggle="modal" data-target="#browseTujuanOPD" data-toggle="tooltip" data-placement="top" title="Pilih Tujuan OPD" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Tujuan OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-iktujuanopd" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].".".$data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD]." ".$data[COL_NM_INDIKATORTUJUANOPD] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-iktujuanopd" data-toggle="modal" data-target="#browseIKTujuanOPD" data-toggle="tooltip" data-placement="top" title="Pilih Indikator Tujuan OPD" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Sasaran OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-sasaranopd" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].".".$data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].".".$data[COL_KD_SASARANOPD]." ".$data[COL_NM_SASARANOPD] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_SASARANOPD?>" value="<?= $edit ? $data[COL_KD_SASARANOPD] : ""?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-sasaranopd" data-toggle="modal" data-target="#browseSasaranOPD" data-toggle="tooltip" data-placement="top" title="Pilih Sasaran OPD" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Sasaran OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-iksasaranopd" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].".".$data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].".".$data[COL_KD_SASARANOPD].".".$data[COL_KD_INDIKATORSASARANOPD]." ".$data[COL_NM_INDIKATORSASARANOPD] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= $edit ? $data[COL_KD_PEMDA] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARANOPD?>" value="<?= $edit ? $data[COL_KD_SASARANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARANOPD] : ""?>" required ><input type="hidden" name="<?=COL_KD_TAHUN_FROM?>" >
                                    <input type="hidden" name="<?=COL_KD_TAHUN_TO?>" >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-iksasaranopd" data-toggle="modal" data-target="#browseIKSasaranOPD" data-toggle="tooltip" data-placement="top" title="Pilih Indikator Sasaran OPD" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <label class="control-label col-sm-2">Bidang</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmBid = "";
                                    if($ruser[COL_ROLEID] == ROLEKABID) {
                                        $this->db->where(COL_KD_URUSAN, $strOPD[0]);
                                        $this->db->where(COL_KD_BIDANG, $strOPD[1]);
                                        $this->db->where(COL_KD_UNIT, $strOPD[2]);
                                        $this->db->where(COL_KD_SUB, $strOPD[3]);
                                        $this->db->where(COL_KD_BID, $strOPD[4]);
                                        $bid = $this->db->get(TBL_SAKIP_MBID)->row_array();
                                        if($bid) {
                                            $nmBid = $bid[COL_NM_BID];
                                        }
                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-bid" value="<?= $edit ? $data[COL_KD_BID].". ".$data[COL_NM_BID] : ($ruser[COL_ROLEID]==ROLEKABID?$strOPD[4].". ".$nmBid:"")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_BID?>" value="<?= $edit ? $data[COL_KD_BID] : ($ruser[COL_ROLEID]==ROLEKABID?$strOPD[4]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browseBid" data-toggle="tooltip" data-placement="top" title="Pilih Bidang" <?=$edit?"":($ruser[COL_ROLEID]==ROLEKABID?"disabled":"")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="<?=COL_ISEPLAN?>" <?=$edit ? ($data[COL_ISEPLAN] ? "checked" : "") : "checked"?>>
                                        Import via e-Planning
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group is-eplan">
                            <label class="control-label col-sm-2">Program</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmProg = "";
                                    if($edit) {
                                        $nmProg = $data[COL_NM_PROGRAMOPD];
                                        /*if(!$nmProg) {
                                            $eplandb = $this->load->database("eplan", true);
                                            $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                            $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                            $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                            $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                            $eplandb->where("Tahun", $data[COL_KD_TAHUN]-1);
                                            $eplandb->where("Kd_Prog", $data[COL_KD_PROGRAMOPD]);
                                            $prog = $eplandb->get("ta_program")->row_array();
                                            if($prog) {
                                                $nmProg = $prog["Ket_Prog"];
                                            }
                                        }*/
                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-prog" value="<?= $edit && $data[COL_ISEPLAN] ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB].".".$data[COL_KD_PROGRAMOPD]." ".$nmProg : ""?>" readonly>
                                    <!--<input type="hidden" name="<?=COL_KD_TAHUN?>" value="<?= $edit && $data[COL_ISEPLAN] ? $data[COL_KD_TAHUN] : ""?>" required >-->
                                    <input type="hidden" name="<?=COL_KD_PROGRAMOPD?>" value="<?= $edit && $data[COL_ISEPLAN] ? $data[COL_KD_PROGRAMOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_NM_PROGRAMOPD?>" value="<?= $edit && $data[COL_ISEPLAN] ? $data[COL_NM_PROGRAMOPD] : ""?>" required >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-prog" data-toggle="modal" data-target="#browseProgram" data-toggle="tooltip" data-placement="top" title="Pilih Program" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group is-not-eplan">
                            <label class="control-label col-sm-2">Program</label>
                            <div class="col-sm-6">
                                <input type="text" placeholder="Nama Program" class="form-control" name="<?=COL_NM_PROGRAMOPD?>" value="<?= $edit ? $data[COL_NM_PROGRAMOPD] : ""?>" required >
                            </div>
                            <div class="col-sm-2">
                                <input type="number" placeholder="Kode Program" class="form-control" name="<?=COL_KD_PROGRAMOPD?>" value="<?= $edit ? $data[COL_KD_PROGRAMOPD] : ""?>" required >
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-2">Tahun</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?= $edit ? $data[COL_KD_TAHUN] : ""?>" required readonly>
                            </div>
                        </div>

                        <?php
                        if(!$edit) {
                            ?>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="IsBulk">
                                            Input 5 Tahunan
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Catatan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?=COL_REMARKS?>" value="<?= $edit ? $data[COL_REMARKS] : ""?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('mbid/program')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>

                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browsePeriod" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="modal fade" id="browseMisi" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseTujuan" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKTujuan" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseSasaran" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKSasaran" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>-->
    <div class="modal fade" id="browseIKU" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse IKU</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="btn btn-primary btn-flat" id="btn-select-iku">Pilih</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="modal fade" id="browseTujuanOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKTujuanOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseSasaranOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>-->
    <div class="modal fade" id="browseIKUOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse IKU</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="btn btn-primary btn-flat" id="btn-select-iku-opd">Pilih</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseBid" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKSasaranOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseProgram" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        var mainform = $("#main-form");
        $(".btn-del-det").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-det").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-det").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-det").show();
            $(".btn-del-det", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });

        $("#main-form").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);

                /*var det = [];
                var rowDet = $("#tbl-det>tbody").find("tr:not(.tr-blueprint-det)");

                for (var i = 0; i < rowDet.length; i++) {
                    var noDet = $("[name=text-det-no]", $(rowDet[i])).val();
                    var ketDet = $("[name=text-det-desc]", $(rowDet[i])).val();
                    det.push({ No: noDet, Ket: ketDet });
                }
                $(".appended", $("#main-form")).remove();
                for (var i = 0; i < det.length; i++) {
                    var newEl = "<input type='hidden' class='appended' name=NoDet[" + i + "] value='" + det[i].No + "' /><input type='hidden' class='appended' name=KetDet[" + i + "] value='" + det[i].Ket + "' />";
                    $("#main-form").append(newEl);
                }*/

                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        alert('Response Error');
                    }
                });
                return false;
            }
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browseOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseOPD"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdSub = $(this).val().split('|');
                    $("[name=Kd_Urusan]").val(kdSub[0]);
                    $("[name=Kd_Bidang]").val(kdSub[1]);
                    $("[name=Kd_Unit]").val(kdSub[2]);
                    $("[name=Kd_Sub]").val(kdSub[3]);

                    $("[name=Kd_Bid]").val("");
                    $("[name=text-bid]").val("");

                    $("[name=Kd_Pemda]").val("");
                    $("[name=Kd_Misi]").val("");
                    $("[name=Kd_Tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-opd]").val($(this).val());
                });
            });
        });

        $('#browseBid').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseBid"));
            $(this).removeData('bs.modal');
            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-bid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Bid]").val($(this).val());
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-bid]").val($(this).val());
                });
            });
        });

        $('#browsePeriod').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browsePeriod"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-pemda")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Pemda]").val($(this).val());

                    $("[name=Kd_Misi]").val("").trigger("change");
                    $(".text-misi").html("");
                    $("[name=Kd_Tujuan]").val("").trigger("change");
                    $(".text-tujuan").html("");
                    $("[name=Kd_IndikatorTujuan]").val("").trigger("change");
                    $(".text-iktujuan").html("");
                    $("[name=Kd_Sasaran]").val("").trigger("change");
                    $(".text-sasaran").html("");
                    $("[name=Kd_IndikatorSasaran]").val("").trigger("change");
                    $(".text-iksasaran").html("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-periode]").val($(this).val());
                });
            });
        });

        $('#browseMisi').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseMisi"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-misi")?>"+"?Kd_Pemda="+kdPemda, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Misi]").val($(this).val());

                    $("[name=Kd_Tujuan]").val("");
                    $("[name=text-tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=text-iktujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=text-sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");

                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=text-tujuanopd]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=text-iktujuanopd").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=text-sasaranopd]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-misi]").val($(this).val());
                });
            });
        });

        $('#browseTujuan').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseTujuan"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-tujuan")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Tujuan]").val($(this).val());

                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=text-iktujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=text-sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");

                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=text-tujuanopd]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=text-iktujuanopd").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=text-sasaranopd]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-tujuan]").val($(this).val());
                });
            });
        });

        $('#browseIKTujuan').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKTujuan"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iktujuan")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_IndikatorTujuan]").val($(this).val());

                    $("[name=Kd_Sasaran]").val("");
                    $("[name=text-sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");

                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=text-tujuanopd]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=text-iktujuanopd").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=text-sasaranopd]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-iktujuan]").val($(this).val());
                });
            });
        });

        $('#browseSasaran').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseSasaran"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            var kdIKTujuan = $("[name=Kd_IndikatorTujuan]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdIKTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-sasaran")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan+"&Kd_IndikatorTujuan="+kdIKTujuan, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Sasaran]").val($(this).val());

                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");

                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=text-tujuanopd]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=text-iktujuanopd").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=text-sasaranopd]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-sasaran]").val($(this).val());
                });
            });
        });

        $('#browseIKSasaran').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKSasaran"));
            $(this).removeData('bs.modal');
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            var kdIKTujuan = $("[name=Kd_IndikatorTujuan]").val();
            var kdSasaran = $("[name=Kd_Sasaran]").val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdIKTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih sasaran terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iksasaran")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan+"&Kd_IndikatorTujuan="+kdIKTujuan+"&Kd_Sasaran="+kdSasaran, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_IndikatorSasaran]").val($(this).val());

                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=text-tujuanopd]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=text-iktujuanopd").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=text-sasaranopd]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-iksasaran]").val($(this).val());
                });
            });
        });

        $('#browseTujuanOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseTujuanOPD"));
            $(this).removeData('bs.modal');
            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            var kdIKTujuan = $("[name=Kd_IndikatorTujuan]").val();
            var kdSasaran = $("[name=Kd_Sasaran]").val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdIKTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih sasaran terlebih dahulu!</p>");
                return;
            }
            if(!kdIkSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator sasaran terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-tujuan-opd")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan+"&Kd_IndikatorTujuan="+kdIKTujuan+"&Kd_Sasaran="+kdSasaran+"&Kd_IndikatorSasaran="+kdIkSasaran+"&Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_TujuanOPD]").val($(this).val());

                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=text-iktujuanopd").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=text-sasaranopd]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-tujuanopd]").val($(this).val());
                });
            });
        });

        $('#browseIKTujuanOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKTujuanOPD"));
            $(this).removeData('bs.modal');
            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            var kdIKTujuan = $("[name=Kd_IndikatorTujuan]").val();
            var kdSasaran = $("[name=Kd_Sasaran]").val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]").val();
            var kdTujuanOPD = $("[name=Kd_TujuanOPD]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdIKTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih sasaran terlebih dahulu!</p>");
                return;
            }
            if(!kdIkSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator sasaran terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuanOPD) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan OPD terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iktujuan-opd-ver-2")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan+"&Kd_IndikatorTujuan="+kdIKTujuan+"&Kd_Sasaran="+kdSasaran+"&Kd_IndikatorSasaran="+kdIkSasaran+"&Kd_TujuanOPD="+kdTujuanOPD+"&Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_IndikatorTujuanOPD]").val($(this).val());

                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=text-sasaranopd]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-iktujuanopd]").val($(this).val());
                });
            });
        });

        $('#browseSasaranOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseSasaranOPD"));
            $(this).removeData('bs.modal');
            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            var kdIKTujuan = $("[name=Kd_IndikatorTujuan]").val();
            var kdSasaran = $("[name=Kd_Sasaran]").val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]").val();
            var kdTujuanOPD = $("[name=Kd_TujuanOPD]").val();
            var kdIkTujuanOPD = $("[name=Kd_IndikatorTujuanOPD]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdIKTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih sasaran terlebih dahulu!</p>");
                return;
            }
            if(!kdIkSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator sasaran terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuanOPD) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan OPD terlebih dahulu!</p>");
                return;
            }
            if(!kdIkTujuanOPD) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan OPD terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-sasaran-opd")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan+"&Kd_IndikatorTujuan="+kdIKTujuan+"&Kd_Sasaran="+kdSasaran+"&Kd_IndikatorSasaran="+kdIkSasaran+"&Kd_TujuanOPD="+kdTujuanOPD+"&Kd_IndikatorTujuanOPD="+kdIkTujuanOPD+"&Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_SasaranOPD]").val($(this).val());

                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-sasaranopd]").val($(this).val());
                });
            });
        });

        $('#browseIKSasaranOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKSasaranOPD"));
            $(this).removeData('bs.modal');
            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();
            var kdPemda = $("[name=Kd_Pemda]").val();
            var kdMisi = $("[name=Kd_Misi]").val();
            var kdTujuan = $("[name=Kd_Tujuan]").val();
            var kdIKTujuan = $("[name=Kd_IndikatorTujuan]").val();
            var kdSasaran = $("[name=Kd_Sasaran]").val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]").val();
            var kdTujuanOPD = $("[name=Kd_TujuanOPD]").val();
            var kdIkTujuanOPD = $("[name=Kd_IndikatorTujuanOPD]").val();
            var kdSasaranOPD = $("[name=Kd_SasaranOPD]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }
            if(!kdMisi) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih misi terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdIKTujuan) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan terlebih dahulu!</p>");
                return;
            }
            if(!kdSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih sasaran terlebih dahulu!</p>");
                return;
            }
            if(!kdIkSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator sasaran terlebih dahulu!</p>");
                return;
            }
            if(!kdTujuanOPD) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih tujuan OPD terlebih dahulu!</p>");
                return;
            }
            if(!kdIkTujuanOPD) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih indikator tujuan OPD terlebih dahulu!</p>");
                return;
            }
            if(!kdSasaranOPD) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih sasaran OPD terlebih dahulu!</p>");
                return;
            }
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iksasaran-opd-ver-2")?>"+"?Kd_Pemda="+kdPemda+"&Kd_Misi="+kdMisi+"&Kd_Tujuan="+kdTujuan+"&Kd_IndikatorTujuan="+kdIKTujuan+"&Kd_Sasaran="+kdSasaran+"&Kd_IndikatorSasaran="+kdIkSasaran+"&Kd_TujuanOPD="+kdTujuanOPD+"&Kd_IndikatorTujuanOPD="+kdIkTujuanOPD+"&Kd_SasaranOPD="+kdSasaranOPD+"&Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_IndikatorSasaranOPD]").val($(this).val());
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-iksasaranopd]").val($(this).val());
                });
            });
        });

        /*$('#browseIKSasaranOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKSasaranOPD"));
            $(this).removeData('bs.modal');

            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iksasaran-opd")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdIKSasaranOPD = $(this).val().split('|');
                    console.log(kdIKSasaranOPD);
                    $("[name=Kd_Pemda]").val(kdIKSasaranOPD[0]);
                    $("[name=Kd_Misi]").val(kdIKSasaranOPD[1]);
                    $("[name=Kd_Tujuan]").val(kdIKSasaranOPD[2]);
                    $("[name=Kd_IndikatorTujuan]").val(kdIKSasaranOPD[3]);
                    $("[name=Kd_Sasaran]").val(kdIKSasaranOPD[4]);
                    $("[name=Kd_IndikatorSasaran]").val(kdIKSasaranOPD[5]);
                    $("[name=Kd_TujuanOPD]").val(kdIKSasaranOPD[6]);
                    $("[name=Kd_IndikatorTujuanOPD]").val(kdIKSasaranOPD[7]);
                    $("[name=Kd_SasaranOPD]").val(kdIKSasaranOPD[8]);
                    $("[name=Kd_IndikatorSasaranOPD]").val(kdIKSasaranOPD[9]);
                    $("[name=Kd_Tahun_From]").val(kdIKSasaranOPD[10]);
                    $("[name=Kd_Tahun_To]").val(kdIKSasaranOPD[11]);

                    $("[name=Kd_Tahun]").val("");
                    $("[name=Kd_ProgramOPD]").val("");
                    $("[name=Nm_ProgramOPD]").val("");
                    $("[name=text-prog]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-iksasaranopd]").val($(this).val());
                });
            });
        });*/

        $('#browseProgram').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseProgram"));
            $(this).removeData('bs.modal');
            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();
            var kdPemda = $("[name=Kd_Pemda]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih Indikator Sasaran terlebih dahulu!</p>");
                return;
            }

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-prog")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub+"&Kd_Pemda="+kdPemda, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var prog = $(this).val().split("|");
                    $("[name=Kd_Tahun]").val(prog[0]);
                    $("[name=Kd_ProgramOPD]").val(prog[1]);
                    $("[name=Nm_ProgramOPD]").val(prog[2]);
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-prog]").val($(this).val());
                });
            });
        });

        $("[name=IsEplan]").change(function() {
            if($(this).is(":checked")) {
                $("input", $(".is-not-eplan")).attr("disabled", true);
                $(".is-not-eplan").hide();

                $("input", $(".is-eplan")).attr("disabled", false);
                $(".is-eplan").show();

                $("[name=Kd_Tahun]").attr("readonly", true);
            }
            else {
                $("input", $(".is-eplan")).attr("disabled", true);
                $(".is-eplan").hide();

                $("input", $(".is-not-eplan")).attr("disabled", false);
                $(".is-not-eplan").show();

                $("[name=Kd_Tahun]").attr("readonly", false);
            }
        }).trigger("change");

        /*$("[name=IsBulk]").change(function(e) {
            if($(this).is(":checked")) {
                var kdPemda = $("[name=Kd_Pemda]").val();
                if(!kdPemda) {
                    alert("Silakan pilih indikator sasaran OPD dahulu!");
                    $(this).attr("checked", false).change();
                    return false;
                }

                var tahunFrom = $("[name=Kd_Tahun_From]").val();
                var tahunTo = $("[name=Kd_Tahun_To]").val();
                $("#div_target").empty();
                $("#div_target_n1").empty();
                for(var i=parseInt(tahunFrom)+1; i<=tahunTo; i++) {
                    $("#div_target").append('<input type="text" class="form-control money" name="Target['+i+']" placeholder="Target '+i+'" style="text-align: right; margin-bottom: 10px" required>');
                    $("#div_target_n1").append('<input type="text" class="form-control money" name="Target_N1['+i+']" placeholder="Target N+1 '+i+'" style="text-align: right; margin-bottom: 10px" required>');
                }
            }
            else {
                $("#div_target").html('<input type="text" class="form-control money" name="Target" style="text-align: right" required>');
                $("#div_target_n1").html('<input type="text" class="form-control money" name="Target_N1" style="text-align: right" required>');
            }
            $(".money", $("#div_target")).number(true, 2, '.', ',');
            $(".money", $("#div_target_n1")).number(true, 2, '.', ',');
        }).trigger("change");*/

        $("[name=Nm_ProgramOPD]").autocomplete({
            serviceUrl: '<?=site_url('ajax/autocomplete-program-opd')?>',
            minChars: 3,
            params: {
                Kd_Pemda: $("[name=Kd_Pemda]").val(),
                Kd_Urusan: $("[name=Kd_Urusan]").val(),
                Kd_Bidang: $("[name=Kd_Bidang]").val(),
                Kd_Unit: $("[name=Kd_Unit]").val(),
                Kd_Sub: $("[name=Kd_Sub]").val()
            },
            onSelect: function(sel) {
                var dat = sel.data.split('|');
                //console.log(dat);
                $("[name=Kd_ProgramOPD]").val(dat[0]);
                $(this).val(dat[1]);
            }
        });

        /*$("[name=Kd_ProgramOPD]").autocomplete({
            serviceUrl: '<?=site_url('ajax/autocomplete-kdprogram-opd')?>',
            params: {
                Kd_Pemda: $("[name=Kd_Pemda]").val(),
                Kd_Urusan: $("[name=Kd_Urusan]").val(),
                Kd_Bidang: $("[name=Kd_Bidang]").val(),
                Kd_Unit: $("[name=Kd_Unit]").val(),
                Kd_Sub: $("[name=Kd_Sub]").val()
            }
        });*/

        $('#browseIKU').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKU"));
            var kdPemda = $("[name=Kd_Pemda]", $("#main-form")).val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }

            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iku-pemda")?>", {Kd_Pemda: kdPemda}, function () {
                var form = $("#main-form");
                $("select", modalBody).not('.no-select2').select2({ width: 'resolve'});
                $("#btn-select-iku").unbind('click').click(function() {
                    var kdMisi = $("[name=Kd_Misi] option:selected", modalBody);
                    var kdTujuan = $("[name=Kd_Tujuan] option:selected", modalBody);
                    var kdIkTujuan = $("[name=Kd_IndikatorTujuan] option:selected", modalBody);
                    var kdSasaran = $("[name=Kd_Sasaran] option:selected", modalBody);
                    var kdIkSasaran = $("[name=Kd_IndikatorSasaran] option:selected", modalBody);

                    if(!kdMisi || !kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                        $("[data-dismiss=modal]", $("#browseIKU")).click();
                        return true;
                    }

                    $("[name=Kd_Misi]", form).val(kdMisi.val()).trigger('change');
                    $(".text-misi", form).html(kdMisi.val()+'. '+kdMisi.html());
                    $("[name=Kd_Tujuan]", form).val(kdTujuan.val()).trigger('change');
                    $(".text-tujuan", form).html(kdMisi.val()+'.'+kdTujuan.val()+'. '+kdTujuan.html());
                    $("[name=Kd_IndikatorTujuan]", form).val(kdIkTujuan.val()).trigger('change');
                    $(".text-iktujuan", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'. '+kdIkTujuan.html());
                    $("[name=Kd_Sasaran]", form).val(kdSasaran.val()).trigger('change');
                    $(".text-sasaran", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'. '+kdSasaran.html());
                    $("[name=Kd_IndikatorSasaran]", form).val(kdIkSasaran.val()).trigger('change');
                    $(".text-iksasaran", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'.'+kdIkSasaran.val()+'. '+kdIkSasaran.html());
                    $("[data-dismiss=modal]", $("#browseIKU")).click();
                });
            });
        });

        $('#browseIKUOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKUOPD"));

            var kdPemda = $("[name=Kd_Pemda]", mainform).val();
            var kdUrusan = $("[name=Kd_Urusan]", mainform).val();
            var kdBidang = $("[name=Kd_Bidang]", mainform).val();
            var kdUnit = $("[name=Kd_Unit]", mainform).val();
            var kdSub = $("[name=Kd_Sub]", mainform).val();
            var kdMisi = $("[name=Kd_Misi]", mainform).val();
            var kdTujuan = $("[name=Kd_Tujuan]", mainform).val();
            var kdIkTujuan = $("[name=Kd_IndikatorTujuan]", mainform).val();
            var kdSasaran = $("[name=Kd_Sasaran]", mainform).val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]", mainform).val();

            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            if(!kdMisi || !kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan tentukan IKU Kabupaten terlebih dahulu!</p>");
                return;
            }

            var param = {
                Kd_Pemda: kdPemda,
                Kd_Urusan: kdUrusan,
                Kd_Bidang: kdBidang,
                Kd_Unit: kdUnit,
                Kd_Sub: kdSub,
                Kd_Misi: kdMisi,
                Kd_Tujuan: kdTujuan,
                Kd_IndikatorTujuan: kdIkTujuan,
                Kd_Sasaran: kdSasaran,
                Kd_IndikatorSasaran: kdIkSasaran
            };

            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iku-opd")?>", param, function () {
                var form = $("#main-form");
                $("select", modalBody).not('.no-select2').select2({ width: 'resolve'});
                $("#btn-select-iku-opd").unbind('click').click(function() {
                    var kdTujuan = $("[name=Kd_TujuanOPD] option:selected", modalBody);
                    var kdIkTujuan = $("[name=Kd_IndikatorTujuanOPD] option:selected", modalBody);
                    var kdSasaran = $("[name=Kd_SasaranOPD] option:selected", modalBody);
                    var kdIkSasaran = $("[name=Kd_IndikatorSasaranOPD] option:selected", modalBody);

                    if(!kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                        $("[data-dismiss=modal]", $("#browseIKUOPD")).click();
                        return true;
                    }
                    $("[name=Kd_TujuanOPD]", form).val(kdTujuan.val()).trigger('change');
                    $(".text-tujuan-opd", form).html(kdTujuan.val()+'. '+kdTujuan.html());
                    $("[name=Kd_IndikatorTujuanOPD]", form).val(kdIkTujuan.val()).trigger('change');
                    $(".text-iktujuan-opd", form).html(kdTujuan.val()+'.'+kdIkTujuan.val()+'. '+kdIkTujuan.html());
                    $("[name=Kd_SasaranOPD]", form).val(kdSasaran.val()).trigger('change');
                    $(".text-sasaran-opd", form).html(kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'. '+kdSasaran.html());
                    $("[name=Kd_IndikatorSasaranOPD]", form).val(kdIkSasaran.val()).trigger('change');
                    $(".text-iksasaran-opd", form).html(kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'.'+kdIkSasaran.val()+'. '+kdIkSasaran.html());
                    $("[data-dismiss=modal]", $("#browseIKUOPD")).click();
                });
            });
        });
        $("[name=Kd_Misi],[name=Kd_Tujuan],[name=Kd_IndikatorTujuan],[name=Kd_Sasaran],[name=Kd_IndikatorSasaran]", mainform).change(function() {
            $("[name=Kd_TujuanOPD]", mainform).val("").change();
            $(".text-tujuan-opd", mainform).html("");
            $("[name=Kd_IndikatorTujuanOPD]", mainform).val("").change();
            $(".text-iktujuan-opd", mainform).html("");
            $("[name=Kd_SasaranOPD]", mainform).val("").change();
            $(".text-sasaran-opd", mainform).html("");
            $("[name=Kd_IndikatorSasaranOPD]", mainform).val("").change();
            $(".text-iksasaran-opd", mainform).html("");
        });

        $("[name=Kd_TujuanOPD],[name=Kd_IndikatorTujuanOPD],[name=Kd_SasaranOPD],[name=Kd_IndikatorSasaranOPD]", mainform).change(function() {
            $("[name=Kd_ProgramOPD]", mainform).val("").change();
            $("[name=Kd_Tahun]", mainform).val("").change();
            $(".text-programopd", mainform).html("");
        });
    </script>
<?php $this->load->view('footer') ?>