<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 06/10/2018
 * Time: 00:02
 */
$this->load->view('header') ?>
<?php
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mbid/sasaran')?>">Sasaran Bidang OPD</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSub = "";
                                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                    if($edit) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                        $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                        $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                        $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }

                                    ?>
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[0]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[1]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[2]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[3]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=$edit?"":($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Sasaran OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-iksasaranopd" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].".".$data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].".".$data[COL_KD_SASARANOPD].".".$data[COL_KD_INDIKATORSASARANOPD]." ".$data[COL_NM_INDIKATORSASARANOPD] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= $edit ? $data[COL_KD_PEMDA] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARANOPD?>" value="<?= $edit ? $data[COL_KD_SASARANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARANOPD] : ""?>" required >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-iksasaranopd" data-toggle="modal" data-target="#browseIKSasaran" data-toggle="tooltip" data-placement="top" title="Pilih Indikator Sasaran OPD" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Bidang</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmBid = "";
                                    if($ruser[COL_ROLEID] == ROLEKABID) {
                                        $this->db->where(COL_KD_URUSAN, $strOPD[0]);
                                        $this->db->where(COL_KD_BIDANG, $strOPD[1]);
                                        $this->db->where(COL_KD_UNIT, $strOPD[2]);
                                        $this->db->where(COL_KD_SUB, $strOPD[3]);
                                        $this->db->where(COL_KD_BID, $strOPD[4]);
                                        $bid = $this->db->get(TBL_SAKIP_MBID)->row_array();
                                        if($bid) {
                                            $nmBid = $bid[COL_NM_BID];
                                        }
                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-bid" value="<?= $edit ? $data[COL_KD_BID].". ".$data[COL_NM_BID] : ($ruser[COL_ROLEID]==ROLEKABID?$strOPD[4].". ".$nmBid:"")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_BID?>" value="<?= $edit ? $data[COL_KD_BID] : ($ruser[COL_ROLEID]==ROLEKABID?$strOPD[4]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browseBid" data-toggle="tooltip" data-placement="top" title="Pilih Bidang" <?=$edit?"":($ruser[COL_ROLEID]==ROLEKABID?"disabled":"")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="control-label col-sm-2">Program</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    /*$nmProg = "";
                                    if($edit) {
                                        if($data[COL_ISEPLAN]) {
                                            $eplandb = $this->load->database("eplan", true);
                                            $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                            $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                            $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                            $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                            $eplandb->where("Tahun", $data[COL_KD_TAHUN]-1);
                                            $eplandb->where("Kd_Prog", $data[COL_KD_PROGRAMOPD]);
                                            $prog = $eplandb->get("ta_program")->row_array();
                                            if($prog) {
                                                $nmProg = $prog["Ket_Prog"];
                                            }
                                        } else {
                                            $nmProg = $data[COL_NM_PROGRAMOPD];
                                        }

                                    }*/
                                    ?>
                                    <input type="text" class="form-control" name="text-programopd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB].".".$data[COL_KD_PROGRAMOPD]." ".$nmProg : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= $edit ? $data[COL_KD_PEMDA] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARANOPD?>" value="<?= $edit ? $data[COL_KD_SASARANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_PROGRAMOPD?>" value="<?= $edit ? $data[COL_KD_PROGRAMOPD] : ""?>" required >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-programopd" data-toggle="modal" data-target="#browseProgramOPD" data-toggle="tooltip" data-placement="top" title="Pilih Program OPD" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <label class="control-label col-sm-2">Tahun</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?= $edit ? $data[COL_KD_TAHUN] : ""?>" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Kode Sasaran</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_SASARANPROGRAMOPD?>" value="<?= $edit ? $data[COL_KD_SASARANPROGRAMOPD] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Nama Sasaran</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?=COL_NM_SASARANPROGRAMOPD?>" value="<?= $edit ? $data[COL_NM_SASARANPROGRAMOPD] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Sasaran</label>
                            <div class="col-sm-8">
                                <table class="table table-bordered" id="tbl-det">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th width="80%">Deskripsi</th>
                                        <th style="width: 40px"><button type="button" id="btn-add-det" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="tr-blueprint-det" style="display: none">
                                        <td><input type="text" name="text-det-no" class="form-control" /></td>
                                        <td><input type="text" name="text-det-desc" class="form-control" /></td>
                                        <td>
                                            <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                    $det = $this->db
                                        ->where(COL_KD_PEMDA, ($edit?$data[COL_KD_PEMDA]:-999))
                                        ->where(COL_KD_MISI, ($edit?$data[COL_KD_MISI]:-999))
                                        ->where(COL_KD_TUJUAN, ($edit?$data[COL_KD_TUJUAN]:-999))
                                        ->where(COL_KD_INDIKATORTUJUAN, ($edit?$data[COL_KD_INDIKATORTUJUAN]:-999))
                                        ->where(COL_KD_SASARAN, ($edit?$data[COL_KD_SASARAN]:-999))
                                        ->where(COL_KD_INDIKATORSASARAN, ($edit?$data[COL_KD_INDIKATORSASARAN]:-999))
                                        ->where(COL_KD_TUJUANOPD, ($edit?$data[COL_KD_TUJUANOPD]:-999))
                                        ->where(COL_KD_INDIKATORTUJUANOPD, ($edit?$data[COL_KD_INDIKATORTUJUANOPD]:-999))
                                        ->where(COL_KD_SASARANOPD, ($edit?$data[COL_KD_SASARANOPD]:-999))
                                        ->where(COL_KD_INDIKATORSASARANOPD, ($edit?$data[COL_KD_INDIKATORSASARANOPD]:-999))
                                        ->where(COL_KD_TAHUN, ($edit?$data[COL_KD_TAHUN]:-999))
                                        ->where(COL_KD_URUSAN, ($edit?$data[COL_KD_URUSAN]:-999))
                                        ->where(COL_KD_BIDANG, ($edit?$data[COL_KD_BIDANG]:-999))
                                        ->where(COL_KD_UNIT, ($edit?$data[COL_KD_UNIT]:-999))
                                        ->where(COL_KD_SUB, ($edit?$data[COL_KD_SUB]:-999))
                                        ->where(COL_KD_BID, ($edit?$data[COL_KD_BID]:-999))
                                        ->where(COL_KD_SASARANPROGRAMOPD, ($edit?$data[COL_KD_SASARANPROGRAMOPD]:-999))
                                        ->order_by(COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                        ->get(TBL_SAKIP_MBID_INDIKATOR)->result_array();
                                    foreach($det as $m) {
                                        ?>
                                        <tr>
                                            <td><input type="text" name="text-det-no" class="form-control" value="<?=$m[COL_KD_INDIKATORPROGRAMOPD]?>" /></td>
                                            <td><input type="text" name="text-det-desc" class="form-control" value="<?=$m[COL_NM_INDIKATORPROGRAMOPD]?>" /></td>
                                            <td>
                                                <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>

                                    <?php
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('mbid/sasaran')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>

                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseBid" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseProgramOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKSasaran" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $(".btn-del-det").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-det").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-det").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-det").show();
            $(".btn-del-det", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });


        $("#main-form").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                var det = [];
                var rowDet = $("#tbl-det>tbody").find("tr:not(.tr-blueprint-det)");

                for (var i = 0; i < rowDet.length; i++) {
                    var noDet = $("[name=text-det-no]", $(rowDet[i])).val();
                    var ketDet = $("[name=text-det-desc]", $(rowDet[i])).val();
                    det.push({ No: noDet, Ket: ketDet });
                }
                $(".appended", $("#main-form")).remove();
                for (var i = 0; i < det.length; i++) {
                    var newEl = "<input type='hidden' class='appended' name=NoDet[" + i + "] value='" + det[i].No + "' /><input type='hidden' class='appended' name=KetDet[" + i + "] value='" + det[i].Ket + "' />";
                    $("#main-form").append(newEl);
                }

                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        //alert('Response Error');
                        $(".modal-body", $("#alertDialog")).html(a.responseText);
                        $("#alertDialog").modal('show');
                    }
                });
                return false;
            }
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browseOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseOPD"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdSub = $(this).val().split('|');
                    $("[name=Kd_Urusan]").val(kdSub[0]);
                    $("[name=Kd_Bidang]").val(kdSub[1]);
                    $("[name=Kd_Unit]").val(kdSub[2]);
                    $("[name=Kd_Sub]").val(kdSub[3]);

                    $("[name=Kd_Bid]").val("");
                    $("[name=text-bid]").val("");

                    $("[name=Kd_Pemda]").val("");
                    $("[name=Kd_Misi]").val("");
                    $("[name=Kd_Tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=Kd_Tahun]").val("");
                    $("[name=Kd_ProgramOPD]").val("");
                    $("[name=text-programopd]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-opd]").val($(this).val());
                });
            });
        });

        $('#browseBid').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseBid"));
            $(this).removeData('bs.modal');
            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-bid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Bid]").val($(this).val());

                    /*$("[name=Kd_Pemda]").val("");
                    $("[name=Kd_Misi]").val("");
                    $("[name=Kd_Tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=Kd_Tahun]").val("");
                    $("[name=Kd_ProgramOPD]").val("");
                    $("[name=text-programopd]").val("");*/
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-bid]").val($(this).val());
                });
            });
        });


        $('#browseProgramOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseProgramOPD"));
            $(this).removeData('bs.modal');

            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();
            var kdBid = $("[name=Kd_Bid]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub || !kdBid) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih Bidang OPD terlebih dahulu!</p>");
                return;
            }

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-program-opd")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub+"&Kd_Bid="+kdBid, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdIKSasaranOPD = $(this).val().split('|');
                    $("[name=Kd_Pemda]").val(kdIKSasaranOPD[0]);
                    $("[name=Kd_Misi]").val(kdIKSasaranOPD[1]);
                    $("[name=Kd_Tujuan]").val(kdIKSasaranOPD[2]);
                    $("[name=Kd_IndikatorTujuan]").val(kdIKSasaranOPD[3]);
                    $("[name=Kd_Sasaran]").val(kdIKSasaranOPD[4]);
                    $("[name=Kd_IndikatorSasaran]").val(kdIKSasaranOPD[5]);
                    $("[name=Kd_TujuanOPD]").val(kdIKSasaranOPD[6]);
                    $("[name=Kd_IndikatorTujuanOPD]").val(kdIKSasaranOPD[7]);
                    $("[name=Kd_SasaranOPD]").val(kdIKSasaranOPD[8]);
                    $("[name=Kd_IndikatorSasaranOPD]").val(kdIKSasaranOPD[9]);
                    $("[name=Kd_Tahun]").val(kdIKSasaranOPD[10]);
                    $("[name=Kd_ProgramOPD]").val(kdIKSasaranOPD[11]);
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-programopd]").val($(this).val());
                });
            });
        });

        $('#browseIKSasaran').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKSasaran"));
            $(this).removeData('bs.modal');

            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iksasaran-opd")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdIKSasaranOPD = $(this).val().split('|');
                    $("[name=Kd_Pemda]").val(kdIKSasaranOPD[0]);
                    $("[name=Kd_Misi]").val(kdIKSasaranOPD[1]);
                    $("[name=Kd_Tujuan]").val(kdIKSasaranOPD[2]);
                    $("[name=Kd_IndikatorTujuan]").val(kdIKSasaranOPD[3]);
                    $("[name=Kd_Sasaran]").val(kdIKSasaranOPD[4]);
                    $("[name=Kd_IndikatorSasaran]").val(kdIKSasaranOPD[5]);
                    $("[name=Kd_TujuanOPD]").val(kdIKSasaranOPD[6]);
                    $("[name=Kd_IndikatorTujuanOPD]").val(kdIKSasaranOPD[7]);
                    $("[name=Kd_SasaranOPD]").val(kdIKSasaranOPD[8]);
                    $("[name=Kd_IndikatorSasaranOPD]").val(kdIKSasaranOPD[9]);

                    $("[name=Kd_Tahun]").val("");
                    $("[name=Kd_ProgramOPD]").val("");
                    $("[name=Nm_ProgramOPD]").val("");
                    $("[name=text-prog]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-iksasaranopd]").val($(this).val());
                });
            });
        });
    </script>
<?php $this->load->view('footer') ?>