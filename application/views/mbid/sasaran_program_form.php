<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 06/10/2018
 * Time: 00:02
 */
$this->load->view('header') ?>
<?php
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mbid/sasaran-program')?>">Sasaran Program OPD</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSub = "";
                                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                    if($edit) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                        $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                        $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                        $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }

                                    ?>
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[0]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[1]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[2]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID?$strOPD[3]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=($ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-periode" value="<?= $edit ? $data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]." : ".$data[COL_NM_PEJABAT] : (!empty($data["DefPeriod"])?$data["DefPeriod"]:"")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= !empty($data[COL_KD_PEMDA])?$data[COL_KD_PEMDA]:"" ?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-period" data-toggle="modal" data-target="#browsePeriod" data-toggle="tooltip" data-placement="top" title="Pilih Periode" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">IKU Kabupaten</label>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#browseIKU" data-toggle="tooltip" data-placement="top" title="Pilih IKU">
                                    Pilih ...
                                </button>
                                <div class="div-iku no-padding" style="margin-top: 10px">
                                    <table class="table table-bordered" style="outline-style: dashed;">
                                        <tr>
                                            <td style="width: 150px" class="control-label">Misi</td>
                                            <td style="width: 10px">:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required >
                                                <span class="text-misi"><?= $edit ? $data[COL_KD_MISI].". ".$data[COL_NM_MISI] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Tujuan</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required >
                                                <span class="text-tujuan"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].". ".$data[COL_NM_TUJUAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Tujuan</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required >
                                                <span class="text-iktujuan"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].". ".$data[COL_NM_INDIKATORTUJUAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required >
                                                <span class="text-sasaran"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].". ".$data[COL_NM_SASARAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required >
                                                <span class="text-iksasaran"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].". ".$data[COL_NM_INDIKATORSASARAN] : ""?></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">IKU OPD</label>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#browseIKUOPD" data-toggle="tooltip" data-placement="top" title="Pilih IKU OPD">
                                    Pilih ...
                                </button>
                                <div class="div-iku no-padding" style="margin-top: 10px">
                                    <table class="table table-bordered" style="outline-style: dashed;">
                                        <tr>
                                            <td style="width: 150px" class="control-label">Tujuan</td>
                                            <td style="width: 10px">:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required >
                                                <span class="text-tujuan-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].". ".$data[COL_NM_TUJUANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Tujuan</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required >
                                                <span class="text-iktujuan-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].". ".$data[COL_NM_INDIKATORTUJUANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_SASARANOPD?>" value="<?= $edit ? $data[COL_KD_SASARANOPD] : ""?>" required >
                                                <span class="text-sasaran-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].".".$data[COL_KD_SASARANOPD].". ".$data[COL_NM_SASARANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORSASARANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARANOPD] : ""?>" required >
                                                <span class="text-iksasaran-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].".".$data[COL_KD_SASARANOPD].".".$data[COL_KD_INDIKATORSASARANOPD].". ".$data[COL_NM_INDIKATORSASARANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Bidang</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmBid = "";
                                    if($ruser[COL_ROLEID] == ROLEKABID) {
                                        $this->db->where(COL_KD_URUSAN, $strOPD[0]);
                                        $this->db->where(COL_KD_BIDANG, $strOPD[1]);
                                        $this->db->where(COL_KD_UNIT, $strOPD[2]);
                                        $this->db->where(COL_KD_SUB, $strOPD[3]);
                                        $this->db->where(COL_KD_BID, $strOPD[4]);
                                        $bid = $this->db->get(TBL_SAKIP_MBID)->row_array();
                                        if($bid) {
                                            $nmBid = $bid[COL_NM_BID];
                                        }
                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-bid" value="<?= $edit ? $data[COL_KD_BID].". ".$data[COL_NM_BID] : ($ruser[COL_ROLEID]==ROLEKABID?$strOPD[4].". ".$nmBid:"")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_BID?>" value="<?= $edit ? $data[COL_KD_BID] : ($ruser[COL_ROLEID]==ROLEKABID?$strOPD[4]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-bid" data-toggle="modal" data-target="#browseBid" data-toggle="tooltip" data-placement="top" title="Pilih Bidang" <?=$edit?"":($ruser[COL_ROLEID]==ROLEKABID?"disabled":"")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Program</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmProg = "";
                                    if($edit) {
                                        $nmProg = $data[COL_NM_PROGRAMOPD];
                                        /*if($data[COL_ISEPLAN]) {
                                            $eplandb = $this->load->database("eplan", true);
                                            $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                            $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                            $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                            $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                            $eplandb->where("Tahun", $data[COL_KD_TAHUN]-1);
                                            $eplandb->where("Kd_Prog", $data[COL_KD_PROGRAMOPD]);
                                            $prog = $eplandb->get("ta_program")->row_array();
                                            if($prog) {
                                                $nmProg = $prog["Ket_Prog"];
                                            }
                                        } else {
                                            $nmProg = $data[COL_NM_PROGRAMOPD];
                                        }*/

                                    }
                                    ?>
                                    <input type="text" class="form-control" name="text-programopd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB].".".$data[COL_KD_PROGRAMOPD]." ".$nmProg : ""?>" readonly>
                                    <!--<input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= $edit ? $data[COL_KD_PEMDA] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TAHUN_FROM?>" value="<?= $edit ? $data[COL_KD_TAHUN_FROM] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TAHUN_TO?>" value="<?= $edit ? $data[COL_KD_TAHUN_TO] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARANOPD?>" value="<?= $edit ? $data[COL_KD_SASARANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARANOPD] : ""?>" required >-->
                                    <input type="hidden" name="<?=COL_KD_PROGRAMOPD?>" value="<?= $edit ? $data[COL_KD_PROGRAMOPD] : ""?>" required >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-programopd" data-toggle="modal" data-target="#browseProgramOPD" data-toggle="tooltip" data-placement="top" title="Pilih Program OPD" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Tahun</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?= $edit ? $data[COL_KD_TAHUN] : ""?>" readonly required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Kode Sasaran</label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-existing-sasaran-prg" data-toggle="modal" data-target="#browseExistingSasaranProgramOPD" data-toggle="tooltip" data-placement="top" title="Pilih Sasaran Program Eksisting" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                    <input type="number" class="form-control" name="<?=COL_KD_SASARANPROGRAMOPD?>" value="<?= $edit ? $data[COL_KD_SASARANPROGRAMOPD] : ""?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Nama Sasaran</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?=COL_NM_SASARANPROGRAMOPD?>" value="<?= $edit ? $data[COL_NM_SASARANPROGRAMOPD] : ""?>" required>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="control-label col-sm-2">Satuan</label>
                            <div class="col-sm-2">
                                <select name="<?=COL_KD_SATUAN?>" class="form-control">
                                    <?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_KD_SATUAN, COL_NM_SATUAN, (!empty($data[COL_KD_SATUAN]) ? $data[COL_KD_SATUAN] : null))?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group div-awal">
                            <label class="control-label col-sm-2">Kondisi Awal</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control money" name="<?=COL_AWAL?>" value="<?= $edit ? $data[COL_AWAL] : ""?>" style="text-align: right" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Target</label>
                            <div class="col-sm-2" id="div_target">
                                <input type="text" class="form-control money" name="<?=COL_TARGET?>" value="<?= $edit ? $data[COL_TARGET] : ""?>" style="text-align: right" required>
                            </div>
                        </div>

                        <div class="form-group div-akhir">
                            <label class="control-label col-sm-2">Kondisi Akhir</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control money" name="<?=COL_AKHIR?>" value="<?= $edit ? $data[COL_AKHIR] : ""?>" style="text-align: right" required>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="box box-default">
                    <div class="box-header">
                        <h4 class="box-title">Indikator Sasaran</h4>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="tbl-det">
                            <thead>
                            <tr>
                                <th style="width: 60px">No.</th>
                                <th style="min-width: 240px">Deskripsi & Target</th>
                                <th>Formulasi</th>
                                <th>Sumber Data</th>
                                <!--<th>Penanggung Jawab</th>-->
                                <th style="width: 40px"><button type="button" id="btn-add-det" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr-blueprint-det" style="display: none">
                                <td><input type="text" name="text-det-no" class="form-control" placeholder="No" style="width: 50px" /></td>
                                <td>
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-sm-12">
                                            <input name="text-det-desc" class="form-control" placeholder="Deskripsi" />
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px">
                                        <div class="col-sm-6">
                                            <select name="select-det-satuan" class="form-control no-select2" style="width: 100%">
                                                <?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_KD_SATUAN, COL_NM_SATUAN)?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="text-det-target" placeholder="Target" class="form-control text-right money" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-offset-6 col-sm-6 div-awal">
                                            <input type="text" name="text-det-awal" placeholder="Kondisi Awal" class="form-control text-right money" />
                                        </div>
                                        <!--<div class="col-sm-offset-6 col-sm-6 div-akhir">
                                            <input type="text" name="text-det-akhir" placeholder="Kondisi Akhir" class="form-control text-right money" />
                                        </div>-->
                                    </div>
                                </td>
                                <td><textarea name="text-det-formula" placeholder="Formulasi Perhitungan" class="form-control" rows="4"></textarea></td>
                                <td><textarea name="text-det-sumber" placeholder="Sumber Data" class="form-control" rows="4"></textarea></td>
                                <!--<td><textarea name="text-det-pic" class="form-control" rows="4"></textarea></td>-->
                                <td>
                                    <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                </td>
                            </tr>
                            <?php
                            $det = $this->db
                                ->where(COL_KD_PEMDA, ($edit?$data[COL_KD_PEMDA]:-999))
                                ->where(COL_KD_MISI, ($edit?$data[COL_KD_MISI]:-999))
                                ->where(COL_KD_TUJUAN, ($edit?$data[COL_KD_TUJUAN]:-999))
                                ->where(COL_KD_INDIKATORTUJUAN, ($edit?$data[COL_KD_INDIKATORTUJUAN]:-999))
                                ->where(COL_KD_SASARAN, ($edit?$data[COL_KD_SASARAN]:-999))
                                ->where(COL_KD_INDIKATORSASARAN, ($edit?$data[COL_KD_INDIKATORSASARAN]:-999))
                                ->where(COL_KD_TUJUANOPD, ($edit?$data[COL_KD_TUJUANOPD]:-999))
                                ->where(COL_KD_INDIKATORTUJUANOPD, ($edit?$data[COL_KD_INDIKATORTUJUANOPD]:-999))
                                ->where(COL_KD_SASARANOPD, ($edit?$data[COL_KD_SASARANOPD]:-999))
                                ->where(COL_KD_INDIKATORSASARANOPD, ($edit?$data[COL_KD_INDIKATORSASARANOPD]:-999))
                                ->where(COL_KD_TAHUN, ($edit?$data[COL_KD_TAHUN]:-999))
                                ->where(COL_KD_URUSAN, ($edit?$data[COL_KD_URUSAN]:-999))
                                ->where(COL_KD_BIDANG, ($edit?$data[COL_KD_BIDANG]:-999))
                                ->where(COL_KD_UNIT, ($edit?$data[COL_KD_UNIT]:-999))
                                ->where(COL_KD_SUB, ($edit?$data[COL_KD_SUB]:-999))
                                ->where(COL_KD_BID, ($edit?$data[COL_KD_BID]:-999))
                                ->where(COL_KD_PROGRAMOPD, ($edit?$data[COL_KD_PROGRAMOPD]:-999))
                                ->where(COL_KD_SASARANPROGRAMOPD, ($edit?$data[COL_KD_SASARANPROGRAMOPD]:-999))
                                ->order_by(COL_KD_INDIKATORPROGRAMOPD, 'asc')
                                ->get(TBL_SAKIP_MBID_PROGRAM_INDIKATOR)->result_array();
                            foreach($det as $m) {
                                ?>
                                <tr>
                                    <td><input type="text" name="text-det-no" class="form-control" value="<?=$m[COL_KD_INDIKATORPROGRAMOPD]?>" style="width: 50px" /></td>
                                    <td>
                                        <div class="row" style="margin-bottom: 10px">
                                            <div class="col-sm-12">
                                                <input name="text-det-desc" class="form-control" placeholder="Deskripsi" value="<?=$m[COL_NM_INDIKATORPROGRAMOPD]?>" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 10px">
                                            <div class="col-sm-6">
                                                <select name="select-det-satuan" class="form-control" style="width: 100%">
                                                    <?=GetCombobox("SELECT * FROM ".TBL_SAKIP_MSATUAN." ORDER BY ".COL_NM_SATUAN, COL_KD_SATUAN, COL_NM_SATUAN, $m[COL_KD_SATUAN])?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" name="text-det-target" placeholder="Target" class="form-control text-right money" value="<?=$m[COL_TARGET]?>" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-offset-6 col-sm-6 div-awal">
                                                <input type="text" name="text-det-awal" placeholder="Kondisi Awal" class="form-control text-right money" value="<?=$m[COL_AWAL]?>" />
                                            </div>
                                            <!--<div class="col-sm-offset-6 col-sm-6 div-akhir">
                                                <input type="text" name="text-det-akhir" placeholder="Kondisi Akhir" class="form-control text-right money" value="<?=$m[COL_AKHIR]?>" />
                                            </div>-->
                                        </div>
                                    </td>
                                    <td><textarea name="text-det-formula" placeholder="Formulasi Perhitungan" class="form-control" rows="4"><?=$m[COL_NM_FORMULA]?></textarea></td>
                                    <td><textarea name="text-det-sumber" placeholder="Sumber Data" class="form-control" rows="4"><?=$m[COL_NM_SUMBERDATA]?></textarea></td>
                                    <!--<td><textarea name="text-det-pic" class="form-control" rows="4"><?=$m[COL_NM_PENANGGUNGJAWAB]?></textarea></td>-->
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>

                            <?php
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                        <a href="<?=site_url('mbid/sasaran_program')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseBid" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseProgramOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKSasaran" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browsePeriod" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKU" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse IKU</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="btn btn-primary btn-flat" id="btn-select-iku">Pilih</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKUOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse IKU</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="btn btn-primary btn-flat" id="btn-select-iku-opd">Pilih</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseExistingSasaranProgramOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        var mainform = $("#main-form");
        $(".btn-del-det").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-det").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-det").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-det").show();
            $('select', blueprint).select2({ width: 'resolve', placeholder: "Satuan", allowClear: true });
            $('select', blueprint).removeClass('.no-select2');
            $(".money", blueprint).number(true, 2, '.', ',');
            $(".btn-del-det", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });


        $("#main-form").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                var det = [];
                var rowDet = $("#tbl-det>tbody").find("tr:not(.tr-blueprint-det)");

                for (var i = 0; i < rowDet.length; i++) {
                    var noDet = $("[name=text-det-no]", $(rowDet[i])).val();
                    var ketDet = $("[name=text-det-desc]", $(rowDet[i])).val();
                    var satuanDet = $("[name=select-det-satuan]", $(rowDet[i])).val();
                    var targetDet = $("[name=text-det-target]", $(rowDet[i])).val();
                    var awalDet = $("[name=text-det-awal]", $(rowDet[i])).val();
                    var akhirDet = $("[name=text-det-akhir]", $(rowDet[i])).val();
                    var formtDet = $("[name=text-det-formula]", $(rowDet[i])).val();
                    var sourceDet = $("[name=text-det-sumber]", $(rowDet[i])).val();
                    var picDet = $("[name=text-det-pic]", $(rowDet[i])).val();
                    det.push({
                        No: noDet,
                        Ket: ketDet,
                        Formula: formtDet,
                        Sumber: sourceDet,
                        PIC: picDet,
                        Kd_Satuan: satuanDet,
                        Target: targetDet,
                        Awal: awalDet,
                        Akhir: akhirDet
                    });
                }
                $(".appended", $("#main-form")).remove();
                for (var i = 0; i < det.length; i++) {
                    var newEl = "";
                    newEl += "<input type='hidden' class='appended' name=NoDet[" + i + "] value='" + det[i].No + "' />";
                    newEl += "<input type='hidden' class='appended' name=KetDet[" + i + "] value='" + det[i].Ket + "' />";
                    newEl += "<input type='hidden' class='appended' name=SatuanDet[" + i + "] value='" + det[i].Kd_Satuan + "' />";
                    newEl += "<input type='hidden' class='appended' name=TargetDet[" + i + "] value='" + det[i].Target + "' />";
                    newEl += "<input type='hidden' class='appended' name=AwalDet[" + i + "] value='" + det[i].Awal + "' />";
                    newEl += "<input type='hidden' class='appended' name=AkhirDet[" + i + "] value='" + det[i].Akhir + "' />";
                    newEl += "<input type='hidden' class='appended' name=FormulaDet[" + i + "] value='" + det[i].Formula + "' />";
                    newEl += "<input type='hidden' class='appended' name=SumberDet[" + i + "] value='" + det[i].Sumber + "' />";
                    //newEl += "<input type='hidden' class='appended' name=PICDet[" + i + "] value='" + det[i].PIC + "' />";
                    $("#main-form").append(newEl);
                }

                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        //alert('Response Error');
                        $(".modal-body", $("#alertDialog")).html(a.responseText);
                        $("#alertDialog").modal('show');
                    }
                });
                return false;
            }
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browseOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseOPD"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdSub = $(this).val().split('|');
                    $("[name=Kd_Urusan]").val(kdSub[0]);
                    $("[name=Kd_Bidang]").val(kdSub[1]);
                    $("[name=Kd_Unit]").val(kdSub[2]);
                    $("[name=Kd_Sub]").val(kdSub[3]);

                    $("[name=Kd_Bid]").val("");
                    $("[name=text-bid]").val("");

                    /*$("[name=Kd_Pemda]").val("");
                    $("[name=Kd_Misi]").val("");
                    $("[name=Kd_Tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");*/
                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=Kd_Tahun_From]").val("");
                    $("[name=Kd_Tahun_To]").val("");
                    $("[name=Kd_Tahun]").val("").change();

                    $("[name=Kd_ProgramOPD]").val("");
                    $("[name=text-programopd]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-opd]").val($(this).val());
                });
            });
        });

        $('#browseBid').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseBid"));
            $(this).removeData('bs.modal');
            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-bid")?>"+"?Kd_Urusan="+kdUrusan+"&Kd_Bidang="+kdBidang+"&Kd_Unit="+kdUnit+"&Kd_Sub="+kdSub, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Bid]").val($(this).val());

                    /*$("[name=Kd_Pemda]").val("");
                     $("[name=Kd_Misi]").val("");
                     $("[name=Kd_Tujuan]").val("");
                     $("[name=Kd_IndikatorTujuan]").val("");
                     $("[name=Kd_Sasaran]").val("");
                     $("[name=Kd_IndikatorSasaran]").val("");
                     $("[name=Kd_TujuanOPD]").val("");
                     $("[name=Kd_IndikatorTujuanOPD]").val("");
                     $("[name=Kd_SasaranOPD]").val("");
                     $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=Kd_Tahun_From]").val("");
                    $("[name=Kd_Tahun_To]").val("");
                     $("[name=Kd_Tahun]").val("").change();*/

                     $("[name=Kd_ProgramOPD]").val("");
                     $("[name=text-programopd]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-bid]").val($(this).val());
                });
            });
        });


        $('#browseProgramOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseProgramOPD"));
            $(this).removeData('bs.modal');

            var kdUrusan = $("[name=Kd_Urusan]").val();
            var kdBidang = $("[name=Kd_Bidang]").val();
            var kdUnit = $("[name=Kd_Unit]").val();
            var kdSub = $("[name=Kd_Sub]").val();
            var kdBid = $("[name=Kd_Bid]").val();

            var kdPemda = $("[name=Kd_Pemda]", mainform).val();
            var kdMisi = $("[name=Kd_Misi]", mainform).val();
            var kdTujuan = $("[name=Kd_Tujuan]", mainform).val();
            var kdIkTujuan = $("[name=Kd_IndikatorTujuan]", mainform).val();
            var kdSasaran = $("[name=Kd_Sasaran]", mainform).val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]", mainform).val();

            var kdTujuanOPD = $("[name=Kd_TujuanOPD]", mainform).val();
            var kdIkTujuanOPD = $("[name=Kd_IndikatorTujuanOPD]", mainform).val();
            var kdSasaranOPD = $("[name=Kd_SasaranOPD]", mainform).val();
            var kdIkSasaranOPD = $("[name=Kd_IndikatorSasaranOPD]", mainform).val();

            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub || !kdBid) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih Bidang OPD terlebih dahulu!</p>");
                return;
            }

            if(!kdMisi || !kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan tentukan IKU Kabupaten terlebih dahulu!</p>");
                return;
            }

            if(!kdTujuanOPD || !kdIkTujuanOPD || !kdSasaranOPD || !kdIkSasaranOPD) {
                modalBody.html("<p style='font-style: italic'>Silakan tentukan IKU OPD terlebih dahulu!</p>");
                return;
            }

            var param = {
                Kd_Pemda: kdPemda,
                Kd_Urusan: kdUrusan,
                Kd_Bidang: kdBidang,
                Kd_Unit: kdUnit,
                Kd_Sub: kdSub,
                Kd_Bid: kdBid,
                Kd_Misi: kdMisi,
                Kd_Tujuan: kdTujuan,
                Kd_IndikatorTujuan: kdIkTujuan,
                Kd_Sasaran: kdSasaran,
                Kd_IndikatorSasaran: kdIkSasaran,
                Kd_TujuanOPD: kdTujuanOPD,
                Kd_IndikatorTujuanOPD: kdIkTujuanOPD,
                Kd_SasaranOPD: kdSasaranOPD,
                Kd_IndikatorSasaranOPD: kdIkSasaranOPD
            };

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-program-opd-2")?>", param, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdIKSasaranOPD = $(this).val().split('|');
                    /*$("[name=Kd_Pemda]").val(kdIKSasaranOPD[0]);
                    $("[name=Kd_Misi]").val(kdIKSasaranOPD[1]);
                    $("[name=Kd_Tujuan]").val(kdIKSasaranOPD[2]);
                    $("[name=Kd_IndikatorTujuan]").val(kdIKSasaranOPD[3]);
                    $("[name=Kd_Sasaran]").val(kdIKSasaranOPD[4]);
                    $("[name=Kd_IndikatorSasaran]").val(kdIKSasaranOPD[5]);
                    $("[name=Kd_TujuanOPD]").val(kdIKSasaranOPD[6]);
                    $("[name=Kd_IndikatorTujuanOPD]").val(kdIKSasaranOPD[7]);
                    $("[name=Kd_SasaranOPD]").val(kdIKSasaranOPD[8]);
                    $("[name=Kd_IndikatorSasaranOPD]").val(kdIKSasaranOPD[9]);
                    $("[name=Kd_Tahun_From]").val(kdIKSasaranOPD[12]);
                    $("[name=Kd_Tahun_To]").val(kdIKSasaranOPD[13]);*/
                    $("[name=Kd_Tahun]").val(kdIKSasaranOPD[10]).change();
                    $("[name=Kd_ProgramOPD]").val(kdIKSasaranOPD[11]);
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-programopd]").val($(this).val());
                });
            });
        });

        $("[name=Kd_Tahun]").change(function() {
            var thn = parseInt($(this).val());
            var thn_fr = parseInt($("[name=Kd_Tahun_From]").val());
            var thn_to = parseInt($("[name=Kd_Tahun_To]").val());

            if(thn == thn_fr + 1) {
                $(".div-awal").show();
                $("input", $(".div-awal")).attr("disabled", false);
                $(".div-akhir").hide();
                $("input", $(".div-akhir")).attr("disabled", true);
            }
            else if(thn == thn_to) {
                $(".div-akhir").show();
                $("input", $(".div-akhir")).attr("disabled", false);
                $(".div-awal").hide();
                $("input", $(".div-awal")).attr("disabled", true);
            }
            else {
                $(".div-awal").hide();
                $("input", $(".div-awal")).attr("disabled", true);
                $(".div-akhir").hide();
                $("input", $(".div-akhir")).attr("disabled", true);
            }
        }).trigger("change");

        $('#browsePeriod').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browsePeriod"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-pemda")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Pemda]").val($(this).val());

                    $("[name=Kd_Misi]").val("").trigger("change");
                    $(".text-misi").html("");
                    $("[name=Kd_Tujuan]").val("").trigger("change");
                    $(".text-tujuan").html("");
                    $("[name=Kd_IndikatorTujuan]").val("").trigger("change");
                    $(".text-iktujuan").html("");
                    $("[name=Kd_Sasaran]").val("").trigger("change");
                    $(".text-sasaran").html("");
                    $("[name=Kd_IndikatorSasaran]").val("").trigger("change");
                    $(".text-iksasaran").html("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-periode]").val($(this).val());
                });
            });
        });
        $('#browseIKU').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKU"));
            var kdPemda = $("[name=Kd_Pemda]", $("#main-form")).val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }

            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iku-pemda")?>", {Kd_Pemda: kdPemda}, function () {
                var form = $("#main-form");
                $("select", modalBody).not('.no-select2').select2({ width: 'resolve'});
                $("#btn-select-iku").unbind('click').click(function() {
                    var kdMisi = $("[name=Kd_Misi] option:selected", modalBody);
                    var kdTujuan = $("[name=Kd_Tujuan] option:selected", modalBody);
                    var kdIkTujuan = $("[name=Kd_IndikatorTujuan] option:selected", modalBody);
                    var kdSasaran = $("[name=Kd_Sasaran] option:selected", modalBody);
                    var kdIkSasaran = $("[name=Kd_IndikatorSasaran] option:selected", modalBody);

                    if(!kdMisi || !kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                        $("[data-dismiss=modal]", $("#browseIKU")).click();
                        return true;
                    }

                    $("[name=Kd_Misi]", form).val(kdMisi.val()).trigger('change');
                    $(".text-misi", form).html(kdMisi.val()+'. '+kdMisi.html());
                    $("[name=Kd_Tujuan]", form).val(kdTujuan.val()).trigger('change');
                    $(".text-tujuan", form).html(kdMisi.val()+'.'+kdTujuan.val()+'. '+kdTujuan.html());
                    $("[name=Kd_IndikatorTujuan]", form).val(kdIkTujuan.val()).trigger('change');
                    $(".text-iktujuan", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'. '+kdIkTujuan.html());
                    $("[name=Kd_Sasaran]", form).val(kdSasaran.val()).trigger('change');
                    $(".text-sasaran", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'. '+kdSasaran.html());
                    $("[name=Kd_IndikatorSasaran]", form).val(kdIkSasaran.val()).trigger('change');
                    $(".text-iksasaran", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'.'+kdIkSasaran.val()+'. '+kdIkSasaran.html());
                    $("[data-dismiss=modal]", $("#browseIKU")).click();
                });
            });
        });

        $('#browseIKUOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKUOPD"));

            var kdPemda = $("[name=Kd_Pemda]", mainform).val();
            var kdUrusan = $("[name=Kd_Urusan]", mainform).val();
            var kdBidang = $("[name=Kd_Bidang]", mainform).val();
            var kdUnit = $("[name=Kd_Unit]", mainform).val();
            var kdSub = $("[name=Kd_Sub]", mainform).val();
            var kdMisi = $("[name=Kd_Misi]", mainform).val();
            var kdTujuan = $("[name=Kd_Tujuan]", mainform).val();
            var kdIkTujuan = $("[name=Kd_IndikatorTujuan]", mainform).val();
            var kdSasaran = $("[name=Kd_Sasaran]", mainform).val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]", mainform).val();

            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }

            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }

            if(!kdMisi || !kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                modalBody.html("<p style='font-style: italic'>Silakan tentukan IKU Kabupaten terlebih dahulu!</p>");
                return;
            }

            var param = {
                Kd_Pemda: kdPemda,
                Kd_Urusan: kdUrusan,
                Kd_Bidang: kdBidang,
                Kd_Unit: kdUnit,
                Kd_Sub: kdSub,
                Kd_Misi: kdMisi,
                Kd_Tujuan: kdTujuan,
                Kd_IndikatorTujuan: kdIkTujuan,
                Kd_Sasaran: kdSasaran,
                Kd_IndikatorSasaran: kdIkSasaran
            };

            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iku-opd")?>", param, function () {
                var form = $("#main-form");
                $("select", modalBody).not('.no-select2').select2({ width: 'resolve'});
                $("#btn-select-iku-opd").unbind('click').click(function() {
                    var kdTujuan = $("[name=Kd_TujuanOPD] option:selected", modalBody);
                    var kdIkTujuan = $("[name=Kd_IndikatorTujuanOPD] option:selected", modalBody);
                    var kdSasaran = $("[name=Kd_SasaranOPD] option:selected", modalBody);
                    var kdIkSasaran = $("[name=Kd_IndikatorSasaranOPD] option:selected", modalBody);

                    if(!kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                        $("[data-dismiss=modal]", $("#browseIKUOPD")).click();
                        return true;
                    }
                    $("[name=Kd_TujuanOPD]", form).val(kdTujuan.val()).trigger('change');
                    $(".text-tujuan-opd", form).html(kdTujuan.val()+'. '+kdTujuan.html());
                    $("[name=Kd_IndikatorTujuanOPD]", form).val(kdIkTujuan.val()).trigger('change');
                    $(".text-iktujuan-opd", form).html(kdTujuan.val()+'.'+kdIkTujuan.val()+'. '+kdIkTujuan.html());
                    $("[name=Kd_SasaranOPD]", form).val(kdSasaran.val()).trigger('change');
                    $(".text-sasaran-opd", form).html(kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'. '+kdSasaran.html());
                    $("[name=Kd_IndikatorSasaranOPD]", form).val(kdIkSasaran.val()).trigger('change');
                    $(".text-iksasaran-opd", form).html(kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'.'+kdIkSasaran.val()+'. '+kdIkSasaran.html());
                    $("[data-dismiss=modal]", $("#browseIKUOPD")).click();
                });
            });
        });
        $("[name=Kd_Misi],[name=Kd_Tujuan],[name=Kd_IndikatorTujuan],[name=Kd_Sasaran],[name=Kd_IndikatorSasaran]", mainform).change(function() {
            $("[name=Kd_TujuanOPD]", mainform).val("").trigger("change");
            $(".text-tujuan-opd", mainform).html("");
            $("[name=Kd_IndikatorTujuanOPD]", mainform).val("").trigger("change");
            $(".text-iktujuan-opd", mainform).html("");
            $("[name=Kd_SasaranOPD]", mainform).val("").trigger("change");
            $(".text-sasaran-opd", mainform).html("");
            $("[name=Kd_IndikatorSasaranOPD]", mainform).val("").trigger("change");
            $(".text-iksasaran-opd", mainform).html("");
        });
        $("[name=Kd_TujuanOPD],[name=Kd_IndikatorTujuanOPD],[name=Kd_SasaranOPD],[name=Kd_IndikatorSasaranOPD]", mainform).change(function() {
            $("[name=Kd_ProgramOPD]", mainform).val("");
            $("[name=Kd_Tahun]", mainform).val("");
            $("[name=text-programopd]", mainform).val("");

        });
        $('#browseExistingSasaranProgramOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseExistingSasaranProgramOPD"));
            $(this).removeData('bs.modal');

            var param = {
                Kd_Pemda: $("[name=Kd_Pemda]", mainform).val(),
                Kd_Urusan: $("[name=Kd_Urusan]", mainform).val(),
                Kd_Bidang: $("[name=Kd_Bidang]", mainform).val(),
                Kd_Unit: $("[name=Kd_Unit]", mainform).val(),
                Kd_Sub: $("[name=Kd_Sub]", mainform).val(),
                Kd_Bid: $("[name=Kd_Bid]", mainform).val(),
                Kd_ProgramOPD: $("[name=Kd_ProgramOPD]", mainform).val(),
                Kd_Tahun: $("[name=Kd_Tahun]", mainform).val()
            };

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-existing-sasaran-program-opd")?>", param, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_SasaranProgramOPD]").val($(this).val());

                    $.extend( param, {Kd_SasaranProgramOPD: $("[name=Kd_SasaranProgramOPD]").val()} );
                    $.post("<?=site_url("ajax/get-indikator-by-sasaran-program-opd")?>", param, function(data) {
                        //console.log(data);
                        var tbldet = $("tbody", $("#tbl-det"));
                        tbldet.append(data);
                        $('select', tbldet).select2({ width: 'resolve', placeholder: "Satuan", allowClear: true });
                        $('select', tbldet).removeClass('.no-select2');
                        $(".money", tbldet).number(true, 2, '.', ',');
                        $(".btn-del-det", tbldet).click(function () {
                            var row = $(this).closest("tr");
                            row.remove();
                        });
                        $("[name=Kd_Tahun]").trigger("change");
                    });
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Nm_SasaranProgramOPD]").val($(this).val());
                });
            });
        });
    </script>
<?php $this->load->view('footer') ?>