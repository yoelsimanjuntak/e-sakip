<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 22/08/2018
 * Time: 00:31
 */
?>
<style>
    table.with-border {
        border-collapse: collapse;
    }

    table.with-border, th.with-border, td.with-border {
        border: 1px solid black;
    }
</style>
<?php
$n = 0;
foreach($res as $dat) {
    $n++;
    ?>
    <div>
        <div style="text-align: center">
            <p style="font-weight: bold; font-size: 14px; margin: 0px">DATA KELUARGA</p>
            <p style="font-style: italic; margin: 0px">(Diisi oleh Kader, sumber Kepala Rumah Tangga)</p>
        </div>

        <div style="margin-top: 10px">
            <table>
                <tr>
                    <td>Dasawisma</td><td>:</td><td><?=$dat[COL_NMKELOMPOK]?></td>
                    <td style="padding-left: 40px">Kecamatan</td><td>:</td><td><?=$dat[COL_NMKECAMATAN]?></td>
                </tr>
                <tr>
                    <td>Dusun / Lingk.</td><td>:</td><td><?=$dat[COL_NMDUSUN]?></td>
                    <td style="padding-left: 40px">Kabupaten</td><td>:</td><td><?=$dat[COL_NMKABUPATEN]?></td>
                </tr>
                <tr>
                    <td>Desa / Kel.</td><td>:</td><td><?=$dat[COL_NMKELURAHAN]?></td>
                    <td style="padding-left: 40px">Provinsi</td><td>:</td><td><?=$dat[COL_NMPROVINSI]?></td>
                </tr>
            </table>
        </div>
        <hr />
        <div style="margin-top: 10px">
            <table>
                <tr>
                    <td>Nama Kepala Rumah Tangga</td><td>:</td><td><?=$dat[COL_NMKEPALAKELUARGA]?></td>
                </tr>
                <tr>
                    <td valign="top">Jlh. Anggota Keluarga</td><td valign="top" width="10px">:</td>
                    <td>
                        <table>
                            <tr>
                                <td></td>
                                <td></td>
                                <td style="text-align: right"><b><?=$dat["COUNT_ANGGOTA"]?></b> orang</td>
                            </tr>
                            <tr>
                                <td>Laki-Laki</td>
                                <td>:</td>
                                <td style="text-align: right"><b><?=$dat["COUNT_LK"]?></b> orang</td>
                            </tr>
                            <tr>
                                <td>Perempuan</td>
                                <td>:</td>
                                <td style="text-align: right"><b><?=$dat["COUNT_PR"]?></b> orang</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 10px">
            <table>
                <tr>
                    <td valign="top">1.</td>
                    <td valign="top" style="table-layout: auto">Jumlah KK</td><td valign="top" width="10px">:</td>
                    <td valign="top" colspan="2"><b><?=$dat["COUNT_KK"]?></b> KK</td>
                </tr>
                <tr>
                    <?php
                    $countBalita = $this->db
                        ->where(COL_KDKELUARGA, $dat[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 0 and TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) <= 4")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->count_all_results(TBL_MWARGA);
                    $countLansia = $this->db
                        ->where(COL_KDKELUARGA, $dat[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 64")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->count_all_results(TBL_MWARGA);
                    $countPUS = $this->db
                        ->where(COL_KDKELUARGA, $dat[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 20 and TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) <= 45")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->where(COL_STATUSKELUARGA, "Suami")
                        ->count_all_results(TBL_MWARGA);
                    $countWUS = $this->db
                        ->where(COL_KDKELUARGA, $dat[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 20 and TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) <= 45")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->where(COL_JENISKELAMIN, "Perempuan")
                        ->count_all_results(TBL_MWARGA);
                    $countDifabel = $this->db
                        ->where(COL_KDKELUARGA, $dat[COL_KDKELUARGA])
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->where(COL_ISDIFABEL, true)
                        ->count_all_results(TBL_MWARGA);
                    ?>
                    <td valign="top">2.</td>
                    <td valign="top" style="table-layout: auto">Jumlah</td><td valign="top" width="10px">:</td>
                    <td valign="top" colspan="2">
                        <table>
                            <tr>
                                <td>a)</td>
                                <td>Balita</td>
                                <td><b><?=$countBalita?></b></td>
                                <td>anak</td>
                                <td width="60px"></td>
                                <td>d)</td>
                                <td>Lansia</td>
                                <td><b><?=$countLansia?></b></td>
                                <td>orang</td>
                            </tr>
                            <tr>
                                <td>b)</td>
                                <td>PUS</td>
                                <td><b><?=$countPUS?></b></td>
                                <td>pasang</td>
                                <td width="60px"></td>
                                <td>e)</td>
                                <td>Berkebutuhan Khusus</td>
                                <td><b><?=$countDifabel?></b></td>
                                <td>orang</td>
                            </tr>
                            <tr>
                                <td>c)</td>
                                <td>WUS</td>
                                <td><b><?=$countWUS?></b></td>
                                <td>orang</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top"></td>
                    <td valign="top" colspan="4">
                        <table class="with-border" cellpadding="5">
                            <thead>
                            <tr>
                                <th class="with-border">No.</th>
                                <th class="with-border">Nama</th>
                                <th class="with-border">Status dalam Keluarga</th>
                                <th class="with-border">Status dalam Perkawinan</th>
                                <th class="with-border">Jenis Kelamin</th>
                                <th class="with-border">Tgl. Lahir / Umur</th>
                                <th class="with-border">Pendidikan</th>
                                <th class="with-border">Pekerjaan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $rwarga = $this->db
                                ->join(TBL_MSTATUSKEL,TBL_MSTATUSKEL.'.'.COL_KDSTATUS." = ".TBL_MWARGA.".".COL_STATUSKELUARGA,"inner")
                                ->where(COL_KDKELUARGA, $dat[COL_KDKELUARGA])
                                ->order_by(TBL_MSTATUSKEL.".".COL_SEQ, 'asc')
                                ->get(TBL_MWARGA)->result_array();
                            $count = 1;
                            foreach($rwarga as $w) {
                                $diffTL = abs(strtotime(date('d-m-Y')) - strtotime($w[COL_TANGGALLAHIR]));
                                $umur = floor($diffTL / (365*60*60*24));
                                ?>
                                <tr>
                                    <td class="with-border"><?=$count?></td>
                                    <td class="with-border"><?=$w[COL_NMANGGOTA]?></td>
                                    <td class="with-border"><?=$w[COL_STATUSKELUARGA]?></td>
                                    <td class="with-border"><?=$w[COL_STATUSKAWIN]?></td>
                                    <td class="with-border"><?=$w[COL_JENISKELAMIN]?></td>
                                    <td class="with-border"><?=date('d-m-Y', strtotime($w[COL_TANGGALLAHIR]))?> / <?=$umur?></td>
                                    <td class="with-border"><?=$w[COL_PENDIDIKAN]?></td>
                                    <td class="with-border"><?=$w[COL_PEKERJAAN]?></td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                            </tbody>
                        </table>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td valign="top">3.</td>
                    <td valign="top" style="table-layout: auto">Makanan Pokok Sehari-hari</td><td valign="top" width="10px">:</td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_NMMAKANANPOKOK?>" value="Beras" <?= $dat[COL_NMMAKANANPOKOK] == "Beras" ? "checked='checked'" : ""?> disabled>
                            Beras
                        </label>
                    </td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_NMMAKANANPOKOK?>" value="Non Beras" <?= $dat[COL_NMMAKANANPOKOK] == "Non Beras" ? "checked='checked'" : ""?> disabled>
                            Non Beras
                        </label>
                        &nbsp;&nbsp;
                        Jenis&nbsp;&nbsp;<b><?=$dat[COL_NMJENISMAKANANPOKOK]?></b>
                    </td>
                </tr>
                <tr>
                    <td valign="top">4.</td>
                    <td valign="top" style="table-layout: auto">Mempunyai Jamban Keluarga</td><td valign="top" width="10px">:</td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_ISPUNYAJAMBANKEL?>" value="1" <?= $dat[COL_ISPUNYAJAMBANKEL] == "1" ? "checked='checked'" : ""?> disabled>
                            Ya
                        </label>
                    </td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_ISPUNYAJAMBANKEL?>" value="0" <?= $dat[COL_ISPUNYAJAMBANKEL] == "0" ? "checked='checked'" : ""?> disabled>
                            Tidak
                        </label>
                        &nbsp;&nbsp;
                        Jumlah&nbsp;&nbsp;<b><?=$dat[COL_JLHJAMBANKEL]?></b>&nbsp;&nbsp;buah
                    </td>
                </tr>
                <tr>
                    <td valign="top">5.</td>
                    <td valign="top" style="table-layout: auto">Sumber Air Keluarga</td><td valign="top" width="10px">:</td>
                    <td valign="top" colspan="2">
                        <?=$dat[COL_NMSUMBERAIR]?>
                    </td>
                </tr>
                <tr>
                    <td valign="top">6.</td>
                    <td valign="top" style="table-layout: auto">Memiliki Tempat Pembuangan Sampah</td><td valign="top" width="10px">:</td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_ISPUNYAPEMBUANGANSAMPAH?>" value="1" <?= $dat[COL_ISPUNYAPEMBUANGANSAMPAH] == "1" ? "checked='checked'" : ""?> disabled>
                            Ya
                        </label>
                    </td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_ISPUNYAPEMBUANGANSAMPAH?>" value="0" <?= $dat[COL_ISPUNYAPEMBUANGANSAMPAH] == "0" ? "checked='checked'" : ""?> disabled>
                            Tidak
                        </label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">7.</td>
                    <td valign="top" style="table-layout: auto">Memiliki Saluran Pembuangan Air Limbah</td><td valign="top" width="10px">:</td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_ISPUNYAPEMBUANGANLIMBAH?>" value="1" <?= $dat[COL_ISPUNYAPEMBUANGANLIMBAH] == "1" ? "checked='checked'" : ""?> disabled>
                            Ya
                        </label>
                    </td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_ISPUNYAPEMBUANGANLIMBAH?>" value="0" <?= $dat[COL_ISPUNYAPEMBUANGANLIMBAH] == "0" ? "checked='checked'" : ""?> disabled>
                            Tidak
                        </label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">8.</td>
                    <td valign="top" style="table-layout: auto">Menempel Stiker P4K</td><td valign="top" width="10px">:</td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_ISTEMPELSTIKERP4K?>" value="1" <?= $dat[COL_ISTEMPELSTIKERP4K] == "1" ? "checked='checked'" : ""?> disabled>
                            Ya
                        </label>
                    </td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_ISTEMPELSTIKERP4K?>" value="0" <?= $dat[COL_ISTEMPELSTIKERP4K] == "0" ? "checked='checked'" : ""?> disabled>
                            Tidak
                        </label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">9.</td>
                    <td valign="top" style="table-layout: auto">Kriteria Rumah</td><td valign="top" width="10px">:</td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_NMKRITERIARUMAH?>" value="Sehat" <?= $dat[COL_NMKRITERIARUMAH] == "Sehat" ? "checked='checked'" : ""?> disabled>
                            Sehat
                        </label>
                    </td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_NMKRITERIARUMAH?>" value="Kurang Sehat" <?= $dat[COL_NMKRITERIARUMAH] == "Kurang Sehat" ? "checked='checked'" : ""?> disabled>
                            Kurang Sehat
                        </label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">10.</td>
                    <td valign="top" style="table-layout: auto">Aktifitas UP2K</td><td valign="top" width="10px">:</td>
                    <td valign="top" colspan="2">
                        <?=$dat[COL_NMUP2K]?>
                    </td>
                </tr>
                <tr>
                    <td valign="top">11.</td>
                    <td valign="top" style="table-layout: auto">Aktifitas dalam Kegiatan Kesehatan Lingkungan</td><td valign="top" width="10px">:</td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_NMAKTIFITASKESEHATANLING?>" value="Ya" <?= $dat[COL_NMAKTIFITASKESEHATANLING] == "Ya" ? "checked='checked'" : ""?> disabled>
                            Ya
                        </label>
                    </td>
                    <td valign="top">
                        <label>
                            <input type="radio" name="<?=COL_NMAKTIFITASKESEHATANLING?>" value="Tidak" <?= $dat[COL_NMAKTIFITASKESEHATANLING] == "Tidak" ? "checked='checked'" : ""?> disabled>
                            Tidak
                        </label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
<?php
    if($n != count($res)) {
        ?>
        <pagebreak />
        <?php
    }
}
?>