<?php $this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mkeluarga/index')?>"> Keluarga</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Data Keluarga</a></li>
                        <li><a href="#tab_2" data-toggle="tab" <?=$edit?"":"style='display: none'"?>>Data Anggota Keluarga</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <br />
                            <?=form_open(current_url(),array('role'=>'form','id'=>'deviceForm','class'=>'form-horizontal'))?>
                            <div style="display: none" class="alert alert-danger errorBox">
                                <i class="fa fa-ban"></i>
                                <span class="errorMsg"></span>
                            </div>
                            <?php
                            if($this->input->get('success') == 1){
                                ?>
                                <div class="alert alert-success">
                                    <i class="fa fa-check"></i>
                                    <span class="">Data disimpan</span>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Kelompok Dasawisma</label>
                                <div class="col-sm-5">
                                    <select name="<?=COL_KDDASAWISMA?>" class="form-control" required>
                                        <?=GetLoggedUser()[COL_ROLEID] == ROLEADMIN ?
                                            GetCombobox("SELECT * FROM ".TBL_MDASAWISMA." ORDER BY ".COL_NMKELOMPOK, COL_KDDASAWISMA, COL_NMKELOMPOK, (!empty($data[COL_KDDASAWISMA]) ? $data[COL_KDDASAWISMA] : null)) :
                                            GetCombobox("SELECT mdasawisma.* FROM ".TBL_MDASAWISMA." where KdDasawisma in('".join("','",explode(",", GetLoggedUser()[COL_COMPANYID]))."') ORDER BY ".COL_NMKELOMPOK, COL_KDDASAWISMA, COL_NMKELOMPOK, (!empty($data[COL_KDDASAWISMA]) ? $data[COL_KDDASAWISMA] : null))
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">No. Kartu Keluarga</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="<?=COL_KDKELUARGA?>" value="<?= $edit ? $data[COL_KDKELUARGA] : ""?>" <?=$edit?"disabled":""?> required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Kepala Keluarga</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="<?=COL_NMKEPALAKELUARGA?>" value="<?= $edit ? $data[COL_NMKEPALAKELUARGA] : ""?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Makanan Pokok Sehari-hari</label>
                                <div class="col-sm-3">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_NMMAKANANPOKOK?>" value="Beras" <?= $edit && $data[COL_NMMAKANANPOKOK] == "Beras" ? "checked='checked'" : ""?>>
                                            Beras
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_NMMAKANANPOKOK?>" value="Non Beras" <?= $edit && $data[COL_NMMAKANANPOKOK] == "Non Beras" ? "checked='checked'" : ""?>>
                                            Non Beras
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Jenis Makanan Pokok Sehari-hari</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="<?=COL_NMJENISMAKANANPOKOK?>" value="<?= $edit ? $data[COL_NMJENISMAKANANPOKOK] : ""?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Mempunyai Jamban Keluarga</label>
                                <div class="col-sm-3">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_ISPUNYAJAMBANKEL?>" value="1" <?= $edit && $data[COL_ISPUNYAJAMBANKEL] == 1 ? "checked='checked'" : ""?>>
                                            Ya
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_ISPUNYAJAMBANKEL?>" value="0" <?= $edit && $data[COL_ISPUNYAJAMBANKEL] == 0 ? "checked='checked'" : ""?>>
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Jumlah Jamban Keluarga</label>
                                <div class="col-sm-3">
                                    <input type="number" class="form-control" name="<?=COL_JLHJAMBANKEL?>" value="<?= $edit ? $data[COL_JLHJAMBANKEL] : ""?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Sumber Air Keluarga</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="<?=COL_NMSUMBERAIR?>" value="<?= $edit ? $data[COL_NMSUMBERAIR] : ""?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Mempunyai Tempat Pembuangan Sampah</label>
                                <div class="col-sm-3">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_ISPUNYAPEMBUANGANSAMPAH?>" value="1" <?= $edit && $data[COL_ISPUNYAPEMBUANGANSAMPAH] == 1 ? "checked='checked'" : ""?>>
                                            Ya
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_ISPUNYAPEMBUANGANSAMPAH?>" value="0" <?= $edit && $data[COL_ISPUNYAPEMBUANGANSAMPAH] == 0 ? "checked='checked'" : ""?>>
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Mempunyai Saluran Pembuangan Air Limbah</label>
                                <div class="col-sm-3">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_ISPUNYAPEMBUANGANLIMBAH?>" value="1" <?= $edit && $data[COL_ISPUNYAPEMBUANGANLIMBAH] == 1 ? "checked='checked'" : ""?>>
                                            Ya
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_ISPUNYAPEMBUANGANLIMBAH?>" value="0" <?= $edit && $data[COL_ISPUNYAPEMBUANGANLIMBAH] == 0 ? "checked='checked'" : ""?>>
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Menempel Stiker P4K</label>
                                <div class="col-sm-3">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_ISTEMPELSTIKERP4K?>" value="1" <?= $edit && $data[COL_ISTEMPELSTIKERP4K] == 1 ? "checked='checked'" : ""?>>
                                            Ya
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_ISTEMPELSTIKERP4K?>" value="0" <?= $edit && $data[COL_ISTEMPELSTIKERP4K] == 0 ? "checked='checked'" : ""?>>
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Kriteria Rumah</label>
                                <div class="col-sm-3">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_NMKRITERIARUMAH?>" value="Sehat" <?= $edit && $data[COL_NMKRITERIARUMAH] == "Sehat" ? "checked='checked'" : ""?>>
                                            Sehat
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_NMKRITERIARUMAH?>" value="Kurang Sehat" <?= $edit && $data[COL_NMKRITERIARUMAH] == "Kurang Sehat" ? "checked='checked'" : ""?>>
                                            Kurang Sehat
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Aktifitas UP2K</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="<?=COL_NMUP2K?>" value="<?= $edit ? $data[COL_NMUP2K] : ""?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Aktifitas dalam Kegiatan Kesehatan Lingkungan</label>
                                <div class="col-sm-3">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_NMAKTIFITASKESEHATANLING?>" value="Ya" <?= $edit && $data[COL_NMAKTIFITASKESEHATANLING] == "Ya" ? "checked='checked'" : ""?>>
                                            Ya
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="<?=COL_NMAKTIFITASKESEHATANLING?>" value="Tidak" <?= $edit && $data[COL_NMAKTIFITASKESEHATANLING] == "Tidak" ? "checked='checked'" : ""?>>
                                            Tidak
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group">
                                <div class="col-sm-12" style="text-align: right">
                                    <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                    <a href="<?=site_url('mkeluarga/index')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                                </div>

                            </div>
                            <?=form_close()?>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="table-responsive">
                                <?php
                                $rwarga = $this->db->where(COL_KDKELUARGA, ($edit?$data[COL_KDKELUARGA]:"-1"))->get(TBL_MWARGA)->result_array();
                                ?>
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th width="80px">
                                            <button onclick="tambahAnggotaKel()" class="btn btn-flat btn-xs btn-success"><i class="fa fa-plus"></i></button>
                                        </th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Status</th>
                                        <th>Pendidikan</th>
                                        <th>Pekerjaan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($rwarga as $w) {
                                        ?>
                                        <tr>
                                            <td>
                                                <button onclick="editAnggotaKel('<?=site_url('mkeluarga/editanggota/'.$w[COL_NIK])."?kk=".$w[COL_KDKELUARGA]?>')" class="btn btn-flat btn-xs btn-primary"><i class="fa fa-pencil"></i></button>
                                                <a href="<?=site_url('mkeluarga/delanggota')."?no=".$w[COL_NIK]."&kk=".$w[COL_KDKELUARGA]?>" class="btn btn-flat btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                            </td>
                                            <td><?=$w[COL_NIK]?></td>
                                            <td><?=$w[COL_NMANGGOTA]?></td>
                                            <td><?=$w[COL_JENISKELAMIN]?></td>
                                            <td><?=$w[COL_STATUSKELUARGA]?></td>
                                            <td><?=$w[COL_PENDIDIKAN]?></td>
                                            <td><?=$w[COL_PEKERJAAN]?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <!--<tr>
                                        <td>
                                            <a class="btn btn-flat btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-flat btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                        <td>11112087</td>
                                        <td>Yoel Rolas Simanjuntak</td>
                                        <td>Laki-Laki</td>
                                        <td>Kepala Keluarga</td>
                                        <td>Diploma</td>
                                    </tr>-->
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
        </div>
        <div class="modal fade modal-default" id="anggotaKelDialog" role="dialog">
            <div class="modal-dialog" style="width: 1080px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                        <h4 class="modal-title">Tambah / Ubah Data Keluarga</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $(document).ready(function() {
            <?php
            if(!empty($this->input->get("tab")) && $this->input->get("tab") == "anggota") {
            ?>
            $('.nav-tabs a[href="#tab_2"]').tab('show');
            <?php
            }
            ?>
        });
        
        function tambahAnggotaKel() {
            $('#anggotaKelDialog').modal('show');
            $("#anggotaKelDialog").find(".modal-body").load("<?=site_url('mkeluarga/addanggota')."?kk=".($edit?$data[COL_KDKELUARGA]:'')?>", function() {

            });
            return false;
        }

        function editAnggotaKel(link) {
            $('#anggotaKelDialog').modal('show');
            $("#anggotaKelDialog").find(".modal-body").load(link, function() {

            });
            return false;
        }


        $("#deviceForm").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        alert('Response Error');
                    }
                });
                return false;
            }
        });
    </script>
<?php $this->load->view('footer') ?>