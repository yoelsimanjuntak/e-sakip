<?php
$noKK = $this->input->get('kk');
?>

<?=form_open(current_url(),array('role'=>'form','id'=>'kelForm','class'=>'form-horizontal'))?>
<div style="display: none" class="alert alert-danger errorBox">
    <i class="fa fa-ban"></i>
    <span class="errorMsg"></span>
</div>
<?php
if($this->input->get('success') == 1){
    ?>
    <div class="alert alert-success">
        <i class="fa fa-check"></i>
        <span class="">Data disimpan</span>
    </div>
    <?php
}
?>
<div class="col-sm-6">
    <input type="hidden" name="<?=COL_KDKELUARGA?>" value="<?=$noKK?>">
    <div class="form-group">
        <label class="control-label col-sm-3">NIK</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="<?=COL_NIK?>" value="<?= $edit ? $data[COL_NIK] : ""?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Nama</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="<?=COL_NMANGGOTA?>" value="<?= $edit ? $data[COL_NMANGGOTA] : ""?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Jenis Kelamin</label>
        <div class="col-sm-8">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_JENISKELAMIN?>" value="Laki-Laki" <?= $edit && $data[COL_JENISKELAMIN] == "Laki-Laki" ? "checked='checked'" : ""?>>
                    Laki-Laki
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_JENISKELAMIN?>" value="Perempuan" <?= $edit && $data[COL_JENISKELAMIN] == "Perempuan" ? "checked='checked'" : ""?>>
                    Perempuan
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Tempat / Tgl Lahir</label>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="<?=COL_TEMPATLAHIR?>" value="<?= $edit ? $data[COL_TEMPATLAHIR] : ""?>">
        </div>
        <div class="col-sm-3">
            <input type="text" class="form-control datepicker" name="<?= COL_TANGGALLAHIR?>" value="<?= $edit ? date("d M Y", strtotime($data[COL_TANGGALLAHIR])) : ""?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Status Perkawinan</label>
        <div class="col-sm-8">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKAWIN?>" value="Menikah" <?= $edit && $data[COL_STATUSKAWIN] == "Menikah" ? "checked='checked'" : ""?>>
                    Menikah
                </label>&nbsp;&nbsp;
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKAWIN?>" value="Lajang" <?= $edit && $data[COL_STATUSKAWIN] == "Lajang" ? "checked='checked'" : ""?>>
                    Lajang
                </label>&nbsp;&nbsp;
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKAWIN?>" value="Janda" <?= $edit && $data[COL_STATUSKAWIN] == "Janda" ? "checked='checked'" : ""?>>
                    Janda
                </label>&nbsp;&nbsp;
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKAWIN?>" value="Duda" <?= $edit && $data[COL_STATUSKAWIN] == "Duda" ? "checked='checked'" : ""?>>
                    Duda
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Status dalam Keluaga</label>
        <div class="col-sm-4">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKELUARGA?>" value="Suami" <?= $edit && $data[COL_STATUSKELUARGA] == "Suami" ? "checked='checked'" : ""?>>
                    Suami
                </label>&nbsp;&nbsp;
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKELUARGA?>" value="Istri" <?= $edit && $data[COL_STATUSKELUARGA] == "Istri" ? "checked='checked'" : ""?>>
                    Istri
                </label>&nbsp;&nbsp;
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKELUARGA?>" value="Anak" <?= $edit && $data[COL_STATUSKELUARGA] == "Anak" ? "checked='checked'" : ""?>>
                    Anak
                </label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKELUARGA?>" value="Menantu" <?= $edit && $data[COL_STATUSKELUARGA] == "Menantu" ? "checked='checked'" : ""?>>
                    Menantu
                </label>&nbsp;&nbsp;
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_STATUSKELUARGA?>" value="Keluarga" <?= $edit && $data[COL_STATUSKELUARGA] == "Keluarga" ? "checked='checked'" : ""?>>
                    Keluarga
                </label>&nbsp;&nbsp;
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Agama</label>
        <div class="col-sm-8">
            <select name="<?=COL_AGAMA?>" class="form-control select2" required>
                <option value="Islam" <?= $edit && $data[COL_AGAMA] == "Islam" ? "selected='selected'" : ""?>>Islam</option>
                <option value="Kristen" <?= $edit && $data[COL_AGAMA] == "Kristen" ? "selected='selected'" : ""?>>Kristen</option>
                <option value="Katolik" <?= $edit && $data[COL_AGAMA] == "Katolik" ? "selected='selected'" : ""?>>Katolik</option>
                <option value="Hindu" <?= $edit && $data[COL_AGAMA] == "Hindu" ? "selected='selected'" : ""?>>Hindu</option>
                <option value="Buddha" <?= $edit && $data[COL_AGAMA] == "Buddha" ? "selected='selected'" : ""?>>Buddha</option>
                <option value="Konghucu" <?= $edit && $data[COL_AGAMA] == "Konghucu" ? "selected='selected'" : ""?>>Konghucu</option>
                <option value="Kepercayaan Lain" <?= $edit && $data[COL_AGAMA] == "Kepercayaan Lain" ? "selected='selected'" : ""?>>Kepercayaan Lain</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-3">Alamat</label>
        <div class="col-sm-8">
            <textarea class="form-control" name="<?=COL_ALAMAT?>" rows="4"><?= $edit ? $data[COL_ALAMAT] : ""?></textarea>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        <label class="control-label col-sm-4">Pendidikan</label>
        <div class="col-sm-6">
            <select name="<?=COL_PENDIDIKAN?>" class="form-control select2">
                <option value="Tidak tamat SD" <?= $edit && $data[COL_PENDIDIKAN] == "Tidak tamat SD" ? "selected='selected'" : ""?>>Tidak tamat SD</option>
                <option value="SD/MI" <?= $edit && $data[COL_PENDIDIKAN] == "SD/MI" ? "selected='selected'" : ""?>>SD/MI</option>
                <option value="SMP/Sederajat" <?= $edit && $data[COL_PENDIDIKAN] == "SMP/Sederajat" ? "selected='selected'" : ""?>>SMP/Sederajat</option>
                <option value="SMU/Sederajat" <?= $edit && $data[COL_PENDIDIKAN] == "SMU/SMK/Sederajat" ? "selected='selected'" : ""?>>SMU/SMK/Sederajat</option>
                <option value="Diploma" <?= $edit && $data[COL_PENDIDIKAN] == "Diploma" ? "selected='selected'" : ""?>>Diploma</option>
                <option value="S1" <?= $edit && $data[COL_PENDIDIKAN] == "S1" ? "selected='selected'" : ""?>>S1</option>
                <option value="S2" <?= $edit && $data[COL_PENDIDIKAN] == "S2" ? "selected='selected'" : ""?>>S2</option>
                <option value="S3" <?= $edit && $data[COL_PENDIDIKAN] == "S3" ? "selected='selected'" : ""?>>S3</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Pekerjaan</label>
        <div class="col-sm-6">
            <select name="<?=COL_PEKERJAAN?>" class="form-control select2">
                <option value="Petani" <?= $edit && $data[COL_PEKERJAAN] == "Petani" ? "selected='selected'" : ""?>>Petani</option>
                <option value="Pedagang" <?= $edit && $data[COL_PEKERJAAN] == "Pedagang" ? "selected='selected'" : ""?>>Pedagang</option>
                <option value="Swasta" <?= $edit && $data[COL_PEKERJAAN] == "Swasta" ? "selected='selected'" : ""?>>Swasta</option>
                <option value="Wirausaha" <?= $edit && $data[COL_PEKERJAAN] == "Wirausaha" ? "selected='selected'" : ""?>>Wirausaha</option>
                <option value="PNS" <?= $edit && $data[COL_PEKERJAAN] == "PNS" ? "selected='selected'" : ""?>>PNS</option>
                <option value="TNI/Polri" <?= $edit && $data[COL_PEKERJAAN] == "TNI/Polri" ? "selected='selected'" : ""?>>TNI/Polri</option>
                <?php
                if ($edit && !in_array($data[COL_PEKERJAAN], ["Petani", "Pedagang", "Swasta", "Wirausaha", "PNS", "TNI/Polri"])) {
                    ?>
                    <option value="<?=$data[COL_PEKERJAAN]?>" selected="selected"><?=$data[COL_PEKERJAAN]?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Akseptor KB</label>
        <div class="col-sm-6">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_ISAKSEPTORKB?>" value="1" <?= $edit && $data[COL_ISAKSEPTORKB] == 1 ? "checked='checked'" : ""?>>
                    Ya
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_ISAKSEPTORKB?>" value="0" <?= $edit && $data[COL_ISAKSEPTORKB] == 0 ? "checked='checked'" : ""?>>
                    Tidak
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Aktif dlm Kegiatan Posyandu</label>
        <div class="col-sm-6">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFPOSYANDU?>" value="1" <?= $edit && $data[COL_ISAKTIFPOSYANDU] == 1 ? "checked='checked'" : ""?>>
                    Ya
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFPOSYANDU?>" value="0" <?= $edit && $data[COL_ISAKTIFPOSYANDU] == 0 ? "checked='checked'" : ""?>>
                    Tidak
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Aktif dlm Program Bina Keluarga Balita</label>
        <div class="col-sm-6">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFBKB?>" value="1" <?= $edit && $data[COL_ISAKTIFBKB] == 1 ? "checked='checked'" : ""?>>
                    Ya
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFBKB?>" value="0" <?= $edit && $data[COL_ISAKTIFBKB] == 0 ? "checked='checked'" : ""?>>
                    Tidak
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Memiliki Tabungan</label>
        <div class="col-sm-6">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_ISPUNYATABUNGAN?>" value="1" <?= $edit && $data[COL_ISPUNYATABUNGAN] == 1 ? "checked='checked'" : ""?>>
                    Ya
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_ISPUNYATABUNGAN?>" value="0" <?= $edit && $data[COL_ISPUNYATABUNGAN] == 0 ? "checked='checked'" : ""?>>
                    Tidak
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Mengikuti Kelompok Belajar</label>
        <div class="col-sm-6">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFKELOMPOKBELAJAR?>" value="1" <?= $edit && $data[COL_ISAKTIFKELOMPOKBELAJAR] == 1 ? "checked='checked'" : ""?>>
                    Ya
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFKELOMPOKBELAJAR?>" value="0" <?= $edit && $data[COL_ISAKTIFKELOMPOKBELAJAR] == 0 ? "checked='checked'" : ""?>>
                    Tidak
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Mengikuti PAUD / Sejenis</label>
        <div class="col-sm-6">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFPAUD?>" value="1" <?= $edit && $data[COL_ISAKTIFPAUD] == 1 ? "checked='checked'" : ""?>>
                    Ya
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFPAUD?>" value="0" <?= $edit && $data[COL_ISAKTIFPAUD] == 0 ? "checked='checked'" : ""?>>
                    Tidak
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Aktif dlm Kegiatan Koperasi</label>
        <div class="col-sm-6">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFKOPERASI?>" value="1" <?= $edit && $data[COL_ISAKTIFKOPERASI] == 1 ? "checked='checked'" : ""?>>
                    Ya
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_ISAKTIFKOPERASI?>" value="0" <?= $edit && $data[COL_ISAKTIFKOPERASI] == 0 ? "checked='checked'" : ""?>>
                    Tidak
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Penyandang Kebutuhan Khusus</label>
        <div class="col-sm-6">
            <div class="radio">
                <label>
                    <input type="radio" name="<?=COL_ISDIFABEL?>" value="1" <?= $edit && $data[COL_ISDIFABEL] == 1 ? "checked='checked'" : ""?>>
                    Ya
                </label>&nbsp;&nbsp;
                <label>
                    <input type="radio" name="<?=COL_ISDIFABEL?>" value="0" <?= $edit && $data[COL_ISDIFABEL] == 0 ? "checked='checked'" : ""?>>
                    Tidak
                </label>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="form-group form-buttons">
    <hr />
    <div class="col-sm-12" style="text-align: right">
        <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
    </div>

</div>
<?=form_close()?>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2", $("#kelForm")).select2({ width: 'resolve', tags: true, width: '100%' });
            $('.datepicker', $("#kelForm")).datepicker({
                autoclose: true,
                format: 'dd M yyyy'
            });
        });
        $("#kelForm").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        alert('Response Error');
                    }
                });
                return false;
            }
        });
    </script>