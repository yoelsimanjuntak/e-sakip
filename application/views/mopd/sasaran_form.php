<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 14:13
 */
$this->load->view('header') ?>
<?php
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mopd/sasaran')?>"> Sasaran OPD</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSub = "";
                                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                    if($edit) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                        $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                        $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                        $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEKADIS) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }

                                    ?>
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required   >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=$edit?"":($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-periode" value="<?= $edit ? $data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]." : ".$data[COL_NM_PEJABAT] : (!empty($data["DefPeriod"])?$data["DefPeriod"]:"")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= !empty($data[COL_KD_PEMDA])?$data[COL_KD_PEMDA]:"" ?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-period" data-toggle="modal" data-target="#browsePeriod" data-toggle="tooltip" data-placement="top" title="Pilih Periode" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">IKU Kabupaten</label>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#browseIKU" data-toggle="tooltip" data-placement="top" title="Pilih IKU">
                                    Pilih ...
                                </button>
                                <div class="div-iku no-padding" style="margin-top: 10px">
                                    <table class="table table-bordered" style="outline-style: dashed;">
                                        <tr>
                                            <td style="width: 150px" class="control-label">Misi</td>
                                            <td style="width: 10px">:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required >
                                                <span class="text-misi"><?= $edit ? $data[COL_KD_MISI].". ".$data[COL_NM_MISI] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Tujuan</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required >
                                                <span class="text-tujuan"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].". ".$data[COL_NM_TUJUAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Tujuan</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required >
                                                <span class="text-iktujuan"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].". ".$data[COL_NM_INDIKATORTUJUAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required >
                                                <span class="text-sasaran"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].". ".$data[COL_NM_SASARAN] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="control-label">Indikator Sasaran</td>
                                            <td>:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required >
                                                <span class="text-iksasaran"><?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].". ".$data[COL_NM_INDIKATORSASARAN] : ""?></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tujuan OPD</label>
                            <div class="col-sm-8">
                                <select name="<?=COL_KD_TUJUANOPD?>" class="form-control" required></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Tujuan OPD</label>
                            <div class="col-sm-8">
                                <select name="<?=COL_KD_INDIKATORTUJUANOPD?>" class="form-control" required></select>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="control-label col-sm-2">Indikator Tujuan</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-iktujuanopd" value="<?= $edit ? $data[COL_KD_MISI].".".$data[COL_KD_TUJUAN].".".$data[COL_KD_INDIKATORTUJUAN].".".$data[COL_KD_SASARAN].".".$data[COL_KD_INDIKATORSASARAN].".".$data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD]." ".$data[COL_NM_INDIKATORTUJUANOPD] : ""?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= $edit ? $data[COL_KD_PEMDA] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_MISI?>" value="<?= $edit ? $data[COL_KD_MISI] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUAN?>" value="<?= $edit ? $data[COL_KD_TUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_SASARAN?>" value="<?= $edit ? $data[COL_KD_SASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORSASARAN?>" value="<?= $edit ? $data[COL_KD_INDIKATORSASARAN] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required >
                                    <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-iktujuanopd" data-toggle="modal" data-target="#browseIKTujuan" data-toggle="tooltip" data-placement="top" title="Pilih Indikator Tujuan" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="control-label col-sm-2">Tujuan OPD</label>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#browseTujuanOPD" data-toggle="tooltip" data-placement="top" title="Pilih IKU">
                                    Pilih ...
                                </button>
                                <div class="div-iku no-padding" style="margin-top: 10px">
                                    <?php
                                    if($edit) {
                                        $r_iku = $this->db
                                            ->join(TBL_SAKIP_MPMD_MISI,
                                                TBL_SAKIP_MPMD_MISI.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
                                                TBL_SAKIP_MPMD_MISI.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI
                                                ,"inner")
                                            ->join(TBL_SAKIP_MPMD_TUJUAN,
                                                TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
                                                TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
                                                TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN
                                                ,"inner")
                                            ->join(TBL_SAKIP_MPMD_IKTUJUAN,
                                                TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
                                                TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
                                                TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".
                                                TBL_SAKIP_MPMD_IKTUJUAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN
                                                ,"inner")
                                            ->join(TBL_SAKIP_MPMD_SASARAN,
                                                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
                                                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
                                                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".
                                                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                                                TBL_SAKIP_MPMD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN
                                                ,"inner")
                                            ->join(TBL_SAKIP_MPMD_IKSASARAN,
                                                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
                                                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
                                                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".
                                                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                                                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN." AND ".
                                                TBL_SAKIP_MPMD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN
                                                ,"inner")
                                            ->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA, $data[COL_KD_PEMDA])
                                            ->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_URUSAN, $data[COL_KD_URUSAN])
                                            ->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_BIDANG, $data[COL_KD_BIDANG])
                                            ->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_UNIT, $data[COL_KD_UNIT])
                                            ->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SUB, $data[COL_KD_SUB])
                                            ->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD, $data[COL_KD_TUJUANOPD])
                                            ->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, $data[COL_KD_INDIKATORTUJUANOPD])
                                            ->where(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, $data[COL_KD_SASARANOPD])
                                            ->get(TBL_SAKIP_MOPD_SASARAN)
                                            ->result_array();
                                        foreach($r_iku as $i) {
                                            ?>
                                            <table class="table table-bordered" style="outline-style: dashed;">
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Misi</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_Misi[]"
                                                               value="<?= $i[COL_KD_MISI] ?>"/><?= $i[COL_KD_MISI] . ". " . $i[COL_NM_MISI] ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Tujuan</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_Tujuan[]"
                                                               value="<?= $i[COL_KD_TUJUAN] ?>"/><?= $i[COL_KD_MISI] . "." . $i[COL_KD_TUJUAN] . ". " . $i[COL_NM_TUJUAN] ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Indikator Tujuan</td>
                                                    <td style="width: 10px">:</td>
                                                    <input type="hidden" name="Kd_IndikatorTujuan[]"
                                                           value="<?= $i[COL_KD_INDIKATORTUJUAN] ?>"/>
                                                    <td><?= $i[COL_KD_MISI] . "." . $i[COL_KD_TUJUAN] . "." . $i[COL_KD_INDIKATORTUJUAN] . ". " . $i[COL_NM_INDIKATORTUJUAN] ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Sasaran</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_Sasaran[]"
                                                               value="<?= $i[COL_KD_SASARAN] ?>"/><?= $i[COL_KD_MISI] . "." . $i[COL_KD_TUJUAN] . "." . $i[COL_KD_INDIKATORTUJUAN] . "." . $i[COL_KD_SASARAN] . ". " . $i[COL_NM_SASARAN] ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Indikator Sasaran</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_IndikatorSasaran[]"
                                                               value="<?= $i[COL_KD_INDIKATORSASARAN] ?>"/><?= $i[COL_KD_MISI] . "." . $i[COL_KD_TUJUAN] . "." . $i[COL_KD_INDIKATORTUJUAN] . "." . $i[COL_KD_SASARAN] . "." . $i[COL_KD_INDIKATORSASARAN] . ". " . $i[COL_NM_INDIKATORSASARAN] ?>
                                                    </td>
                                                </tr>
                                            </table>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="div-tujuan-opd no-padding" style="margin-top: 10px">
                                    <table class="table table-bordered" style="outline-style: dashed;">
                                        <tr>
                                            <td style="width: 200px" class="control-label">Tujuan OPD</td>
                                            <td style="width: 10px">:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required >
                                                <span class="text-tujuan-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].". ".$data[COL_NM_TUJUANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 200px" class="control-label">Indikator Tujuan OPD</td>
                                            <td style="width: 10px">:</td>
                                            <td>
                                                <input type="hidden" name="<?=COL_KD_INDIKATORTUJUANOPD?>" value="<?= $edit ? $data[COL_KD_INDIKATORTUJUANOPD] : ""?>" required >
                                                <span class="text-iktujuan-opd"><?= $edit ? $data[COL_KD_TUJUANOPD].".".$data[COL_KD_INDIKATORTUJUANOPD].". ".$data[COL_NM_INDIKATORTUJUANOPD] : ""?></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <label class="control-label col-sm-2">Sasaran</label>
                            <div class="col-sm-2">
                                <!--<div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#browseExistingSasaran" data-toggle="tooltip" data-placement="top" title="Pilih Sasaran yang Sudah Ada"><i class="fa fa-ellipsis-h"></i></button>
                                    </div>

                                </div>-->
                                <input type="number" placeholder="No" class="form-control" name="<?=COL_KD_SASARANOPD?>" value="<?= $edit ? $data[COL_KD_SASARANOPD] : ""?>" required>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" placeholder="Sasaran" class="form-control" name="<?=COL_NM_SASARANOPD?>" value="<?= $edit ? $data[COL_NM_SASARANOPD] : ""?>" required>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="box box-default">
                    <div class="box-header">
                        <h5 class="box-title">Indikator Sasaran</h5>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="tbl-det">
                            <thead>
                            <tr>
                                <th style="width: 60px">No.</th>
                                <th style="min-width: 240px">Deskripsi</th>
                                <th>Formulasi</th>
                                <th>Sumber Data</th>
                                <!--<th>Penanggung Jawab</th>-->
                                <th style="width: 40px"><button type="button" id="btn-add-det" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr-blueprint-det" style="display: none">
                                <td><input type="text" name="text-det-no" class="form-control" style="width: 50px" /></td>
                                <td><textarea name="text-det-desc" class="form-control" rows="4"></textarea></td>
                                <td><textarea name="text-det-formula" class="form-control" rows="4"></textarea></td>
                                <td><textarea name="text-det-sumber" class="form-control" rows="4"></textarea></td>
                                <!--<td><textarea name="text-det-pic" class="form-control" rows="4"></textarea></td>-->
                                <td>
                                    <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                </td>
                            </tr>
                            <?php
                            $det = $this->db
                                ->where(COL_KD_URUSAN, ($edit?$data[COL_KD_URUSAN]:-999))
                                ->where(COL_KD_BIDANG, ($edit?$data[COL_KD_BIDANG]:-999))
                                ->where(COL_KD_UNIT, ($edit?$data[COL_KD_UNIT]:-999))
                                ->where(COL_KD_SUB, ($edit?$data[COL_KD_SUB]:-999))

                                ->where(COL_KD_PEMDA, ($edit?$data[COL_KD_PEMDA]:-999))
                                ->where(COL_KD_MISI, ($edit?$data[COL_KD_MISI]:-999))
                                ->where(COL_KD_TUJUAN, ($edit?$data[COL_KD_TUJUAN]:-999))
                                ->where(COL_KD_INDIKATORTUJUAN, ($edit?$data[COL_KD_INDIKATORTUJUAN]:-999))
                                ->where(COL_KD_SASARAN, ($edit?$data[COL_KD_SASARAN]:-999))
                                ->where(COL_KD_INDIKATORSASARAN, ($edit?$data[COL_KD_INDIKATORSASARAN]:-999))
                                ->where(COL_KD_TUJUANOPD, ($edit?$data[COL_KD_TUJUANOPD]:-999))
                                ->where(COL_KD_INDIKATORTUJUANOPD, ($edit?$data[COL_KD_INDIKATORTUJUANOPD]:-999))
                                ->where(COL_KD_SASARANOPD, ($edit?$data[COL_KD_SASARANOPD]:-999))
                                ->order_by(COL_KD_INDIKATORSASARANOPD, 'asc')
                                ->get(TBL_SAKIP_MOPD_IKSASARAN)->result_array();
                            foreach($det as $m) {
                                ?>
                                <tr>
                                    <td><input type="text" name="text-det-no" class="form-control" value="<?=$m[COL_KD_INDIKATORSASARANOPD]?>" style="width: 50px" /></td>
                                    <td><textarea name="text-det-desc" class="form-control" rows="4"><?=$m[COL_NM_INDIKATORSASARANOPD]?></textarea></td>
                                    <td><textarea name="text-det-formula" class="form-control" rows="4"><?=$m[COL_NM_FORMULA]?></textarea></td>
                                    <td><textarea name="text-det-sumber" class="form-control" rows="4"><?=$m[COL_NM_SUMBERDATA]?></textarea></td>
                                    <!--<td><textarea name="text-det-pic" class="form-control" rows="4"><?=$m[COL_NM_PENANGGUNGJAWAB]?></textarea></td>-->
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>

                            <?php
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                        <a href="<?=site_url('mopd/sasaran')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </section>

    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browsePeriod" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKU" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse IKU</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="btn btn-primary btn-flat" id="btn-select-iku">Pilih</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseTujuanOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse IKU</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="btn btn-primary btn-flat" id="btn-select-iku">Pilih</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseExistingSasaran" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        var mainform = $("#main-form");
        $(".btn-del-det").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-det").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-det").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-det").show();
            $(".btn-del-det", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });
        $("#main-form").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                var det = [];
                var rowDet = $("#tbl-det>tbody").find("tr:not(.tr-blueprint-det)");

                for (var i = 0; i < rowDet.length; i++) {
                    var noDet = $("[name=text-det-no]", $(rowDet[i])).val();
                    var ketDet = $("[name=text-det-desc]", $(rowDet[i])).val();
                    var formtDet = $("[name=text-det-formula]", $(rowDet[i])).val();
                    var sourceDet = $("[name=text-det-sumber]", $(rowDet[i])).val();
                    var picDet = $("[name=text-det-pic]", $(rowDet[i])).val();
                    det.push({ No: noDet, Ket: ketDet, Formula: formtDet, Sumber: sourceDet, PIC: picDet });
                }
                $(".appended", $("#main-form")).remove();
                for (var i = 0; i < det.length; i++) {
                    var newEl = "";
                    newEl += "<input type='hidden' class='appended' name=NoDet[" + i + "] value='" + det[i].No + "' />";
                    newEl += "<input type='hidden' class='appended' name=KetDet[" + i + "] value='" + det[i].Ket + "' />";
                    newEl += "<input type='hidden' class='appended' name=FormulaDet[" + i + "] value='" + det[i].Formula + "' />";
                    newEl += "<input type='hidden' class='appended' name=SumberDet[" + i + "] value='" + det[i].Sumber + "' />";
                    //newEl += "<input type='hidden' class='appended' name=PICDet[" + i + "] value='" + det[i].PIC + "' />";
                    $("#main-form").append(newEl);
                }
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        //alert('Response Error');
                        $(".modal-body", $("#alertDialog")).html("<span style='font-style: italic;'>"+a.status+": "+a.statusText+"</span>");
                        $("#alertDialog").modal('show');
                    }
                });
                return false;
            }
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browseOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseOPD"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdSub = $(this).val().split('|');
                    $("[name=Kd_Urusan]").val(kdSub[0]);
                    $("[name=Kd_Bidang]").val(kdSub[1]);
                    $("[name=Kd_Unit]").val(kdSub[2]);
                    $("[name=Kd_Sub]").val(kdSub[3]);
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Misi]", mainform).val("").trigger("change");
                    $("[name=Kd_Tujuan]", mainform).val("").trigger("change");
                    $("[name=Kd_IndikatorTujuan]", mainform).val("").trigger("change");
                    $("[name=Kd_Sasaran]", mainform).val("").trigger("change");
                    $("[name=Kd_IndikatorSasaran]", mainform).val("").trigger("change");
                    $("[name=text-opd]").val($(this).val());
                });
            });
        });

        $('#browsePeriod').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browsePeriod"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-pemda")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Pemda]").val($(this).val());
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-periode]").val($(this).val());
                });
            });
        });

        $('#browseIKU').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKU"));
            var kdPemda = $("[name=Kd_Pemda]", $("#main-form")).val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }

            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iku-pemda")?>", {Kd_Pemda: kdPemda}, function () {
                var form = $("#main-form");
                $("select", modalBody).not('.no-select2').select2({ width: 'resolve'});
                $("#btn-select-iku").unbind('click').click(function() {
                    var kdMisi = $("[name=Kd_Misi] option:selected", modalBody);
                    var kdTujuan = $("[name=Kd_Tujuan] option:selected", modalBody);
                    var kdIkTujuan = $("[name=Kd_IndikatorTujuan] option:selected", modalBody);
                    var kdSasaran = $("[name=Kd_Sasaran] option:selected", modalBody);
                    var kdIkSasaran = $("[name=Kd_IndikatorSasaran] option:selected", modalBody);

                    if(!kdMisi || !kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                        $("[data-dismiss=modal]", $("#browseIKU")).click();
                        return true;
                    }

                    $("[name=Kd_Misi]", form).val(kdMisi.val()).trigger('change');
                    $(".text-misi", form).html(kdMisi.val()+'. '+kdMisi.html());
                    $("[name=Kd_Tujuan]", form).val(kdTujuan.val()).trigger('change');
                    $(".text-tujuan", form).html(kdMisi.val()+'.'+kdTujuan.val()+'. '+kdTujuan.html());
                    $("[name=Kd_IndikatorTujuan]", form).val(kdIkTujuan.val()).trigger('change');
                    $(".text-iktujuan", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'. '+kdIkTujuan.html());
                    $("[name=Kd_Sasaran]", form).val(kdSasaran.val()).trigger('change');
                    $(".text-sasaran", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'. '+kdSasaran.html());
                    $("[name=Kd_IndikatorSasaran]", form).val(kdIkSasaran.val()).trigger('change');
                    $(".text-iksasaran", form).html(kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'.'+kdIkSasaran.val()+'. '+kdIkSasaran.html());
                    $("[data-dismiss=modal]", $("#browseIKU")).click();
                });
            });
        });

        $("[name=Kd_IndikatorSasaran]", mainform).change(function() {
            var kdPemda = $("[name=Kd_Pemda]", mainform).val();
            var kdUrusan = $("[name=Kd_Urusan]", mainform).val();
            var kdBidang = $("[name=Kd_Bidang]", mainform).val();
            var kdUnit = $("[name=Kd_Unit]", mainform).val();
            var kdSub = $("[name=Kd_Sub]", mainform).val();
            var kdMisi = $("[name=Kd_Misi]", mainform).val();
            var kdTujuan = $("[name=Kd_Tujuan]", mainform).val();
            var kdIkTujuan = $("[name=Kd_IndikatorTujuan]", mainform).val();
            var kdSasaran = $("[name=Kd_Sasaran]", mainform).val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]", mainform).val();

            if(kdPemda && kdMisi && kdTujuan && kdIkTujuan && kdSasaran && kdIkSasaran && kdUrusan && kdBidang && kdUnit && kdSub) {
                var param = {
                    Kd_Pemda: kdPemda,
                    Kd_Urusan: kdUrusan,
                    Kd_Bidang: kdBidang,
                    Kd_Unit: kdUnit,
                    Kd_Sub: kdSub,
                    Kd_Misi: kdMisi,
                    Kd_Tujuan: kdTujuan,
                    Kd_IndikatorTujuan: kdIkTujuan,
                    Kd_Sasaran: kdSasaran,
                    Kd_IndikatorSasaran: kdIkSasaran
                };
                $("[name=Kd_TujuanOPD]", mainform).load("<?=site_url("ajax/get-opt-tujuanopd")?>", param, function () {
					<?php
					if(!empty($data[COL_KD_TUJUANOPD])) {
						?>
						if(<?=!empty($data[COL_KD_TUJUANOPD])?$data[COL_KD_TUJUANOPD]:''?> != '') {
							$("[name=Kd_TujuanOPD]", mainform).val(<?=$data[COL_KD_TUJUANOPD]?>);
						}
						<?php
					}
					?>
                    
                    $("[name=Kd_IndikatorTujuanOPD]", mainform).html("").trigger("change");
                    $("[name=Kd_TujuanOPD]", mainform).trigger("change");
                });
            }else {
                $("[name=Kd_TujuanOPD]", mainform).html("").trigger("change");
            }
        }).trigger('change');

        $("[name=Kd_TujuanOPD]", mainform).change(function() {
            var kdPemda = $("[name=Kd_Pemda]", mainform).val();
            var kdUrusan = $("[name=Kd_Urusan]", mainform).val();
            var kdBidang = $("[name=Kd_Bidang]", mainform).val();
            var kdUnit = $("[name=Kd_Unit]", mainform).val();
            var kdSub = $("[name=Kd_Sub]", mainform).val();
            var kdMisi = $("[name=Kd_Misi]", mainform).val();
            var kdTujuan = $("[name=Kd_Tujuan]", mainform).val();
            var kdIkTujuan = $("[name=Kd_IndikatorTujuan]", mainform).val();
            var kdSasaran = $("[name=Kd_Sasaran]", mainform).val();
            var kdIkSasaran = $("[name=Kd_IndikatorSasaran]", mainform).val();
            var kdTujuanOPD = $("[name=Kd_TujuanOPD]", mainform).val();

            if(kdTujuanOPD) {
                var param = {
                    Kd_Pemda: kdPemda,
                    Kd_Urusan: kdUrusan,
                    Kd_Bidang: kdBidang,
                    Kd_Unit: kdUnit,
                    Kd_Sub: kdSub,
                    Kd_Misi: kdMisi,
                    Kd_Tujuan: kdTujuan,
                    Kd_IndikatorTujuan: kdIkTujuan,
                    Kd_Sasaran: kdSasaran,
                    Kd_IndikatorSasaran: kdIkSasaran,
                    Kd_TujuanOPD: kdTujuanOPD
                };
                $("[name=Kd_IndikatorTujuanOPD]", mainform).load("<?=site_url("ajax/get-opt-iktujuanopd")?>", param, function () {
                    <?php
					if(!empty($data[COL_KD_INDIKATORTUJUANOPD])) {
						?>
						if(<?=!empty($data[COL_KD_INDIKATORTUJUANOPD])?$data[COL_KD_INDIKATORTUJUANOPD]:''?> != '') {
							$("[name=Kd_IndikatorTujuanOPD]", mainform).val(<?=$data[COL_KD_INDIKATORTUJUANOPD]?>);
						}
						<?php
					}
					?>
                    $("[name=Kd_IndikatorTujuanOPD]", mainform).trigger("change");
                });
            }else {
                $("[name=Kd_IndikatorTujuanOPD]", mainform).html("").trigger("change");
            }
        }).trigger('change');

        $('#browseExistingSasaran').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseExistingSasaran"));
            $(this).removeData('bs.modal');

            var param = {
                Kd_Urusan: $("[name=Kd_Urusan]", mainform).val(),
                Kd_Bidang: $("[name=Kd_Bidang]", mainform).val(),
                Kd_Unit: $("[name=Kd_Unit]", mainform).val(),
                Kd_Sub: $("[name=Kd_Sub]", mainform).val()
            };

            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-existing-sasaran-opd")?>", param, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Pemda]").val($(this).val());
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Nm_SasaranOPD]").val($(this).val());
                });
            });
        });

        $('#browseTujuanOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseTujuanOPD"));
            var kdPemda = $("[name=Kd_Pemda]", $("#main-form")).val();
            var kdUrusan = $("[name=Kd_Urusan]", $("#main-form")).val();
            var kdBidang = $("[name=Kd_Bidang]", $("#main-form")).val();
            var kdUnit = $("[name=Kd_Unit]", $("#main-form")).val();
            var kdSub = $("[name=Kd_Sub]", $("#main-form")).val();
            if(!kdUrusan || !kdBidang || !kdUnit || !kdSub) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih OPD terlebih dahulu!</p>");
                return;
            }
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }

            var param = {
                Kd_Pemda: kdPemda,
                Kd_Urusan: kdUrusan,
                Kd_Bidang: kdBidang,
                Kd_Unit: kdUnit,
                Kd_Sub: kdSub
            };

            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-tujuan-opd-2")?>", param, function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var sel = $(this).val().split('|');
                    $("[name=Kd_TujuanOPD]").val(sel[0]);
                    $("[name=Kd_IndikatorTujuanOPD]").val(sel[1]);

                    var prm = {
                        Kd_Pemda: kdPemda,
                        Kd_Urusan: kdUrusan,
                        Kd_Bidang: kdBidang,
                        Kd_Unit: kdUnit,
                        Kd_Sub: kdSub,
                        Kd_TujuanOPD: sel[0],
                        Kd_IndikatorTujuanOPD: sel[1]
                    };
                    $(".div-iku").load("<?=site_url("ajax/get-iku-by-tujuan-opd")?>", prm, function () {

                    });
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    var sel = $(this).val().split('|');
                    $(".text-tujuan-opd").html(sel[0]);
                    $(".text-iktujuan-opd").html(sel[1]);
                });
            });
        });
    </script>
<?php $this->load->view('footer') ?>