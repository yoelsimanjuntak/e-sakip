<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 17:49
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $nmSub = "";
    $eplandb = $this->load->database("eplan", true);
    $eplandb->where(COL_KD_URUSAN, $d[COL_KD_URUSAN]);
    $eplandb->where(COL_KD_BIDANG, $d[COL_KD_BIDANG]);
    $eplandb->where(COL_KD_UNIT, $d[COL_KD_UNIT]);
    $eplandb->where(COL_KD_SUB, $d[COL_KD_SUB]);
    $subunit = $eplandb->get("ref_sub_unit")->row_array();
    if($subunit) {
        $nmSub = $subunit["Nm_Sub_Unit"];
    }

    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d["ID"] . '" />',
        //$d[COL_KD_MISI]." ".$d[COL_NM_MISI],
        //$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN]." ".$d[COL_NM_TUJUAN],
        /*$d[COL_KD_URUSAN].".".$d[COL_KD_BIDANG].".".$d[COL_KD_UNIT].".".$d[COL_KD_SUB]."<br />".*/$nmSub,
        $d[COL_NM_BID],
        /*$d[COL_KD_MISI].".".$d[COL_KD_TUJUAN].".".$d[COL_KD_INDIKATORTUJUAN].".".$d[COL_KD_SASARAN].".".$d[COL_KD_INDIKATORSASARAN]."<br />".*///$d[COL_NM_INDIKATORTUJUANOPD],
        anchor('mopd/subbid-edit/'.$d["ID"],$d[COL_KD_URUSAN].".".$d[COL_KD_BIDANG].".".$d[COL_KD_UNIT].".".$d[COL_KD_SUB].".".$d[COL_KD_BID].".".$d[COL_KD_SUBBID]." ".$d[COL_NM_SUBBID]),
        $d[COL_NM_KASUBBID]
    );
    $i++;
}
$data = json_encode($res);
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Sub Bidang OPD
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('mopd/subbid-del','<i class="fa fa-trash-o"></i> Hapus',array('class'=>'cekboxaction btn btn-danger','confirm'=>'Apa anda yakin?'))
            ?>
            <?=anchor('mopd/subbid-add','<i class="fa fa-plus"></i> Data Baru',array('class'=>'btn btn-primary'))
            ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : 400,
                //"scrollX": "200%",
                "scrollX": true,
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","sWidth":15,bSortable:false},
                    //{"sTitle": "Misi"},
                    //{"sTitle": "Tujuan"},
                    {"sTitle": "OPD"},
                    {"sTitle": "Bidang"},
                    //{"sTitle": "Indikator Tujuan"},
                    {"sTitle": "Sub Bidang"},
                    {"sTitle": "Ka. Sub Bidang"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')
?>