<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 30/09/2018
 * Time: 11:12
 */
$this->load->view('header') ?>
<?php
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mopd/tujuan')?>"> Tujuan</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-periode" value="<?= $edit ? $data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]." : ".$data[COL_NM_PEJABAT] : (!empty($data["DefPeriod"])?$data["DefPeriod"]:"")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= !empty($data[COL_KD_PEMDA])?$data[COL_KD_PEMDA]:"" ?>" required   >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-period" data-toggle="modal" data-target="#browsePeriod" data-toggle="tooltip" data-placement="top" title="Pilih Periode" <?=$edit?"":""?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">IKU Kabupaten</label>
                            <div class="col-sm-8">
                                <?php
                                if(!$edit) {
                                    ?>
                                    <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#browseIKU" data-toggle="tooltip" data-placement="top" title="Tambah IKU">
                                        <i class="fa fa-plus"></i> Tambah
                                    </button>
                                    <?php
                                }
                                ?>
                                <div class="div-iku no-padding" style="margin-top: 10px">
                                    <?php
                                    if($edit) {
                                        $r_iku = $this->db
                                            ->join(TBL_SAKIP_MPMD_MISI, TBL_SAKIP_MPMD_MISI . '.' . COL_KD_PEMDA . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_PEMDA . " AND " . TBL_SAKIP_MPMD_MISI . '.' . COL_KD_MISI . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_MISI, "inner")
                                            ->join(TBL_SAKIP_MPMD_TUJUAN, TBL_SAKIP_MPMD_TUJUAN . '.' . COL_KD_PEMDA . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_PEMDA . " AND " . TBL_SAKIP_MPMD_TUJUAN . '.' . COL_KD_MISI . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_MISI . " AND " . TBL_SAKIP_MPMD_TUJUAN . '.' . COL_KD_TUJUAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_TUJUAN, "inner")
                                            ->join(TBL_SAKIP_MPMD_IKTUJUAN, TBL_SAKIP_MPMD_IKTUJUAN . '.' . COL_KD_PEMDA . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_PEMDA . " AND " . TBL_SAKIP_MPMD_IKTUJUAN . '.' . COL_KD_MISI . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_MISI . " AND " . TBL_SAKIP_MPMD_IKTUJUAN . '.' . COL_KD_TUJUAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_TUJUAN . " AND " . TBL_SAKIP_MPMD_IKTUJUAN . '.' . COL_KD_INDIKATORTUJUAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_INDIKATORTUJUAN, "inner")
                                            ->join(TBL_SAKIP_MPMD_SASARAN, TBL_SAKIP_MPMD_SASARAN . '.' . COL_KD_PEMDA . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_PEMDA . " AND " . TBL_SAKIP_MPMD_SASARAN . '.' . COL_KD_MISI . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_MISI . " AND " . TBL_SAKIP_MPMD_SASARAN . '.' . COL_KD_TUJUAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_TUJUAN . " AND " . TBL_SAKIP_MPMD_SASARAN . '.' . COL_KD_INDIKATORTUJUAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_INDIKATORTUJUAN . " AND " . TBL_SAKIP_MPMD_SASARAN . '.' . COL_KD_SASARAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_SASARAN, "inner")
                                            ->join(TBL_SAKIP_MPMD_IKSASARAN, TBL_SAKIP_MPMD_IKSASARAN . '.' . COL_KD_PEMDA . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_PEMDA . " AND " . TBL_SAKIP_MPMD_IKSASARAN . '.' . COL_KD_MISI . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_MISI . " AND " . TBL_SAKIP_MPMD_IKSASARAN . '.' . COL_KD_TUJUAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_TUJUAN . " AND " . TBL_SAKIP_MPMD_IKSASARAN . '.' . COL_KD_INDIKATORTUJUAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_INDIKATORTUJUAN . " AND " . TBL_SAKIP_MPMD_IKSASARAN . '.' . COL_KD_SASARAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_SASARAN . " AND " . TBL_SAKIP_MPMD_IKSASARAN . '.' . COL_KD_INDIKATORSASARAN . " = " . TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_INDIKATORSASARAN, "inner")
                                            ->where(TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_URUSAN, $data[COL_KD_URUSAN])
                                            ->where(TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_BIDANG, $data[COL_KD_BIDANG])
                                            ->where(TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_UNIT, $data[COL_KD_UNIT])
                                            ->where(TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_SUB, $data[COL_KD_SUB])
                                            ->where(TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_PEMDA, $data[COL_KD_PEMDA])
                                            ->where(TBL_SAKIP_MOPD_TUJUAN . "." . COL_KD_TUJUANOPD, $data[COL_KD_TUJUANOPD])
                                            ->get(TBL_SAKIP_MOPD_TUJUAN)
                                            ->result_array();
                                        foreach ($r_iku as $i) {
                                            ?>
                                            <table class="table table-bordered" style="outline-style: dashed;">
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Misi</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_Misi[]"
                                                               value="<?= $i[COL_KD_MISI] ?>"/><?= $i[COL_KD_MISI] . ". " . $i[COL_NM_MISI] ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Tujuan</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_Tujuan[]"
                                                               value="<?= $i[COL_KD_TUJUAN] ?>"/><?= $i[COL_KD_MISI] . "." . $i[COL_KD_TUJUAN] . ". " . $i[COL_NM_TUJUAN] ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Indikator Tujuan</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_IndikatorTujuan[]"
                                                               value="<?= $i[COL_KD_INDIKATORTUJUAN] ?>"/><?= $i[COL_KD_MISI] . "." . $i[COL_KD_TUJUAN] . "." . $i[COL_KD_INDIKATORTUJUAN] . ". " . $i[COL_NM_INDIKATORTUJUAN] ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Sasaran</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_Sasaran[]"
                                                               value="<?= $i[COL_KD_SASARAN] ?>"/><?= $i[COL_KD_MISI] . "." . $i[COL_KD_TUJUAN] . "." . $i[COL_KD_INDIKATORTUJUAN] . "." . $i[COL_KD_SASARAN] . ". " . $i[COL_NM_SASARAN] ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="control-label" style="width: 200px">Indikator Sasaran</td>
                                                    <td style="width: 10px">:</td>
                                                    <td><input type="hidden" name="Kd_IndikatorSasaran[]"
                                                               value="<?= $i[COL_KD_INDIKATORSASARAN] ?>"/><?= $i[COL_KD_MISI] . "." . $i[COL_KD_TUJUAN] . "." . $i[COL_KD_INDIKATORTUJUAN] . "." . $i[COL_KD_SASARAN] . "." . $i[COL_KD_INDIKATORSASARAN] . ". " . $i[COL_NM_INDIKATORSASARAN] ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">OPD</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php
                                    $nmSub = "";
                                    $strOPD = explode('.', $ruser[COL_COMPANYID]);
                                    if($edit) {

                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $data[COL_KD_URUSAN]);
                                        $eplandb->where(COL_KD_BIDANG, $data[COL_KD_BIDANG]);
                                        $eplandb->where(COL_KD_UNIT, $data[COL_KD_UNIT]);
                                        $eplandb->where(COL_KD_SUB, $data[COL_KD_SUB]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEKADIS) {
                                        $eplandb = $this->load->database("eplan", true);
                                        $eplandb->where(COL_KD_URUSAN, $strOPD[0]);
                                        $eplandb->where(COL_KD_BIDANG, $strOPD[1]);
                                        $eplandb->where(COL_KD_UNIT, $strOPD[2]);
                                        $eplandb->where(COL_KD_SUB, $strOPD[3]);
                                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                                        if($subunit) {
                                            $nmSub = $subunit["Nm_Sub_Unit"];
                                        }
                                    }

                                    ?>
                                    <input type="text" class="form-control" name="text-opd" value="<?= $edit ? $data[COL_KD_URUSAN].".".$data[COL_KD_BIDANG].".".$data[COL_KD_UNIT].".".$data[COL_KD_SUB]." ".$nmSub : ($ruser[COL_ROLEID] == ROLEKADIS ? $strOPD[0].".".$strOPD[1].".".$strOPD[2].".".$strOPD[3]." ".$nmSub : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_URUSAN?>" value="<?= $edit ? $data[COL_KD_URUSAN] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[0]:"")?>" required >
                                    <input type="hidden" name="<?=COL_KD_BIDANG?>" value="<?= $edit ? $data[COL_KD_BIDANG] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[1]:"")?>" required >
                                    <input type="hidden" name="<?=COL_KD_UNIT?>" value="<?= $edit ? $data[COL_KD_UNIT] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[2]:"")?>" required >
                                    <input type="hidden" name="<?=COL_KD_SUB?>" value="<?= $edit ? $data[COL_KD_SUB] : ($ruser[COL_ROLEID]==ROLEKADIS?$strOPD[3]:"")?>" required >
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD" <?=$edit?"":($ruser[COL_ROLEID] == ROLEKADIS ? "disabled" : "")?>><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">No.</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TUJUANOPD?>" value="<?= $edit ? $data[COL_KD_TUJUANOPD] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tujuan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?=COL_NM_TUJUANOPD?>" value="<?= $edit ? $data[COL_NM_TUJUANOPD] : ""?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Indikator Tujuan</label>
                            <div class="col-sm-8">
                                <table class="table table-bordered" id="tbl-det">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th width="80%">Deskripsi</th>
                                        <th style="width: 40px"><button type="button" id="btn-add-det" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="tr-blueprint-det" style="display: none">
                                        <td><input type="text" name="text-det-no" class="form-control" /></td>
                                        <td><input type="text" name="text-det-desc" class="form-control" /></td>
                                        <td>
                                            <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                    $det = $this->db
                                        ->where(COL_KD_URUSAN, ($edit?$data[COL_KD_URUSAN]:-999))
                                        ->where(COL_KD_BIDANG, ($edit?$data[COL_KD_BIDANG]:-999))
                                        ->where(COL_KD_UNIT, ($edit?$data[COL_KD_UNIT]:-999))
                                        ->where(COL_KD_SUB, ($edit?$data[COL_KD_SUB]:-999))

                                        ->where(COL_KD_PEMDA, ($edit?$data[COL_KD_PEMDA]:-999))
                                        ->where(COL_KD_MISI, ($edit?$data[COL_KD_MISI]:-999))
                                        ->where(COL_KD_TUJUAN, ($edit?$data[COL_KD_TUJUAN]:-999))
                                        ->where(COL_KD_INDIKATORTUJUAN, ($edit?$data[COL_KD_INDIKATORTUJUAN]:-999))
                                        ->where(COL_KD_SASARAN, ($edit?$data[COL_KD_SASARAN]:-999))
                                        ->where(COL_KD_INDIKATORSASARAN, ($edit?$data[COL_KD_INDIKATORSASARAN]:-999))
                                        ->where(COL_KD_TUJUANOPD, ($edit?$data[COL_KD_TUJUANOPD]:-999))
                                        ->order_by(COL_KD_INDIKATORTUJUANOPD, 'asc')
                                        ->get(TBL_SAKIP_MOPD_IKTUJUAN)->result_array();
                                    foreach($det as $m) {
                                        ?>
                                        <tr>
                                            <td><input type="text" name="text-det-no" class="form-control" value="<?=$m[COL_KD_INDIKATORTUJUANOPD]?>" /></td>
                                            <td><input type="text" name="text-det-desc" class="form-control" value="<?=$m[COL_NM_INDIKATORTUJUANOPD]?>" /></td>
                                            <td>
                                                <button type="button" class="btn btn-default btn-flat btn-del-det"><i class="fa fa-minus"></i></button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>

                                    <?php
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('mopd/tujuan')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browsePeriod" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseIKU" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse IKU</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer" style="text-align: right">
                    <button type="button" class="btn btn-primary btn-flat" id="btn-select-iku">Pilih</button>
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $(".btn-del-det").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-det").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-det").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-det").show();
            $(".btn-del-det", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });
        $("#main-form").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                var det = [];
                var rowDet = $("#tbl-det>tbody").find("tr:not(.tr-blueprint-det)");

                for (var i = 0; i < rowDet.length; i++) {
                    var noDet = $("[name=text-det-no]", $(rowDet[i])).val();
                    var ketDet = $("[name=text-det-desc]", $(rowDet[i])).val();
                    det.push({ No: noDet, Ket: ketDet });
                }
                $(".appended", $("#main-form")).remove();
                for (var i = 0; i < det.length; i++) {
                    var newEl = "<input type='hidden' class='appended' name=NoDet[" + i + "] value='" + det[i].No + "' /><input type='hidden' class='appended' name=KetDet[" + i + "] value='" + det[i].Ket + "' />";
                    $("#main-form").append(newEl);
                }
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        console.log(a);
                        console.log(b);
                        console.log(c);
                        $(".modal-body", $("#alertDialog")).html("<span style='font-style: italic;'>"+a.status+": "+a.statusText+"</span>");
                        $("#alertDialog").modal('show');
                        //alert('Response Error');
                    }
                });
                return false;
            }
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browsePeriod').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browsePeriod"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-pemda")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=Kd_Pemda]").val($(this).val());

                    $("[name=Kd_Misi]").val("");
                    $("[name=text-misi]").val("");
                    $("[name=Kd_Tujuan]").val("");
                    $("[name=text-tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=text-iktujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=text-sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=text-iksasaran]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-periode]").val($(this).val());
                });
            });
        });

        $('#browseOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseOPD"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdSub = $(this).val().split('|');
                    $("[name=Kd_Urusan]").val(kdSub[0]);
                    $("[name=Kd_Bidang]").val(kdSub[1]);
                    $("[name=Kd_Unit]").val(kdSub[2]);
                    $("[name=Kd_Sub]").val(kdSub[3]);
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-opd]").val($(this).val());
                });
            });
        });

        $('#browseIKU').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseIKU"));
            var kdPemda = $("[name=Kd_Pemda]", $("#main-form")).val();
            if(!kdPemda) {
                modalBody.html("<p style='font-style: italic'>Silakan pilih periode pemerintahan terlebih dahulu!</p>");
                return;
            }

            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-iku-pemda")?>", {Kd_Pemda: kdPemda}, function () {
                $("select", modalBody).not('.no-select2').select2({ width: 'resolve'});
                $("#btn-select-iku").unbind('click').click(function() {
                    var kdMisi = $("[name=Kd_Misi] option:selected", modalBody);
                    var kdTujuan = $("[name=Kd_Tujuan] option:selected", modalBody);
                    var kdIkTujuan = $("[name=Kd_IndikatorTujuan] option:selected", modalBody);
                    var kdSasaran = $("[name=Kd_Sasaran] option:selected", modalBody);
                    var kdIkSasaran = $("[name=Kd_IndikatorSasaran] option:selected", modalBody);

                    if(!kdMisi || !kdTujuan || !kdIkTujuan || !kdSasaran || !kdIkSasaran) {
                        $("[data-dismiss=modal]", $("#browseIKU")).click();
                        return true;
                    }
                    var html = '';
                    html += '<table class="table table-bordered" style="outline-style: dashed;">';
                    html += '<tr><td colspan="3" style="text-align: right"><button type="button" class="btn btn-default btn-sm btn-flat btn-del-iku"><i class="fa fa-minus"></i></button></td></tr>';
                    html += '<tr><td class="control-label">Misi</td><td>:</td><td>'+kdMisi.val()+'. '+kdMisi.html()+'<input type="hidden" name="Kd_Misi[]" value="'+kdMisi.val()+'" /></td></tr>';
                    html += '<tr><td class="control-label">Tujuan</td><td>:</td><td>'+kdMisi.val()+'.'+kdTujuan.val()+'. '+kdTujuan.html()+'<input type="hidden" name="Kd_Tujuan[]" value="'+kdTujuan.val()+'" /></td></tr>';
                    html += '<tr><td class="control-label">Indikator Tujuan</td><td>:</td><td>'+kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'. '+kdIkTujuan.html()+'<input type="hidden" name="Kd_IndikatorTujuan[]" value="'+kdIkTujuan.val()+'" /></td></tr>';
                    html += '<tr><td class="control-label">Sasaran</td><td>:</td><td>'+kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'. '+kdSasaran.html()+'<input type="hidden" name="Kd_Sasaran[]" value="'+kdSasaran.val()+'" /></td></tr>';
                    html += '<tr><td class="control-label">Indikator Sasaran</td><td>:</td><td>'+kdMisi.val()+'.'+kdTujuan.val()+'.'+kdIkTujuan.val()+'.'+kdSasaran.val()+'.'+kdIkSasaran.val()+'. '+kdIkSasaran.html()+'<input type="hidden" name="Kd_IndikatorSasaran[]" value="'+kdIkSasaran.val()+'" /></td></tr>';
                    html += '</table>';
                    var html_ = $(html);
                    html_.appendTo($(".div-iku"));
                    $(".btn-del-iku", html_).click(function() {
                        $(this).closest('table').remove();
                    });

                    $("[data-dismiss=modal]", $("#browseIKU")).click();
                });
            });
        });
    </script>
<?php $this->load->view('footer') ?>