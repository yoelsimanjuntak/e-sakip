<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/4/2019
 * Time: 7:39 PM
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $nmSub = "";
    $eplandb = $this->load->database("eplan", true);
    $eplandb->where(COL_KD_URUSAN, $d[COL_KD_URUSAN]);
    $eplandb->where(COL_KD_BIDANG, $d[COL_KD_BIDANG]);
    $eplandb->where(COL_KD_UNIT, $d[COL_KD_UNIT]);
    $eplandb->where(COL_KD_SUB, $d[COL_KD_SUB]);
    $subunit = $eplandb->get("ref_sub_unit")->row_array();
    if($subunit) {
        $nmSub = $subunit["Nm_Sub_Unit"];
    }

    $nmProg = "";
    /*if($d[COL_ISEPLAN]) {
        $eplandb->where(COL_KD_URUSAN, $d[COL_KD_URUSAN]);
        $eplandb->where(COL_KD_BIDANG, $d[COL_KD_BIDANG]);
        $eplandb->where(COL_KD_UNIT, $d[COL_KD_UNIT]);
        $eplandb->where(COL_KD_SUB, $d[COL_KD_SUB]);
        $eplandb->where("Tahun", $d[COL_KD_TAHUN]-1);
        $eplandb->where("Kd_Prog", $d[COL_KD_PROGRAMOPD]);
        $prog = $eplandb->get("ta_program")->row_array();
        if($prog) {
            $nmProg = $prog["Ket_Prog"];
        }
    } else {
        $nmProg = $d[COL_NM_PROGRAMOPD];
    }*/


    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d["ID"] . '" />',
        $nmSub,
        $d[COL_NM_BID],
        $d[COL_NM_SUBBID],
        //$nmProg,
        anchor('msubbid/sasaran-edit/'.$d["ID"],$d[COL_NM_SASARANSUBBIDANG])
    );
    $i++;
}
$data = json_encode($res);
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Sasaran Sub Bidang OPD
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('msubbid/sasaran-del','<i class="fa fa-trash-o"></i> Hapus',array('class'=>'cekboxaction btn btn-sm btn-danger','confirm'=>'Apa anda yakin?'))
            ?>
            <?=anchor('msubbid/sasaran-add','<i class="fa fa-plus"></i> Data Baru',array('class'=>'btn btn-sm btn-primary'))
            ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : 400,
                //"scrollX": "200%",
                "scrollX": true,
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","sWidth":15,bSortable:false},
                    {"sTitle": "OPD"},
                    {"sTitle": "Bidang"},
                    {"sTitle": "Sub Bidang"},
                    {"sTitle": "Sasaran"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });
    </script>

<?php $this->load->view('footer')
?>