<?php $this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('mwarga/meninggal')?>"> Penduduk Meninggal</a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'deviceForm','class'=>'form-horizontal'))?>
                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i>
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">NIK</label>
                            <div class="col-sm-5">
                                <select name="<?=COL_NIK?>" class="form-control" required>
                                    <?=GetCombobox("SELECT * FROM ".TBL_MWARGA." ORDER BY ".COL_NMANGGOTA, COL_NIK, COL_NMANGGOTA, (!empty($data[COL_NIK]) ? $data[COL_NIK] : null))?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tanggal Kematian</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control datepicker" name="<?= COL_TANGGALKEMATIAN?>" value="<?= $edit ? date("d M Y", strtotime($data[COL_TANGGALKEMATIAN])) : ""?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Sebab Kematian</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" name="<?=COL_SEBABKEMATIAN?>" rows="4"><?= $edit ? $data[COL_SEBABKEMATIAN] : ""?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                <a href="<?=site_url('mdusun/index')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                            </div>

                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $("#deviceForm").validate({
            submitHandler : function(form){
                $(form).find('btn').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox').show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        alert('Response Error');
                    }
                });
                return false;
            }
        });
    </script>
<?php $this->load->view('footer') ?>