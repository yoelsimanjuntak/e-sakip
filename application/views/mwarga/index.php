<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<a href="javascript: viewWarga(\''.site_url('mkeluarga/editanggota/'.$d[COL_NIK]).'\')">'.$d[COL_NIK].'</a>',
        $d[COL_NMANGGOTA],
        $d[COL_JENISKELAMIN],
        $d[COL_TEMPATLAHIR].", ".date('d-m-Y', strtotime($d[COL_TANGGALLAHIR])),
        anchor('mkeluarga/edit/'.$d[COL_KDKELUARGA],$d[COL_KDKELUARGA], array('target' => '_blank'))
    );
    $i++;
}
$data = json_encode($res);
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Penduduk Aktif
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
    </section>

    <div class="modal fade" id="detailDialog" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="min-width: 1080px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                    <h4 class="modal-title">Detail Penduduk</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : 400,
                "scrollX": "200%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "aoColumns": [
                    {"sTitle": "NIK"},
                    {"sTitle": "Nama"},
                    {"sTitle": "Jenis Kelamin"},
                    {"sTitle": "Tempat, Tgl. Lahir"},
                    {"sTitle": "No. KK"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
        });

        function viewWarga(link) {
            var modal = $("#detailDialog").modal("show");
            $(".modal-body", modal).html("<p style='font-style: italic'>Loading...</p>");
            $(".modal-body", modal).load(link, function() {
                $("input,select,textarea", $(".modal-body", modal)).attr("disabled", true);
                $(".form-buttons", $(".modal-body", modal)).hide();
            });
        }
    </script>

<?php $this->load->view('footer')
?>