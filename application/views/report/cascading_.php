<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 3/17/2019
 * Time: 8:58 PM
 */
$arrMisi = array();
$eplandb = $this->load->database("eplan", true);
$arrTujuan = array();
foreach($rtujuan as $t) {
    $arrSasaran = array();
    $sasaran = $this->db
        ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
        ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
        ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
        ->where(COL_KD_SUB, $t[COL_KD_SUB])

        ->where(COL_KD_PEMDA, $t[COL_KD_PEMDA])
        ->where(COL_KD_MISI, $t[COL_KD_MISI])
        ->where(COL_KD_TUJUAN, $t[COL_KD_TUJUAN])
        ->where(COL_KD_INDIKATORTUJUAN, $t[COL_KD_INDIKATORTUJUAN])
        ->where(COL_KD_SASARAN, $t[COL_KD_SASARAN])
        ->where(COL_KD_INDIKATORSASARAN, $t[COL_KD_INDIKATORSASARAN])
        ->where(COL_KD_TUJUANOPD, $t[COL_KD_TUJUANOPD])
        ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc')
        ->get(TBL_SAKIP_MOPD_SASARAN)
        ->result_array();

    foreach($sasaran as $s) {
        $arrSasaranBid = array();
        $sasaranbid = $this->db
            ->where(COL_KD_URUSAN, $s[COL_KD_URUSAN])
            ->where(COL_KD_BIDANG, $s[COL_KD_BIDANG])
            ->where(COL_KD_UNIT, $s[COL_KD_UNIT])
            ->where(COL_KD_SUB, $s[COL_KD_SUB])

            ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $s[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
            ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
            ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
            ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
            ->where(COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
            ->where(COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
            ->where(COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
            ->where(COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN))
            ->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_BID, 'asc')
            ->order_by(TBL_SAKIP_MBID_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc')
            ->get(TBL_SAKIP_MBID_SASARAN)
            ->result_array();

        foreach($sasaranbid as $sbid) {
            $arrSasaranSubbid = array();
            $sasaransubbid = $this->db
                ->where(COL_KD_URUSAN, $sbid[COL_KD_URUSAN])
                ->where(COL_KD_BIDANG, $sbid[COL_KD_BIDANG])
                ->where(COL_KD_UNIT, $sbid[COL_KD_UNIT])
                ->where(COL_KD_SUB, $sbid[COL_KD_SUB])

                ->where(COL_KD_PEMDA, $sbid[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $sbid[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $sbid[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $sbid[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $sbid[COL_KD_SASARAN])
                ->where(COL_KD_INDIKATORSASARAN, $sbid[COL_KD_INDIKATORSASARAN])
                ->where(COL_KD_TUJUANOPD, $sbid[COL_KD_TUJUANOPD])
                ->where(COL_KD_INDIKATORTUJUANOPD, $sbid[COL_KD_INDIKATORTUJUANOPD])
                ->where(COL_KD_SASARANOPD, $sbid[COL_KD_SASARANOPD])
                ->where(COL_KD_INDIKATORSASARANOPD, $sbid[COL_KD_INDIKATORSASARANOPD])
                ->where(COL_KD_SASARANPROGRAMOPD, $sbid[COL_KD_SASARANPROGRAMOPD])
                ->where(COL_KD_TAHUN, $sbid[COL_KD_TAHUN])
                ->where(COL_KD_BID, $sbid[COL_KD_BID])
                ->order_by(TBL_SAKIP_MSUBBID_SASARAN.".".COL_KD_SASARANSUBBIDANG, 'asc')
                ->get(TBL_SAKIP_MSUBBID_SASARAN)
                ->result_array();
            foreach($sasaransubbid as $ssub) {
                $arrSasaranSubbid[] = array(
                    "text" => array("name"=> $ssub[COL_KD_TUJUANOPD].".".$ssub[COL_KD_SASARANOPD].".".$ssub[COL_KD_BID].".".$ssub[COL_KD_SASARANPROGRAMOPD].".".$ssub[COL_KD_SUBBID].".".$ssub[COL_KD_SASARANSUBBIDANG].". Sasaran Subbidang (Eselon 4)", "title"=> $ssub[COL_NM_SASARANSUBBIDANG]),
                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                    //"children" => $arrProgramChild,
                    "HTMLclass" => "bg-fuchsia"
                );
            }

            $arrSasaranBid[] = array(
                "text" => array("name"=> $sbid[COL_KD_TUJUANOPD].".".$sbid[COL_KD_SASARANOPD].".".$sbid[COL_KD_BID].".".$sbid[COL_KD_SASARANPROGRAMOPD].". Sasaran Bidang (Eselon 3)", "title"=> $sbid[COL_NM_SASARANPROGRAMOPD]),
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "children" => $arrSasaranSubbid,
                "HTMLclass" => "bg-orange"
            );
        }

        $arrSasaran[] = array(
            "text" => array("name"=> $s[COL_KD_TUJUANOPD].".".$s[COL_KD_SASARANOPD].". Sasaran OPD (Eselon 2)", "title"=> $s[COL_NM_SASARANOPD]),
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrSasaranBid,
            "HTMLclass" => "bg-lime"
        );
    }

    $arrTujuan[] = array(
        "text" => array("name"=> $t[COL_KD_TUJUANOPD].". Tujuan", "title"=> $t[COL_NM_TUJUANOPD]),
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrSasaran,
        "HTMLclass" => "bg-teal"
    );
}
$nodes = array(
    "text" => array("name"=> "OPD", "title"=> strtoupper($nmSub)),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrTujuan,
    "HTMLclass" => "bg-aqua"
);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? 'E-SAKIP | '.$title : SITENAME?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
    <link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/skins/_all-skins.min.css">
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
</head>
<body>
<style>
    .nodeExample1 {
        border: 1px solid #000;
        padding : 0px !important;
        width : 240px !important;
        font-size: 8pt;
        color: #000 !important;
    }
    .nodeExample1 .node-name {
        font-weight: bold;
        margin: 0 0 5px !important;
        border-bottom: 1px solid #000;
        padding: 2px;
    }
    .nodeExample1 .node-title {
        text-align: justify;
        padding: 2px;
    }
    .chart {
        overflow: auto;
    }
</style>
<h4 style="text-align: center">CASCADING <?=strtoupper($nmSub)?> KABUPATEN HUMBANG HASUNDUTAN</h4><hr />
<div class="chart" id="basic-example">

</div>
<script>
    console.log(<?=json_encode($nodes)?>);
    var chart_config = {
        chart: {
            container: "#basic-example",
            scrollbar: "fancy",
            //animateOnInit: true,
            rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
            connectors: {
                type: "step",
                style: {
                    "stroke-width": 1
                }
            },
            node: {
                HTMLclass: 'nodeExample1'
            },
            nodeAlign: 'TOP'
            /*animation: {
             nodeAnimation: "easeOutBounce",
             nodeSpeed: 700,
             connectorsAnimation: "bounce",
             connectorsSpeed: 700
             }*/
        },
        nodeStructure: <?=json_encode($nodes)?>
    };
    new Treant( chart_config );
</script>
</body>