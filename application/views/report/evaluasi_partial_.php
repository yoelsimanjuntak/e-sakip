<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 17/07/2019
 * Time: 00:43
 */
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=ESAKIP - Evaluasi Kinerja.xls");
}
?>
<style>
    .tbl-info td {
        padding: 4px;
    }
    td.blank {
        color: transparent !important;
    }
</style>

<div class="table-responsive">
    <table class="table table-bordered" style="font-size: 9pt !important; max-width: 200%; overflow-x: scroll" border="1">
        <caption style="text-align: center">
            <h5><?="EVALUASI TERHADAP HASIL RENJA PERANGKAT DAERAH LINGKUP KABUPATEN / KOTA<br /> RENJA PERANGKAT DAERAH ".strtoupper($nmSub)."<br />PERIODE PELAKSANAAN JANUARI s.d DESEMBER ".$data[COL_KD_TAHUN]?>
        </caption>
        <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Sasaran</th>
            <th rowspan="2" style="white-space: nowrap">Program / Kegiatan</th>
            <th rowspan="2">Indikator Kinerja Program (Outcomes) / Kegiatan (Output)</th>
            <th rowspan="2" colspan="3">Target Renstra Perangkat Daerah pada Tahun <?=!empty($rpemda) ? $rpemda[COL_KD_TAHUN_TO] : '-'?> (Akhir Periode Renstra Perangkat Daerah)</th>
            <th rowspan="2" colspan="2">Realisasi Capaian Kinerja Renstra Perangkat Daerah s/d Renja Perangkat daerah</th>
            <th rowspan="2" colspan="2">Target Kinerja dan  Anggaran Renja Perangkat Daerah Tahun Berjalan yang di Evaluasi</th>
            <th colspan="8">Realisasi Kinerja pada Triwulan (Realisasi DPA Tahun <?=$data[COL_KD_TAHUN]?>)</th>
            <th rowspan="2" colspan="2">Realisasi Capaian Kinerja dan Realisasi Anggaran Renja Perangkat Daerah yang di Evaluasi</th>
            <th rowspan="2" colspan="2">Realisasi Kinerja dan Anggaran Renstra Perangkat Daerah s/d tahun <?=$data[COL_KD_TAHUN]?></th>
            <th rowspan="2">Unit Penanggung Jawab</th>
            <th rowspan="2">Keterangan</th>
        </tr>
        <tr>
            <th colspan="2">I</th>
            <th colspan="2">II</th>
            <th colspan="2">II</th>
            <th colspan="2">IV</th>
        </tr>
        <tr>
            <th rowspan="2">1</th>
            <th rowspan="2">2</th>
            <th rowspan="2">3</th>
            <th rowspan="2">4</th>
            <th colspan="3">5</th>
            <th colspan="2">6</th>
            <th colspan="2">7</th>
            <th colspan="2">8</th>
            <th colspan="2">9</th>
            <th colspan="2">10</th>
            <th colspan="2">11</th>
            <th colspan="2">12</th>
            <th colspan="2">13</th>
            <th rowspan="2">14</th>
            <th rowspan="2">15</th>
        </tr>
        <tr>
            <th>Satuan</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
            <th>Kinerja</th>
            <th>Rupiah</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $lastprg = null;
        $last = array(
            'UNIQ_SASARAN_OPD' => "@@",
            'UNIQ_PRG' => "@@",
            'UNIQ_KEGIATAN' => "@@"
        );
        $no_sasaran = 1;
        foreach($rsasaran as $s) {
            $params = array(
                $this->input->post(COL_KD_TAHUN),
                $this->input->post(COL_KD_URUSAN),
                $this->input->post(COL_KD_BIDANG),
                $this->input->post(COL_KD_UNIT),
                $this->input->post(COL_KD_SUB),
                $s[COL_KD_TUJUANOPD],
                $s[COL_KD_INDIKATORTUJUANOPD],
                $s[COL_KD_SASARANOPD]
            );
            $q = @"
            SELECT
            CONCAT(sakip_dpa_program_indikator.Kd_Pemda,'.',
                sakip_dpa_program_indikator.Kd_TujuanOPD,'.',
                sakip_dpa_program_indikator.Kd_IndikatorTujuanOPD,'.',
                sakip_dpa_program_indikator.Kd_SasaranOPD,'.',
                sakip_dpa_program_indikator.Kd_IndikatorSasaranOPD,'.',
                sakip_dpa_program_indikator.Kd_ProgramOPD,'.'
            ) AS UNIQ_PRG,
            (
                SELECT COUNT(DISTINCT
                        i.Kd_Pemda,
                        i.Kd_Urusan,
                        i.Kd_Bidang,
                        i.Kd_Unit,
                        i.Kd_Sub,
                        i.Kd_TujuanOPD,
                        i.Kd_IndikatorTujuanOPD,
                        i.Kd_SasaranOPD,
                        i.Kd_IndikatorSasaranOPD,
                        i.Kd_ProgramOPD,
                        i.Kd_SasaranProgramOPD,
                        i.Kd_IndikatorProgramOPD
                ) FROM sakip_dpa_program_indikator i
                WHERE
                    i.`Kd_Urusan` = sakip_dpa_program.`Kd_Urusan`
                    AND i.`Kd_Bidang` = sakip_dpa_program.`Kd_Bidang`
                    AND i.`Kd_Unit` = sakip_dpa_program.`Kd_Unit`
                    AND i.`Kd_Sub` = sakip_dpa_program.`Kd_Sub`
                    AND i.`Kd_Pemda` = sakip_dpa_program.`Kd_Pemda`
                    AND i.`Kd_TujuanOPD` = sakip_dpa_program.`Kd_TujuanOPD`
                    AND i.`Kd_IndikatorTujuanOPD` = sakip_dpa_program.`Kd_IndikatorTujuanOPD`
                    AND i.`Kd_SasaranOPD` = sakip_dpa_program.`Kd_SasaranOPD`
                    AND i.`Kd_IndikatorSasaranOPD` = sakip_dpa_program.`Kd_IndikatorSasaranOPD`
                    AND i.`Kd_ProgramOPD` = sakip_dpa_program.`Kd_ProgramOPD`
            ) AS count_ikprogram,
            sakip_mopd_sasaran.Nm_SasaranOPD,
            sakip_dpa_program.Kd_Tahun,
            sakip_dpa_program.Nm_ProgramOPD,
            (
                SELECT SUM(sas.Kinerja_TW1 + sas.Kinerja_TW2 + sas.Kinerja_TW3 + sas.Kinerja_TW4) FROM sakip_dpa_program_indikator sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` >= sakip_mpemda.`Kd_Tahun_From`
                    AND sas.`Kd_Tahun` < sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS kinerja_akumulasi,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW1, 0) + IFNULL(sas.Anggaran_TW2, 0) + IFNULL(sas.Anggaran_TW3, 0) + IFNULL(sas.Anggaran_TW4, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` >= sakip_mpemda.`Kd_Tahun_From`
                    AND sas.`Kd_Tahun` < sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_akumulasi,
            (
                SELECT SUM(IFNULL(sas.Budget, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS budget_program,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW1, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_tw1_program,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW2, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_tw2_program,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW3, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_tw3_program,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW4, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND sas.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
            ) AS anggaran_tw4_program,
            (
                SELECT SUM(IFNULL(keg.Total, 0)) FROM sakip_msubbid_kegiatan keg
                WHERE
                    keg.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND keg.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND keg.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND keg.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND keg.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND keg.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                    AND keg.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND keg.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND keg.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND keg.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND keg.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND keg.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND keg.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND keg.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND keg.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND keg.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                    AND keg.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
                    AND keg.`Kd_KegiatanOPD` in (
                      select DISTINCT(dpa_keg.Kd_KegiatanOPD) from sakip_dpa_kegiatan dpa_keg
                      where dpa_keg.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                        AND dpa_keg.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                        AND dpa_keg.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                        AND dpa_keg.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                        AND dpa_keg.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                        AND dpa_keg.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                        AND dpa_keg.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                        AND dpa_keg.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                        AND dpa_keg.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                        AND dpa_keg.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                        AND dpa_keg.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                        AND dpa_keg.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                        AND dpa_keg.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                        AND dpa_keg.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                        AND dpa_keg.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                        AND dpa_keg.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                        AND dpa_keg.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
                    )
            ) AS anggaran_renstra,
            #sakip_mbid_program_sasaran.Akhir as target_renstra,
            (
                SELECT SUM(IFNULL(prog.Target, 0)) FROM sakip_mbid_program_indikator prog
                WHERE
                    prog.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                    AND prog.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                    AND prog.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                    AND prog.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                    AND prog.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                    AND prog.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                    AND prog.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                    AND prog.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                    AND prog.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                    AND prog.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                    AND prog.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                    AND prog.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                    AND prog.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                    AND prog.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                    AND prog.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    AND prog.`Kd_ProgramOPD` in (
                      select DISTINCT(dpa_prog.Kd_ProgramOPD) from sakip_dpa_program dpa_prog
                      where dpa_prog.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                        AND dpa_prog.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                        AND dpa_prog.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                        AND dpa_prog.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                        AND dpa_prog.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                        AND dpa_prog.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                        AND dpa_prog.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                        AND dpa_prog.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                        AND dpa_prog.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                        AND dpa_prog.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                        AND dpa_prog.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                        AND dpa_prog.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                        AND dpa_prog.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                        AND dpa_prog.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                        AND dpa_prog.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                    )
                    AND prog.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
                    AND prog.`Kd_IndikatorProgramOPD` = sakip_dpa_program_indikator.`Kd_IndikatorProgramOPD`
            ) as target_renstra,
            sakip_mbid.Nm_Bid,
            sakip_dpa_program_indikator.*
            FROM sakip_dpa_program_indikator
            LEFT JOIN sakip_mpemda
                ON sakip_mpemda.Kd_Pemda = sakip_dpa_program_indikator.Kd_Pemda
            LEFT JOIN sakip_mbid
                ON sakip_mbid.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                AND sakip_mbid.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                AND sakip_mbid.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                AND sakip_mbid.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                AND sakip_mbid.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
            LEFT JOIN sakip_mopd_sasaran
                ON sakip_mopd_sasaran.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                AND sakip_mopd_sasaran.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                AND sakip_mopd_sasaran.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                AND sakip_mopd_sasaran.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                AND sakip_mopd_sasaran.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                AND sakip_mopd_sasaran.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                AND sakip_mopd_sasaran.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                AND sakip_mopd_sasaran.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                AND sakip_mopd_sasaran.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                AND sakip_mopd_sasaran.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                AND sakip_mopd_sasaran.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                AND sakip_mopd_sasaran.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_mopd_sasaran.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
            LEFT JOIN sakip_dpa_program
                ON sakip_dpa_program.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                AND sakip_dpa_program.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                AND sakip_dpa_program.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                AND sakip_dpa_program.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                AND sakip_dpa_program.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                AND sakip_dpa_program.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                AND sakip_dpa_program.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                AND sakip_dpa_program.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                AND sakip_dpa_program.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                AND sakip_dpa_program.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                AND sakip_dpa_program.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                AND sakip_dpa_program.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                AND sakip_dpa_program.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_dpa_program.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                AND sakip_dpa_program.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                AND sakip_dpa_program.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                AND sakip_dpa_program.`Kd_Tahun` = sakip_dpa_program_indikator.`Kd_Tahun`
            LEFT JOIN sakip_mbid_program_sasaran
                ON sakip_mbid_program_sasaran.`Kd_Urusan` = sakip_dpa_program_indikator.`Kd_Urusan`
                AND sakip_mbid_program_sasaran.`Kd_Bidang` = sakip_dpa_program_indikator.`Kd_Bidang`
                AND sakip_mbid_program_sasaran.`Kd_Unit` = sakip_dpa_program_indikator.`Kd_Unit`
                AND sakip_mbid_program_sasaran.`Kd_Sub` = sakip_dpa_program_indikator.`Kd_Sub`
                AND sakip_mbid_program_sasaran.`Kd_Bid` = sakip_dpa_program_indikator.`Kd_Bid`
                AND sakip_mbid_program_sasaran.`Kd_Pemda` = sakip_dpa_program_indikator.`Kd_Pemda`
                AND sakip_mbid_program_sasaran.`Kd_Misi` = sakip_dpa_program_indikator.`Kd_Misi`
                AND sakip_mbid_program_sasaran.`Kd_Tujuan` = sakip_dpa_program_indikator.`Kd_Tujuan`
                AND sakip_mbid_program_sasaran.`Kd_IndikatorTujuan` = sakip_dpa_program_indikator.`Kd_IndikatorTujuan`
                AND sakip_mbid_program_sasaran.`Kd_Sasaran` = sakip_dpa_program_indikator.`Kd_Sasaran`
                AND sakip_mbid_program_sasaran.`Kd_IndikatorSasaran` = sakip_dpa_program_indikator.`Kd_IndikatorSasaran`
                AND sakip_mbid_program_sasaran.`Kd_TujuanOPD` = sakip_dpa_program_indikator.`Kd_TujuanOPD`
                AND sakip_mbid_program_sasaran.`Kd_IndikatorTujuanOPD` = sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_mbid_program_sasaran.`Kd_SasaranOPD` = sakip_dpa_program_indikator.`Kd_SasaranOPD`
                AND sakip_mbid_program_sasaran.`Kd_IndikatorSasaranOPD` = sakip_dpa_program_indikator.`Kd_IndikatorSasaranOPD`
                AND sakip_mbid_program_sasaran.`Kd_ProgramOPD` = sakip_dpa_program_indikator.`Kd_ProgramOPD`
                AND sakip_mbid_program_sasaran.`Kd_Tahun` = sakip_mpemda.Kd_Tahun_To
                AND sakip_mbid_program_sasaran.`Kd_SasaranProgramOPD` = sakip_dpa_program_indikator.`Kd_SasaranProgramOPD`
                AND sakip_mbid_program_sasaran.`Kd_Satuan` = sakip_dpa_program_indikator.`Kd_Satuan`
            WHERE
                sakip_dpa_program_indikator.`Kd_Tahun` = ?
                AND sakip_dpa_program_indikator.`Kd_Urusan` = ?
                AND sakip_dpa_program_indikator.`Kd_Bidang` = ?
                AND sakip_dpa_program_indikator.`Kd_Unit` = ?
                AND sakip_dpa_program_indikator.`Kd_Sub` = ?
                AND sakip_dpa_program_indikator.`Kd_TujuanOPD` = ?
                AND sakip_dpa_program_indikator.`Kd_IndikatorTujuanOPD` = ?
                AND sakip_dpa_program_indikator.`Kd_SasaranOPD` = ?
            group by
                sakip_dpa_program_indikator.Kd_TujuanOPD,
                sakip_dpa_program_indikator.Kd_IndikatorTujuanOPD,
                sakip_dpa_program_indikator.Kd_SasaranOPD,
                sakip_dpa_program_indikator.Kd_IndikatorSasaranOPD,
                sakip_dpa_program_indikator.Kd_ProgramOPD,
                sakip_dpa_program_indikator.Kd_SasaranProgramOPD,
                sakip_dpa_program_indikator.Kd_IndikatorProgramOPD
            ORDER BY
                sakip_dpa_program_indikator.Kd_ProgramOPD,
                sakip_dpa_program_indikator.Kd_SasaranProgramOPD,
                sakip_dpa_program_indikator.Kd_IndikatorProgramOPD
            ";
            $rprogram = $this->db->query($q, $params)->result_array();
            $idx_prg = 1;
            foreach($rprogram as $prg) {
                if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                    $idx_prg = 1;
                } else {
                    $idx_prg++;
                }
                ?>
                <tr style="font-weight: bold">
                    <?php
                    if($s['UNIQ_SASARAN_OPD'] != $last['UNIQ_SASARAN_OPD']) {
                        ?>
                        <td <?=$s['count_ikkegiatan']+$s['count_ikprogram']>1?'rowspan="'.($s['count_ikkegiatan']+$s['count_ikprogram']).'"':''?>><?=$no_sasaran?></td>
                        <td <?=$s['count_ikkegiatan']+$s['count_ikprogram']>1?'rowspan="'.($s['count_ikkegiatan']+$s['count_ikprogram']).'"':''?>><?=$s[COL_NM_SASARANOPD]?></td>
                        <?php
                        $no_sasaran++;
                        $last['UNIQ_SASARAN_OPD'] = $s['UNIQ_SASARAN_OPD'];
                    }
                    ?>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?>><?=$prg[COL_NM_PROGRAMOPD]?></td>
                        <?php
                    }
                    ?>
                    <td><?=$prg[COL_NM_INDIKATORPROGRAMOPD]?></td>
                    <td><?=$prg[COL_KD_SATUAN]?></td>
                    <td style="text-align: right"><?=number_format($prg["target_renstra"], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg["anggaran_renstra"], 0)?></td>
                        <?php
                    }
                    ?>
                    <td style="text-align: right"><?=number_format($prg["kinerja_akumulasi"], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg["anggaran_akumulasi"], 0)?></td>
                    <?php
                    }
                    ?>
                    <td style="text-align: right"><?=number_format($prg[COL_TARGET], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg['budget_program'], 0)?></td>
                    <?php
                    }
                    ?>
                    <td style="text-align: right"><?=number_format($prg[COL_KINERJA_TW1], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg['anggaran_tw1_program'], 0)?></td>
                    <?php
                    }
                    ?>
                    <td style="text-align: right"><?=number_format($prg[COL_KINERJA_TW2], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg['anggaran_tw2_program'], 0)?></td>
                    <?php
                    }
                    ?>
                    <td style="text-align: right"><?=number_format($prg[COL_KINERJA_TW3], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg['anggaran_tw3_program'], 0)?></td>
                    <?php
                    }
                    ?>
                    <td style="text-align: right"><?=number_format($prg[COL_KINERJA_TW4], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg['anggaran_tw4_program'], 0)?></td>
                    <?php
                    }
                    ?>
                    <td style="text-align: right"><?=number_format($prg[COL_KINERJA_TW1]+$prg[COL_KINERJA_TW2]+$prg[COL_KINERJA_TW3]+$prg[COL_KINERJA_TW4], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg['anggaran_tw1_program']+$prg['anggaran_tw2_program']+$prg['anggaran_tw3_program']+$prg['anggaran_tw4_program'], 0)?></td>
                    <?php
                    }
                    ?>
                    <td style="text-align: right"><?=number_format($prg[COL_KINERJA_TW1]+$prg[COL_KINERJA_TW2]+$prg[COL_KINERJA_TW3]+$prg[COL_KINERJA_TW4]+$prg["kinerja_akumulasi"], 2)?></td>
                    <?php
                    if($prg['UNIQ_PRG'] != $last['UNIQ_PRG']) {
                        ?>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?> style="text-align: right"><?=number_format($prg['anggaran_tw1_program']+$prg['anggaran_tw2_program']+$prg['anggaran_tw3_program']+$prg['anggaran_tw4_program']+$prg["anggaran_akumulasi"], 0)?></td>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?>><?='Kepala '.$prg[COL_NM_BID]?></td>
                        <td <?=$prg['count_ikprogram']>1?'rowspan="'.($prg['count_ikprogram']).'"':''?>></td>
                    <?php
                        $last['UNIQ_PRG'] = $prg['UNIQ_PRG'];
                        $lastprg = $prg;
                    }
                    ?>
                </tr>
                <?php
                if($idx_prg == $prg['count_ikprogram']) {
                    $qkegiatan = @"
            SELECT
            CONCAT(sakip_dpa_kegiatan_indikator.Kd_Pemda,'.',
                sakip_dpa_kegiatan_indikator.Kd_Misi,'.',
                sakip_dpa_kegiatan_indikator.Kd_Tujuan,'.',
                sakip_dpa_kegiatan_indikator.Kd_IndikatorTujuan,'.',
                sakip_dpa_kegiatan_indikator.Kd_Sasaran,'.',
                sakip_dpa_kegiatan_indikator.Kd_IndikatorSasaran,'.',
                sakip_dpa_kegiatan_indikator.Kd_TujuanOPD,'.',
                sakip_dpa_kegiatan_indikator.Kd_IndikatorTujuanOPD,'.',
                sakip_dpa_kegiatan_indikator.Kd_SasaranOPD,'.',
                sakip_dpa_kegiatan_indikator.Kd_IndikatorSasaranOPD,'.',
                sakip_dpa_kegiatan_indikator.Kd_ProgramOPD,'.',
                sakip_dpa_kegiatan_indikator.Kd_SasaranProgramOPD,'.',
                sakip_dpa_kegiatan_indikator.Kd_KegiatanOPD,'.',
                sakip_dpa_kegiatan_indikator.Kd_Urusan,'.',
                sakip_dpa_kegiatan_indikator.Kd_Bidang,'.',
                sakip_dpa_kegiatan_indikator.Kd_Unit,'.',
                sakip_dpa_kegiatan_indikator.Kd_Sub,'.',
                sakip_dpa_kegiatan_indikator.Kd_Bid,'.',
                sakip_dpa_kegiatan_indikator.Kd_Subbid
            ) AS UNIQ_KEGIATAN,
            sakip_dpa_kegiatan.Nm_KegiatanOPD,
            (
                SELECT COUNT(*) FROM sakip_dpa_kegiatan_indikator
                WHERE
                    sakip_dpa_kegiatan_indikator.`Kd_Urusan` = sakip_dpa_kegiatan.`Kd_Urusan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Bidang` = sakip_dpa_kegiatan.`Kd_Bidang`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Unit` = sakip_dpa_kegiatan.`Kd_Unit`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Sub` = sakip_dpa_kegiatan.`Kd_Sub`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Bid` = sakip_dpa_kegiatan.`Kd_Bid`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Subbid` = sakip_dpa_kegiatan.`Kd_Subbid`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Misi` = sakip_dpa_kegiatan.`Kd_Misi`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Tujuan` = sakip_dpa_kegiatan.`Kd_Tujuan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan` = sakip_dpa_kegiatan.`Kd_IndikatorTujuan`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Sasaran` = sakip_dpa_kegiatan.`Kd_Sasaran`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran` = sakip_dpa_kegiatan.`Kd_IndikatorSasaran`
                    AND sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD` = sakip_dpa_kegiatan.`Kd_TujuanOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD` = sakip_dpa_kegiatan.`Kd_IndikatorTujuanOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD` = sakip_dpa_kegiatan.`Kd_SasaranOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD` = sakip_dpa_kegiatan.`Kd_IndikatorSasaranOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD` = sakip_dpa_kegiatan.`Kd_ProgramOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_Tahun` = sakip_dpa_kegiatan.`Kd_Tahun`
                    AND sakip_dpa_kegiatan_indikator.`Kd_SasaranProgramOPD` = sakip_dpa_kegiatan.`Kd_SasaranProgramOPD`
                    AND sakip_dpa_kegiatan_indikator.`Kd_KegiatanOPD` = sakip_dpa_kegiatan.`Kd_KegiatanOPD`
            ) AS span_kegiatan,
            (
                SELECT SUM(IFNULL(sas.Kinerja_TW1, 0) + IFNULL(sas.Kinerja_TW2, 0) + IFNULL(sas.Kinerja_TW3, 0) + IFNULL(sas.Kinerja_TW4, 0)) FROM sakip_dpa_kegiatan_indikator sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_kegiatan_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_kegiatan_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_kegiatan_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_kegiatan_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_kegiatan_indikator.`Kd_Bid`
                    AND sas.`Kd_Subbid` = sakip_dpa_kegiatan_indikator.`Kd_Subbid`
                    AND sas.`Kd_Misi` = sakip_dpa_kegiatan_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_kegiatan_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_kegiatan_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` >= sakip_mpemda.`Kd_Tahun_From`
                    AND sas.`Kd_Tahun` < sakip_dpa_kegiatan_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranProgramOPD`
                    AND sas.`Kd_KegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_KegiatanOPD`
                    #AND sas.`Kd_SasaranKegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranKegiatanOPD`
            ) AS kinerja_akumulasi,
            (
                SELECT SUM(IFNULL(sas.Anggaran_TW1, 0) + IFNULL(sas.Anggaran_TW2, 0) + IFNULL(sas.Anggaran_TW3, 0) + IFNULL(sas.Anggaran_TW4, 0)) FROM sakip_dpa_kegiatan sas
                WHERE
                    sas.`Kd_Urusan` = sakip_dpa_kegiatan_indikator.`Kd_Urusan`
                    AND sas.`Kd_Bidang` = sakip_dpa_kegiatan_indikator.`Kd_Bidang`
                    AND sas.`Kd_Unit` = sakip_dpa_kegiatan_indikator.`Kd_Unit`
                    AND sas.`Kd_Sub` = sakip_dpa_kegiatan_indikator.`Kd_Sub`
                    AND sas.`Kd_Bid` = sakip_dpa_kegiatan_indikator.`Kd_Bid`
                    AND sas.`Kd_Subbid` = sakip_dpa_kegiatan_indikator.`Kd_Subbid`
                    AND sas.`Kd_Misi` = sakip_dpa_kegiatan_indikator.`Kd_Misi`
                    AND sas.`Kd_Tujuan` = sakip_dpa_kegiatan_indikator.`Kd_Tujuan`
                    AND sas.`Kd_IndikatorTujuan` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan`
                    AND sas.`Kd_Sasaran` = sakip_dpa_kegiatan_indikator.`Kd_Sasaran`
                    AND sas.`Kd_IndikatorSasaran` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran`
                    AND sas.`Kd_TujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD`
                    AND sas.`Kd_IndikatorTujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD`
                    AND sas.`Kd_SasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD`
                    AND sas.`Kd_IndikatorSasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD`
                    AND sas.`Kd_ProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD`
                    AND sas.`Kd_Tahun` >= sakip_mpemda.`Kd_Tahun_From`
                    AND sas.`Kd_Tahun` < sakip_dpa_kegiatan_indikator.`Kd_Tahun`
                    AND sas.`Kd_SasaranProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranProgramOPD`
                    AND sas.`Kd_KegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_KegiatanOPD`
                    #AND sas.`Kd_SasaranKegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranKegiatanOPD`
            ) AS anggaran_akumulasi,
            (
                SELECT SUM(IFNULL(keg.Total, 0)) FROM sakip_msubbid_kegiatan keg
                WHERE
                    keg.`Kd_Urusan` = sakip_dpa_kegiatan_indikator.`Kd_Urusan`
                    AND keg.`Kd_Bidang` = sakip_dpa_kegiatan_indikator.`Kd_Bidang`
                    AND keg.`Kd_Unit` = sakip_dpa_kegiatan_indikator.`Kd_Unit`
                    AND keg.`Kd_Sub` = sakip_dpa_kegiatan_indikator.`Kd_Sub`
                    AND keg.`Kd_Bid` = sakip_dpa_kegiatan_indikator.`Kd_Bid`
                    AND keg.`Kd_Subbid` = sakip_dpa_kegiatan_indikator.`Kd_Subbid`
                    AND keg.`Kd_Pemda` = sakip_dpa_kegiatan_indikator.`Kd_Pemda`
                    AND keg.`Kd_Misi` = sakip_dpa_kegiatan_indikator.`Kd_Misi`
                    AND keg.`Kd_Tujuan` = sakip_dpa_kegiatan_indikator.`Kd_Tujuan`
                    AND keg.`Kd_IndikatorTujuan` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan`
                    AND keg.`Kd_Sasaran` = sakip_dpa_kegiatan_indikator.`Kd_Sasaran`
                    AND keg.`Kd_IndikatorSasaran` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran`
                    AND keg.`Kd_TujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD`
                    AND keg.`Kd_IndikatorTujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD`
                    AND keg.`Kd_SasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD`
                    AND keg.`Kd_IndikatorSasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD`
                    AND keg.`Kd_ProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD`
                    AND keg.`Kd_SasaranProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranProgramOPD`
                    AND keg.`Kd_KegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_KegiatanOPD`
            ) AS anggaran_renstra,
            #sakip_msubbid_kegiatan_sasaran.Akhir AS target_renstra,
            (
                SELECT SUM(IFNULL(keg.Target, 0)) FROM sakip_msubbid_kegiatan_indikator keg
                WHERE
                    keg.`Kd_Urusan` = sakip_dpa_kegiatan_indikator.`Kd_Urusan`
                    AND keg.`Kd_Bidang` = sakip_dpa_kegiatan_indikator.`Kd_Bidang`
                    AND keg.`Kd_Unit` = sakip_dpa_kegiatan_indikator.`Kd_Unit`
                    AND keg.`Kd_Sub` = sakip_dpa_kegiatan_indikator.`Kd_Sub`
                    AND keg.`Kd_Bid` = sakip_dpa_kegiatan_indikator.`Kd_Bid`
                    AND keg.`Kd_Subbid` = sakip_dpa_kegiatan_indikator.`Kd_Subbid`
                    AND keg.`Kd_Pemda` = sakip_dpa_kegiatan_indikator.`Kd_Pemda`
                    AND keg.`Kd_Misi` = sakip_dpa_kegiatan_indikator.`Kd_Misi`
                    AND keg.`Kd_Tujuan` = sakip_dpa_kegiatan_indikator.`Kd_Tujuan`
                    AND keg.`Kd_IndikatorTujuan` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan`
                    AND keg.`Kd_Sasaran` = sakip_dpa_kegiatan_indikator.`Kd_Sasaran`
                    AND keg.`Kd_IndikatorSasaran` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran`
                    AND keg.`Kd_TujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD`
                    AND keg.`Kd_IndikatorTujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD`
                    AND keg.`Kd_SasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD`
                    AND keg.`Kd_IndikatorSasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD`
                    AND keg.`Kd_ProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD`
                    AND keg.`Kd_SasaranProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranProgramOPD`
                    AND keg.`Kd_KegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_KegiatanOPD`
                    AND keg.`Kd_SasaranKegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranKegiatanOPD`
                    AND keg.`Kd_IndikatorKegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorKegiatanOPD`
            ) AS target_renstra,
            sakip_msubbid.Nm_Subbid,
            sakip_dpa_kegiatan_indikator.*,
            sakip_dpa_kegiatan.Budget,
            sakip_dpa_kegiatan.Anggaran_TW1 as Anggaran_TW1,
            sakip_dpa_kegiatan.Anggaran_TW2 as Anggaran_TW2,
            sakip_dpa_kegiatan.Anggaran_TW3 as Anggaran_TW3,
            sakip_dpa_kegiatan.Anggaran_TW4 as Anggaran_TW4
            FROM sakip_dpa_kegiatan_indikator
            LEFT JOIN sakip_mpemda
                ON sakip_mpemda.Kd_Pemda = sakip_dpa_kegiatan_indikator.Kd_Pemda
            LEFT JOIN sakip_msubbid
                ON sakip_msubbid.`Kd_Urusan` = sakip_dpa_kegiatan_indikator.`Kd_Urusan`
                AND sakip_msubbid.`Kd_Bidang` = sakip_dpa_kegiatan_indikator.`Kd_Bidang`
                AND sakip_msubbid.`Kd_Unit` = sakip_dpa_kegiatan_indikator.`Kd_Unit`
                AND sakip_msubbid.`Kd_Sub` = sakip_dpa_kegiatan_indikator.`Kd_Sub`
                AND sakip_msubbid.`Kd_Bid` = sakip_dpa_kegiatan_indikator.`Kd_Bid`
                AND sakip_msubbid.`Kd_Subbid` = sakip_dpa_kegiatan_indikator.`Kd_Subbid`
            LEFT JOIN sakip_mopd_sasaran
                ON sakip_mopd_sasaran.`Kd_Urusan` = sakip_dpa_kegiatan_indikator.`Kd_Urusan`
                AND sakip_mopd_sasaran.`Kd_Bidang` = sakip_dpa_kegiatan_indikator.`Kd_Bidang`
                AND sakip_mopd_sasaran.`Kd_Unit` = sakip_dpa_kegiatan_indikator.`Kd_Unit`
                AND sakip_mopd_sasaran.`Kd_Sub` = sakip_dpa_kegiatan_indikator.`Kd_Sub`
                AND sakip_mopd_sasaran.`Kd_Pemda` = sakip_dpa_kegiatan_indikator.`Kd_Pemda`
                AND sakip_mopd_sasaran.`Kd_Misi` = sakip_dpa_kegiatan_indikator.`Kd_Misi`
                AND sakip_mopd_sasaran.`Kd_Tujuan` = sakip_dpa_kegiatan_indikator.`Kd_Tujuan`
                AND sakip_mopd_sasaran.`Kd_IndikatorTujuan` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan`
                AND sakip_mopd_sasaran.`Kd_Sasaran` = sakip_dpa_kegiatan_indikator.`Kd_Sasaran`
                AND sakip_mopd_sasaran.`Kd_IndikatorSasaran` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran`
                AND sakip_mopd_sasaran.`Kd_TujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD`
                AND sakip_mopd_sasaran.`Kd_IndikatorTujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_mopd_sasaran.`Kd_SasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD`
            LEFT JOIN sakip_dpa_kegiatan
                ON sakip_dpa_kegiatan.`Kd_Urusan` = sakip_dpa_kegiatan_indikator.`Kd_Urusan`
                AND sakip_dpa_kegiatan.`Kd_Bidang` = sakip_dpa_kegiatan_indikator.`Kd_Bidang`
                AND sakip_dpa_kegiatan.`Kd_Unit` = sakip_dpa_kegiatan_indikator.`Kd_Unit`
                AND sakip_dpa_kegiatan.`Kd_Sub` = sakip_dpa_kegiatan_indikator.`Kd_Sub`
                AND sakip_dpa_kegiatan.`Kd_Bid` = sakip_dpa_kegiatan_indikator.`Kd_Bid`
                AND sakip_dpa_kegiatan.`Kd_Subbid` = sakip_dpa_kegiatan_indikator.`Kd_Subbid`
                AND sakip_dpa_kegiatan.`Kd_Pemda` = sakip_dpa_kegiatan_indikator.`Kd_Pemda`
                AND sakip_dpa_kegiatan.`Kd_Misi` = sakip_dpa_kegiatan_indikator.`Kd_Misi`
                AND sakip_dpa_kegiatan.`Kd_Tujuan` = sakip_dpa_kegiatan_indikator.`Kd_Tujuan`
                AND sakip_dpa_kegiatan.`Kd_IndikatorTujuan` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan`
                AND sakip_dpa_kegiatan.`Kd_Sasaran` = sakip_dpa_kegiatan_indikator.`Kd_Sasaran`
                AND sakip_dpa_kegiatan.`Kd_IndikatorSasaran` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran`
                AND sakip_dpa_kegiatan.`Kd_TujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD`
                AND sakip_dpa_kegiatan.`Kd_IndikatorTujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_dpa_kegiatan.`Kd_SasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD`
                AND sakip_dpa_kegiatan.`Kd_IndikatorSasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD`
                AND sakip_dpa_kegiatan.`Kd_ProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD`
                AND sakip_dpa_kegiatan.`Kd_Tahun` = sakip_dpa_kegiatan_indikator.`Kd_Tahun`
                AND sakip_dpa_kegiatan.`Kd_SasaranProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranProgramOPD`
                AND sakip_dpa_kegiatan.`Kd_KegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_KegiatanOPD`
            LEFT JOIN sakip_msubbid_kegiatan_sasaran
                ON sakip_msubbid_kegiatan_sasaran.`Kd_Urusan` = sakip_dpa_kegiatan_indikator.`Kd_Urusan`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Bidang` = sakip_dpa_kegiatan_indikator.`Kd_Bidang`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Unit` = sakip_dpa_kegiatan_indikator.`Kd_Unit`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Sub` = sakip_dpa_kegiatan_indikator.`Kd_Sub`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Bid` = sakip_dpa_kegiatan_indikator.`Kd_Bid`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Subbid` = sakip_dpa_kegiatan_indikator.`Kd_Subbid`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Pemda` = sakip_dpa_kegiatan_indikator.`Kd_Pemda`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Misi` = sakip_dpa_kegiatan_indikator.`Kd_Misi`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Tujuan` = sakip_dpa_kegiatan_indikator.`Kd_Tujuan`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_IndikatorTujuan` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Sasaran` = sakip_dpa_kegiatan_indikator.`Kd_Sasaran`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_IndikatorSasaran` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_TujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_IndikatorTujuanOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_SasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_IndikatorSasaranOPD` = sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_ProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Tahun` = sakip_mpemda.Kd_Tahun_To
                AND sakip_msubbid_kegiatan_sasaran.`Kd_SasaranProgramOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranProgramOPD`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_KegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_KegiatanOPD`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_SasaranKegiatanOPD` = sakip_dpa_kegiatan_indikator.`Kd_SasaranKegiatanOPD`
                AND sakip_msubbid_kegiatan_sasaran.`Kd_Satuan` = sakip_dpa_kegiatan_indikator.`Kd_Satuan`
            WHERE
                sakip_dpa_kegiatan_indikator.`Kd_Tahun` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_Urusan` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_Bidang` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_Unit` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_Sub` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_Bid` = ?

                AND sakip_dpa_kegiatan_indikator.`Kd_Pemda` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_Misi` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_Tujuan` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuan` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_Sasaran` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaran` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_TujuanOPD` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorTujuanOPD` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_SasaranOPD` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_IndikatorSasaranOPD` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_ProgramOPD` = ?
                AND sakip_dpa_kegiatan_indikator.`Kd_SasaranProgramOPD` = ?
            ORDER BY
                sakip_dpa_kegiatan_indikator.Kd_KegiatanOPD,
                sakip_dpa_kegiatan_indikator.Kd_SasaranKegiatanOPD,
                sakip_dpa_kegiatan_indikator.Kd_IndikatorKegiatanOPD
            ";
                    $params = array(
                        $lastprg[COL_KD_TAHUN],
                        $lastprg[COL_KD_URUSAN],
                        $lastprg[COL_KD_BIDANG],
                        $lastprg[COL_KD_UNIT],
                        $lastprg[COL_KD_SUB],
                        $lastprg[COL_KD_BID],
                        $lastprg[COL_KD_PEMDA],
                        $lastprg[COL_KD_MISI],
                        $lastprg[COL_KD_TUJUAN],
                        $lastprg[COL_KD_INDIKATORTUJUAN],
                        $lastprg[COL_KD_SASARAN],
                        $lastprg[COL_KD_INDIKATORSASARAN],
                        $lastprg[COL_KD_TUJUANOPD],
                        $lastprg[COL_KD_INDIKATORTUJUANOPD],
                        $lastprg[COL_KD_SASARANOPD],
                        $lastprg[COL_KD_INDIKATORSASARANOPD],
                        $lastprg[COL_KD_PROGRAMOPD],
                        $lastprg[COL_KD_SASARANPROGRAMOPD]
                    );
                    $rkegiatan = $this->db->query($qkegiatan, $params)->result_array();
                    foreach($rkegiatan as $keg) {
                        ?>
                        <tr>
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?>><?=$keg[COL_NM_KEGIATANOPD]?></td>
                            <?php
                            }
                            ?>
                            <td><?=$keg[COL_NM_INDIKATORKEGIATANOPD]?></td>
                            <td><?=$keg[COL_KD_SATUAN]?></td>
                            <td style="text-align: right"><?=number_format($keg["target_renstra"], 2)?></td>
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?> style="text-align: right"><?=number_format($keg['anggaran_renstra'], 0)?></td>
                            <?php
                            }
                            ?>
                            <td style="text-align: right"><?=number_format($keg["kinerja_akumulasi"], 2)?></td>
                            <td style="text-align: right"><?=number_format($keg["anggaran_akumulasi"], 0)?></td>
                            <td style="text-align: right"><?=number_format($keg[COL_TARGET], 2)?></td>
                            <!--<td style="text-align: right"><?=number_format($keg[COL_BUDGET], 0)?></td>-->
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?> style="text-align: right"><?=number_format($keg[COL_BUDGET], 0)?></td>
                            <?php
                            }
                            ?>
                            <td style="text-align: right"><?=number_format($keg[COL_KINERJA_TW1], 2)?></td>
                            <!--<td style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW1], 0)?></td>-->
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?> style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW1], 0)?></td>
                            <?php
                            }
                            ?>
                            <td style="text-align: right"><?=number_format($keg[COL_KINERJA_TW2], 2)?></td>
                            <!--<td style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW2], 0)?></td>-->
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?> style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW2], 0)?></td>
                            <?php
                            }
                            ?>
                            <td style="text-align: right"><?=number_format($keg[COL_KINERJA_TW3], 2)?></td>
                            <!--<td style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW3], 0)?></td>-->
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?> style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW3], 0)?></td>
                            <?php
                            }
                            ?>
                            <td style="text-align: right"><?=number_format($keg[COL_KINERJA_TW4], 2)?></td>
                            <!--<td style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW4], 0)?></td>-->
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?> style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW4], 0)?></td>
                            <?php
                            }
                            ?>
                            <td style="text-align: right"><?=number_format($keg[COL_KINERJA_TW1]+$keg[COL_KINERJA_TW2]+$keg[COL_KINERJA_TW3]+$keg[COL_KINERJA_TW4], 2)?></td>
                            <!--<td style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW1]+$keg[COL_ANGGARAN_TW2]+$keg[COL_ANGGARAN_TW3]+$keg[COL_ANGGARAN_TW4], 0)?></td>-->
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?> style="text-align: right"><?=number_format($keg[COL_ANGGARAN_TW1]+$keg[COL_ANGGARAN_TW2]+$keg[COL_ANGGARAN_TW3]+$keg[COL_ANGGARAN_TW4], 0)?></td>
                            <?php
                            }
                            ?>
                            <td style="text-align: right"><?=number_format($keg[COL_KINERJA_TW1]+$keg[COL_KINERJA_TW2]+$keg[COL_KINERJA_TW3]+$keg[COL_KINERJA_TW4]+$keg["kinerja_akumulasi"], 0)?></td>
                            <!--<td style="text-align: right"><?=number_format(($keg[COL_ANGGARAN_TW1]+$keg[COL_ANGGARAN_TW2]+$keg[COL_ANGGARAN_TW3]+$keg[COL_ANGGARAN_TW4]+$keg["anggaran_akumulasi"]), 0)?></td>-->
                            <?php
                            if($keg['UNIQ_KEGIATAN'] != $last['UNIQ_KEGIATAN']) {
                                ?>
                                <td <?=$keg['span_kegiatan']>1?'rowspan="'.($keg['span_kegiatan']).'"':''?> style="text-align: right"><?=number_format(($keg[COL_ANGGARAN_TW1]+$keg[COL_ANGGARAN_TW2]+$keg[COL_ANGGARAN_TW3]+$keg[COL_ANGGARAN_TW4]+$keg["anggaran_akumulasi"]), 0)?></td>
                            <?php
                            }
                            ?>
                            <td><?='Kepala '.$keg[COL_NM_SUBBID]?></td>
                            <td>-</td>
                        </tr>
                        <?php
                        $last['UNIQ_KEGIATAN'] = $keg['UNIQ_KEGIATAN'];
                    }
                }
            }
        }
        ?>
        </tbody>
    </table>
</div>