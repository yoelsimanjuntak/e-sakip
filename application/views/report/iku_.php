<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 05/07/2019
 * Time: 16:09
 */
?>
<style>
    body {
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    }
    th, td {
        padding: 5px;
    }
</style>
<html>
<head>
    <title>INDIKATOR KINERJA UTAMA</title>
</head>
<body>
<?php
$ruser = GetLoggedUser();
$kd_urusan = $this->input->get(COL_KD_URUSAN);
$kd_bidang = $this->input->get(COL_KD_BIDANG);
$kd_unit = $this->input->get(COL_KD_UNIT);
$kd_sub = $this->input->get(COL_KD_SUB);
$kd_bid = $this->input->get(COL_KD_BID);
$kd_subbid = $this->input->get(COL_KD_SUBBID);
$pagebreak = false;
if(!empty($kd_urusan) && !empty($kd_bidang) && !empty($kd_unit) && !empty($kd_sub) && ((empty($kd_bid) && empty($kd_subbid)) || $ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID)) {
    $pagebreak = true;
    ?>
    <table width="100%">
        <tr>
            <td colspan="2" style="text-align: center">
                <img class="user-image" src="<?=MY_IMAGEURL?>/logo.jpg" style="width: 60px" alt="Logo">
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; vertical-align: middle">
                <h4>INDIKATOR KINERJA UTAMA</h4>
                <h4><?=strtoupper($nmSub)?> KABUPATEN HUMBANG HASUNDUTAN</h4>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left; vertical-align: middle">
                <table style="border: 1px solid #000; border-spacing: 0;page-break-after:always" border="1" width="100%">
                    <tr>
                        <th>No.</th>
                        <th>Kinerja Utama / Sasaran</th>
                        <th>Indikator Kinerja Utama</th>
                        <th>Penjelasan / Formulasi Perhitungan</th>
                        <th>Sumber Data</th>
                        <th>Penanggung Jawab</th>
                    </tr>
                    <?php
                    $this->db->select("*,
                    concat(
                        sakip_mopd_sasaran.Kd_Pemda,
                        sakip_mopd_sasaran.Kd_TujuanOPD,
                        sakip_mopd_sasaran.Kd_IndikatorTujuanOPD,
                        sakip_mopd_sasaran.Kd_SasaranOPD
                    ) as UNIQ_SASARAN,
                    (SELECT COUNT(DISTINCT

                        iks.Kd_TujuanOPD,
                        iks.Kd_IndikatorTujuanOPD,
                        iks.Kd_SasaranOPD,
                        iks.Kd_IndikatorSasaranOPD
                    ) FROM sakip_mopd_iksasaran iks
                    left join sakip_mopd_sasaran s on
                        s.`Kd_Urusan` = iks.`Kd_Urusan`
                      AND s.`Kd_Bidang` = iks.`Kd_Bidang`
                      AND s.`Kd_Unit` = iks.`Kd_Unit`
                      AND s.`Kd_Sub` = iks.`Kd_Sub`
                      AND s.`Kd_Pemda` = iks.`Kd_Pemda`
                      /*AND s.`Kd_Misi` = iks.`Kd_Misi`
                      AND s.`Kd_Tujuan` = iks.`Kd_Tujuan`
                      AND s.`Kd_IndikatorTujuan` = iks.`Kd_IndikatorTujuan`
                      AND s.`Kd_Sasaran` = iks.`Kd_Sasaran`
                      AND s.`Kd_IndikatorSasaran` = iks.`Kd_IndikatorSasaran`*/
                      AND s.`Kd_TujuanOPD` = iks.`Kd_TujuanOPD`
                      AND s.`Kd_IndikatorTujuanOPD` = iks.`Kd_IndikatorTujuanOPD`
                      AND s.`Kd_SasaranOPD` = iks.`Kd_SasaranOPD`
                        WHERE iks.`Kd_Urusan` = `sakip_mopd_sasaran`.`Kd_Urusan`
                        AND iks.`Kd_Bidang` = `sakip_mopd_sasaran`.`Kd_Bidang`
                        AND iks.`Kd_Unit` = `sakip_mopd_sasaran`.`Kd_Unit`
                        AND iks.`Kd_Sub` = `sakip_mopd_sasaran`.`Kd_Sub`
                        AND iks.`Kd_Pemda` = `sakip_mopd_sasaran`.`Kd_Pemda`
                        /*AND iks.`Kd_Misi` = `sakip_mopd_sasaran`.`Kd_Misi`
                        AND iks.`Kd_Tujuan` = `sakip_mopd_sasaran`.`Kd_Tujuan`
                        AND iks.`Kd_IndikatorTujuan` = `sakip_mopd_sasaran`.`Kd_IndikatorTujuan`
                        AND iks.`Kd_Sasaran` = `sakip_mopd_sasaran`.`Kd_Sasaran`
                        AND iks.`Kd_IndikatorSasaran` = `sakip_mopd_sasaran`.`Kd_IndikatorSasaran`*/
                        AND iks.`Kd_TujuanOPD` = `sakip_mopd_sasaran`.`Kd_TujuanOPD`
                        AND iks.`Kd_IndikatorTujuanOPD` = `sakip_mopd_sasaran`.`Kd_IndikatorTujuanOPD`
                        AND s.`Kd_SasaranOPD` = `sakip_mopd_sasaran`.`Kd_SasaranOPD`
                        #GROUP BY iks.Nm_IndikatorSasaranOPD
                    ) AS count_indikator");
                    $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA,"inner");
                    $this->db->join(TBL_SAKIP_MOPD_IKSASARAN,
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_URUSAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_BIDANG." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_UNIT." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SUB." AND ".

                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_PEMDA." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_MISI." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                        //TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TAHUN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD
                        ,"left");
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
                    $this->db->where(TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
                    $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD, 'asc');
                    $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc');
                    $this->db->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_NM_SASARANOPD, 'asc');
                    $this->db->order_by(TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD, 'asc');
                    $this->db->group_by(array(
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_PEMDA,
                        /*TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_MISI,
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUAN,
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUAN,
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARAN,
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARAN,*/
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_TUJUANOPD,
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORTUJUANOPD,
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_SASARANOPD,
                        TBL_SAKIP_MOPD_IKSASARAN.".".COL_KD_INDIKATORSASARANOPD
                    ));
                    $rsasaran = $this->db->get(TBL_SAKIP_MOPD_SASARAN)->result_array();
                    $counter = 1;
                    $last = "";
                    foreach($rsasaran as $s) {
                        ?>
                        <tr>
                            <?php
                            if($last != $s["UNIQ_SASARAN"]) {
                                ?>
                                <td <?=$s["count_indikator"]>1?'rowspan="'.$s["count_indikator"].'"':''?> style="text-align: center"><?=$counter?></td>
                                <td <?=$s["count_indikator"]>1?'rowspan="'.$s["count_indikator"].'"':''?>><?=$s[COL_NM_SASARANOPD]?></td>
                                <?php
                                $counter++;
                            }
                            ?>
                            <td><?=$s[COL_NM_INDIKATORSASARANOPD]?></td>
                            <td><?=$s[COL_NM_FORMULA]?></td>
                            <td><?=$s[COL_NM_SUMBERDATA]?></td>
                            <td><?='Kepala '.$nmSub?></td>
                        </tr>
                        <?php
                        $last = $s["UNIQ_SASARAN"];
                    }
                    ?>
                </table>
            </td>
        </tr>
    </table>
<?php
}

if(!empty($kd_urusan) && !empty($kd_bidang) && !empty($kd_unit) && !empty($kd_sub) && ((!empty($kd_bid) && empty($kd_subbid)) || $ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID)) {
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
    $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
    if(!empty($kd_bid)) $this->db->where(TBL_SAKIP_MBID.'.'.COL_KD_BID, $this->input->get(COL_KD_BID));
    $this->db->order_by(TBL_SAKIP_MBID.".".COL_KD_BID, 'asc');
    $rbid = $this->db->get(TBL_SAKIP_MBID)->result_array();
    foreach($rbid as $bid) {
        ?>
        <?=$pagebreak?'<pagebreak></pagebreak>':''?>
        <table width="100%">
            <tr>
                <td colspan="2" style="text-align: center">
                    <img class="user-image" src="<?= MY_IMAGEURL ?>logo.png" style="width: 60px" alt="Logo">
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle">
                    <h4>INDIKATOR KINERJA PROGRAM <?= $data[COL_KD_TAHUN] ?></h4>
                    <h4><?=strtoupper($bid[COL_NM_BID])?></h4>
                    <h4><?=strtoupper($nmSub)?> KABUPATEN HUMBANG HASUNDUTAN</h4>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left; vertical-align: middle">
                    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
                        <tr>
                            <th>No.</th>
                            <th>Kinerja Utama / Sasaran</th>
                            <th>Indikator Kinerja Utama</th>
                            <th>Penjelasan / Formulasi Perhitungan</th>
                            <th>Sumber Data</th>
                            <th>Penanggung Jawab</th>
                        </tr>
                        <?php
                        $this->db->select("*,
                        concat(
                            sakip_mbid_program_indikator.Kd_Pemda,
                            sakip_mbid_program_indikator.Kd_TujuanOPD,
                            sakip_mbid_program_indikator.Kd_IndikatorTujuanOPD,
                            sakip_mbid_program_indikator.Kd_SasaranOPD,
                            sakip_mbid_program_indikator.Kd_IndikatorSasaranOPD,
                            '.',
                            sakip_mbid_program_indikator.Kd_ProgramOPD,
                            '.',
                            sakip_mbid_program_indikator.Kd_SasaranProgramOPD,
                            sakip_mbid_program_indikator.Kd_IndikatorProgramOPD
                        ) as UNIQ_,
                        (SELECT COUNT(DISTINCT
                            prg_ik.Kd_TujuanOPD,
                            prg_ik.Kd_IndikatorTujuanOPD,
                            prg_ik.Kd_IndikatorSasaranOPD,
                            prg_ik.Kd_SasaranOPD,
                            prg_ik.Kd_ProgramOPD,
                            prg_ik.Kd_SasaranProgramOPD,
                            prg_ik.Kd_IndikatorProgramOPD
                            ) FROM sakip_mbid_program_indikator prg_ik
                        left join sakip_mbid_program prg on
                            prg.`Kd_Urusan` = prg_ik.`Kd_Urusan`
                          AND prg.`Kd_Bidang` = prg_ik.`Kd_Bidang`
                          AND prg.`Kd_Unit` = prg_ik.`Kd_Unit`
                          AND prg.`Kd_Sub` = prg_ik.`Kd_Sub`
                          AND prg.`Kd_Bid` = prg_ik.`Kd_Bid`
                          AND prg.`Kd_Pemda` = prg_ik.`Kd_Pemda`
                          AND prg.`Kd_Misi` = prg_ik.`Kd_Misi`
                          AND prg.`Kd_Tujuan` = prg_ik.`Kd_Tujuan`
                          AND prg.`Kd_IndikatorTujuan` = prg_ik.`Kd_IndikatorTujuan`
                          AND prg.`Kd_Sasaran` = prg_ik.`Kd_Sasaran`
                          AND prg.`Kd_IndikatorSasaran` = prg_ik.`Kd_IndikatorSasaran`
                          AND prg.`Kd_TujuanOPD` = prg_ik.`Kd_TujuanOPD`
                          AND prg.`Kd_IndikatorTujuanOPD` = prg_ik.`Kd_IndikatorTujuanOPD`
                          AND prg.`Kd_SasaranOPD` = prg_ik.`Kd_SasaranOPD`
                          AND prg.`Kd_IndikatorSasaranOPD` = prg_ik.`Kd_IndikatorSasaranOPD`
                          AND prg.`Kd_ProgramOPD` = prg_ik.`Kd_ProgramOPD`
                          AND prg.`Kd_Tahun` = prg_ik.`Kd_Tahun`
                            WHERE prg_ik.`Kd_Urusan` = `sakip_mbid_program`.`Kd_Urusan`
                            AND prg_ik.`Kd_Bidang` = `sakip_mbid_program`.`Kd_Bidang`
                            AND prg_ik.`Kd_Unit` = `sakip_mbid_program`.`Kd_Unit`
                            AND prg_ik.`Kd_Sub` = `sakip_mbid_program`.`Kd_Sub`
                            AND prg_ik.`Kd_Bid` = `sakip_mbid_program`.`Kd_Bid`
                            AND prg_ik.`Kd_Pemda` = `sakip_mbid_program`.`Kd_Pemda`
                            /*AND prg_ik.`Kd_Misi` = `sakip_mbid_program`.`Kd_Misi`*/
                            AND prg.`Kd_ProgramOPD` = `sakip_mbid_program`.`Kd_ProgramOPD`
                        ) AS count_indikator");
                        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,"inner");
                        $this->db->join(TBL_SAKIP_MBID,
                            TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                            TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                            TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                            TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".
                            TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID
                            ,"inner");
                        $this->db->join(TBL_SAKIP_MBID_PROGRAM_SASARAN,
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_URUSAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BIDANG." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_UNIT." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SUB." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_BID." AND ".

                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_MISI." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TAHUN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_INDIKATORSASARANOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_SASARAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD
                            ,"left");
                        $this->db->join(TBL_SAKIP_MBID_PROGRAM_INDIKATOR,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_URUSAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BIDANG." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_UNIT." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_SUB." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SUB." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_BID." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_BID." AND ".

                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PEMDA." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_MISI." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_MISI." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TAHUN." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD
                            ,"left");
                        $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
                        $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN));
                        $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
                        $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
                        $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
                        $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
                        $this->db->where(TBL_SAKIP_MBID_PROGRAM.'.'.COL_KD_BID, $bid[COL_KD_BID]);
                        $this->db->where(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD." != ", null);
                        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc');
                        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_SASARAN.".".COL_KD_SASARANPROGRAMOPD, 'asc');
                        $this->db->order_by(TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD, 'asc');
                        $this->db->group_by(array(
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_PEMDA,
                            /*TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_MISI,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_TUJUAN,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORTUJUAN,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_SASARAN,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORSASARAN,*/
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_TUJUANOPD,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORTUJUANOPD,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_SASARANOPD,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORSASARANOPD,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_PROGRAMOPD,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORPROGRAMOPD
                        ));
                        $rprogram = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->result_array();
                        $counter = 1;
                        $last = "";
                        foreach($rprogram as $s) {
                            if($s["count_indikator"]<=0) continue;
                            ?>
                            <tr>
                                <?php
                                if($last != $s[COL_KD_PROGRAMOPD]) {
                                    ?>
                                    <td <?=$s["count_indikator"]>1?'rowspan="'.$s["count_indikator"].'"':''?> style="text-align: center"><?=$counter?></td>
                                    <td <?=$s["count_indikator"]>1?'rowspan="'.$s["count_indikator"].'"':''?>><?=$s[COL_NM_PROGRAMOPD]?></td>
                                    <?php
                                    $counter++;
                                }
                                ?>
                                <td><?=$s[COL_NM_INDIKATORPROGRAMOPD]?></td>
                                <td><?=$s[COL_NM_FORMULA]?></td>
                                <td><?=$s[COL_NM_SUMBERDATA]?></td>
                                <td><?='Kepala '.$s[COL_NM_BID]?></td>
                            </tr>
                            <?php
                            $last = $s[COL_KD_PROGRAMOPD];
                        }
                        ?>
                    </table>
                </td>
            </tr>
        </table>
    <?php
        $pagebreak = true;
    }
}
if(!empty($kd_urusan) && !empty($kd_bidang) && !empty($kd_unit) && !empty($kd_sub) && ((!empty($kd_bid) && !empty($kd_subbid)) || $ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEKADIS || $ruser[COL_ROLEID] == ROLEKABID)) {
    $this->db->join(TBL_SAKIP_MBID,
        TBL_SAKIP_MBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID.".".COL_KD_URUSAN." AND ".
        TBL_SAKIP_MBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID.".".COL_KD_BIDANG." AND ".
        TBL_SAKIP_MBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID.".".COL_KD_UNIT." AND ".
        TBL_SAKIP_MBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID.".".COL_KD_SUB." AND ".
        TBL_SAKIP_MBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID.".".COL_KD_BID,"inner");
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
    $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
    if(!empty($kd_bid)) $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_BID, $this->input->get(COL_KD_BID));
    if(!empty($kd_subbid)) $this->db->where(TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID, $this->input->get(COL_KD_SUBBID));
    $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_BID, 'asc');
    $this->db->order_by(TBL_SAKIP_MSUBBID.".".COL_KD_SUBBID, 'asc');
    $rsubbid = $this->db->get(TBL_SAKIP_MSUBBID)->result_array();
    foreach($rsubbid as $sbid) {
        ?>
        <?=$pagebreak?'<pagebreak></pagebreak>':''?>
        <table width="100%">
            <tr>
                <td colspan="2" style="text-align: center">
                    <img class="user-image" src="<?= MY_IMAGEURL ?>logo.png" style="width: 60px" alt="Logo">
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle">
                    <h4>INDIKATOR KINERJA KEGIATAN <?= $data[COL_KD_TAHUN] ?></h4>
                    <h4><?=strtoupper($sbid[COL_NM_SUBBID])?></h4>
                    <h4><?=strtoupper($nmSub)?> KABUPATEN HUMBANG HASUNDUTAN</h4>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left; vertical-align: middle">
                    <table style="border: 1px solid #000; border-spacing: 0" border="1" width="100%">
                        <tr>
                            <th>No.</th>
                            <th>Kinerja Utama / Sasaran</th>
                            <th>Indikator Kinerja Utama</th>
                            <th>Penjelasan / Formulasi Perhitungan</th>
                            <th>Sumber Data</th>
                            <th>Penanggung Jawab</th>
                        </tr>
                        <?php
                        $this->db->select("*,
                        concat(
                            sakip_msubbid_kegiatan_indikator.Kd_ProgramOPD,
                            '.',
                            sakip_msubbid_kegiatan_indikator.Kd_SasaranProgramOPD,
                            sakip_msubbid_kegiatan_indikator.Kd_KegiatanOPD
                        ) as UNIQ_,
                        (SELECT COUNT(DISTINCT
                            keg_ik.Kd_TujuanOPD,
                            keg_ik.Kd_IndikatorTujuanOPD,
                            keg_ik.Kd_IndikatorSasaranOPD,
                            keg_ik.Kd_SasaranOPD,
                            keg_ik.Kd_indikatorSasaranOPD,
                            keg_ik.Kd_ProgramOPD,
                            keg_ik.Kd_SasaranProgramOPD,
                            keg_ik.Kd_KegiatanOPD,
                            keg_ik.Kd_IndikatorKegiatanOPD
                        ) FROM sakip_msubbid_kegiatan_indikator keg_ik
                        left join sakip_msubbid_kegiatan keg on
                            keg.`Kd_Urusan` = keg_ik.`Kd_Urusan`
                          AND keg.`Kd_Bidang` = keg_ik.`Kd_Bidang`
                          AND keg.`Kd_Unit` = keg_ik.`Kd_Unit`
                          AND keg.`Kd_Sub` = keg_ik.`Kd_Sub`
                          AND keg.`Kd_Bid` = keg_ik.`Kd_Bid`
                          AND keg.`Kd_Subbid` = keg_ik.`Kd_Subbid`
                          AND keg.`Kd_Pemda` = keg_ik.`Kd_Pemda`
                          AND keg.`Kd_Misi` = keg_ik.`Kd_Misi`
                          AND keg.`Kd_Tujuan` = keg_ik.`Kd_Tujuan`
                          AND keg.`Kd_IndikatorTujuan` = keg_ik.`Kd_IndikatorTujuan`
                          AND keg.`Kd_Sasaran` = keg_ik.`Kd_Sasaran`
                          AND keg.`Kd_IndikatorSasaran` = keg_ik.`Kd_IndikatorSasaran`
                          AND keg.`Kd_TujuanOPD` = keg_ik.`Kd_TujuanOPD`
                          AND keg.`Kd_IndikatorTujuanOPD` = keg_ik.`Kd_IndikatorTujuanOPD`
                          AND keg.`Kd_SasaranOPD` = keg_ik.`Kd_SasaranOPD`
                          AND keg.`Kd_IndikatorSasaranOPD` = keg_ik.`Kd_IndikatorSasaranOPD`
                          AND keg.`Kd_ProgramOPD` = keg_ik.`Kd_ProgramOPD`
                          AND keg.`Kd_Tahun` = keg_ik.`Kd_Tahun`
                          AND keg.`Kd_SasaranProgramOPD` = keg_ik.`Kd_SasaranProgramOPD`
                          AND keg.`Kd_KegiatanOPD` = keg_ik.`Kd_KegiatanOPD`
                            WHERE keg_ik.`Kd_Urusan` = `sakip_msubbid_kegiatan`.`Kd_Urusan`
                            AND keg_ik.`Kd_Bidang` = `sakip_msubbid_kegiatan`.`Kd_Bidang`
                            AND keg_ik.`Kd_Unit` = `sakip_msubbid_kegiatan`.`Kd_Unit`
                            AND keg_ik.`Kd_Sub` = `sakip_msubbid_kegiatan`.`Kd_Sub`
                            AND keg_ik.`Kd_Bid` = `sakip_msubbid_kegiatan`.`Kd_Bid`
                            AND keg_ik.`Kd_Subbid` = `sakip_msubbid_kegiatan`.`Kd_Subbid`
                            AND keg_ik.`Kd_Pemda` = `sakip_msubbid_kegiatan`.`Kd_Pemda`
                            #AND keg_ik.`Kd_Misi` = `sakip_msubbid_kegiatan`.`Kd_Misi`
                            AND keg.`Kd_ProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_ProgramOPD`
                            AND keg.`Kd_SasaranProgramOPD` = `sakip_msubbid_kegiatan`.`Kd_SasaranProgramOPD`
                            AND keg.`Kd_KegiatanOPD` = `sakip_msubbid_kegiatan`.`Kd_KegiatanOPD`
                        ) AS count_indikator");
                        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA,"inner");
                        $this->db->join(TBL_SAKIP_MSUBBID,
                            TBL_SAKIP_MSUBBID.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
                            TBL_SAKIP_MSUBBID.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
                            TBL_SAKIP_MSUBBID.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
                            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".
                            TBL_SAKIP_MSUBBID.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID." AND ".
                            TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUBBID
                            ,"inner");
                        $this->db->join(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN,
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_URUSAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BIDANG." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_UNIT." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUB." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_BID." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SUBBID." AND ".

                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_MISI." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TAHUN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_TUJUANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PROGRAMOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.'.'.COL_KD_KEGIATANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_KEGIATANOPD
                            ,"left");
                        $this->db->join(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR,
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_URUSAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BIDANG." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_UNIT." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_UNIT." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_SUB." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUB." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_BID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_BID." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_SUBBID." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SUBBID." AND ".

                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PEMDA." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_MISI." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_MISI." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARAN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_TAHUN." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TAHUN." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_TUJUANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_INDIKATORSASARANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_PROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_PROGRAMOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_SASARANPROGRAMOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANPROGRAMOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_KEGIATANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_KEGIATANOPD." AND ".
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.'.'.COL_KD_SASARANKEGIATANOPD." = ".TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANKEGIATANOPD
                            ,"left");
                        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_PEMDA, $rpemda[COL_KD_PEMDA]);
                        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN));
                        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_URUSAN, $this->input->get(COL_KD_URUSAN));
                        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BIDANG, $this->input->get(COL_KD_BIDANG));
                        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_UNIT, $this->input->get(COL_KD_UNIT));
                        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUB, $this->input->get(COL_KD_SUB));
                        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_BID, $sbid[COL_KD_BID]);
                        $this->db->where(TBL_SAKIP_MSUBBID_KEGIATAN.'.'.COL_KD_SUBBID, $sbid[COL_KD_SUBBID]);
                        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_KEGIATANOPD, 'asc');
                        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_SASARAN.".".COL_KD_SASARANKEGIATANOPD, 'asc');
                        $this->db->order_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD, 'asc');
                        //$this->db->group_by(TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_NM_INDIKATORKEGIATANOPD, 'asc');
                        $this->db->group_by(array(
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_PEMDA,
                            /*TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_MISI,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_TUJUAN,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORTUJUAN,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_SASARAN,
                            TBL_SAKIP_MBID_PROGRAM_INDIKATOR.".".COL_KD_INDIKATORSASARAN,*/
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_TUJUANOPD,
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORTUJUANOPD,
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_SASARANOPD,
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORSASARANOPD,
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_PROGRAMOPD,
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_SASARANPROGRAMOPD,
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_KEGIATANOPD,
                            TBL_SAKIP_MSUBBID_KEGIATAN_INDIKATOR.".".COL_KD_INDIKATORKEGIATANOPD
                        ));
                        $rkeg = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN)->result_array();
                        $counter = 1;
                        $last = "";
                        foreach($rkeg as $s) {
                            if($s["count_indikator"]<=0) continue;
                            ?>
                            <tr>
                                <?php
                                if($last != $s["UNIQ_"]) {
                                    ?>
                                    <td <?=$s["count_indikator"]>1?'rowspan="'.$s["count_indikator"].'"':''?> style="text-align: center"><?=$counter?></td>
                                    <td <?=$s["count_indikator"]>1?'rowspan="'.$s["count_indikator"].'"':''?>><?=$s[COL_NM_KEGIATANOPD]?></td>
                                    <?php
                                    $counter++;
                                }
                                ?>
                                <td><?=$s[COL_NM_INDIKATORKEGIATANOPD]?></td>
                                <td><?=$s[COL_NM_FORMULA]?></td>
                                <td><?=$s[COL_NM_SUMBERDATA]?></td>
                                <td><?='Kepala '.$s[COL_NM_SUBBID]?></td>
                            </tr>
                            <?php
                            $last = $s["UNIQ_"];
                        }
                        ?>
                    </table>
                </td>
            </tr>
        </table>
    <?php
        $pagebreak = true;
    }
}
?>
</body>
</html>
