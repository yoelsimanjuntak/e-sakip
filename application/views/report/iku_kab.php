<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 8/28/2019
 * Time: 11:28 PM
 */
$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <section class="content-header">
        <h1> <?= $title ?> <small> Generate</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">IKU Kabupaten</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Periode</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="text-periode" value="<?= $edit ? $data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]." : ".$data[COL_NM_PEJABAT] : ($rpemda ? $rpemda[COL_KD_TAHUN_FROM]." s.d ".$rpemda[COL_KD_TAHUN_TO]." : ".$rpemda[COL_NM_PEJABAT] : "")?>" readonly>
                                    <input type="hidden" name="<?=COL_KD_PEMDA?>" value="<?= $edit ? $data[COL_KD_PEMDA] : ($rpemda ? $rpemda[COL_KD_PEMDA] : "")?>" required />
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-flat btn-browse-period" data-toggle="modal" data-target="#browsePeriod" data-toggle="tooltip" data-placement="top" title="Pilih Periode" ><i class="fa fa-ellipsis-h"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Tahun</label>
                            <div class="col-sm-2">
                                <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?= $edit ? $data[COL_KD_TAHUN] : date("Y")?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10" style="text-align: right">
                                <button type="submit" class="btn btn-success btn-flat" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> Cetak</button>
                                <button type="submit" class="btn btn-primary btn-flat" title="Lihat"><i class="fa fa-arrow-circle-right"></i> Lihat</button>
                            </div>
                        </div>
                        <?=form_close()?>
                        <?php if(!empty($data)) $this->load->view('report/iku_kab_'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="browsePeriod" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $('.modal').on('hidden.bs.modal', function (event) {
                $(this).find(".modal-body").empty();
            });

            $('#browsePeriod').on('show.bs.modal', function (event) {
                var modalBody = $(".modal-body", $("#browsePeriod"));
                $(this).removeData('bs.modal');
                modalBody.html("<p style='font-style: italic'>Loading..</p>");
                modalBody.load("<?=site_url("ajax/browse-pemda")?>", function () {
                    $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=Kd_Pemda]").val($(this).val());

                        $("[name=Kd_Misi]").val("");
                        $("[name=text-misi]").val("");
                        $("[name=Kd_Tujuan]").val("");
                        $("[name=text-tujuan]").val("");
                        $("[name=Kd_IndikatorTujuan]").val("");
                        $("[name=text-iktujuan]").val("");
                        $("[name=Kd_Sasaran]").val("");
                        $("[name=text-sasaran]").val("");
                        $("[name=Kd_IndikatorSasaran]").val("");
                        $("[name=text-iksasaran]").val("");
                    });
                    $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                        $("[name=text-periode]").val($(this).val());
                    });
                });
            });
        });
    </script>
<?php $this->load->view('footer') ?>