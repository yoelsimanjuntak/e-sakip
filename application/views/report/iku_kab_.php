<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 8/28/2019
 * Time: 11:29 PM
 */
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=ESAKIP - IKU Kabupaten.xls");
}
$eplandb = $this->load->database("eplan", true);
?>
<div class="table-responsive">
    <table class="table table-bordered" style="font-size: 9pt !important;" border="1">
        <caption style="text-align: center">
            <h5>INDIKATOR KINERJA UTAMA KABUPATEN HUMBANG HASUNDUTAN PERIODE <?=$data[COL_KD_TAHUN_FROM]." s.d ".$data[COL_KD_TAHUN_TO]?></h5>
        </caption>
        <thead>
        <tr>
            <th>(1)</th>
            <th>(2)</th>
            <th>(3)</th>
            <th>(4)</th>
            <th>(5)</th>
            <th>(6)</th>
            <th>(7)</th>
        </tr>
        <tr>
            <th>No.</th>
            <th>SASARAN</th>
            <th>INDIKATOR SASARAN</th>
            <th>FORMULASI</th>
            <th>SUMBER DATA</th>
            <th>OPD PENANGGUNG JAWAB / PROGRAM</th>
            <!--<th>PROGRAM</th>-->
        </tr>
        </thead>
        <tbody>
        <?php
        $no = 1;
        if(isset($sasaran)) {
            $last = array(
                COL_KD_MISI => "@@",
                COL_KD_TUJUAN => "@@",
                COL_KD_INDIKATORTUJUAN => "@@",
                COL_KD_SASARAN => "@@"
            );
            foreach($sasaran as $s) {
                $tujuan = $this->db
                    ->select("Kd_Urusan, Kd_Bidang, Kd_Unit, Kd_Sub")
                    ->distinct()
                    ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                    ->where(COL_KD_MISI, $s[COL_KD_MISI])
                    ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                    ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                    ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                    ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                    ->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_URUSAN, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_BIDANG, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_UNIT, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_SUB, 'asc')
                    ->order_by(TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_TUJUANOPD, 'asc')
                    ->get(TBL_SAKIP_MOPD_TUJUAN)
                    ->result_array();

                $opd_ = "";

                if(count($tujuan) > 0) {
                    if(count($tujuan) > 1) {
                        $opd_ = "<ol style='margin-left: 0px; padding-left: 15px; text-align: left'>";
                        foreach($tujuan as $t) {
                            $eplandb->where(COL_KD_URUSAN, $t[COL_KD_URUSAN]);
                            $eplandb->where(COL_KD_BIDANG, $t[COL_KD_BIDANG]);
                            $eplandb->where(COL_KD_UNIT, $t[COL_KD_UNIT]);
                            $eplandb->where(COL_KD_SUB, $t[COL_KD_SUB]);
                            $subunit = $eplandb->get("ref_sub_unit")->row_array();

                            $program = $this->db
                                ->select("Nm_ProgramOPD")
                                ->distinct()
                                ->where(COL_KD_PEMDA, $s[COL_KD_PEMDA])
                                ->where(COL_KD_MISI, $s[COL_KD_MISI])
                                ->where(COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                                ->where(COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                                ->where(COL_KD_SASARAN, $s[COL_KD_SASARAN])
                                ->where(COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                                ->where(COL_KD_TAHUN, $data[COL_KD_TAHUN])
                                ->where(COL_KD_URUSAN, $t[COL_KD_URUSAN])
                                ->where(COL_KD_BIDANG, $t[COL_KD_BIDANG])
                                ->where(COL_KD_UNIT, $t[COL_KD_UNIT])
                                ->where(COL_KD_SUB, $t[COL_KD_SUB])
                                ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB, 'asc')
                                ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD, 'asc')
                                ->get(TBL_SAKIP_DPA_PROGRAM)
                                ->result_array();

                            $prg_ = "<ul style='margin-left: 0px; padding-left: 20px; text-align: left'>";
                            foreach($program as $p) {
                                $prg_ .= "<li>".$p[COL_NM_PROGRAMOPD]."</li>";
                            }
                            $prg_ .= "</ul>";

                            if($subunit) {
                                $opd_ .= "<li><span style='font-weight: bold'>".$subunit["Nm_Sub_Unit"]."</span><br />".$prg_."</li>";
                            }
                        }
                        $opd_ .= "</ol>";
                    }
                    else {
                        $eplandb->where(COL_KD_URUSAN, $tujuan[0][COL_KD_URUSAN]);
                        $eplandb->where(COL_KD_BIDANG, $tujuan[0][COL_KD_BIDANG]);
                        $eplandb->where(COL_KD_UNIT, $tujuan[0][COL_KD_UNIT]);
                        $eplandb->where(COL_KD_SUB, $tujuan[0][COL_KD_SUB]);
                        $subunit = $eplandb->get("ref_sub_unit")->row_array();
                        if($subunit) {
                            $opd_ .= $opd_ = $subunit["Nm_Sub_Unit"];
                        }
                    }
                }
                ?>
                <tr>
                    <?php
                    if($s[COL_KD_MISI].$s[COL_KD_TUJUAN].$s[COL_KD_INDIKATORTUJUAN].$s[COL_KD_SASARAN]!=$last[COL_KD_MISI].$last[COL_KD_TUJUAN].$last[COL_KD_INDIKATORTUJUAN].$last[COL_KD_SASARAN]){
                        ?>
                        <td <?=$s["span_sasaran"]>1?'rowspan="'.$s["span_sasaran"].'"':''?>><?=$no?></td>
                        <td <?=$s["span_sasaran"]>1?'rowspan="'.$s["span_sasaran"].'"':''?>><?=$s[COL_NM_SASARAN]?></td>
                    <?php
                        $no++;
                    }
                    ?>
                    <td><?=$s[COL_NM_INDIKATORSASARAN]?></td>
                    <td><?=$s[COL_NM_FORMULA]?></td>
                    <td><?=$s[COL_NM_SUMBERDATA]?></td>
                    <td><?=$opd_?></td>
                    <!--<td><?=$prg_?></td>-->
                </tr>
                <?php
                $last = array(
                    COL_KD_MISI => $s[COL_KD_MISI],
                    COL_KD_TUJUAN => $s[COL_KD_TUJUAN],
                    COL_KD_INDIKATORTUJUAN => $s[COL_KD_INDIKATORTUJUAN],
                    COL_KD_SASARAN => $s[COL_KD_SASARAN]
                );
            }
        }
        ?>
        </tbody>
    </table>
</div>