<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 8/13/2019
 * Time: 9:27 PM
 */
$eplandb = $this->load->database("eplan", true);
$arrMisi = array();
foreach($misi as $m) {
    $arrKabTujuan = array();
    $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_PEMDA,"inner");
    $this->db->where(TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_PEMDA, $m[COL_KD_PEMDA]);
    $this->db->where(TBL_SAKIP_MPMD_TUJUAN.'.'.COL_KD_MISI, $m[COL_KD_MISI]);
    $this->db->order_by(TBL_SAKIP_MPMD_TUJUAN.".".COL_KD_TUJUAN, 'asc');
    $rkab_tujuan = $this->db->get(TBL_SAKIP_MPMD_TUJUAN)->result_array();

    foreach($rkab_tujuan as $tujuankab) {
        $arrKabIkTujuan = array();
        $kab_iktujuan = $this->db
            ->where(COL_KD_PEMDA, $tujuankab[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $tujuankab[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $tujuankab[COL_KD_TUJUAN])
            ->order_by(TBL_SAKIP_MPMD_IKTUJUAN.".".COL_KD_INDIKATORTUJUAN, 'asc')
            ->get(TBL_SAKIP_MPMD_IKTUJUAN)
            ->result_array();

        $htmlTujuan = "<p class='node-name'>Tujuan ".$tujuankab[COL_KD_MISI].".".$tujuankab[COL_KD_TUJUAN]."</p><p class='node-title'>".$tujuankab[COL_NM_TUJUAN]."</p>";
        $iktujuan_ = "";
        if(count($kab_iktujuan) > 0) {
            $iktujuan_ .= "<p class='node-title'>Indikator :</p><ul style='margin-left: 0px; padding-left: 15px; text-align: justify'>";
            foreach($kab_iktujuan as $ikt) {
                $iktujuan_ .= "<li>".$ikt[COL_NM_INDIKATORTUJUAN]."</li>";
            }
            $iktujuan_ .= "</ul>";
        }
        $htmlTujuan .= $iktujuan_;

        $arrKabSasaran = array();
        $kab_sasaran = $this->db
            ->where(COL_KD_PEMDA, $tujuankab[COL_KD_PEMDA])
            ->where(COL_KD_MISI, $tujuankab[COL_KD_MISI])
            ->where(COL_KD_TUJUAN, $tujuankab[COL_KD_TUJUAN])
            ->order_by(TBL_SAKIP_MPMD_SASARAN.".".COL_KD_SASARAN, 'asc')
            ->get(TBL_SAKIP_MPMD_SASARAN)
            ->result_array();

        foreach($kab_sasaran as $skab) {
            $arrKabIkSasaran = array();
            $kab_iksasaran = $this->db
                ->where(COL_KD_PEMDA, $skab[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $skab[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $skab[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $skab[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $skab[COL_KD_SASARAN])
                ->order_by(TBL_SAKIP_MPMD_IKSASARAN.".".COL_KD_INDIKATORSASARAN, 'asc')
                ->get(TBL_SAKIP_MPMD_IKSASARAN)
                ->result_array();

            $htmlSasaran = "<p class='node-name'>Sasaran ".$skab[COL_KD_MISI].".".$skab[COL_KD_TUJUAN].".".$skab[COL_KD_INDIKATORTUJUAN].".".$skab[COL_KD_SASARAN]."</p><p class='node-title'>".$skab[COL_NM_SASARAN]."</p>";
            $iksasaran_ = "";
            if(count($kab_iksasaran) > 0) {
                $iksasaran_ .= "<p class='node-title'>Indikator :</p><ul style='margin-left: 0px; padding-left: 15px; text-align: justify'>";
                foreach($kab_iksasaran as $iks) {
                    $iksasaran_ .= "<li>".$iks[COL_NM_INDIKATORSASARAN]."</li>";
                }
                $iksasaran_ .= "</ul>";
            }
            $htmlSasaran .= $iksasaran_;

            $kdUrusan = $this->input->get(COL_KD_URUSAN);
            $kdBidang = $this->input->get(COL_KD_BIDANG);
            $kdUnit = $this->input->get(COL_KD_UNIT);
            $kdSub = $this->input->get(COL_KD_SUB);

            $arrSasaran = array();

            if(!empty($kdUrusan) && !empty($kdBidang) && !empty($kdUnit) && !empty($kdSub)) {
                $this->db
                    ->where(COL_KD_URUSAN, $kdUrusan)
                    ->where(COL_KD_BIDANG, $kdBidang)
                    ->where(COL_KD_UNIT, $kdUnit)
                    ->where(COL_KD_SUB, $kdSub);
            }
            $sasaran = $this->db
                ->where(COL_KD_PEMDA, $skab[COL_KD_PEMDA])
                ->where(COL_KD_MISI, $skab[COL_KD_MISI])
                ->where(COL_KD_TUJUAN, $skab[COL_KD_TUJUAN])
                ->where(COL_KD_INDIKATORTUJUAN, $skab[COL_KD_INDIKATORTUJUAN])
                ->where(COL_KD_SASARAN, $skab[COL_KD_SASARAN])
                ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_TUJUANOPD, 'asc')
                ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_INDIKATORTUJUANOPD, 'asc')
                ->order_by(TBL_SAKIP_MOPD_SASARAN.".".COL_KD_SASARANOPD, 'asc')
                ->get(TBL_SAKIP_MOPD_SASARAN)
                ->result_array();

            foreach($sasaran as $s) {
                $eplandb->where(COL_KD_URUSAN, $s[COL_KD_URUSAN]);
                $eplandb->where(COL_KD_BIDANG, $s[COL_KD_BIDANG]);
                $eplandb->where(COL_KD_UNIT, $s[COL_KD_UNIT]);
                $eplandb->where(COL_KD_SUB, $s[COL_KD_SUB]);
                $subunit = $eplandb->get("ref_sub_unit")->row_array();
                if($subunit) {
                    $nmSub = $subunit["Nm_Sub_Unit"];
                }

                $arrProgram = array();
                $program = $this->db
                    ->select("sakip_dpa_program.*, sakip_mopd_sasaran.Nm_SasaranOPD, sakip_mopd_iksasaran.Nm_IndikatorSasaranOPD,
                            (SELECT SUM(sakip_dpa_kegiatan.Budget) FROM `sakip_dpa_kegiatan`
                                WHERE `sakip_dpa_kegiatan`.`Kd_Urusan` = `sakip_dpa_program`.`Kd_Urusan`
                                AND `sakip_dpa_kegiatan`.`Kd_Bidang` = `sakip_dpa_program`.`Kd_Bidang`
                                AND `sakip_dpa_kegiatan`.`Kd_Unit` = `sakip_dpa_program`.`Kd_Unit`
                                AND `sakip_dpa_kegiatan`.`Kd_Sub` = `sakip_dpa_program`.`Kd_Sub`
                                AND `sakip_dpa_kegiatan`.`Kd_Pemda` = `sakip_dpa_program`.`Kd_Pemda`
                                AND `sakip_dpa_kegiatan`.`Kd_Misi` = `sakip_dpa_program`.`Kd_Misi`
                                AND `sakip_dpa_kegiatan`.`Kd_Tujuan` = `sakip_dpa_program`.`Kd_Tujuan`
                                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuan` = `sakip_dpa_program`.`Kd_IndikatorTujuan`
                                AND `sakip_dpa_kegiatan`.`Kd_Sasaran` = `sakip_dpa_program`.`Kd_Sasaran`
                                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaran` = `sakip_dpa_program`.`Kd_IndikatorSasaran`
                                AND `sakip_dpa_kegiatan`.`Kd_TujuanOPD` = `sakip_dpa_program`.`Kd_TujuanOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_IndikatorTujuanOPD` = `sakip_dpa_program`.`Kd_IndikatorTujuanOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_SasaranOPD` = `sakip_dpa_program`.`Kd_SasaranOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_IndikatorSasaranOPD` = `sakip_dpa_program`.`Kd_IndikatorSasaranOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_ProgramOPD` = `sakip_dpa_program`.`Kd_ProgramOPD`
                                AND `sakip_dpa_kegiatan`.`Kd_Tahun` = `sakip_dpa_program`.`Kd_Tahun`
                            ) AS TotalProgram")
                    ->join(TBL_SAKIP_MOPD_SASARAN,
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                        TBL_SAKIP_MOPD_SASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD
                        ,"left")
                    ->join(TBL_SAKIP_MOPD_IKSASARAN,
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_URUSAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_BIDANG." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_UNIT." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SUB." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB." AND ".

                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_MISI." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARAN." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_TUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORTUJUANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_SASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD." AND ".
                        TBL_SAKIP_MOPD_IKSASARAN.'.'.COL_KD_INDIKATORSASARANOPD." = ".TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARANOPD
                        ,"left")
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_URUSAN, $s[COL_KD_URUSAN])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_BIDANG, $s[COL_KD_BIDANG])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_UNIT, $s[COL_KD_UNIT])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SUB, $s[COL_KD_SUB])

                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PEMDA, $s[COL_KD_PEMDA])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_MISI, $s[COL_KD_MISI])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUAN, $s[COL_KD_TUJUAN])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUAN, $s[COL_KD_INDIKATORTUJUAN])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARAN, $s[COL_KD_SASARAN])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORSASARAN, $s[COL_KD_INDIKATORSASARAN])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TUJUANOPD, $s[COL_KD_TUJUANOPD])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_INDIKATORTUJUANOPD, $s[COL_KD_INDIKATORTUJUANOPD])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD, $s[COL_KD_SASARANOPD])
                    ->where(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_TAHUN, $this->input->get(COL_KD_TAHUN))
                    ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_SASARANOPD, 'asc')
                    ->order_by(TBL_SAKIP_DPA_PROGRAM.".".COL_KD_PROGRAMOPD, 'asc')
                    ->get(TBL_SAKIP_DPA_PROGRAM)
                    ->result_array();

                foreach($program as $p) {
                    $htmlProgram = "<p class='node-name'>".$p[COL_NM_PROGRAMOPD]."</p><p class='node-title'>Rp. ".number_format($p["TotalProgram"], 0)."</p>";
                    $htmlProgram .= "<p class='node-title'><b>Sasaran OPD :</b><br />".$p[COL_NM_SASARANOPD].".</p>";
                    $htmlProgram .= "<p class='node-title'><b>Indikator :</b><br />".$p[COL_NM_INDIKATORSASARANOPD].".</p>";
                    $arrProgram[] = array(
                        //"text" => array("name"=> $p[COL_NM_PROGRAMOPD], "title"=> "Rp. ".number_format($p["TotalProgram"], 0)),
                        "innerHTML" => $htmlProgram,
                        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                        "HTMLclass" => "bg-gray node-wide"
                    );
                }

                $arrSasaran[] = array(
                    "text" => array("name"=> $nmSub, "title"=> $s[COL_NM_SASARANOPD]),
                    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                    "children" => $arrProgram,
                    "HTMLclass" => "bg-teal"
                );
            }

            $arrKabSasaran[] = array(
                "innerHTML" => $htmlSasaran,
                "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
                "children" => $arrSasaran,
                "HTMLclass" => "bg-yellow node-wide"
            );
        }

        $arrKabTujuan[] = array(
            "innerHTML" => $htmlTujuan,
            "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
            "children" => $arrKabSasaran,
            "HTMLclass" => "bg-green node-wide"
        );
    }

    $arrMisi[] = array(
        "text" => array("name"=> "Misi ".$m[COL_KD_MISI], "title"=> $m[COL_NM_MISI]),
        "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
        "children" => $arrKabTujuan,
        "HTMLclass" => "bg-aqua"
    );
}
$nodes = array(
    "text" => array("name"=> "Visi", "title"=> $rpemda[COL_NM_VISI]),
    "connectors" => array("style" => array("stroke" => "#000", "arrow-end" => "block-wide-long")),
    "children" => $arrMisi,
    "HTMLclass" => "bg-primary"
);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? 'E-SAKIP | '.$title : SITENAME?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/raphael.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/Treant.js"></script>
    <link href="<?=base_url()?>assets/treant/Treant.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/skins/_all-skins.min.css">
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/treant/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
</head>
<body>
<style>
    .nodeExample1 {
        border: 1px solid #000;
        padding : 0px !important;
        width : 25vh !important;
        font-size: 8pt;
        color: #000 !important;
    }
    .node-wide {
        width : 32vh !important;
    }
    .nodeExample1 .node-name {
        font-weight: bold;
        margin: 0 0 5px !important;
        border-bottom: 1px solid #000;
        padding: 2px;
    }
    .nodeExample1 .node-title {
        #text-align: justify;
        padding: 2px;
    }
    .chart {
        overflow: auto;
    }
    .bg-greenlight {
        background-color: #c4dd39 !important;
    }
    .node-title {
        padding: 5px !important;
    }
    .nodeExample1 ul {
        margin-left: 5px !important;
    }
</style>
<a id="btn-download" download="Pohon Kinerja.jpg" href="">Download</a>
<div id="chart">
    <h4 style="text-align: center">POHON KINERJA PEMERINTAH KABUPATEN HUMBANG HASUNDUTAN</h4><hr />
    <div class="chart" id="basic-example">

    </div>
</div>
<div id="canvas" style="display: none">

</div>
<script src="<?=base_url()?>assets/js/html2canvas.min.js"></script>
<script>
    var chart_config = {
        chart: {
            container: "#basic-example",
            scrollbar: "fancy",
            //animateOnInit: true,
            rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
            connectors: {
                type: "step",
                style: {
                    "stroke-width": 1
                }
            },
            node: {
                HTMLclass: 'nodeExample1'
            },
            nodeAlign: 'TOP'
            /*animation: {
             nodeAnimation: "easeOutBounce",
             nodeSpeed: 700,
             connectorsAnimation: "bounce",
             connectorsSpeed: 700
             }*/
        },
        nodeStructure: <?=json_encode($nodes)?>
    };
    new Treant( chart_config );
    html2canvas(document.querySelector("#chart"), {scale: 1.75}).then(canvas => {
        document.getElementById("canvas").appendChild(canvas);
        var img = canvas.toDataURL("image/jpg");
        $("#btn-download").attr("href", img);
    });
</script>
</body>