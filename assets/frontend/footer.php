</section>
<!-- /.content -->
</div>
<!-- /.container -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?=date("Y")?> E-SAKIP</strong>. Strongly developed by <b>Partopi Tao</b>.
</footer>
</div>

<!-- Bootstrap 3.3.6 -->
<script src="<?=base_url()?>assets/adminlte/bootstrap/js/bootstrap.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/adminlte/plugins/fastclick/fastclick.js"></script>

<script src="<?=base_url()?>assets/adminlte/plugins/morris/raphael-min.js"></script>
<script src="<?=base_url()?>assets/adminlte/plugins/morris/morris.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/adminlte/dist/js/app.min.js"></script>
<script>
    $('a[href="<?=current_url()?>"]').addClass('active').parents('li').addClass('active');
</script>
</body>