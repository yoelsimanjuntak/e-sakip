<?php $this->load->view('frontend/header') ?>
<div class="row">
    <div class="col-sm-8">
        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green-gradient">
                <div class="widget-user-image">
                    <img class="img-circle" src="<?=MY_IMAGEURL?>logo.png" style="margin-right: 10px" alt="Logo">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">Sistem Akuntabilitas Kinerja Instansi Pemerintah</h3>
                <h5 class="widget-user-desc">Kabupaten Humbang Hasundutan</h5>
            </div>
            <div class="box-body">
                <?php
                $this->db->where(COL_KD_TAHUN_FROM." <=", date("Y"));
                $this->db->where(COL_KD_TAHUN_TO." >=", date("Y"));
                $this->db->order_by(COL_KD_TAHUN_FROM, "desc");
                $rperiod = $this->db->get(TBL_SAKIP_MPEMDA)->row_array();
                ?>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td style="width: 20%">PERIODE</td>
                        <td style="width: 1%">:</td>
                        <td><b><?=$rperiod?$rperiod[COL_KD_TAHUN_FROM]." s.d ".$rperiod[COL_KD_TAHUN_TO]:"-"?></b></td>
                    </tr>
                    <tr>
                        <td style="width: 20%">KEPALA DAERAH</td>
                        <td style="width: 1%">:</td>
                        <td><b><?=$rperiod?strtoupper($rperiod[COL_NM_PEJABAT]):"-"?></b></td>
                    </tr>
                    <tr>
                        <td style="width: 20%">VISI</td>
                        <td style="width: 1%">:</td>
                        <td><b><?=$rperiod?strtoupper($rperiod[COL_NM_VISI]):"-"?></b></td>
                    </tr>
                    <tr>
                        <td style="width: 20%">MISI</td>
                        <td style="width: 1%">:</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <ul class="todo-list ui-sortable">
                    <?php
                    if($rperiod) {
                        $rmisi = $this->db->where(COL_KD_PEMDA, $rperiod[COL_KD_PEMDA])->order_by(COL_KD_MISI, "asc")->get(TBL_SAKIP_MPMD_MISI)->result_array();
                        $i = 1;
                        foreach($rmisi as $m) {
                            ?>
                            <li>
                                <span class="badge <?=$i%2==0?"bg-yellow":"bg-green"?>"><?=strtoupper($m[COL_KD_MISI])?></span><span class="text"><?=strtoupper($m[COL_NM_MISI])?></span>
                                <div class="tools" style="display: none">
                                    <a href="<?=site_url()?>"><i class="fa fa-edit <?=$i%2==0?"text-yellow":"text-green"?>"></i></a>
                                </div>
                            </li>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <!--<div class="box box-solid">
                        <div class="box-header with-border">
                            <i class="fa fa-image"></i>
                            <h3 class="box-title">Galeri</h3>
                        </div>
                        <div class="box-body">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img src="<?=MY_IMAGEURL?>/slide/2.jpeg" alt="SIDAMA">
                                    </div>
                                    <div class="item">
                                        <img src="<?=MY_IMAGEURL?>/slide/3.jpeg" alt="SIDAMA">
                                    </div>
                                    <div class="item">
                                        <img src="<?=MY_IMAGEURL?>/slide/4.jpeg" alt="SIDAMA">
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                    <span class="fa fa-angle-left"></span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                    <span class="fa fa-angle-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>-->

        <div class="box box-solid">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="fa fa-calendar"></i>
                <h3 class="box-title">Kalender</h3>

                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

            <div class="box-body no-padding" style="display: block;">
                <!--The calendar -->
                <div id="calendar" style="width: 100%">
                </div>
            </div>
        </div>
    </div>
</div>
    <script>

        $("#calendar").datepicker().datepicker('update', new Date(<?=date('Y')?>,<?=date('m')?>-1,<?=date('d')?>));
    </script>
<?php $this->load->view('frontend/footer') ?>