<?php $this->load->view('frontend/header') ?>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h4 class="box-title">Status OPD</h4>
            </div>
            <div class="box-body">
                <?=form_open(current_url(),array('role'=>'form','id'=>'filterForm', 'class'=>'form-horizontal', 'method'=>'GET'))?>
                <div class="form-group">
                    <?php
                    $nmSub = "";
                    $eplandb = $this->load->database("eplan", true);
                    $eplandb->where(COL_KD_URUSAN, $filter_opd[COL_KD_URUSAN]);
                    $eplandb->where(COL_KD_BIDANG, $filter_opd[COL_KD_BIDANG]);
                    $eplandb->where(COL_KD_UNIT, $filter_opd[COL_KD_UNIT]);
                    $eplandb->where(COL_KD_SUB, $filter_opd[COL_KD_SUB]);
                    $subunit = $eplandb->get("ref_sub_unit")->row_array();
                    if($subunit) {
                        $nmSub = $subunit["Nm_Sub_Unit"];
                    }
                    ?>
                    <label class="control-label col-sm-2">Pilih OPD</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="text" class="form-control" name="text-opd" value="<?=$filter_opd[COL_KD_URUSAN].".".$filter_opd[COL_KD_BIDANG].".".$filter_opd[COL_KD_UNIT].".".$filter_opd[COL_KD_SUB]." ".$nmSub?>" disabled>
                            <input type="hidden" name="<?=COL_KD_URUSAN?>" required>
                            <input type="hidden" name="<?=COL_KD_BIDANG?>" required>
                            <input type="hidden" name="<?=COL_KD_UNIT?>" required>
                            <input type="hidden" name="<?=COL_KD_SUB?>" required>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default btn-flat btn-browse-opd" data-toggle="modal" data-target="#browseOPD" data-toggle="tooltip" data-placement="top" title="Pilih OPD"><i class="fa fa-ellipsis-h"></i></button>
                                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <?=form_close()?>

                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th>OPD</th>
                        <th>Tujuan</th>
                        <th>Program</th>
                        <th>Kegiatan</th>
                    </tr>
                    <?php
                    foreach($opd as $o) {
                        $kdUrusan = $o["Kd_Urusan"];
                        $kdBidang = $o["Kd_Bidang"];
                        $kdUnit = $o["Kd_Unit"];
                        $kdSub = $o["Kd_Sub"];

                        $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MOPD_TUJUAN.".".COL_KD_PEMDA,"inner");
                        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
                        $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
                        $this->db->where(COL_KD_URUSAN, $kdUrusan);
                        $this->db->where(COL_KD_BIDANG, $kdBidang);
                        $this->db->where(COL_KD_UNIT, $kdUnit);
                        $this->db->where(COL_KD_SUB, $kdSub);
                        $ctujuan = $this->db->get(TBL_SAKIP_MOPD_TUJUAN)->num_rows();

                        $this->db->select('*,'.TBL_SAKIP_MBID.'.'.COL_KD_BID.' AS ID,
                        CONCAT('.TBL_SAKIP_MBID.'.'.COL_KD_BID.',\'. \','.TBL_SAKIP_MBID.'.'.COL_NM_BID.') AS Text');
                        $this->db->where(COL_KD_URUSAN, $kdUrusan);
                        $this->db->where(COL_KD_BIDANG, $kdBidang);
                        $this->db->where(COL_KD_UNIT, $kdUnit);
                        $this->db->where(COL_KD_SUB, $kdSub);
                        $rbidang = $this->db->get(TBL_SAKIP_MBID)->result_array();
                        ?>
                        <tr>
                            <td><?=strtoupper($o["Text"])?></td>
                            <td style="text-align: right"><span class="badge bg-<?=$ctujuan>0?"aqua":"gray"?>"><?=number_format($ctujuan)?></span></td>
                            <td colspan="2"></td>
                        </tr>
                        <?php
                        foreach($rbidang as $bid) {
                            $kdBid = $bid["Kd_Bid"];
                            $this->db->select('*,'.TBL_SAKIP_MSUBBID.'.'.COL_KD_SUBBID.' AS ID,
                                CONCAT('.TBL_SAKIP_MSUBBID.'.'.COL_KD_BID.',\'.\','.TBL_SAKIP_MSUBBID.".".COL_KD_SUBBID.',\'. \','.TBL_SAKIP_MSUBBID.'.'.COL_NM_SUBBID.') AS Text');
                            $this->db->where(COL_KD_URUSAN, $kdUrusan);
                            $this->db->where(COL_KD_BIDANG, $kdBidang);
                            $this->db->where(COL_KD_UNIT, $kdUnit);
                            $this->db->where(COL_KD_SUB, $kdSub);
                            $this->db->where(COL_KD_BID, $kdBid);
                            $rsubbid = $this->db->get(TBL_SAKIP_MSUBBID)->result_array();

                            $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MBID_PROGRAM.".".COL_KD_PEMDA,"inner");
                            $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
                            $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
                            $this->db->where(COL_KD_URUSAN, $kdUrusan);
                            $this->db->where(COL_KD_BIDANG, $kdBidang);
                            $this->db->where(COL_KD_UNIT, $kdUnit);
                            $this->db->where(COL_KD_SUB, $kdSub);
                            $this->db->where(COL_KD_BID, $kdBid);
                            $cprogram = $this->db->get(TBL_SAKIP_MBID_PROGRAM)->num_rows();
                            ?>
                            <tr>
                                <td style="text-indent: 2em;" colspan="2"><?=strtoupper($bid["Text"])?></td>
                                <td style="text-align: right"><span class="badge bg-<?=$cprogram>0?"aqua":"gray"?>"><?=number_format($cprogram)?></span></td>
                                <td colspan="1"></td>
                            </tr>
                            <?php
                            foreach($rsubbid as $sub) {
                                $kdSubBid = $sub["Kd_Subbid"];
                                $this->db->join(TBL_SAKIP_MPEMDA,TBL_SAKIP_MPEMDA.'.'.COL_KD_PEMDA." = ".TBL_SAKIP_MSUBBID_KEGIATAN.".".COL_KD_PEMDA,"inner");
                                $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_FROM." <=", date("Y"));
                                $this->db->where(TBL_SAKIP_MPEMDA.".".COL_KD_TAHUN_TO." >=", date("Y"));
                                $this->db->where(COL_KD_URUSAN, $kdUrusan);
                                $this->db->where(COL_KD_BIDANG, $kdBidang);
                                $this->db->where(COL_KD_UNIT, $kdUnit);
                                $this->db->where(COL_KD_SUB, $kdSub);
                                $this->db->where(COL_KD_BID, $kdBid);
                                $this->db->where(COL_KD_SUBBID, $kdSubBid);
                                $ckegiatan = $this->db->get(TBL_SAKIP_MSUBBID_KEGIATAN)->num_rows();

                                ?>
                                <tr>
                                    <td style="text-indent: 4em;" colspan="3"><?=strtoupper($sub["Text"])?></td>
                                    <td style="text-align: right"><span class="badge bg-<?=$ckegiatan>0?"aqua":"gray"?>"><?=number_format($ckegiatan)?></span></td>
                                </tr>
                                <?php
                            }
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    <div class="modal fade" id="browseOPD" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $('.modal').on('hidden.bs.modal', function (event) {
            $(this).find(".modal-body").empty();
        });

        $('#browseOPD').on('show.bs.modal', function (event) {
            var modalBody = $(".modal-body", $("#browseOPD"));
            $(this).removeData('bs.modal');
            modalBody.html("<p style='font-style: italic'>Loading..</p>");
            modalBody.load("<?=site_url("ajax/browse-opd")?>", function () {
                $("[name=selID][type=hidden]", modalBody).unbind().change(function () {
                    var kdSub = $(this).val().split('|');
                    $("[name=Kd_Urusan]").val(kdSub[0]);
                    $("[name=Kd_Bidang]").val(kdSub[1]);
                    $("[name=Kd_Unit]").val(kdSub[2]);
                    $("[name=Kd_Sub]").val(kdSub[3]);

                    $("[name=Kd_Bid]").val("");
                    $("[name=text-bid]").val("");

                    $("[name=Kd_Pemda]").val("");
                    $("[name=Kd_Misi]").val("");
                    $("[name=Kd_Tujuan]").val("");
                    $("[name=Kd_IndikatorTujuan]").val("");
                    $("[name=Kd_Sasaran]").val("");
                    $("[name=Kd_IndikatorSasaran]").val("");
                    $("[name=Kd_TujuanOPD]").val("");
                    $("[name=Kd_IndikatorTujuanOPD]").val("");
                    $("[name=Kd_SasaranOPD]").val("");
                    $("[name=Kd_IndikatorSasaranOPD]").val("");
                    $("[name=text-iksasaranopd]").val("");
                });
                $("[name=selText][type=hidden]", modalBody).unbind().change(function () {
                    $("[name=text-opd]").val($(this).val());
                });
            });
        });
    </script>
<?php $this->load->view('frontend/footer') ?>