/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - team1_esakip_new
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `sakip_dpa_kegiatan` */

DROP TABLE IF EXISTS `sakip_dpa_kegiatan`;

CREATE TABLE `sakip_dpa_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Total` double NOT NULL,
  `Budget` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  `Kd_SumberDana` varchar(200) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `FK_Subbid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_DPA_SasaranProgram2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_dpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Subbid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_dpa_kegiatan` */

/*Table structure for table `sakip_dpa_kegiatan_indikator` */

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_indikator`;

CREATE TABLE `sakip_dpa_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  CONSTRAINT `FK_DPA_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_dpa_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_dpa_kegiatan_indikator` */

/*Table structure for table `sakip_dpa_kegiatan_sasaran` */

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_sasaran`;

CREATE TABLE `sakip_dpa_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(50) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Budget` double DEFAULT NULL,
  `Budget_TW1` double DEFAULT NULL,
  `Budget_TW2` double DEFAULT NULL,
  `Budget_TW3` double DEFAULT NULL,
  `Budget_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  `Anggaran_TW1` double DEFAULT NULL,
  `Anggaran_TW2` double DEFAULT NULL,
  `Anggaran_TW3` double DEFAULT NULL,
  `Anggaran_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `FK_DPA_Subbid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_DPA_Kegiatan` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_dpa_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_DPA_Subbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_dpa_kegiatan_sasaran` */

/*Table structure for table `sakip_dpa_program` */

DROP TABLE IF EXISTS `sakip_dpa_program`;

CREATE TABLE `sakip_dpa_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `FK_DPA_Bid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_DPA_Bid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_dpa_program` */

/*Table structure for table `sakip_dpa_program_indikator` */

DROP TABLE IF EXISTS `sakip_dpa_program_indikator`;

CREATE TABLE `sakip_dpa_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  CONSTRAINT `FK_DPA_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_dpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_dpa_program_indikator` */

/*Table structure for table `sakip_dpa_program_sasaran` */

DROP TABLE IF EXISTS `sakip_dpa_program_sasaran`;

CREATE TABLE `sakip_dpa_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  `Kinerja_TW1` double DEFAULT NULL,
  `Kinerja_TW2` double DEFAULT NULL,
  `Kinerja_TW3` double DEFAULT NULL,
  `Kinerja_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `FK_DPA_Bid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `DK_DPA_Program2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_dpa_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_DPA_Bid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_dpa_program_sasaran` */

/*Table structure for table `sakip_mbid` */

DROP TABLE IF EXISTS `sakip_mbid`;

CREATE TABLE `sakip_mbid` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Nm_Bid` varchar(200) DEFAULT NULL,
  `Nm_Kabid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mbid` */

insert  into `sakip_mbid`(`Uniq`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Nm_Bid`,`Nm_Kabid`) values (29,1,1,1,1,1,'Sekretaris Dinas Pendidikan','Drs. Danner Panjaiatan'),(30,1,1,1,1,2,'Bidang Pembinaan PAUD dan Pendidikan Non Formal','Rosita Gultom, S.Pd'),(31,1,1,1,1,3,'Bidang Pembinaan Pendidikan Dasar','Chiristison R. Marbun, M.Pd'),(32,1,1,1,1,4,'Bidang Pembinaan Ketenagaan','Marudut Simanullang, SH'),(33,1,2,1,1,1,'Sekretaris Dinas Kesehatan','dr. Binner Sinaga, M.Kes.'),(34,1,2,1,1,2,'Bidang Kesehatan Masyarakat','Restu Imanesa, SKM'),(35,1,2,1,1,3,'Bidang Pencegahan dan Pengendalian Penyakit','dr. Harland Sihombing'),(36,1,2,1,1,4,'Bidang Pelayanan dan Sumberdaya Kesehatan','dr. Lusianna Silaban, M.Kes'),(153,1,2,1,2,1,'Bagian Tata Usaha','dr. Heppi Suranta Depari'),(154,1,2,1,2,2,'Bidang Pelayanan Medik','drg. Hasudungan Silaban, M. Kes'),(155,1,2,1,2,3,'Bidang Keperawatan','Meldaria Lumbantoruan,SKM'),(156,1,2,1,2,4,'Bidang Penunjang Medik dan Sarana/ Prasarana','Hendrika Simamora, S.H.'),(37,1,3,1,1,1,'Sekretaris Dinas Pekerjaan Umum dan Penataan Ruang','BERNARD MAORI SIMAMORA, ST, M.Si'),(38,1,3,1,1,2,'Bidang  Bina Marga','GIBSON PANJAITAN, ST, MM'),(39,1,3,1,1,3,'Bidang Sumber Daya Air','SAUT SIMANULLANG, ST'),(40,1,3,1,1,4,'Bidang Penataan Ruang ','JOHAN V. PANDIANGAN, ST'),(41,1,4,1,1,1,'Sekretaris Dinas Perumahan dan Kawasan Pemukiman','Robert Silaban, SKM. M.Kes'),(42,1,4,1,1,2,'Bidang  Perumahan','Barita Manullang, ST, MM'),(43,1,4,1,1,3,'Bidang Kawasan Permukiman','Renward Henry, ST. M.Si'),(164,1,5,1,1,1,'Sekretaris Satpol PP','Rommel Silaban, SH'),(165,1,5,1,1,2,'Bidang Ketentraman dan Ketertiban Umum','Gilberd Simanjuntak, SE'),(166,1,5,1,1,3,'Bidang Pencegahan dan Pemadam Kebakaran','Rickson Tambunan, SP, M.Si'),(118,1,5,1,4,1,'Sekretaris Pelaksanan Badan Penanggulangan Bencana Daerah','Drs. Maringan Sinaga, AP. MM'),(119,1,5,1,4,2,'Bidang Pencegahan dan Kesiapsiagaan','Binsar Alfonsius Situmorang, S.E'),(120,1,5,1,4,3,'Bidang Kedaruratan dan Logistik','Astri Handayani Sitompul, ST'),(121,1,5,1,4,4,'Bidang Rehabilitasi dan Rekonstruksi','Anggiat Simanullang, ST. MT.'),(48,1,6,1,1,1,'Sekretaris Dinas Sosial','Josefin Simamora,SE'),(49,1,6,1,1,2,'Bidang Pemberdayaan dan Bantuan Sosial','Delima Sianturi,S.Pd'),(50,1,6,1,1,3,'Bidang Pelayanan dan Rehabilitasi Sosial','Serinaya Tinambunan,S.Sos'),(51,2,1,1,1,1,'Sekretaris Dinas Ketenagakerjaan','Victor A. S. Simanullang, S.Sos'),(52,2,1,1,1,2,'Bidang Pelatihan dan Penempatan Ketenagakerjaan','Nancy Frida Banjarnahor, SE'),(53,2,1,1,1,3,'Bidang Hubungan Industrial','Pentus Simatupang, SH'),(144,2,1,1,1,4,'UPT BLK ','RIDWAN PASARIBU'),(54,2,3,1,1,1,'Sekretariat','Bottor Purba, SE'),(55,2,3,1,1,2,'Bidang Ketersediaan dan Distribusi Pangan','Nurmala Purba, SP, MM'),(56,2,3,1,1,3,'Bidang Konsumsi dan Keamanan Pangan','Titih V. S Sihotang, SP'),(145,2,5,1,1,1,'Sekretariat','Harin Tua Pane S.Pd, S.SoS'),(146,2,5,1,1,2,'Bidang Penataan dan Kajian Dampak Lingkungan Hidup','Efendi R. Nainggolan, ST'),(147,2,5,1,1,3,'Bidang Pengendalian dan Pencemaran Lingkungan Hidup','Halomoan JA. Manullang, S.Hut'),(148,2,5,1,1,4,'Bidang Pelayanan Kebersihan dan Pengelolaan Sampah','Ricardo Lumban Toruan, S.Sos'),(61,2,6,1,1,1,'Sekretaris Dinas Kependudukan dan Pencatatan Sipil','Drs. Junias Lumban gaol'),(62,2,6,1,1,2,'Bidang Pelayanan Administrasi Kependudukan','Edward Siregar.SE.'),(63,2,6,1,1,3,'Bidang Piak Dan Pemanfaatan Data',' Jonni Maruhum,ST'),(64,2,7,1,1,1,'Sekretaris Dinas Pemberdayaan Masyarakat Desa, Perempuan, Perlindungan Anak','Frans Judika B. Pasaribu, SE, M. Si'),(65,2,7,1,1,2,'Bidang Administrasi Pemerintahan Desa','Jusmar Efendi Simamora, ST, MT'),(66,2,7,1,1,3,'Bidang Pemberdayaan Masyarakat dan Lembaga Adat','Jerry Silitonga, SH, MM'),(67,2,7,1,1,4,'Bidang Pemberdayaan Perempuan dan Perlindungan Anak','Jeni Dewi Koriana Siagian, SH'),(68,2,8,1,1,1,'Sekretaris Dinas Pengendalian dan Keluarga Berencana','dr. Sugito'),(69,2,8,1,1,2,'Bidang Pengendalian Penduduk, Penyuluhan dan Penggerakan','Rich Joney Simamora,SIP,Msi'),(70,2,8,1,1,3,'Bidang Keluarga Berencana, Ketahanan dan Kesejahteraan Keluarga','Saritua Harianja,SKM,M.Kes'),(71,2,9,1,1,1,'Sekretaris Dinas Perhubungan ','Tumbur Hutasoit,M.Pd'),(72,2,9,1,1,2,'Bidang Manajemen dan Rekayasa Lalu Lintas dan Angkutan','Drs.Pantas Purba,M.Si'),(73,2,9,1,1,3,'Bidang Prasarana dan Sarana','Ramli Nababan,S.P'),(74,2,10,1,1,1,'Sekretariat Dinas Komunikasi dan Informatika','Jinitua Malau, S.Pd'),(75,2,10,1,1,2,'Bidang Komunikasi','Mangadar Lumban Raja, S.Sos'),(76,2,10,1,1,3,'Bidang Informatika','Deddy D.P. Situmorang, SE, M.Si'),(140,2,11,1,1,1,'Koperasi dan UMKM','Nurliza Pasaribu, S.Kom, M.Si'),(141,2,11,1,1,2,'Perdagangan','Rut Anita Purba, S.Sos, M.Si'),(142,2,11,1,1,3,'Perindustrian','Damianus Lumbantoruan, S.Sos'),(143,2,11,1,1,4,'Sekretariat','Henry Hamonangan, SE'),(81,2,12,1,1,1,'Sekretaris Dinas Penanaman Modal dan Pelayanan Perizinan Terpadu Satu Pintu',NULL),(82,2,12,1,1,2,'Bidang Penanaman Modal',NULL),(83,2,12,1,1,3,'Bidang Pelayanan Terpadu Satu Pintu',NULL),(84,2,12,1,1,4,'Bidang BUMD dan ESDM',NULL),(85,2,13,1,1,1,'Sekretaris Dinas Kepemudaan dan Olahraga','OPZEN SIMAMORA, S.Pd, MM'),(86,2,13,1,1,2,'Bidang Kepemudaan','ASWIN SILABAN, SE'),(87,2,13,1,1,3,'Bidang Olahraga','Ir. TORANG PURBA'),(88,2,17,1,1,1,'Sekretaris Dinas Perpustakaan dan Kearsipan','Drs. Jonny Manalu'),(89,2,17,1,1,2,'Bidang Perpustakaan','Adrianus Mahulae, M.Pd'),(90,2,17,1,1,3,'Bidang Kearsipan','Geser Pane, Ama.Pd '),(99,3,2,1,1,1,'Sekretaris Dinas Pariwisata','Resva Panjaitan, SE, MM'),(100,3,2,1,1,2,'Bidang Pengembangan Destinasi dan Industri Pariwisata','Dra. Christina Clara E, M.AP'),(101,3,2,1,1,3,'Bidang Promosi dan Kelembagaan','Barton Naibaho, SE'),(102,3,2,1,1,4,'Bidang Kebudayaan','Nelson Lumbantoruan, SS, M.Hum'),(91,3,3,1,1,1,'Sekretaris Dinas Pertanian','Dra. Ida Maria Manullang'),(92,3,3,1,1,2,'Bidang Tanaman Pangan dan Hortikultura','Yonepta Habeahan, SP, MM'),(93,3,3,1,1,3,'Bidang Perkebunan','Ir. Buroniani Naibaho'),(94,3,3,1,1,4,'Bidang Penyuluhan, Prasarana dan Sarana Pertanian','Lenny Sihombing, SP'),(95,3,3,1,2,1,'Sekretariat','drh. Nella Dumawati Simamora'),(96,3,3,1,2,2,'Bidang Peternakan','Sanggam K Sihombing, S.Pt'),(97,3,3,1,2,3,'Bidang Kesehatan Hewan, Ikan dan Masyarakat Veteriner','Martongam Lumbantopruan, S.Pt'),(98,3,3,1,2,4,'Bidang Perikanan','Rudy T H Simamora, S.Pi'),(12,4,1,1,1,1,'Asisten Pemerintahan','MAKDEN SIHOMBING, S.Sos, M.M'),(13,4,1,1,1,2,'Asisten Perekonomian dan Pembangunan','Drs. LAMHOT HOTASOIT, M.IP'),(14,4,1,1,1,3,'Asisten Administrasi dan Kesejahteraan Rakyat','Drs. JANTER SINAGA'),(15,4,1,1,1,4,'Bagian Tata Pemerintahan','NAEK M. SINAMBELA, S.Sos, MM'),(16,4,1,1,1,5,'Bagian Hukum','SUHUT SILABAN, SH'),(17,4,1,1,1,6,'Bagian Organisasi','Drs. KAMARUDDIN GULTOM'),(18,4,1,1,1,7,'Bagian Perekonomian dan Pembangunan','Ir. JAMARLIN SIREGAR'),(167,4,1,1,1,8,'Bagian Protokol','SABAR LAMPOS PURBA, ST'),(20,4,1,1,1,9,'Bagian Umum','ROMMEL SILABAN, SH'),(21,4,1,1,1,10,'Bagian Kesejahteraan Sosial','JAKKON H. MARBUN,SE, MM'),(168,4,1,1,1,11,'Bagian Pengadaan Barang/Jasa ','Benthon J. Lumban Gaol, S.E., M.Si.'),(22,4,1,1,4,1,'Bagian Umum','Darma Silaban, SE'),(23,4,1,1,4,2,'Bagian Persidangan','Tarianus Simatupang'),(24,4,1,1,4,3,'Bagian Perencanaan dan Keuangan','Drs. Pantas Purba, M.Si'),(115,4,1,1,5,1,'Sekretariat','Pieter Marbun, S.Pd'),(116,4,1,1,5,2,'Bidang Politik Dalam Negeri dan Organisasi Kemasyarakatan','Jonsihar Simanullang, S.Sos'),(117,4,1,1,5,3,'Bidang Penanganan Konflik dan Kewaspadaan Nasional','Drs. Tua Hasian Gultom, MA'),(25,4,2,1,1,1,'Sekretaris','Pangarantoan Lumbantoruan, S.H., M.Pd'),(26,4,2,1,1,2,'Inspektur Pembantu Pemerintahan','Drs. Manusun Lumbantoruan'),(27,4,2,1,1,3,'Inspektur Pembantu Perekonomian dan Pembangunan','Halim Sinabutar, S.E.'),(28,4,2,1,1,4,'Inspektur Pembantu Administrasi dan Kesejahteraan Masyarakat','Edison Manurung, S.E.'),(103,4,3,1,1,1,'Sekretaris Badan Perencanaan Pembangunan Daerah','Andi Saut Sihombing, SE'),(104,4,3,1,1,2,'Bidang Pemerintahan','Manongam SP Pasaribu, SE'),(105,4,3,1,1,3,'Bidang Ekonomi dan Pembangunan','Pahala H.L. Gaol, ST, M.Sc, M.Eng'),(106,4,3,1,1,4,'Bidang Administrasi, Kesejahteraan Rakyat dan Penelitian Pengembangan','Melati Simamora, ST'),(107,4,4,1,1,1,'Sekretaris Badan Pengelolaan Keuangan, Pendapatan dan Aset Daerah','Martogi Purba . ST'),(108,4,4,1,1,2,'Bidang Pendapatan','Tua Marsakti Marbun, SE. MSi'),(109,4,4,1,1,3,'Bidang Anggaran','Drs. MARADU NAPITUPULU, M.Si'),(110,4,4,1,1,4,'Bidang Akuntansi Perbendaharaan','BATARA FRANZ SIREGAR, SE.'),(111,4,4,1,1,5,'Bidang Aset Daerah','PAUL AGUSTINUS SIMAMORA, SE. M. Si'),(112,4,5,1,1,1,'Sekretaris Badan Kepegawaian Daerah','Sabar H. Purba, SH, MM'),(113,4,5,1,1,2,'Bidang Pendayagunaan dan Kesejahteraan','Mewah Risma, SE'),(114,4,5,1,1,3,'Bidang Pembinaan Karir dan Disiplin','Syah Rijal Simamora, SH');

/*Table structure for table `sakip_mbid_indikator` */

DROP TABLE IF EXISTS `sakip_mbid_indikator`;

CREATE TABLE `sakip_mbid_indikator` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  KEY `FK_BID3` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_BID3` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_BID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mbid_indikator` */

/*Table structure for table `sakip_mbid_program` */

DROP TABLE IF EXISTS `sakip_mbid_program`;

CREATE TABLE `sakip_mbid_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranProgram` varchar(200) DEFAULT NULL,
  `Nm_IndikatorProgram` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FK_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_BID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_OPD_IKSASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) REFERENCES `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mbid_program` */

/*Table structure for table `sakip_mbid_program_indikator` */

DROP TABLE IF EXISTS `sakip_mbid_program_indikator`;

CREATE TABLE `sakip_mbid_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_Bid6` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_Bid6` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_IndikatorSasaran` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mbid_program_indikator` */

/*Table structure for table `sakip_mbid_program_sasaran` */

DROP TABLE IF EXISTS `sakip_mbid_program_sasaran`;

CREATE TABLE `sakip_mbid_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_OPD_IKSASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  KEY `FK_BID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_SasaranBid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_mbid_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mbid_program_sasaran` */

/*Table structure for table `sakip_mbid_sasaran` */

DROP TABLE IF EXISTS `sakip_mbid_sasaran`;

CREATE TABLE `sakip_mbid_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_BID2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  KEY `FK_OPD_IKSASARAN_TO_BID_SASARAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  CONSTRAINT `FK_BID2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_OPD_IKSASARAN_TO_BID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) REFERENCES `sakip_mopd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mbid_sasaran` */

/*Table structure for table `sakip_mopd_file` */

DROP TABLE IF EXISTS `sakip_mopd_file`;

CREATE TABLE `sakip_mopd_file` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Type` varchar(50) NOT NULL,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Nm_Keterangan` varchar(200) DEFAULT NULL,
  `Nm_File` varchar(200) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `Uniq` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mopd_file` */

/*Table structure for table `sakip_mopd_iksasaran` */

DROP TABLE IF EXISTS `sakip_mopd_iksasaran`;

CREATE TABLE `sakip_mopd_iksasaran` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Nm_IndikatorSasaranOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`),
  CONSTRAINT `FK_OPD_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`) REFERENCES `sakip_mopd_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mopd_iksasaran` */

insert  into `sakip_mopd_iksasaran`(`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Nm_IndikatorSasaranOPD`,`Nm_Formula`,`Nm_SumberData`,`Nm_PenanggungJawab`) values (8,3,1,1,1,4,NULL,2,10,1,1,1,1,1,1,'Indeks Sistem Pemerintahan Berbasis Elektronik','Indeks SPBE dari Kemenpan','Bid. Informatika',NULL),(8,4,2,1,2,1,NULL,2,10,1,1,1,1,1,1,'Indeks Sistem Pemerintahan Berbasis Elektronik','Indeks SPBE dari Kemenpan','Bid. Informatika',NULL);

/*Table structure for table `sakip_mopd_iktujuan` */

DROP TABLE IF EXISTS `sakip_mopd_iktujuan`;

CREATE TABLE `sakip_mopd_iktujuan` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorTujuanOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`),
  KEY `FK_OPD_TUJUAN` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_TujuanOPD`),
  CONSTRAINT `FK_OPD_TUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`) REFERENCES `sakip_mopd_tujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mopd_iktujuan` */

insert  into `sakip_mopd_iktujuan`(`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Nm_IndikatorTujuanOPD`) values (8,3,1,1,1,4,NULL,2,10,1,1,1,1,'Indeks Sistem Pemerintahan Berbasis Elektronik'),(8,4,2,1,2,1,NULL,2,10,1,1,1,1,'Indeks Sistem Pemerintahan Berbasis Elektronik');

/*Table structure for table `sakip_mopd_sasaran` */

DROP TABLE IF EXISTS `sakip_mopd_sasaran`;

CREATE TABLE `sakip_mopd_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Nm_SasaranOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_OPD_IKTUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`) REFERENCES `sakip_mopd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mopd_sasaran` */

insert  into `sakip_mopd_sasaran`(`Uniq`,`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Nm_SasaranOPD`) values (2,8,3,1,1,1,4,NULL,2,10,1,1,1,1,1,'Meningkatnya Pelaksanaan SPBE Pemerintah Daerah'),(3,8,4,2,1,2,1,NULL,2,10,1,1,1,1,1,'Meningkatnya Pelaksanaan SPBE Pemerintah Daerah');

/*Table structure for table `sakip_mopd_tujuan` */

DROP TABLE IF EXISTS `sakip_mopd_tujuan`;

CREATE TABLE `sakip_mopd_tujuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) DEFAULT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Nm_TujuanOPD` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_PEMDA_IKSASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`) REFERENCES `sakip_mpmd_iksasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mopd_tujuan` */

insert  into `sakip_mopd_tujuan`(`Uniq`,`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Nm_TujuanOPD`) values (1,8,3,1,1,1,4,NULL,2,10,1,1,1,'Terwujudnya SPBE Di Pemerintah Daerah'),(2,8,4,2,1,2,1,NULL,2,10,1,1,1,'Terwujudnya SPBE Di Pemerintah Daerah');

/*Table structure for table `sakip_mpemda` */

DROP TABLE IF EXISTS `sakip_mpemda`;

CREATE TABLE `sakip_mpemda` (
  `Kd_Pemda` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Prov` bigint(10) NOT NULL,
  `Kd_Kab` bigint(10) NOT NULL,
  `Kd_Tahun_From` bigint(10) NOT NULL,
  `Kd_Tahun_To` bigint(10) NOT NULL,
  `Nm_Kab` varchar(200) DEFAULT NULL,
  `Nm_Pejabat` varchar(200) DEFAULT NULL,
  `Nm_Posisi` varchar(200) DEFAULT NULL,
  `Nm_Visi` varchar(200) DEFAULT NULL,
  `Nm_Alamat_Kab` varchar(200) DEFAULT NULL,
  `Nm_Ket_Periode` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Prov`,`Kd_Kab`,`Kd_Tahun_From`,`Kd_Tahun_To`),
  KEY `Kd_Pemda` (`Kd_Pemda`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mpemda` */

insert  into `sakip_mpemda`(`Kd_Pemda`,`Kd_Prov`,`Kd_Kab`,`Kd_Tahun_From`,`Kd_Tahun_To`,`Nm_Kab`,`Nm_Pejabat`,`Nm_Posisi`,`Nm_Visi`,`Nm_Alamat_Kab`,`Nm_Ket_Periode`) values (8,12,16,2016,2021,NULL,'Dosmar Banjarnahor, SE',NULL,'Mewujudkan Humbang Hasundutan HEBAT dan bermentalitas unggul',NULL,NULL);

/*Table structure for table `sakip_mpmd_iksasaran` */

DROP TABLE IF EXISTS `sakip_mpmd_iksasaran`;

CREATE TABLE `sakip_mpmd_iksasaran` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Nm_IndikatorSasaran` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`),
  CONSTRAINT `FK_PEMDA_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) REFERENCES `sakip_mpmd_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mpmd_iksasaran` */

insert  into `sakip_mpmd_iksasaran`(`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Nm_IndikatorSasaran`,`Nm_Formula`,`Nm_SumberData`) values (8,1,1,1,1,1,'Jumlah konflik antar umat beragama ','Jumlah konflik antar umat beragama tahun n','Data primer'),(8,2,1,1,1,1,'Angka rata-rata lama sekolah (ARLS)','Kombinasi antara partisipasi sekolah, jenjang pendidikan yang sedang dijalani, kelas yang diduduki dan pendidikan yang ditamatkan','Data pokok pendidikan'),(8,2,1,1,2,1,'Angka harapan hidup','Jumah umur yang meninggal/Total orang yang meninggal','Data primer'),(8,2,1,1,3,1,'Persentase penurunan angka kemiskinan','Jumlah Penduduk miskin/Jumlah Penduduk x 100%','Data primer'),(8,3,1,1,1,1,'Peringkat LPPD','Hasil Penilaian Kemendagri',''),(8,3,1,1,1,2,'Nilai evaluasi AKIP','Hasil evaluasi Kemen PANRB',''),(8,3,1,1,1,3,'Opini BPK','Hasil Opini BPK atas Lapkeu',''),(8,3,1,1,1,4,'Persentase Implementasi e-Government','Indeks Sistem Pemerintahan Berbasis Elektronik',''),(8,4,1,1,1,1,'Persentase pertumbuhan produksi pertanian','(Jumlah produksi komoditi bahan pangan tahun n) -(Jumlah produksi komoditi bahan pangan tahun n-1)/Jumah produksi tahun n-1 x 100%',''),(8,4,1,1,1,2,'Persentase pertumbuhan populasi peternakan dan perikanan','(Jumlah produksi ternak tahun n) -(Jumlah produksi ternak tahun n-1)/Jumah produksi ternak tahun n-1 x 100%',''),(8,4,2,1,1,1,'Persentase pertumbuhan PDRB ','(PDRB tahun n)-(PDRB tahun n-1)/PDRB tahun n-1 x 100%',''),(8,4,2,1,2,1,'Jumlah wisatawan','Jumlah wisatawan tahun n',''),(8,5,1,1,1,1,'Persentase jaringan jalan dalam kondisi mantap','Panjang jaringan jalan dalam kondisi mantap/Total Panjang jaringan jalan x 100%','');

/*Table structure for table `sakip_mpmd_iktujuan` */

DROP TABLE IF EXISTS `sakip_mpmd_iktujuan`;

CREATE TABLE `sakip_mpmd_iktujuan` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Nm_IndikatorTujuan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`),
  CONSTRAINT `FK_TUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`) REFERENCES `sakip_mpmd_tujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mpmd_iktujuan` */

insert  into `sakip_mpmd_iktujuan`(`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Nm_IndikatorTujuan`) values (8,1,1,1,'Jumlah konflik antar umat beragama'),(8,2,1,1,'Indeks Pembangunan Manusia (IPM)'),(8,3,1,1,'Indeks Reformasi Birokrasi'),(8,4,1,1,'Persentase pertumbuhan produksi bahan pangan'),(8,4,2,1,'Laju pertumbuhan ekonomi'),(8,5,1,1,'Persentase jaringan jalan yang menghubungkan pusat-pusat kegiatan dalam wilayah dalam kondisi baik');

/*Table structure for table `sakip_mpmd_misi` */

DROP TABLE IF EXISTS `sakip_mpmd_misi`;

CREATE TABLE `sakip_mpmd_misi` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Nm_Misi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`),
  CONSTRAINT `FK_PEMDA` FOREIGN KEY (`Kd_Pemda`) REFERENCES `sakip_mpemda` (`Kd_Pemda`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mpmd_misi` */

insert  into `sakip_mpmd_misi`(`Kd_Pemda`,`Kd_Misi`,`Nm_Misi`) values (8,1,'Meningkatkan iman dan taqwa kepada Tuhan Yang Maha Esa'),(8,2,'Meningkatkan kualitas sumber daya manusia dan sumber daya alam'),(8,3,'Meningkatkan tata kelola pemerintahan yang baik'),(8,4,'Meningkatkan kedaulatan pangan dan ekonomi kerakyatan'),(8,5,'Meningkatkan ketersediaan infrastruktur dan pengembangan wilayah');

/*Table structure for table `sakip_mpmd_sasaran` */

DROP TABLE IF EXISTS `sakip_mpmd_sasaran`;

CREATE TABLE `sakip_mpmd_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Nm_Sasaran` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_PEMDA_IKTUJUAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`) REFERENCES `sakip_mpmd_iktujuan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mpmd_sasaran` */

insert  into `sakip_mpmd_sasaran`(`Uniq`,`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Nm_Sasaran`) values (1,8,1,1,1,1,'Terpeliharanya toleransi antar umat beragama'),(2,8,2,1,1,1,'Meningkatnya akses pendidikan'),(3,8,2,1,1,2,'Meningkatnya derajat kesehatan masyarakat'),(4,8,2,1,1,3,'Menurunnya angka kemiskinan'),(5,8,3,1,1,1,'Meningkatnya akuntabilitas pemerintahan'),(6,8,4,1,1,1,'Meningkatnya produksi bahan pangan utama'),(7,8,4,2,1,1,'Meningkatnya PDRB '),(10,8,4,2,1,2,'Meningkatkan Kunjungan Wisatawan'),(9,8,5,1,1,1,'Meningkatnya infrastruktur jaringan jalan ');

/*Table structure for table `sakip_mpmd_tujuan` */

DROP TABLE IF EXISTS `sakip_mpmd_tujuan`;

CREATE TABLE `sakip_mpmd_tujuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Nm_Tujuan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_MISI` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`) REFERENCES `sakip_mpmd_misi` (`Kd_Pemda`, `Kd_Misi`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_mpmd_tujuan` */

insert  into `sakip_mpmd_tujuan`(`Uniq`,`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Nm_Tujuan`) values (1,8,1,1,'Terciptanya kehidupan umat beragama yang harmonis'),(2,8,2,1,'Meningkatkan produktivitas sumber daya manusia '),(3,8,3,1,'Meningkatkan profesionalisme birokrasi'),(4,8,4,1,'Mewujudkan ketahanan pangan Kabupaten Humbang Hasundutan'),(5,8,4,2,'Meningkatkan pertumbuhan ekonomi daerah '),(6,8,5,1,'Meningkatkan konektivitas antar wilayah');

/*Table structure for table `sakip_msatuan` */

DROP TABLE IF EXISTS `sakip_msatuan`;

CREATE TABLE `sakip_msatuan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Satuan` varchar(50) NOT NULL,
  `Nm_Satuan` varchar(50) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

/*Data for the table `sakip_msatuan` */

insert  into `sakip_msatuan`(`Uniq`,`Kd_Satuan`,`Nm_Satuan`,`Create_By`,`Create_Date`,`Edit_By`,`Edit_Date`) values (1,'%','Persen','admin','2019-03-14 23:59:59',NULL,NULL),(2,'Nilai','Nilai','admin','2019-03-14 23:59:59',NULL,NULL),(3,'Indeks','Indeks','admin','2019-03-14 23:59:59',NULL,NULL),(4,'Unit','Unit','admin','2019-03-14 23:59:59','admin','2019-08-27 02:23:25'),(5,'ORG','Orang','admin','2019-03-14 23:59:59',NULL,NULL),(6,'OP','Opini','admin','2019-03-14 23:59:59',NULL,NULL),(7,'KEL','Kelompok','admin','2019-03-14 23:59:59',NULL,NULL),(8,'CABOR','Cabor','admin','2019-03-14 23:59:59',NULL,NULL),(9,'TIM','Tim','admin','2019-03-14 23:59:59',NULL,NULL),(10,'Dokumen','Dokumen','admin','2019-03-14 23:59:59','admin','2019-08-27 02:23:33'),(11,'Jasa','Jasa','admin','2019-03-14 23:59:59',NULL,NULL),(12,'Item','Item','admin','2019-03-14 23:59:59',NULL,NULL),(13,'Jenis','Jenis','admin','2019-03-14 23:59:59',NULL,NULL),(14,'Bahan Bacaan','Bahan Bacaan','admin','2019-03-14 23:59:59',NULL,NULL),(15,'Kebutuhan','Kebutuhan','admin','2019-03-14 23:59:59',NULL,NULL),(16,'Kali','Kali','admin','2019-03-14 23:59:59',NULL,NULL),(17,'Lembaga','Lembaga','admin','2019-03-14 23:59:59',NULL,NULL),(18,'Paket','Paket','admin','2019-03-14 23:59:59',NULL,NULL),(19,'Gugus & IGTK','Gugus & IGTK','admin','2019-03-14 23:59:59',NULL,NULL),(20,'Ruang','Ruang','admin','2019-03-14 23:59:59',NULL,NULL),(21,'KK-DATADIK PAUD','KK-DATADIK PAUD','admin','2019-03-14 23:59:59',NULL,NULL),(22,'LS','LS','admin','2019-03-14 23:59:59',NULL,NULL),(23,'Siswa','Siswa','admin','2019-03-14 23:59:59',NULL,NULL),(24,'Mapel','Mapel','admin','2019-03-14 23:59:59',NULL,NULL),(25,'Mata Lomba','Mata Lomba','admin','2019-03-14 23:59:59',NULL,NULL),(26,'Kegiatan','Kegiatan','admin','2019-03-14 23:59:59',NULL,NULL),(27,'Buku','Buku','admin','2019-03-14 23:59:59',NULL,NULL),(28,'Sekolah','Sekolah','admin','2019-03-14 23:59:59',NULL,NULL),(29,'Lokasi','Lokasi','admin','2019-03-14 23:59:59',NULL,NULL),(30,'Exemplar','Exemplar','admin','2019-03-14 23:59:59',NULL,NULL),(31,'Set','Set','admin','2019-03-14 23:59:59',NULL,NULL),(32,'Event','Event','admin','2019-03-14 23:59:59',NULL,NULL),(33,'Keping','Keping','admin','2019-03-14 23:59:59',NULL,NULL),(34,'Aplikasi','Aplikasi','admin','2019-03-14 23:59:59',NULL,NULL),(35,'Pagelaran','Pagelaran','admin','2019-03-14 23:59:59',NULL,NULL),(36,'Wadah','Wadah','admin','2019-03-14 23:59:59',NULL,NULL),(37,'Buah','Buah','admin','2019-03-14 23:59:59',NULL,NULL),(38,'Desa','Desa','admin','2019-03-14 23:59:59',NULL,NULL),(39,'Batas Perwilayahan','Batas Perwilayahan','admin','2019-03-14 23:59:59',NULL,NULL),(40,'Produk Hukum','Produk Hukum','admin','2019-03-14 23:59:59',NULL,NULL),(41,'WP','WP','admin','2019-03-14 23:59:59',NULL,NULL),(42,'Rekening','Rekening','admin','2019-03-15 08:49:13',NULL,NULL),(43,'OH','OH','admin','2019-03-15 08:49:23',NULL,NULL),(44,'Ha','Hektare','admin','2019-03-19 11:01:43','admin','2019-08-27 02:23:47'),(45,'Ton','Ton','admin','2019-03-19 11:01:59',NULL,NULL),(47,'Batang','Batang','admin','2019-03-19 11:02:30',NULL,NULL),(48,'Komoditi','Komoditi','admin','2019-03-19 11:02:51',NULL,NULL),(49,'Poktan','Poktan','admin','2019-03-19 12:15:56',NULL,NULL),(51,'Km','Kilometer','admin','2019-03-20 12:03:37',NULL,NULL),(52,'Penangkar','Penangkar','admin','2019-03-20 12:51:18',NULL,NULL),(53,'WTP','Wajar Tanpa Pengecualian','admin','2019-03-20 12:56:22',NULL,NULL),(54,'WDP','Wajar Dengan Pengecualian','admin','2019-03-20 12:56:37',NULL,NULL),(55,'Disclaimer','Disclaimer','admin','2019-03-20 12:57:04',NULL,NULL),(56,'Level','Level','admin','2019-03-20 12:57:14',NULL,NULL),(57,'Laporan','Laporan','admin','2019-03-20 12:57:25',NULL,NULL),(58,'Rencana Aksi','Rencana Aksi','admin','2019-03-20 12:57:41',NULL,NULL),(59,'OPD','OPD','admin','2019-03-20 12:57:59','admin','2019-08-22 09:09:23'),(60,'Koperasi','Koperasi','admin','2019-03-21 04:26:35',NULL,NULL),(61,'UMKM','UMKM','admin','2019-03-21 04:26:48',NULL,NULL),(62,'IKM','IKM','admin','2019-03-21 04:27:32',NULL,NULL),(63,'Pasar','Pasar','admin','2019-03-21 04:28:21',NULL,NULL),(64,'Rumah Tangga','Rumah Tangga','admin','2019-03-21 05:07:57',NULL,NULL),(65,'Meter','Meter','admin','2019-03-21 05:08:06',NULL,NULL),(66,'65','kecamatan','admin','2019-05-06 09:53:05',NULL,NULL),(67,'Bulan','Bulan','admin','2019-07-16 03:55:03',NULL,NULL),(68,'Unit Kerja','Unit Kerja','admin','2019-08-15 03:49:22',NULL,NULL),(69,'Perpustakaan','Perpustakaan','admin','2019-08-15 03:49:33',NULL,NULL),(72,'Ekor','Ekor','admin','2019-08-15 03:49:54',NULL,NULL),(74,'Hari','Hari','admin','2019-08-19 07:13:55',NULL,NULL),(75,'Bungkus','Bungkus','admin','2019-08-19 07:14:08',NULL,NULL),(76,'Tilang','Tilang','admin','2019-08-19 07:14:24',NULL,NULL),(77,'Perusahaan','Perusahaan','admin','2019-08-19 07:14:33',NULL,NULL),(78,'Organisasi','Organisasi','admin','2019-08-20 07:02:57',NULL,NULL),(79,'DI','Daerah Irigasi','admin','2019-08-20 07:03:07',NULL,NULL),(80,'Peserta','Peserta','admin','2019-08-20 11:05:19',NULL,NULL),(81,'kg','kg','admin','2019-08-21 03:43:21',NULL,NULL),(82,'kg/kapita/tahun','kg/kapita/tahun','admin','2019-08-21 03:46:01',NULL,NULL),(83,'Produk','Produk','admin','2019-08-21 04:14:14',NULL,NULL),(84,'Boot/Stand','Boot/Stand','admin','2019-08-21 04:15:10',NULL,NULL),(85,'Tepat Waktu','Tepat Waktu','admin','2019-08-21 08:07:00',NULL,NULL),(86,'SP2D','SP2D','admin','2019-08-21 08:07:22',NULL,NULL),(87,'Temuan','Temuan','admin','2019-08-21 08:07:29',NULL,NULL),(88,'Rp','Rupiah','admin','2019-08-21 08:07:55',NULL,NULL),(89,'RTP','RTP','admin','2019-08-21 09:11:11',NULL,NULL),(90,'Pedagang','Pedagang','admin','2019-08-21 09:29:02',NULL,NULL),(91,'Faskes','Fasilitas Kesehatan','admin','2019-08-21 09:36:01',NULL,NULL),(92,'PPKBD dan Sub PPKKBD','PPKBD dan Sub PPKKBD','admin','2019-08-21 09:55:14',NULL,NULL),(93,'Bangunan','Bangunan','admin','2019-08-21 10:00:22',NULL,NULL),(94,'Kepala Keluarga','Kepala Keluarga (KK)','admin','2019-08-21 10:04:27',NULL,NULL),(95,'Skor','Skor','admin','2019-08-21 11:20:46',NULL,NULL),(96,'Angka','Angka','admin','2019-08-21 11:20:57',NULL,NULL),(97,'Kampung KB','Kampung KB','admin','2019-08-21 11:32:27',NULL,NULL),(98,'Informasi','Informasi','admin','2019-08-22 02:41:47',NULL,NULL),(99,'Meter Persegi','Meter Persegi','admin','2019-08-22 05:20:17',NULL,NULL),(100,'KWT','KWT','admin','2019-08-22 07:56:32',NULL,NULL),(101,'Poktan','Poktan','admin','2019-08-22 08:13:52',NULL,NULL),(102,'Predikat','Predikat','admin','2019-08-22 09:09:04',NULL,NULL),(103,'Kasus','Kasus','admin','2019-08-27 09:29:28',NULL,NULL),(104,'Responden','Responden','admin','2019-08-27 10:18:45',NULL,NULL),(105,'Sertifikat','Sertifikat','admin','2019-08-27 11:36:15',NULL,NULL),(106,'Pelaku usaha dan atau kegiatan','Pelaku usaha dan atau kegiatan','admin','2019-08-27 12:52:54',NULL,NULL),(107,'Judul','Judul','admin','2019-08-28 00:39:02',NULL,NULL),(108,'Peraturan','Peraturan','admin','2019-08-28 01:09:45',NULL,NULL),(109,'Titik','Titik','admin','2019-08-28 04:53:05',NULL,NULL),(110,'Puskesmas','Puskesmas','admin','2019-08-28 04:56:10',NULL,NULL),(111,'Titik','Titik','admin','2019-08-28 05:27:58',NULL,NULL),(112,'Media Informasi','Media Informasi','admin','2019-08-28 07:43:41',NULL,NULL),(113,'Wajib Pajak','Wajib Pajak','admin','2019-08-28 09:50:29',NULL,NULL),(114,'SPPT','SPPT','admin','2019-08-28 09:51:15',NULL,NULL),(115,'Balai Penyuluhan','Balai Penyuluhan','admin','2019-08-28 12:08:03',NULL,NULL),(116,'Rumah','Rumah','admin','2019-08-29 04:37:30',NULL,NULL),(117,'Sampel','Sampel','admin','2019-08-30 03:05:53',NULL,NULL),(118,'Leaflet/baliho','Leaflet/baliho','admin','2019-08-30 03:06:12',NULL,NULL),(119,'Pemotong','Pemotong','admin','2019-08-30 03:24:07',NULL,NULL),(120,'Dosis','Dosis','admin','2019-08-30 04:25:40',NULL,NULL);

/*Table structure for table `sakip_msubbid` */

DROP TABLE IF EXISTS `sakip_msubbid`;

CREATE TABLE `sakip_msubbid` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Nm_Subbid` varchar(200) DEFAULT NULL,
  `Nm_Kasubbid` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  KEY `Uniq` (`Uniq`),
  CONSTRAINT `FK_BID4` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=389 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_msubbid` */

insert  into `sakip_msubbid`(`Uniq`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`,`Nm_Subbid`,`Nm_Kasubbid`) values (33,1,1,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Tagor Sihombing, SE'),(34,1,1,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Humiras Pakpahan, ST'),(35,1,1,1,1,2,1,'Seksi Pembinaan PAUD','Serly W.L. Simamora, S.Kom'),(36,1,1,1,1,2,2,'Seksi Pembinaan Pendidikan Non Formal','Yohannes Sugiarto Fritz Esa, S.Kom, M.Si'),(37,1,1,1,1,2,3,'Seksi Prasarana dan Sarana','Tahi Purba, S.Pd'),(38,1,1,1,1,3,1,'Seksi Pendidikan Sekolah Dasar','Monalon Munthe,S.Pd'),(39,1,1,1,1,3,2,'Seksi Pendidikan Sekolah Menengah Pertama ','Christina Siregar,S.Psi'),(40,1,1,1,1,3,3,'Seksi Prasarana dan Sarana','Tulus Parlindungan Sipahutar,ST'),(41,1,1,1,1,4,1,'Seksi Pendayagunaan','Marolop Sihotang, S.Pd'),(42,1,1,1,1,4,2,'Seksi Pengembangan Karir','Magdalena Sinaga,S.Pd'),(43,1,1,1,1,4,3,'Seksi Displin dan Etika Profesi','Andry Dolok Purba,SH, MM'),(44,1,2,1,1,1,1,'Sub Bagian Program dan Informasi Kesehatan','Juita Sinambela, SKM'),(45,1,2,1,1,1,2,'Sub Bagian Keuangan, Kepegawaian dan Umum','Santy Elfrida Simanjuntak, SKM'),(46,1,2,1,1,2,1,'Seksi Kesehatan Keluarga dan Gizi','Tumour Rumondang R.S, SKM'),(47,1,2,1,1,2,2,'Seksi Promosi dan Pemberdayaan Kesehatan','Hodi Raju Lumban Gaol,SKM'),(48,1,2,1,1,2,3,'Seksi Kesehatan Lingkungan, Kesehatan Kerja dan Olahraga ','Benne Ditha Simamora, Amd'),(49,1,2,1,1,3,1,'Seksi Surveilans dan Imunisasi','Rommel Pasaribu'),(50,1,2,1,1,3,2,'Seksi Pencegahan dan Pengendalian Penyakit Menular','Sahala Manalu'),(51,1,2,1,1,3,3,'Seksi Pengendalian Penyakit Tidak Menular dan Kesehatan Jiwa','Tierni Sitorus, AMK'),(52,1,2,1,1,4,1,'Seksi Pelayanan dan Jaminan Kesehatan','Linda Hutasoit, SKM'),(53,1,2,1,1,4,2,'Seksi Kefarmasian dan Sarana Prasarana','Marlince Sihite, S.Farm., Apt'),(54,1,2,1,1,4,3,'Seksi Sumber Daya Manusia dan Perizinan','Bertua SM Siregar, SKM'),(55,1,2,1,1,4,4,'UPT Puskesmas Matiti',NULL),(56,1,2,1,1,4,5,'Sub Bagian Tata Usaha UPT Puskesmas Matiti',NULL),(57,1,2,1,1,4,6,'UPT Puskesmas Sigompul',NULL),(58,1,2,1,1,4,7,'Sub Bagian Tata Usaha UPT Puskesmas Sigompul',NULL),(59,1,2,1,1,4,8,'UPT Puskesmas Paranginan',NULL),(60,1,2,1,1,4,9,'Sub Bagian Tata Usaha UPT Puskesmas Paranginan',NULL),(61,1,2,1,1,4,10,'UPT Puskesmas Hutapaung',NULL),(62,1,2,1,1,4,11,'Sub Bagian Tata Usaha UPT Puskesmas Hutapaung',NULL),(63,1,2,1,1,4,12,'UPT Puskesmas Bonandolok',NULL),(64,1,2,1,1,4,13,'Sub Bagian Tata Usaha UPT Puskesmas Bonandolok',NULL),(65,1,2,1,1,4,14,'UPT Puskesmas Onan Ganjang',NULL),(66,1,2,1,1,4,15,'Sub Bagian Tata Usaha UPT Puskesmas Onan Ganjang',NULL),(67,1,2,1,1,4,16,'UPT Puskesmas Tarabintang',NULL),(68,1,2,1,1,4,17,'Sub Bagian Tata Usaha UPT Puskeskas Tarabintang',NULL),(69,1,2,1,1,4,18,'UPT Puskesmas Saitnihuta',NULL),(70,1,2,1,1,4,19,'Sub Bagian Tata Usaha UPT Puskesmas Saitnihuta',NULL),(71,1,2,1,1,4,20,'UPT Puskesmas Hutagalung',NULL),(72,1,2,1,1,4,21,'Sub Bagian Tata Usaha UPT Puskesmas Hutagalung',NULL),(73,1,2,1,1,4,22,'UPT Puskesmas Baktiraja',NULL),(74,1,2,1,1,4,23,'Sub Bagian Tata Usaha UPT Puskesmas Baktiraja',NULL),(75,1,2,1,1,4,24,'UPT Puskesmas Pakkat',NULL),(76,1,2,1,1,4,25,'Sub Bagian Tata Usaha UPT Puskesmas Pakkat',NULL),(77,1,2,1,1,4,26,'UPT Puskesmas Parlilitan',NULL),(78,1,2,1,1,4,27,'Sub Bagian Tata Usaha UPT Puskesmas Parlilitan',NULL),(336,1,2,1,2,1,1,'Subbag Keuangan','Darma Manalu, SE'),(337,1,2,1,2,1,2,'Subbag Umum','Fernando MT. Purba, SE'),(338,1,2,1,2,2,1,'Seksi Rawat Jalan','Anna Berliana Siregar, S.Kep'),(339,1,2,1,2,2,2,'Seksi Peningkatan Mutu','dr. Henry R. Manalu'),(340,1,2,1,2,3,1,'Seksi Kebidanan','Berliana S. Sinamo'),(341,1,2,1,2,3,2,'Seksi Keperawatan','Tine Sinaga'),(343,1,2,1,2,4,1,'Seksi Sarana dan Prasarana','Nikson Manalu'),(344,1,2,1,2,4,2,'Seksi Penunjang Medik','Arimbi P. Siahaan'),(297,1,3,1,1,1,3,'Kepala Sub.bagian Perencanaan dan Keuangan','R. SAHAT NELSON SIAGIAN, SE'),(298,1,3,1,1,1,4,'Kepala Sub.bagian Umum dan Kepegawaian ','HOSDIANI M. SITUMORANG, ST'),(81,1,3,1,1,2,1,'Seksi Pembangunan dan Pemeliharaan Jalan ','PETRUS SHF RADJAGUKGUK, ST'),(82,1,3,1,1,2,2,'Seksi Pembangunan dan Pemeliharaan Jembatan','BENNY H. MARBUN, ST'),(83,1,3,1,1,2,3,'Seksi Peralatan, Pengujian dan Laboratorium','POMMER M. HUTABARAT, ST'),(84,1,3,1,1,3,1,'Seksi Pembangunan Irigasi, Sungai dan Rawa','MISAEL H. SIMAMORA, ST'),(85,1,3,1,1,3,2,'Seksi Pemeliharaan Sarana Irigasi, Sungai dan Rawa','PASKA RIA PURBA, ST , MM'),(86,1,3,1,1,3,3,'Seksi Pembangunan dan Pemeliharaan drainase, Pengelolaan Air Limbah dan Persampahan','RONNI SILALAHI, ST'),(387,1,3,1,1,4,1,'Kepala Seksi Penataan Ruang','Irwan Baktiar Siregar, ST'),(388,1,3,1,1,4,2,'Kepaa Seksi Perkotaan dan Perdesaan','DIMPU OJAK P. ARITONANG,ST,M.Si'),(89,1,4,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Royani Sitorus, ST'),(90,1,4,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Libertyna Febriyanthi Br. Pinem, SE'),(91,1,4,1,1,2,1,'Seksi Pembinaan dan Pengembangan Lingkungan Hunian Perumahan ','Robinhot J. Lbn Toruan, ST. MT'),(92,1,4,1,1,2,2,'Seksi Pembangunan dan Pemeliharaan Perumahan','Else Ambarita, ST'),(93,1,4,1,1,2,3,'Seksi Pelayanan Air Bersih dan Pemeliharaan Utilitas Perumahan','Dina. V.W.O. Simamora, ST'),(365,1,4,1,1,2,4,'UPT SPAM (Sistem Penyediaan Air Minum)','Eliston . ST'),(94,1,4,1,1,3,1,'Seksi Pertanahan dan Pengembangan Kawasan Permukiman','Boima Tambunan, ST'),(95,1,4,1,1,3,2,'Seksi Tata Bangunan dan Lingkungan Kawasan Permukiman','Tulus Manullang, ST'),(96,1,4,1,1,3,3,'Seksi Pembangunan Infrastruktur dan Utilitas Kawasan Permukiman ','Samuel H.H.D. Butar-Butar, ST'),(356,1,5,1,1,1,1,'Kepala Sub Bagian Umum dan Kepegawaian','Japosman Sihite, S.Pd'),(358,1,5,1,1,1,2,'Kepala Sub Bagian Perencanaan dan Keuangan','Arar P.M. Purba, SE, MM'),(359,1,5,1,1,2,1,'Kepala Seksi Penegakan Produk Hukum Daerah dan Peningkatan SDM','Monang Folmer, S, Sos'),(376,1,5,1,1,2,3,'Kepala Seksi Peralatan dan Perlindungan Masyarakat','Dasmer M.P. Purba, SE'),(361,1,5,1,1,2,4,'Kepala Seksi Operasi','Makmur Simanullang'),(366,1,5,1,1,3,1,'Kepala Seksi Pemadaman','Anjur Situmorang, S. Sos'),(367,1,5,1,1,3,2,'Kepala Seksi Pencegahan','Tommy Sigalingging, S, Sos'),(368,1,5,1,1,3,3,'Kepala Seksi Inspeksi Peralatan dan Investigasi Kejadian','Oloan Pasaribu, SP'),(264,1,5,1,4,1,1,'Sub Bagian Umum dan Kepegawaian','Arta Sariwati Siahaan, S.sos'),(265,1,5,1,4,1,2,'Sub Bagian Keuangan','Romauli, SH'),(266,1,5,1,4,1,3,'Sub Bagian Program dan Perencanaan ','Gandi S.J. Purba, SE.'),(267,1,5,1,4,2,1,'Seksi Pencegahan','Edi Dharmawan Lumban Gaol, ST, M.Si'),(268,1,5,1,4,2,2,'Seksi Kesiapsiagaan','Hotman Hutagalung, ST'),(269,1,5,1,4,3,1,'Seksi  Kedaruratan','Denny IP Manik, ST.'),(270,1,5,1,4,3,2,'Seksi Logistik','Nasib Martua Lumban Batu, ST'),(271,1,5,1,4,4,1,'Seksi Rehabilitasi','Limboy P Ompusunggu, ST'),(272,1,5,1,4,4,2,'Seksi Rekonstruksi','Henry Victor Sinaga, ST.'),(106,1,6,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Mey Rianna Pasaribu,SE'),(107,1,6,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Togi Lumban Haol, S.Pd'),(108,1,6,1,1,2,1,'Seksi Pemberdayaan Sosial','Jusup Sihotang,SP'),(109,1,6,1,1,2,2,'Seksi Bantuan dan Jaminan Sosial','Hobby Sinaga,SE'),(110,1,6,1,1,3,1,'Seksi Pelayanan Sosial','Marta Lasma Rohana Bintang'),(111,1,6,1,1,3,2,'Seksi Rehabilitasi Sosial','Sanggam Lumban Gaol,S.Pd'),(112,2,1,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Luhut Parhusip'),(113,2,1,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Lasma Ester J. Simamora, SE, MM'),(114,2,1,1,1,2,1,'Seksi Pelatihan Ketenagakerjaan','Maklum Purba, SH'),(115,2,1,1,1,2,2,'Seksi Penempatan Ketenagakerjaan dan Transmigrasi','Sabam Riduan Sihombing, S.Pd, MM'),(116,2,1,1,1,3,1,'Seksi Penyelesaian Perselisihan','Hariandi Sitanggang'),(117,2,1,1,1,3,2,'Seksi Organisasi, Persyaratan Kerja dan Pengupahan','Sulastri BJ Simanullang, S.Sos,M.si'),(290,2,1,1,1,4,4,'UPT BLK','Ridwan Pasaribu, S.Pd'),(118,2,3,1,1,1,1,'Sub Bagian Perencanaan dan Evaluasi','Ratna Sulastri Sibuea, S.Sos'),(119,2,3,1,1,1,2,'Sub Bagian Umum dan Kepegawaian','Vera J. Lumban Gaol, SP'),(120,2,3,1,1,2,1,'Seksi Ketersediaan Pangan','Ika Dewi Pakpahan, SP'),(121,2,3,1,1,2,2,'Seksi Distribusi dan Kerawanan Pangan','Tetty Manurung, SP'),(122,2,3,1,1,3,1,'Seksi Konsumsi Pangan','Helfridawati Purba, SP'),(123,2,3,1,1,3,2,'Seksi Keamanan Pangan','drh. Jenita Pita Aritonang'),(307,2,5,1,1,1,1,'Kasubbag Program, Informasi dan Pelaporan','Brettyince br Simamora, SE'),(311,2,5,1,1,1,2,'Kasubbag Keuangan, Kepegawaian dan Umum','Clara Agustina Perangin-angin, A.Md'),(312,2,5,1,1,2,1,'Kasi Perencanaan , Pengendalian dan Kajian Dampak Lingkungan','Josephine Tambuanan, SP'),(313,2,5,1,1,2,2,'Kasi Pengaduan dan Penyelesaian Sengketa Penegakan Hukum Lingkungan','Maristella Simamora, SP'),(314,2,5,1,1,3,1,'Kasi Pencemaran Lingkungan Hidup','Dwisapta Vivi, O.S, ST'),(315,2,5,1,1,3,2,'Kasi Pemulihan dan Kerusakan Lingkungan Hidup','Marihot Nauli Manalu, S.Pt'),(316,2,5,1,1,4,1,'Kasi Pelayanan Kebersihan','Marihot Siagian, SH'),(317,2,5,1,1,4,2,'Kasi Pengelolaan Sampah','Rianto Hutahaean, ST'),(132,2,6,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Posmaria, SE.'),(133,2,6,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Bontor H. Silaban,SE.'),(134,2,6,1,1,2,1,'Seksi Pelayanan Pendaftaran Penduduk','Moses Simanjuntak,ST,MM.'),(135,2,6,1,1,2,2,'Seksi Pelayanan Pencatatan Sipil','Guntara Romeo Sitorus,S.Kom'),(136,2,6,1,1,2,3,'Seksi Pendokumentasian Dokumen Kependudukan','Jabatan belum diisi'),(137,2,6,1,1,3,1,'Seksi Sistem Informasi Administrasi Kependudukan','Makmur Silaban,S.Pd'),(138,2,6,1,1,3,2,'Seksi Pengolahan dan penyajian Data Kependudukan','Agus Bonar P. Gultom,S.Sos'),(139,2,6,1,1,3,3,'Seksi Kerjasama dan Inovasi Pelayanan','Pandapotan Siregar,ST'),(140,2,7,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Octovina Rita Tandi T, S. Sos'),(141,2,7,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Manaek R. Hutagalung, A. Md'),(142,2,7,1,1,2,1,'Seksi Kelembagaan dan Perangkat Desa','Ida Murniati Harianja, SH'),(143,2,7,1,1,2,2,'Seksi Administrasi Keuangan Desa','Venny Sibarani, S. Sos'),(144,2,7,1,1,2,3,'Seksi Evaluasi dan Pengendalian Administrasi Pemerintahan Desa','Dimpos Situmorang, S. Kom'),(145,2,7,1,1,3,1,'Seksi Bina Kehidupan dan Partisipasi Masyarakat','Dumaria J. Simamora, SE'),(146,2,7,1,1,3,2,'Seksi Pendayagunaan Sumber Daya Alam dan Teknologi Tepat Guna','Herman E. Damanik, SH'),(147,2,7,1,1,3,3,'Seksi Usaha Ekonomi Desa','Albertus P. Siahaan, S. Kom, MM'),(148,2,7,1,1,4,1,'Seksi Pelembagaan Pengarusutamaan Gender','Darni Asni Pakpahan, SE'),(149,2,7,1,1,4,2,'Seksi Pengembangan Pelayanan Perempuan dan Anak','Henny Dwi Putri Silaban, S. Sos'),(150,2,8,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Leny Uli Lumban Batu'),(151,2,8,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Erlin  Kurniawati SKM'),(152,2,8,1,1,2,1,'Seksi Advokasi, Penggerakan, Penyuluhan dan Pemberdayaan PLKB','Romasi Manalu'),(153,2,8,1,1,2,2,'Seksi Pengendalian Penduduk dan Informasi Keluarga','Cahaya Purba SKM'),(154,2,8,1,1,3,1,'Seksi Jaminan ber-KB dan Pembinaan Kesertaan ber-KB','Leni Gurning'),(155,2,8,1,1,3,2,'Seksi Ketahanan dan Kesejahteraan','Rotua Siregar'),(156,2,9,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Lamtiur Siburian,SE'),(157,2,9,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Dasmer M.P.Purba,SE'),(334,2,9,1,1,1,3,'Sekretaris Dinas Perhubungan','Tumbur Hutasoit M.pd'),(158,2,9,1,1,2,1,'Seksi Manajemen dan Rekayasa Lalu Lintas','Sanggul Simatupang,ST'),(159,2,9,1,1,2,2,'Seksi Angkutan','Faisal Uti Damanik,S.Sos'),(160,2,9,1,1,3,1,'Seksi Prasarana dan Sarana','Abdimpuan Hutabarat,S.IP'),(161,2,9,1,1,3,2,'Seksi Pengujian Kendaraan Bermotor Dan Perbengkelan','Erikson Natalion Panjaitan,ST'),(162,2,10,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Dongan Manurung, SS'),(163,2,10,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Piolisma Sipahutar, SE'),(164,2,10,1,1,2,1,'Seksi Pengelolaan Informasi dan Komunikasi Publik','Sanggam Tanjung, S.Sos'),(165,2,10,1,1,2,2,'Seksi Statistik dan Persandian','Naudur Purba, SE, MM'),(166,2,10,1,1,3,1,'Seksi Infrastruktur dan Teknologi','Syukur B. Marbun, S.Kom'),(167,2,10,1,1,3,2,'Seksi Penyelenggaraan e-Government','Rahmat Gunawan Manik, S.Kom'),(281,2,11,1,1,1,1,'Kepala Seksi Koperasi','Darmo Hasugian, S.Sos'),(282,2,11,1,1,1,2,'Kepala Seksi UMKM','Marintan R. Simbolon, SE'),(283,2,11,1,1,2,1,'Kepala Seksi Pemasaran dan Perlindungan Konsumen','Ronal Situmorang'),(284,2,11,1,1,2,2,'Kepala Seksi Bina Usaha Perdagangan','Panal Manalu'),(285,2,11,1,1,3,1,'Kepala Seksi Industri Agro','Evi Rumapea, ST'),(286,2,11,1,1,3,2,'Kepala Seksi Aneka Industri','Guntari Aritonang, SE'),(287,2,11,1,1,4,1,'Kasubbag Umum Dan Kepegawaian','Horsarnim Purba, SH'),(288,2,11,1,1,4,2,'Kasubbag Perencanaan dan Keuangan','Eva Duma HN. Simamora, SE'),(176,2,12,1,1,1,1,'Sub Bagian Umum dan Kepegawaian',NULL),(177,2,12,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan',NULL),(178,2,12,1,1,2,1,'Seksi Pengembangan Iklim Penanaman Modal',NULL),(179,2,12,1,1,2,2,'Seksi Promosi dan Kerjasama',NULL),(180,2,12,1,1,3,1,'Seksi Administrasi',NULL),(181,2,12,1,1,3,2,'Seksi Teknis',NULL),(182,2,12,1,1,4,1,'Seksi BUMD',NULL),(183,2,12,1,1,4,2,'Seksi ESDM',NULL),(184,2,13,1,1,1,1,'Kepala Sub Bagian Umum dan Kepegawaian','TARULI SIMORANGKIR, SH'),(185,2,13,1,1,1,2,'Kasub Bagian Perencanaan dan Keuangan','AGUSTINUS L. HUTASOIT, SE'),(186,2,13,1,1,2,1,'Seksi Pemberdayaan dan Pengembangan Pemuda','SABAR SARAGIH, SE'),(187,2,13,1,1,2,2,'Seksi Infrastruktur dan Kemitraan Pemuda','BOY CHRISTIAN SH SILABAN, S.Pd'),(188,2,13,1,1,3,1,'Seksi Pembudayaan dan Peningkatan Prestasi Olahraga','TOGI LUMBANGAOL, S.Pd'),(189,2,13,1,1,3,2,'Seksi Infrastruktur dan Kemitraan Olahraga','CINGKA SIMAMORA, S.Pd'),(190,2,17,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Sadakita Br. Tarigan'),(191,2,17,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Teofilus Rajagukguk, SKM'),(192,2,17,1,1,2,1,'Seksi Pengelolaan Perpustakaan','Lisbet Gultom '),(193,2,17,1,1,2,2,'Seksi Pembinaan dan Pengembangan Perpustakaan','Heppy Y.V.R Siahaan, S.Sos'),(194,2,17,1,1,3,1,'Seksi Pengelolaan dan Pelestarian Arsip','Swandy Mirsal, SKM'),(195,2,17,1,1,3,2,'Seksi Pembinaan Kearsipan','Rosma S.L Lumbantoruan , SE'),(216,3,2,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Megawaty Simanjuntak, SP, MM'),(217,3,2,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Saida Sulastri Situmorang, SE'),(218,3,2,1,1,2,1,'Seksi Pengembangan Destinasi','Hernes Renaldo Pangaribuan, ST'),(219,3,2,1,1,2,2,'Seksi Industri Pariwisata','Saut Sitinjak, S.Sos'),(220,3,2,1,1,3,1,'Seksi Promosi','Harapan Sibarani, SS'),(221,3,2,1,1,3,2,'Seksi Kelembagaan','SM. Fereddy Siahaan, ST'),(222,3,2,1,1,4,1,'Seksi Pengembangan dan Pelestarian Seni Budaya','Indra Hutabarat, SS'),(223,3,2,1,1,4,2,'Seksi Sejarah dan Kepurbakalaan','Dame Tudiru Samosir, S.Pd'),(196,3,3,1,1,1,1,'Sub Bagian Perencanaan dan Evaluasi','Ermawati Marbun, SP'),(197,3,3,1,1,1,2,'Sub Bagian Keuangan dan Aset','Tanrein Sianturi, SP'),(198,3,3,1,1,1,3,'Sub Bagian Umum dan Kepegawaian','Kartini Herawaty Sihombing, SE'),(199,3,3,1,1,2,1,'Seksi Produksi','Jenni Dewita Aristia Purba, S.Hut'),(200,3,3,1,1,2,2,'Seksi Perbenihan dan Perlindungan','Sanita A.M Hutabarat, SP'),(201,3,3,1,1,2,3,'Seksi Pengolahan dan Pemasaran','Emmy Lisda Situmorang, SP'),(202,3,3,1,1,3,1,'Seksi Produksi','Chandra Marbun, S.Pi'),(203,3,3,1,1,3,2,'Seksi Perbenihan dan Perlindungan','Irianto N. Simanullang, SP, M.Si'),(204,3,3,1,1,3,3,'Seksi Pengolahan dan Pemasaran','Hawardi, SP'),(205,3,3,1,1,4,1,'Seksi Kelembagaan dan Ketenagaan Penyuluhan Pertanian','Ali Reinhard Manalu, SP'),(206,3,3,1,1,4,2,'Seksi Metode dan Informasi Penyuluh Pertanian','Desmery C. Napitupulu, STP'),(207,3,3,1,1,4,3,'Seksi Prasarana dan Sarana','Bahara Hutasoit , SP'),(324,3,3,1,2,1,1,'Kasubbag Umum dan Kepegawaian','Eva Mutiara Bangun, S.Pt'),(325,3,3,1,2,1,2,'Kasubbag Perencanaan dan Keuangan','Jetty Dumaria Naibaho, SP'),(326,3,3,1,2,2,3,'Kasi Produksi dan Perbibitan','Basthian T P Sihombing, S.Pt'),(327,3,3,1,2,2,4,'Kasi Pengelolaan, Sarana dan Pemasaran','-'),(328,3,3,1,2,3,5,'Seksi Kesehatan Masyarakat Veteriner','drh. Helena Juwita Lumbantobing'),(329,3,3,1,2,3,6,'Kasi Kesehatan Hewan dan Ikan','drh. Ronal Ferinando Sembiring'),(330,3,3,1,2,4,7,'Kasi Budidaya Perikanan','Benget Derita Siregar'),(331,3,3,1,2,4,8,'Kasi Penangkapan, Pengolahan dan Pemasaran','Sudarman E Pasaribu, S.Pi'),(7,4,1,1,1,4,1,'Sub Bagian Otonomi Daerah dan Kerjasama','POSMA ST.SIMANULLANG, SE.AK'),(8,4,1,1,1,4,2,'Sub Bagian Bina Kecamatan, Kelurahan dan Pemerintah Desa','ASTRI J SIANTURI, S.Sos'),(9,4,1,1,1,4,3,'Sub Bagian Penataan Wilayah ','HEMAT A. SITANGGANG, S.IP'),(10,4,1,1,1,5,1,'Sub Bagian Perundang-undangan dan Kajian Hukum','SARWONO SIHOTANG, SH'),(11,4,1,1,1,5,2,'Sub Bagian Pelayanan dan Dokumentasi Hukum','BOY ORLANDO T. SIRAIT, SH'),(12,4,1,1,1,6,1,'Sub Bagian Kelembagaan dan Standarisasi','Saul Halomoan Tua Hutabarat, S.STP'),(13,4,1,1,1,6,2,'Sub Bagian Ketatalaksanaan dan Pelayanan Publik ','Sri Ervina Hotnida Manalu, S.Pi'),(14,4,1,1,1,7,1,'Sub Bagian Bina Pertanian','NURMALA M. SIHOTANG, SE'),(15,4,1,1,1,7,2,'Sub Bagian Bina Perdagangan, Penanaman Modal dan Pariwisata','PARLIN SIAHAAN, ST'),(16,4,1,1,1,7,3,'Sub Bagian Pembangunan','BOYKE SIMANJUNTAK,SE,MM'),(370,4,1,1,1,8,1,'SUB BAGIAN PROTOKOL','SENTI FITRI MANURUNG,S.Sos'),(371,4,1,1,1,8,2,'SUB BAGIAN TATA USAHA PIMPINAN','DANIEL MATONDANG, SSTP'),(19,4,1,1,1,9,1,'Sub Bagian Ketatausahaan dan Rumah Tangga','MARLINA NAINGGOLAN, S.Sos'),(20,4,1,1,1,9,2,'Sub Bagian Keuangan','MARIANI F.SINAGA, SS'),(21,4,1,1,1,9,3,'Sub Bagian Perlengkapan','ROLAND V. MARBUN, S.STP'),(22,4,1,1,1,10,1,'Sub Bagian Bina Pendidikan','SABAR MANULLANG, SH'),(23,4,1,1,1,10,2,'Sub Bagian Bina Sosial','EDWARD SIREGAR, SE'),(24,4,1,1,1,10,3,'Sub Bagian Bina Kesehatan','RUDI H.HUTASOIT, SKM'),(372,4,1,1,1,11,1,'Subbagian Pengadaan Barang/Jasa','Mulyadi Nainggolan, S.T.'),(373,4,1,1,1,11,2,'Subbagian Layanan Pengadaan Secara Elektronik','Horas P. Aritonang, S.T.'),(374,4,1,1,1,11,3,'Subbagian Advokasi dan Pembinaan','P. Nestor Simanjuntak, S.H.'),(25,4,1,1,4,1,1,'Sub Bagian Umum dan Kepegawaian','Juli Chirstofel Simanjuntak, SE'),(26,4,1,1,4,1,2,'Sub Bagian Humas, Keprotokolan dan Dokumentasi','Imelda Vera Simanjuntak, SE'),(27,4,1,1,4,2,1,'Sub Bagian Persidangan dan Risalah','Elly Marpaung, SH'),(28,4,1,1,4,2,2,'Sub Bagian Perundang-undangan','Rickson J. Simamora, SH., MM'),(29,4,1,1,4,3,1,'Sub Bagian Perencanaan ','Amran Simanullang, SH., MM'),(30,4,1,1,4,3,2,'Sub Bagian Keuangan','Sempurna Silaban, SE., M.Si'),(258,4,1,1,5,1,1,'Sub Bagian Umum dan Kepegawaian','Pakkat Marbun'),(259,4,1,1,5,1,2,'Sub Bagian Perencanaan dan Keuangan','Febrianti, SE'),(261,4,1,1,5,2,3,'Sub Bidang Organisasi Kemasyarakatan','Torang Marudut Simanullang, SS, MM'),(260,4,1,1,5,2,4,'Sub Bidang Politik Dalam Negeri','Hamzah Surya Situmeang, S. Sos'),(335,4,1,1,5,3,5,'Sub. Bidang Kewaspadaan Dini','Frengki M. R. Simanjuntak, SH'),(262,4,1,1,5,3,7,'Sub Bidang Penanganan Konflik','Budi Simamora, S. Pd, MM'),(31,4,2,1,1,1,1,'Sub Bagian Umum dan Keuangan','Firma T.A.P. Sitorus, S.Sos., M.M. '),(32,4,2,1,1,1,2,'Sub Bagian Perencanaan dan Pelaporan','Irwan Sihombing, S.E.'),(292,4,2,1,1,2,1,'Auditor Muda','Ganda Tua Napitupulu, S.P.'),(377,4,2,1,1,2,2,'Auditor Muda','Sudirman Sinaga, S.T.'),(385,4,2,1,1,2,3,'Auditor Madya','Drs. Lamhot Simanullang'),(293,4,2,1,1,3,1,'Auditor Muda','Ermince Naibaho, S.Sos'),(378,4,2,1,1,3,2,'Auditor Muda','De Zon Franatha, S.T.'),(386,4,2,1,1,3,3,'Auditor Muda','Efendy Napitupulu, S.E.'),(291,4,2,1,1,4,1,'Auditor Muda','Junior Sinaga, S.T.'),(383,4,2,1,1,4,2,'Auditor Muda','Lukman Pasaribu, SE., M.Si.'),(384,4,2,1,1,4,3,'Auditor Muda','Robby Fadly Purba, S.P., M.M.'),(224,4,3,1,1,1,1,'Sub Bagian Umum dan Kepegawaian',NULL),(225,4,3,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan',NULL),(226,4,3,1,1,2,1,'Sub Bidang Sekretariat dan Aparatur',NULL),(227,4,3,1,1,2,2,'Sub Bidang Kewilayahan dan Pemberdayaan Masyarakat',NULL),(228,4,3,1,1,2,3,'Sub Bidang Kependudukan, Komunikasi, Ketertiban ',NULL),(229,4,3,1,1,3,1,'Sub Bidang Ekonomi',NULL),(230,4,3,1,1,3,2,'Sub Bidang Pekerjaan Umum, Tata Ruang dan Perhubungan',NULL),(231,4,3,1,1,3,3,'Sub Bidang Perumahan, Permukiman dan Lingkungan Hidup',NULL),(232,4,3,1,1,4,1,'Sub Bidang Administrasi, Pendidikan dan Sosial',NULL),(233,4,3,1,1,4,2,'Sub Bidang Kesehatan, KB dan Ketenagakerjaan',NULL),(234,4,3,1,1,4,3,'Sub Bidang Penelitian dan Pengembangan',NULL),(235,4,4,1,1,1,1,'Sub Bagian Umum','Rismauli Simanullang, SE'),(323,4,4,1,1,1,2,'Sub Bagian Perencanaan','Tetty Situmeang, A. Md'),(237,4,4,1,1,1,3,'Sub Bagian Keuangan','Seriawan Lumban Tobing ,SH.MM'),(238,4,4,1,1,2,1,'Sub Bidang Pendataan dan Penetapan','Dewi Sri AR Siregar, SE, MM'),(239,4,4,1,1,2,2,'Sub Bidang Pemungutan dan Pengendalian','Arthur D. Y. Siahaan, SE'),(240,4,4,1,1,2,3,'Sub Bidang Pengelolaan PBB dan BPHTB','Rambe M. Manalu, SH, MM'),(241,4,4,1,1,3,1,'Sub Bidang Anggaran Pemerintahan','Budi Lumbantobing, SE, MM'),(242,4,4,1,1,3,2,'Sub Bidang Anggaran Perekonomian dan Pembangunan','Jeffri Siahaan, A.Md'),(243,4,4,1,1,3,3,'Sub Bidang Anggaran Administrasi dan Kesejahteraan Rakyat','Kristina JG Sianturi , SE. MM'),(244,4,4,1,1,4,1,'Sub Bidang Pembukuan dan Pelaporan Keuangan','Lastri M. Hutaruk, SE'),(245,4,4,1,1,4,2,'Sub Bidang Verifikasi dan Perbendaharaan','Bintang M Sitohang, SE, MM'),(246,4,4,1,1,4,3,'Sub Bidang Penggajian','Listika Manalu, SE'),(247,4,4,1,1,5,1,'Sub Bidang Penilaian dan Penghapusan','Rudi Antoni Sianturi, SE, M.Si'),(248,4,4,1,1,5,2,'Sub Bidang Pemeliharaan dan Pemanfaatan','Marsinta Situmorang, SE'),(249,4,4,1,1,5,3,'Sub Bidang Inventarisasi dan Pelaporan','Maria Rosanti Samosir, SE, MM'),(250,4,5,1,1,1,1,'Sub Bagian Umum dan Kepegawaian','Denni Ana Sinaga, SH'),(251,4,5,1,1,1,2,'Sub Bagian Perencanaan dan Keuangan','Ave Santi Marbun, SE'),(252,4,5,1,1,2,1,'Sub Bidang Perencanaan dan Pendayagunaan','Hanna B. Marbun, SE'),(253,4,5,1,1,2,2,'Sub Bidang Data dan Informasi','Barneges, S.Kom'),(254,4,5,1,1,2,3,'Sub Bidang Kesejahteraan','Andi Posman Simamora, SE, MM'),(255,4,5,1,1,3,1,'Sub Bidang Pembinaan dan Disiplin','Irma Oktovia Sinambela, S. Psi,MM'),(256,4,5,1,1,3,2,'Sub Bidang Jabatan Struktural','Ferry Pirdo Panjaitan, SH,MM'),(257,4,5,1,1,3,3,'Sub Bidang Jabatan Fungsional','Julusarlius Manalu');

/*Table structure for table `sakip_msubbid_indikator` */

DROP TABLE IF EXISTS `sakip_msubbid_indikator`;

CREATE TABLE `sakip_msubbid_indikator` (
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) NOT NULL,
  `Kd_IndikatorSubbidang` bigint(10) NOT NULL,
  `Nm_IndikatorSubbidang` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_Subbid`,`Kd_SasaranSubbidang`,`Kd_IndikatorSubbidang`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_SUBBID5` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID_SASARAN` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_Subbid`, `Kd_SasaranSubbidang`) REFERENCES `sakip_msubbid_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`, `Kd_Subbid`, `Kd_SasaranSubbidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_msubbid_indikator` */

/*Table structure for table `sakip_msubbid_kegiatan` */

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan`;

CREATE TABLE `sakip_msubbid_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) DEFAULT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Kd_SumberDana` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Target_N1` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Total` double DEFAULT '0',
  `Total_N1` double DEFAULT NULL,
  `IsEplan` tinyint(1) NOT NULL DEFAULT '1',
  `Remarks` varchar(200) DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_Kegiatan_SasaranProg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_mbid_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_msubbid_kegiatan` */

/*Table structure for table `sakip_msubbid_kegiatan_indikator` */

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan_indikator`;

CREATE TABLE `sakip_msubbid_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) DEFAULT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_IndikatorSasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_msubbid_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_msubbid_kegiatan_indikator` */

/*Table structure for table `sakip_msubbid_kegiatan_sasaran` */

DROP TABLE IF EXISTS `sakip_msubbid_kegiatan_sasaran`;

CREATE TABLE `sakip_msubbid_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) DEFAULT NULL,
  `Kd_Satuan` varchar(200) DEFAULT NULL,
  `Awal` double DEFAULT NULL,
  `Target` double DEFAULT NULL,
  `Akhir` double DEFAULT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_msubbid_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SasaranSubbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_msubbid_kegiatan_sasaran` */

/*Table structure for table `sakip_msubbid_sasaran` */

DROP TABLE IF EXISTS `sakip_msubbid_sasaran`;

CREATE TABLE `sakip_msubbid_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_SasaranSubbidang` bigint(10) NOT NULL,
  `Nm_SasaranSubbidang` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`,`Kd_Subbid`,`Kd_SasaranSubbidang`),
  KEY `Uniq` (`Uniq`),
  KEY `FK_SUBBID2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_BID_INDIKATOR` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`) REFERENCES `sakip_mbid_indikator` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_SasaranProgramOPD`, `Kd_IndikatorProgramOPD`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SUBBID4` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_msubbid_sasaran` */

/*Table structure for table `sakip_msumberdana` */

DROP TABLE IF EXISTS `sakip_msumberdana`;

CREATE TABLE `sakip_msumberdana` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_SumberDana` varchar(50) NOT NULL,
  `Nm_SumberDana` varchar(50) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_msumberdana` */

insert  into `sakip_msumberdana`(`Uniq`,`Kd_SumberDana`,`Nm_SumberDana`,`Create_By`,`Create_Date`,`Edit_By`,`Edit_Date`) values (1,'DAU','DAU','admin','2019-03-14 19:00:54',NULL,NULL),(2,'DAK','DAK','admin','2019-03-14 19:01:09','admin','2019-03-14 19:03:30'),(3,'PAD','PAD','admin','2019-03-14 19:01:35',NULL,NULL),(4,'DAU & DAK','DAU & DAK','admin','2019-08-28 05:28:44',NULL,NULL);

/*Table structure for table `sakip_roles` */

DROP TABLE IF EXISTS `sakip_roles`;

CREATE TABLE `sakip_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `sakip_roles` */

insert  into `sakip_roles`(`RoleID`,`RoleName`) values (1,'Administrator'),(2,'Bappeda'),(3,'Operator OPD'),(4,'Operator Bidang OPD'),(5,'Operator Sub Bidang OPD'),(6,'Operator Keuangan');

/*Table structure for table `sakip_settings` */

DROP TABLE IF EXISTS `sakip_settings`;

CREATE TABLE `sakip_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_settings` */

/*Table structure for table `sakip_userinformation` */

DROP TABLE IF EXISTS `sakip_userinformation`;

CREATE TABLE `sakip_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_userinformation` */

insert  into `sakip_userinformation`(`UserName`,`Email`,`CompanyID`,`Name`,`IdentityNo`,`BirthDate`,`ReligionID`,`Gender`,`Address`,`PhoneNumber`,`EducationID`,`UniversityName`,`FacultyName`,`MajorName`,`IsGraduated`,`GraduatedDate`,`YearOfExperience`,`RecentPosition`,`RecentSalary`,`ExpectedSalary`,`CVFilename`,`ImageFilename`,`RegisteredDate`) values ('admin','admin@sidasma.humbanghasundutankab.go.id',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),('bappeda','lautdinsitinjak@gmail.com','4.3.1.1','Drs.Lautdin Sitinjak,M.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-11-29'),('bappeda.bid.adm.kesra-litbang','melati@gmail.com','4.3.1.1.4','Melati Simamora, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.bid.ekonomi-pembangunan','pahala@gmail.com','4.3.1.1.3','Pahala H.L. Gaol, ST, M.Sc, M. Eng',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.bid.pemerintahan','manongam@gmail.com','4.3.1.1.2','Manongam SP Pasaribu, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.sekretaris','andi@gmail.com','4.3.1.1.1','Andi Saut Sihombing, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.sekretaris.subbag.perencanaan-keuangan','roudur@gmail.com','4.3.1.1','Roudur I. Situmorang, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.sekretaris.subbag.umu-kepeg','r_hutasoit@gmail.com','4.3.1.1.1.1','Rommel Hutasoit',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.adm-pendidikan','mangampu@gmail.com','4.3.1.1.4.1','Mangampu T. Sipahutar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.ekonomi','parluasan@gmail.com','4.3.1.1.3.1','Parluasan Silaban',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.kepedundukan.kom.ketertiban-bencana','henri@gmail.com','4.3.1.1.2.3','Henri S. Silitonga',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.kesehatan.sosial.kb-ketenagakerjaan','enrico@gmail.com','4.3.1.1.4.2','Enrico P. L. Tobing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.kewilayahan&pemberdayaanmasyarakat','sri@gmail.com','4.3.1.1.2.2','Sri Ulina G',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.penelitian&pengembangan','verikasi@gmail.com','4.3.1.1.4.3','Verikasi HMT Sinaga, S.Sos, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.perum,permukiman&lindup','yulendya@gmail.com','4.3.1.1.3.3','Yulendya T.B. Rajagukguk',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.pu,tataruang&perhubungan','novaria@gmail.com','4.3.1.1.3.2','Novaria, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bappeda.subbid.sekretariat&aparatur','dorlan@gmail.com','4.3.1.1.2.1','Dorlan R. Sihombing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.bid.pembinaankarir&disiplin','syahrijalsimamora@gmail.com','4.5.1.1.3','SYAH RIJAL SIMAMORA, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.bid.pendayagunaan&kesejahteraan','mewahrisma@gmail.com','4.5.1.1.2','MEWAH RISMA, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.kaban','domulumbangaol@gmail.com','4.5.1.1','DOMU LUMBAN GAOL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-03-05'),('bkd.keuangan','avesanti@gmail.com','4.5.1.1','Ave Santi Marbun, SE.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('bkd.sekretaris','sabarhasiholanpurba@gmail.com','4.5.1.1.1','SABAR HASIHOLAN PURBA, SH, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.subbagperencanaan&keuangan','avesantimarbun@gmail.com','4.5.1.1.1.2','AVE SANTI MARBUN, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.subbagumum&kepegawaian','brettyincesimamora@gmail.com','4.5.1.1.1.1','BRETTYINCE SIMAMORA, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.subbiddata&informasi','barneges@gmail.com','4.5.1.1.2.2','BARNEGES, S.Kom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.subbidkesejahteraan','andiposmansimamora@gmail.com','4.5.1.1.2.3','ANDI POSMAN SIMAMORA, SE, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.subbidpembinaan&disiplin','irmaoktavianasinambela@gmail.com','4.5.1.1.3.1','IRMA OKTAVIANA SINAMBELA, S,Psi, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bkd.subbidperencanaan&pendayagunaan','hannabethesdamarbun@gmail.com','4.5.1.1.2.1','HANNA BETHESDA MARBUN, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.bid.kedaruratan&logistik','astrihandayanisitompul@gmail.com','1.5.1.4.3','ASTRI HANDAYANI SITOMPUL, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.bid.pencegahan&kesiapsiagaan','binsaralfonsussitumorang@gmail.com','1.5.1.4.2','BINSAR ALFONSUS SITUMORANG, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.bid.rehabilitasi&rekonstruksi','anggiatsimanullang@gmail.com','1.5.1.4.4','ANGGIAT SIMANULLANG, ST, MT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.sekretaris','bottorpurba@gmail.com','1.5.1.4.1','BOTTOR PURBA, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.seksikesiapsiagaan','hotmanhutagalung@gmail.com','1.5.1.4.2.2','HOTMAN HUTAGALUNG, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.seksilogistik','nasibmartualumbanbatu@gmail.com','1.5.1.4.3.2','NASIB MARTUA LUMBANBATU, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.seksipenanganankedaruratan','dennyipmanik@gmail.com','1.5.1.4.3.1','DENNY IP MANIK, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.seksipencegahan','edidharmawanlumbangaol@gmail.com','1.5.1.4.2.1','EDI DHARMAWAN LUMBAN GAOL, ST, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.seksirehabilitasi','limboypompusunggu@gmail.com','1.5.1.4.4.1','LIM BOY P. OMPUSUNGGU, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.seksirekonstruksi','henryvictorsinaga@gmail.com','1.5.1.4.4.2','HENRY VICTOR SINAGA, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.subbagkeuangan','panalsitumorang@gmail.com','1.5.1.4','PANAL SITUMORANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.subbagprogram&perencanaan ','gandisjpurba@gmail.com','1.5.1.4.1.3','GANDI S.J PURBA, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbd.subbagumum&kepegawaian','artasariwatisiahaan@gmail.com','1.5.1.4.1.1','ARTA SARIWATI SIAHAAN,S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpbdhh','tumburhutagaol@gmail.com','1.5.1.4','TUMBUR HUTAGAOL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-03-05'),('bpbdsubbagprogramperencanaan','gandi.purba81@gmail.com','1.5.1.4.1.3','GANDI PURBA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-03-05'),('bpkpad','johnharrym@gmail.com','4.4.1.1','Drs. JOHN HARRY M, MA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.bid.akuntansiperbendaharaan','batarafranzsiregar@gmail.com','4.4.1.1.4','BATARA FRANZ SIREGAR,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.bid.anggaran','maradunapitupulu@gmail.com','4.4.1.1.3','Drs. MARADU NAPITUPULU, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.bid.asetdaerah','paulasimamora@gmail.com','4.4.1.1.5','PAUL A. SIMAMORA, SE, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.bid.pendapatan','martogipurba@gmail.com','4.4.1.1.2','MARTOGI PURBA, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.kasubbagperencanaan','b@bpkpad.kasubbagperencanaan','4.4.1.1','Tetty Situmeang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-19'),('bpkpad.sekretaris','zimrobenompusunggu@gmail.com','4.4.1.1.1','ZIMROBEN OMPUSUNGGU,SE,M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbagumum','rismaulisimanullang@gmail.com','4.4.1.1.1.1','RISMAULI SIMANULLANG,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidanggaranadministrasi&kesejahteraanrak','tuamarsattimarbun@gmail.com','4.4.1.1.3.3','TUA MARSATTI MARBUN, SE, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidanggaranpemerintahan','budilumbantobing@gmail.com','4.4.1.1.3.1','BUDI LUMBANTOBING, SE, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidanggaranperekonomian&pembangunan','jeffrisiahaan@gmail.com','4.4.1.1.3.2','JEFFRI SIAHAAN,A.Md',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidinventarisasi&pelaporan','mariarosantisamosir@gmail.com','4.4.1.1.5.3','MARIA ROSANTI SAMOSIR, SE, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidpembukuan&pelaporankeuangan','lastrimhutaruk@gmail.com','4.4.1.1.4.1','LASTRI M.HUTARUK,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidpemeliharaan&pemanfaatan','marsintasitumorang@gmail.com','4.4.1.1.5.2','MARSINTA SITUMORANG, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidpemungutan&pengendalian','arthurdysiahaan@gmail.com','4.4.1.1.2.2','ARTHUR D.Y SIAHAAN, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidpendataan&penetapan','dewisriarsiregar@gmail.com','4.4.1.1.2.1','DEWI SRI AR SIREGAR, SE, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidpengelolaanPBB&BPHTB','rambemmanalu@gmail.com','4.4.1.1.2.3','RAMBE M. MANALU, SH,MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidpenggajian','listikamanalu@gmail.com','4.4.1.1.4.3','LISTIKA MANALU, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidpenilaian&penghapusan','rudiantonisianturi@gmail.com','4.4.1.1.5.1','RUDI ANTONI SIANTURI, SE,M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('bpkpad.subbidverifikasi&perbendaharaan','bintangmsitohang@gmail.com','4.4.1.1.4.2','BINTANG M SITOHANG,SE, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('developer','developer@humbanghasundutankab.go.id','4.3.1.1.1','Developer Humbahas',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-07'),('dinaspendidikan','disdik@humbanghasundutankab.go.id','1.1.1.1','Opr. Dinas Pendidikan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-07'),('dinaspendidikan2','disdik2@humbanghasundutankab.go.id','1.1.1.1.1','Opr. Bidang Dinas Pendidikan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-07'),('dinaspendidikan3','disdik3@humbanghasundutankab.go.id','1.1.1.1.1.1','Opr. Sub Bidang Dinas Pendidikan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-07'),('dinkes','budiman@gmail.com','1.2.1.1','dr. Binner Sinaga, M.Kes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.bid.kesmasyarakat','tahan@gmail.com','1.2.1.1.2','dr. Tahan M. Simamora',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.bid.pelayanan&sumberdayakesehatan','lusianna@gmail.com','1.2.1.1.4','dr. Lusianna Silaban',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.bid.pencegahan&pengendalianpenyakit','harland@gmail.com','1.2.1.1.3','dr. Harland T. Sihombing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.sekretaris','binner@gmail.com','1.2.1.1.1','dr. Binner Sinaga, M.Kes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.sekretaris.subbag.keuangan','santy@gmail.com','1.2.1.1','Santy E. Simanjuntak',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.sekretaris.subbag.prog&informasikes','mey@gmail.com','1.2.1.1.1.1','Mey R. Pasaribu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.kefarmasian&sarpras','marlince@gmail.com','1.2.1.1.4.2','Marlince Sihite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.keskeluarga&gizi','tumour@gmail.com','1.2.1.1.2.1','Tumour R. R. S',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.keslingkungan,keskerja&olahraga','benne@gmail.com','1.2.1.1.2.3','Benne D.Simamora',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.pelayanan&jaminankesehatan','linda@gmail.com','1.2.1.1.4.1','Linda Hutasoit',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.pengendalianpenyakitmenular','sahala@gmail.com','1.2.1.1.3.2','Sahala Manalu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.pengendalianpenyakittidakmenular&kesw','tierni@gmail.com','1.2.1.1.3.3','Tierni Sitorus',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.promosi&pemberdayaankesehatan','restu@gmail.com','1.2.1.1.2.2','Restu Imanesa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.sdm&perizinan','bertua@gmail.com','1.2.1.1.4.3','Bertua SM SIregar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinkes.seksi.surveilans&imunisasi','rommelpasaribu@gmail.com','1.2.1.1.3.1','Rommel Pasaribu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb','elson@gmail.com','2.8.1.1','Drs. Janter Sinaga',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.bid.kb&kk','saritua@gmail.com','2.8.1.1.3','Saritua Harianja, SKM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.bid.pp&pp','jeni@gmail.com','2.8.1.1.2','Jeni Dewi K.Siagian',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.sekretaris','hasudungan@gmail.com','2.8.1.1','Erlin Kurniawati',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.seksi.advokasi,pp&pemb.plkb','romasi@gmail.com','2.8.1.1.2.1','Romasi Manalu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.seksi.jaminanber-kb','leni@gmail.com','2.8.1.1.3.1','Leni S. Gurning',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.seksi.ketahanan&kk','rotua@gmail.com','2.8.1.1.3.2','Rotua Siregar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.seksi.pp&ik','firdaus@gmail.com','2.8.1.1.2.2','Firdaus S. H. Gultom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.subbag.perencanaan&keuangan','erlin@gmail.com','2.8.1.1.1.2','Erlin Kurniawati, SKM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinp2kb.subbag.umum&kepeg','leny@gmail.com','2.8.1.1.1.1','Leny Uli M. L. Batu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinsos','paiman@gmail.com','1.6.1.1','Drs. Vandeik Simanungkalit, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinsos.bid.pelayanan&rehabilitasisosial','serinaya@gmail.com','1.6.1.1.3','Serinaya Tinambunan, S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinsos.bid.pemberdayaan&bantuansosial','delima@gmail.com','1.6.1.1.2','Delima Sianturi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinsos.kabid.bidang.pelayanan.rehabilitasi.sosial','seri@gmail.com','1.6.1.1.3','Serinaya Tambunan,S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('dinsos.kabid.pemberdayaan.bantuan.sosial','dsianturi@gmail.com','1.6.1.1.2','Delima Sianturi, S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('dinsos.kasi.bantuan.jaminan.sosial','jusuf@gmail.com','1.6.1.1.2.2','Jusuf Sihotang, S.P',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('dinsos.kasi.pelayanan.sosial','marta@gmail.com','1.6.1.1.3.1','Marta Lasma Rohana Bintang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('dinsos.kasi.pemberdayaan.sosial','hobby@gmail.com','1.6.1.1.2.1','Hobby Murtopo Sinaga, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('dinsos.kasi.rehabilitasi.sosial','sanggam_lg@gmail.com','1.6.1.1.3.2','Sanggam Lumban Gaol, S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('dinsos.subbag.perencanaan&keuangan','romauli@gmail.com','1.6.1.1.1.2','Romauli, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dinsos.subbag.perencanaan.keuangan','roma@gmail.com','1.6.1.1','Romauli,SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('dinsos.subbag.umum.kepegawaian','aa@gmail.com','1.6.1.1.1.1','AA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('dinsossekretaris','Yosefin@gmail.com','1.6.1.1.1','Yosefin Samosir, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-02-07'),('disdik','jamilin@gmail.com','1.1.1.1','Drs. Jamilin Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.bid.paud.seksi.sarpras','tahi@gmail.com','1.1.1.1.2.3','Tahi Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.bid.pembinaanketenagaan','armin@gmail.com','1.1.1.1.4','Drs. Armin Banjarnahor',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.bid.pembinaanpaud','rosita@gmail.com','1.1.1.1.2','Rosita Gultom, S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.bid.pembinaanpendidikandasar','christison@gmail.com','1.1.1.1.3','Christison R. Marbun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.keuangan','sarprashumbahas@gmail.com','1.1.1.1','Humiras Pakpahan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-28'),('disdik.sekretaris','danner@gmail.com','1.1.1.1.1','Drs. Danner Panjaitan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.sekretaris.subbag.perencanaankeuangan','humiras@gmail.com','1.1.1.1','Humiras Pakpahan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.sekretaris.subbag.umum','tagor@gmail.com','1.1.1.1.1.1','Tagor Sihombing, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.seksi.disiplin&etikaprofesi','andry@gmail.com','1.1.1.1.4.3','Andry Dolok Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.seksi.pemb.paud','serly@gmail.com','1.1.1.1.2.1','Serly W.L. Simamora',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.seksi.pemb.paud&pendnonformal','david@gmail.com','1.1.1.1.2.2','David R. Siahaan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.seksi.pendayagunaan','sabam@gmail.com','1.1.1.1.4.1','Sabam R. Sihombing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.seksi.pendidikansd','torang@gmail.com','1.1.1.1.3.1','Torang M. Simanullang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.seksi.pendidikansmp','christina@gmail.com','1.1.1.1.3.2','Christina Siregar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disdik.seksi.saranaprasarana','tulus@gmail.com','1.1.1.1.3.3','Tulus P. Sipahutar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub','jaulim@gmail.com','2.9.1.1','Jaulim Simanullang, S.Pd., MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.bid.mrlla','pantas@gmail.com','2.9.1.1.2','Drs. Pantas Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.bid.prasarana&sarana','ramli@gmail.com','2.9.1.1.3','Ramli Nababan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.sekretaris','tumbur@gmail.com','2.9.1.1.1','Tumbur Hutasoit, M.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.sekretaris.subbag.perencanaan&keuangan','dasmer@gmail.com','2.9.1.1.1.2','Dasmer M.P. Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.sekretaris.subbagumum&kepeg','lamtiur@gmail.com','2.9.1.1.1.1','Lamtiur Siburian',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.seksi.angkutan','Faisal@gmail.com','2.9.1.1.2.2','Faisal Uti Damanik',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.seksi.mrll','Sanggul@gmail.com','2.9.1.1.2.1','Sanggul Smatupang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.seksi.pengujiankendaraanbermotor&perbengkel','Erikson@gmail.com','2.9.1.1.3.2','Erikson Natalion Panjaitan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dishub.seksi.prasarana&sarana','Abdimpuan@gmail.com','2.9.1.1.3.1','Abdimpuan Hutabarat',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('diskominfo','situmorangdeddy@gmail.com','2.10.1.1','Drs. Hotman Hutasoit',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-09'),('diskominfo.bid.informatika','situmorangdeddy4@gmail.com','2.10.1.1.3','Deddy Situmorang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-09'),('diskominfo.bid.informatika.egov','manik@gmail.com','2.10.1.1.3.2','Rahmat G. Manik',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('diskominfo.bid.informatika.teknologi','syukur@gmail.com','2.10.1.1.3.1','syukur berkat marbun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-09'),('diskominfo.bid.komunikasi','Mangadar@gmail.com','2.10.1.1.2','Mangadar Lumbanraja',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('diskominfo.bid.komunikasi.pengelolainformasi&komun','sanggam@gmail.com','2.10.1.1.2.1','Sanggam Tanjung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('diskominfo.bid.komunikasi.statistik&persandian','naudur@gmail.com','2.10.1.1.2.2','Naudur Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('diskominfo.sekretaris','jinituamalau@gmail.com','2.10.1.1.1','Jinitua Malau',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-09'),('diskominfo.sekretaris.subbag.perencanaankeuangan','piolisma@gmail.com','2.10.1.1','Piolisma Sipahutar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('diskominfo.sekretaris.subbag.umum&kepeg','dongan@gmail.com','2.10.1.1.1.1','Dongan Manurung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan','luhutmarbun@gmail.com','3.3.1.2','LUHUT MARBUN, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.bid.kesehatanhewan,ikan&masyarakatveterin','suryajayajunjungan@gmail.com','3.3.1.2.3','Drh. SURYA JAYA JUNJUNGAN, SKH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.bid.perikanan','rudythsimamora@gmail.com','3.3.1.2.4','RUDY T H SIMAMORA,S.Pi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.bid.peternakan','sanggamksihombing@gmail.com','3.3.1.2.2','SANGGAM K SIHOMBING, S.Pt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.sekretaris','nelladumawatisimamora@gmail.com','3.3.1.2.1','Drh. NELLA DUMAWATI SIMAMORA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.seksibudidayaperikanan','bengetderitasiregar@gmail.com','3.3.1.2.4.1','BENGET DERITA SIREGAR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.seksikesehatanhewan&ikan','ronalferinandosembiring@gmail.com','3.3.1.2.3.1','Drh. RONAL FERINANDO SEMBIRING',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.seksikesehatanmasyarakatveteriner','helenajuwitalumbantobing@gmail.com','3.3.1.2.3.2','Drh. HELENA JUWITA LUMBANTOBING',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.seksipenangkapan,pengolahan&pemasaran','sudarmanepasaribu@gmail.com','3.3.1.2.4.2','SUDARMAN E PASARIBU, S.Pi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.seksipengelolaan,sarana&pemasaran','martongamlumbantoruan@gmail.com','3.3.1.2.2.2','MARTONGAM LUMBANTORUAN, S.Pt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.seksiproduksi&perbibitan','basthiantpsihombing@gmail.com','3.3.1.2.2.1','BASTHIAN T P SIHOMBING, S.Pt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.subbagperencanaan&keuangan','jettydumarianaibaho@gmail.com','3.3.1.2.1.2','JETTY DUMARIA NAIBAHO, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnakan.subbagumum&kepegawaian','evamutiarabangun@gmail.com','3.3.1.2.1.1','EVA MUTIARA BANGUN, S.Pt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker','Jonny@gmail.com','2.1.1.1','Drs. Jonny Gultom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.bid.hubindustrial','pentus@gmail.com','2.1.1.1.3','Pentus Simatupang, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.bid.pelatihan&penempatan','sumurung@gmail.com','2.1.1.1.2','Sumurung Sutrisno,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.sekretaris','Victor@gmail.com','2.1.1.1.1','Victor A.S.Manullang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.sekretaris.perencanaan&keuangan','Lasma@gmail.com','2.1.1.1.1.2','Lasma Ester J.Simamora',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.sekretaris.umum','Luhut@gmail.com','2.1.1.1.1.1','Luhut Parhusip',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.seksi.organisasi,persyaratankerja&pengupa','sulastri@gmail.com','2.1.1.1.3.2','Sulastri B.J. Simanullang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.seksi.pelatihantenagakerja','maklum@gmail.com','2.1.1.1.2.1','Maklum Purba, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.seksi.penempatantenagakerja','horas@gmail.com','2.1.1.1.2.2','Horas P.Aritonang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('disnaker.seksi.penyelesaianperselisihan','hariandi@gmail.com','2.1.1.1.3.1','Hariandi H. Sitanggang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar','hotmaidadinaulibutarbutar@gmail.com','3.2.1.1','HOTMAIDA DINA ULI BUTARBUTAR, ST, MT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.bid.kebudayaan','nelsonlumbantoruan@gmail.com','3.2.1.1.4','NELSON LUMBANTORUAN, SS, M.Hum',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.bid.pengembangandestinasi&industripariwisat','ferdinansitinjak@gmail.com','3.2.1.1.2','FERDINAN SITINJAK, ST, MT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.bid.promosi&kelembagaan','bartonnaibaho@gmail.com','3.2.1.1.3','BARTON NAIBAHO,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.sekretaris','nancyfbanjarnahor@gmail.com','3.2.1.1.1','NANCY F.BANJARNAHOR, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.seksiindustripariwisata','sautsitinjak@gmail.com','3.2.1.1.2.2','SAUT SITINJAK, S.Sos, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.seksikelembagaan','baritamanullang@gmail.com','3.2.1.1.3.2','BARITA MANULLANG, ST, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.seksipengembangan&pelestariansenibudaya','indrahutabarat@gmail.com','3.2.1.1.4.1','INDRA HUTABARAT, SS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.seksipengembangandestinasi','hernesrenaldopangaribuan@gmail.com','3.2.1.1.2.1','HERNES RENALDO PANGARIBUAN, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.seksipromosi','harapansibarani@gmail.com','3.2.1.1.3.1','HARAPAN SIBARANI, SS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.seksisejarah&kepurbakalaan','dametudirusamosir@gmail.com','3.2.1.1.4.2','DAME TUDIRU SAMOSIR, S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.subbagperencanaan&keuangan','saidasulastrisitumorang@gmail.com','3.2.1.1.1.2','SAIDA SULASTRI SITUMORANG, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispar.subbagumum&kepegawaian','megawatysimanjuntak@gmail.com','3.2.1.1.1.1','MEGAWATY SIMANJUNTAK, SP, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora','jonniwaslinpurba@gmail.com','2.13.1.1','Drs. JONNI WASLIN PURBA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.bid.kepemudaan','aswinjannessilaban@gmail.com','2.13.1.1.2','ASWIN JANNES SILABAN, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.bid.olahraga','torangpurba@gmail.com','2.13.1.1.3','Ir. TORANG PURBA, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.keuangan','a@dispora.keuangan','2.13.1.1','dispora.keuangan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('dispora.sekretaris','opzensimamora@gmail.com','2.13.1.1.1','OPZEN SIMAMORA, S.Pd, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.seksiinfrastruktur&kemitraanolahraga','cingkasimamora@gmail.com','2.13.1.1.3.2','CINGKA SIMAMORA,S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.seksiinfrastruktur&kemitraanpemuda','boychristiansilaban@gmail.com','2.13.1.1.2.2','BOY CHRISTIAN SILABAN, S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.seksipemberdayaan&pengembanganpemuda','sabarsaragih@gmail.com','2.13.1.1.2.1','SABAR SARAGIH, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.seksipembudayaan&peningkatanprestasiolahra','togilgaol@gmail.com','2.13.1.1.3.1','TOGI L.GAOL,S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.subbagperencanaan&keuangan','agustinuslhutasoit@gmail.com','2.13.1.1.1.2','AGUSTINUS L. HUTASOIT, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dispora.subbagumum&kepegawaian','tarulisimorangkir@gmail.com','2.13.1.1.1.1','TARULI SIIMORANGKIR, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan','juntermarbun@gmail.com','3.3.1.1','Ir. JUNTER MARBUN, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.bid.penyuluhan,prasarana&saranapertanian','lennysihombing@gmail.com','3.3.1.1.4','LENNY SIHOMBING, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.bid.perkebunan','buronianinaibaho@gmail.com','3.3.1.1.3','Ir. BURONIANI NAIBAHO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.bid.tanamanpangan&hortikultura','yoneptahabeahan@gmail.com','3.3.1.1.2','YONEPTA HABEAHAN, SP, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.bidperkebunan.seksipengolahan&pemasaran','hawardi@gmail.com','3.3.1.1.3.3','HAWARDI, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.bidperkebunan.seksiperbenihan&perlindungan','iriantonsimanulang@gmail.com','3.3.1.1.3.2','IRIANTO N. SIMANULLANG, SP, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.bidperkebunan.seksiproduksi','ferrysinulingga@gmail.com','3.3.1.1.3','FERRY SINULINGGA, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.keuangan','a@distan.keuangan','3.3.1.1','distan.keuangan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('distan.sekretaris','idamariamanullang@gmail.com','3.3.1.1.1','Dra. IDA MARIA MANULLANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.seksikelembagaan&ketenagaanpenyuluhanpertan','alireinhardmanalu@gmail.com','3.3.1.1.4.1','ALI REINHARD MANALU, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.seksimetode&informasipenyuluhpertanian','desmerycnapitupulu@gmail.com','3.3.1.1.4.2','DESMERY C. NAPITUPULU, STP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.seksipengolahan&pemasaran','emmylisdasitumorang@gmail.com','3.3.1.1.2.3','EMMY LISDA SITUMORANG, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.seksiperbenihan&perlindungan','sanitaamhutabarat@gmail.com','3.3.1.1.2.2','SANITA A.M HUTABARAT, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.seksiprasarana&sarana','baharahutasoit@gmail.com','3.3.1.1.4.3','BAHARA HUTASOIT, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.seksiproduksi','jennidewitaaristiapurba@gmail.com','3.3.1.1.2.1','JENNI DEWITA ARISTIA PURBA, S.Hut',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.subbagkeuangan&aset','tanreinsianturi@gmail.com','3.3.1.1.1.2','TANREIN SIANTURI, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.subbagperencanaan&evaluasi','ermawatimarbun@gmail.com','3.3.1.1.1.1','ERMAWATI MARBUN, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('distan.subbagumum&kepegawaian','kartiniherawatysihombing@gmail.com','3.3.1.1.1.3','KARTINI HERAWATY SIHOMBING, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil','junias@gmail.com','2.6.1.1','Drs. JUNIAS LUMBANGAOL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.bid.pelayananadministrasipenduduk','tua@gmail.com','2.6.1.1.2','Drs. Tua H. Gultom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.bid.piak&pemanfaatandata','jonni@gmail.com','2.6.1.1.3','Jonni Maruhum,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.sekretaris','kosong@gmail.com','2.6.1.1.1','.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.seksi.kerjasama&inovasipelayanan','jaya@gmail.com','2.6.1.1.3.3','Jayatri H. Sihaloho',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.seksi.pelayananpencatatansipil','guntara@gmail.com','2.6.1.1.2.2','Guntara Romeo Sitorus',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.seksi.pelayananpendaftaranpenduduk','moses@gmail.com','2.6.1.1.2.1','Moses Simanjuntak',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.seksi.pengolahan&penyajiandatakependuduk','agus@gmail.com','2.6.1.1.3.2','Agus Bonar P. Gultom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.subbag.perencanaan&keuangan','bontor@gmail.com','2.6.1.1.1.2','Bontor Silaban',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('dukcatpil.subbag.umum&kepeg','rohani@gmail.com','2.6.1.1.1.1','Rohani Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('inspektorat','bpsiahaan@gmail.com','4.2.1.1','Drs. B.P SIAHAAN, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('inspektorat.irbanadministrasi&kesejahteraanmasyara','edisonmanurung@gmail.com','4.2.1.1.4','EDISON MANURUNG, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('inspektorat.irbanpemerintahan','manusunlumbantoruan@gmail.com','4.2.1.1.2','Drs. MANUSUN LUMBANTORUAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('inspektorat.irbanperekonomian&pembangunan','halimsinabutar@gmail.com','4.2.1.1.3','HALIM SINABUTAR, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('inspektorat.keuangan','firman@gmail.com','4.2.1.1','Firma T.A.P. Sitorus',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-11'),('inspektorat.sekretaris','parmanlumbangaol@gmail.com','4.2.1.1.1','Pangarantoan Lumbantoruan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('inspektorat.subbagperencanaan&pelaporan','irwansihombing@gmail.com','4.2.1.1.1.2','IRWAN SIHOMBING, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('inspektorat.subbagumum&keuangan','firmatapsitorus@gmail.com','4.2.1.1.1.1','FIRMA T.A.P SITORUS,S.Sos,MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol','thomson@gmail.com','4.1.1.5','Thomson Hutasoit, SH, MH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.bid.penanganankonflik&wasnas','pantas_lg@gmail.com','4.1.1.5.3','Pantas Lumban Gaol, S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.bid.poldagri&ormas','robert@gmail.com','4.1.1.5.2','Robert Marbun, S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.sekretaris','pieter@gmail.com','4.1.1.5','Pieter Marbun, S.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.sekretaris.subbag.perencanaan&keuangan','febrianti@gmail.com','4.1.1.5.1.2','Febrianti',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.sekretaris.subbag.umum&kepeg','pakkat@gmail.com','4.1.1.5.1.1','Pakkat Marbun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.subbid.kewaspadaandini','frengki@gmail.com','4.1.1.5.3.2','Frengki M.R. Simanjuntak',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.subbid.ormas','torangsimanullang@gmail.com','4.1.1.5.2.2','Torang M.Simanullang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.subbid.penanganankonflik','budi@gmail.com','4.1.1.5.3.1','Budi Simamora',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kesbangpol.subbid.poldagri','hamzah@gmail.com','4.1.1.5.2.1','Hamzah Surya Situmeang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang','sabarsitanggang@gmail.com','2.3.1.1','Bottor Purba, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.bid.ketersediaan&distribusi','nurmala_purba@gmail.com','2.3.1.1.2','Nurmala Purba, SP, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.bid.konsumsi&keamananpangan','titih@gmail.com','2.3.1.1.3','Titih V. Sihotang, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.sekretaris','sabar_sitanggang@gmail.com','2.3.1.1.1','Sabar Sitanggang,SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.sekretaris.subbag.perencanaan&evaluasi','ratna@gmail.com','2.3.1.1.1.1','Ratna S. Sibuea',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.sekretaris.subbag.umum&kepeg','vera@gmail.com','2.3.1.1.1.2','Vera J. Lumban Gaol',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.seksi.distribusi&kerawananpangan','tetty@gmail.com','2.3.1.1.2.2','Tetty Manurung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.seksi.keamananpangan','jenita@gmail.com','2.3.1.1.3.2','drh. Jenita Pita Aritonang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.seksi.ketersediaanpangan','ika@gmail.com','2.3.1.1.2.1','Ika Dewi Pakpahan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('ketapang.seksi.konsumsipangan','helfridawati@gmail.com','2.3.1.1.3.1','Helfridawati Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('keuangan.diskominfo','diskominfo@humbanghasundutankab.go.id','2.10.1.1','keuangan diskominfo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-07-15'),('keuangan.disnakan','q@keuangan.disnakan','3.3.1.2','keuangan.disnakan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('keuangan.dpmp2tsp','a@keuangan.dpmp2tsp','2.12.1.1','keuangan.dpmp2tsp',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('keuangan.dukcatpil','catpil@keuangan.dukcatpil','2.6.1.1','BONTOR SILABAN, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-15'),('keuangan.ketapang','2@keuangan.ketapang','2.3.1.1','keuangan.ketapang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('keuangan.ketenagakerjaan','s@keuangan.ketenagakerjaan','2.1.1.1','keuangan.ketenagakerjaan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('keuangan.lindup','w@keuangan.lindup','2.5.1.1','keuangan.lindup',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('keuangan.perkim','perkim@s.s','1.4.1.1','perkim',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-15'),('keuangan.sekwan','aw@keuangan.sekwan','4.1.1.4','keuangan.sekwan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('kominfo2','riris@g.g','2.10.1.11','Drs. Hotman Hutasoit',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-06-26'),('kopedagin','radnafridemarbun@gmail.com','2.11.1.1','Ratna Pride Marbun,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.koperasi&umkm','nurlizaepasaribu@gmail.com','2.11.1.1.2','NURLIZA E.PASARIBU, S.Kom,M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.koperasi&umkm.subbidkoperasi','darmohasugian@gmail.com','2.11.1.1.2.1','DARMO HASUGIAN,S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.koperasi&umkm.subbidumkm','marintanrsimbolon@gmail.com','2.11.1.1.2.2','MARINTAN R.SIMBOLON,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.perdagangan','rutanitapurba@gmail.com','2.11.1.1','Eva Duma HNS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.perdagangan.subbidbinausahaperdagangan','panalmanalu@gmail.com','2.11.1.1.3.2','PANAL MANALU',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.perdagangan.subbidpemasaran&perlindungan','ronalsitumorang@gmail.com','2.11.1.1.3.1','RONAL SITUMORANG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.perindustrian','damianuslumbantoruan@gmail.com','2.11.1.1.4','DAMIANUS LUMBAN TORUAN,S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.perindustrian.subbidanekaindusri','guntariaritonang@gmail.com','2.11.1.1.4.2','GUNTARI ARITONANG, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.perindustrian.subbidindustriagro','evigustinarumapea@gmail.com','2.11.1.1.4.1','EVI GUSTINA RUMAPEA,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.sekretaris','henryhamonangan@gmail.com','2.11.1.1.1','HENRY HAMONANGAN,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.subbagperencanaan&keuangan','evadumahnsimamora@gmail.com','2.11.1.1.1.2','EVA DUMA HN. SIMAMORA,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('kopedagin.subbagumum&kepegawaian','rosdinanarosadsitumorang@gmail.com','2.11.1.1.1.1','ROSDIANA ROSA D. SITUMORANG,ST,MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup','minrodsigalingging@gmail.com','2.5.1.1','Ir. MINROD SIGALINGGING',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.bid.pelayanankebersihanpengelolaansampah','ricardolumbantoruan@gmail.com','2.5.1.1.4.','RICARDO LUMBAN TORUAN,S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.bid.penataan&kajiandampaklingkungan','efendirnainggolan@gmail.com','2.5.1.1.2','EFENDI R. NAINGGOLAN, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.bid.pengendalianpencemaran&kerusakanlingkun','halomoanjamanullang@gmail.com','2.5.1.1.3','HALOMOAN J.A MANULLANG, S.Hut',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.sekretaris','harintuapane@gmail.com','2.5.1.1.1','HARIN TUA PANE, S.Pd, S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.seksipelayanankebersihan','marihotsiagian@gmail.com','2.5.1.1.4.1','MARIHOT SIAGIAN,SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.seksipemulihan&kerusakanlingkunganhidup','marihotnaulimanalu@gmail.com','2.5.1.1.3.2','MARIHOT NAULI MANALU, S.Pt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.seksipencemaranlingkunganhidup','dwisaptavivi@gmail.com','2.5.1.1.3.1','DWISAPTA VIVI, O.S, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.seksipengaduan&penyelesaiansengketapenegaka','maristellasimamora@gmail.com','2.5.1.1.2.2','MARISTELLA SIMAMORA, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.seksipengelolaansampah','riantohutahaean@gmail.com','2.5.1.1.4.2','RIANTO HUTAHAEAN, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.seksiperencanaan,pengendalian&kajiandampakl','josephinevtambunan@gmail.com','2.5.1.1.2.1','JOSEPHINE V. TAMBUNAN, SP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.subbagkeuangan,kepegawaian&umum','damesimanullang@gmail.com','2.5.1.1.1.2','DAME SIMANULLANG, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('lindup.subbagprogram,informasi&pelaporan','yosefinsamosir@gmail.com','2.5.1.1.1.1','YOSEFIN SAMOSIR, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('p2kb.keuangan','erlinmanullang@gmail.com','2.8.1.1','Erlin Kurniawati',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-21'),('pariwisata.keuangan','a@pariwisata.keuangan','3.2.1.1','pariwisata.keuangan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('partopitao','partopitao@humbanghasundutankab.go.id',NULL,'Partopi Tao',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-07'),('perhubungan.keuangan','a@perhubungan.keuangan','2.9.1.1','perhubungan.keuangan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('perkim','rockefeller@gmail.com','1.4.1.1','Ir. Rockefeller Simamora',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.bid.kawasanpermukiman','renward@gmail.com','1.4.1.1.3','Renward Henry, ST, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.bid.perumahan','nipson@gmail.com','1.4.1.1.2','Nipson L. Gaol, ST, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.sekretaris','robert@gmail.com','1.4.1.1.1','Robert Silaban, SKM, M.Kes',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.sekretaris.subbag.perencanaandankeuangan','libertynafebriyanthipinem@gmail.com','1.4.1.1','LIBERTYNA FEBRIYANTHI PINEM, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-17'),('perkim.sekretaris.subbag.umum&kepeg','tonggo@gmail.com','1.4.1.1.1.1','Tonggo M. Hutabarat',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.seksi.pembangunan&pemeliharaanperum','canro@gmail.com','1.4.1.1.2.2','Canro Purba, ST, MSi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.seksi.pembangunaninfrastruktur&utilitaskawa','samuel@gmail.com','1.4.1.1.3.3','Samuel Hendra HD. Butarbutar, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.seksi.pembinaan&pengembanganlingkunganhunia','robinhot@gmail.com','1.4.1.1.2.1','Robinhot J. L. Gaol, ST, MT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.seksi.pengawasan&pemeliharaanperum','mulyadi@gmail.com','1.4.1.1.2.3','Mulyadi Ninggolan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.seksi.pertanahan&pengembangankawasanpermuki','boima@gmail.com','1.4.1.1.3.1','Boima Tambunan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perkim.seksi.tatabangunan&lingkungankawasanpermuki','tulus_m@gmail.com','1.4.1.1.3.2','Tulus Manullang, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip','teoffkaem@gmail.com','2.17.1.1','Drs. Jamilin Purba MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-03-05'),('perpusip.bid.kearsipan','geserpane@gmail.com','2.17.1.1.3','GESER PANE, Ama.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip.bid.perpustakaan','adrianusthmahulae@gmail.com','2.17.1.1.2','ADRIANUS TH. MAHULAE, M.Pd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip.sekretaris','jonnymanalu@gmail.com','2.17.1.1.1','Drs. JONNY MANALU',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip.seksipembinaan&pengembanganperpustakaan','heppyyvrsiahaan@gmail.com','2.17.1.1.2.2','HEPPY Y.V.R. SIAHAAN, S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip.seksipembinaankearsipan','rosmasllumbantoruan@gmail.com','2.17.1.1.3.2','ROSMA S.L LUMBANTORUAN, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip.seksipengelolaan&pelestarianarsip','swandymirsal@gmail.com','2.17.1.1.3.1','SWANDY MIRSAL, S.KM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip.seksipengelolaanperpustakaan','lisbetgultom@gmail.com','2.17.1.1.2.1','LISBET GULTOM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip.subbagperencanaan&keuangan','teofilusrajagukguk@gmail.com','2.17.1.1.1.2','TEOFILUS RAJAGUKGUK, SKM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusip.subbagumum&kepegawaian','sadakitabrtarigan@gmail.com','2.13.1.1.1.1','SADAKITA BR. TARIGAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('perpusipsubbagperencanaankeuangan','teoffkaem@gmail.com','2.17.1.1','TEOFILUS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-03-05'),('pmdp2a','vandeik@gmail.com','2.7.1.1','Drs. Elson Sihotang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.bid.administrasipemerintahandesa','jusmar@gmail.com','2.7.1.1.2','Jusmar Efendi Simamora,ST, MT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.bid.pemberdayaanmasyarakat&lembagaadat','jerry@gmail.com','2.7.1.1.3','Jerry Silitonga, SH, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.bid.pemberdayaanperempuan&perlindungananak','resva@gmail.com','2.7.1.1.4','Resva Panjaitan, SE,MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.sekretaris','frans@gmail.com','2.7.1.1.1','Frans Judika B.Pasaribu, SE, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.seksi.admkeuangandesa','venny@gmail.com','2.7.1.1','Venny Sibarani, S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.seksi.binakehidupan&partisipasimasy','dumaria@gmail.com','2.7.1.1.3.1','Dumaria J. Situmorang, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.seksi.evaluasi&pengendalianadministrasipemd','dimpos@gmail.com','2.7.1.1.2.3','Dimpos Situmorang, S.Kom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.seksi.kelembagaan&perangkatdesa','ida@gmail.com','2.7.1.1.2.1','Ida Murniati Harianja, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.seksi.pelembagaanpengarusutamaangender','darni@gmail.com','2.7.1.1.4.1','Darni Asni Pakpahan, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.seksi.pendSDA&TTG','herman@gmail.com','2.7.1.1.3.2','Herman E. Damanik, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.seksi.pengembanganpelayananperlindunganpere','henny@gmail.com','2.7.1.1.4.2','Henny Dwi Putri Silaban',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.seksi.usahaekonomidesa','albertus@gmail.com','2.7.1.1.3.3','Albertus P. Siahaan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.subbag.perencanaan&keuangan','manaek@gmail.com','2.7.1.1.1.2','Manaek R. Hutagalung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmdp2a.subbag.perencanaan.keuangan','manaek_hutagalung@gmail.com','2.7.1.1','Manaek R Hutagalung',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-03-28'),('pmdp2a.subbag.umum&kepeg','octovina@gmail.com','2.7.1.1.1.1','Octovina Rita Tandi T, S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('pmp2tsp','rudolfmanalu@gmail.com','2.12.1.1','Drs. RUDOLF MANALU',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.bid.BUMD dan ESDM','benthonjuberlumbangaol@gmail.com','2.12.1.1.4','BENTHON JUBER LUMBAN GAOL, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.bid.pelayananterpadusatupintu','sunaryosinaga@gmail.com','2.12.1.1.3','SUNARYO SINAGA,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.bid.penanamanmodal','lusianamargrettamba@gmail.com','2.12.1.1.2','LUSIANA MARGRET TAMBA,S.Sos, MPA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.sekretaris','naecisiregar@gmail.com','2.12.1.1.1','NAECI SIREGAR,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.seksiadministrasi','indramarlinatambunan@gmail.com','2.12.1.1.3.1','INDRA MARLINA TAMBUNAN, S,Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.seksiBUMD','pnestorsimanjuntak@gmail.com','2.12.1.1.4.1','P. NESTOR SIMANJUNTAK, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.seksiESDM','bernatmartuaampenpurba@gmail.com','2.12.1.1.4.2','BERNAT MARTUA AMPEN PURBA, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.seksipengembanganiklimpenanamanmodal','heryyosep@gmail.com','2.12.1.1.2.1','HERY YOSEP, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.seksipromosi&kerjasama','tiogarsimanullang@gmail.com','2.12.1.1.2.2','TIOGAR SIMANULLANG,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.seksiteknis','evanaldosinaga@gmail.com','2.12.1.1.3.2','EVANALDO SINAGA, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.subbagperencanaan&keuangan','sumintaaprianisimanullang@gmail.com','2.12.1.1.1.2','SUMINTA APRIANI SIMANULLANG,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pmp2tsp.subbagumum&kepegawaian','agneslastiurpurba@gmail.com','2.12.1.1.1.1','AGNES LASTIUR PURBA, SH, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.bid.binamarga','gibsonpanjaitan@gmail.com','1.3.1.1.2','GIBSON PANJAITAN,ST,MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.bid.penataanruang&bangunangedung','johanvpandiangan@gmail.com','1.3.1.1.4','JOHAN V.PANDIANGAN,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.bid.sumberdayaair','sautsimanullang@gmail.com','1.3.1.1.3','SAUT SIMANULLANG, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.kadis','jhonson@gmail.com','1.3.1.1','JHONSON,ST,M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.keuangan','a@pupr.keuangan','1.3.1.1','pupr.keuangan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-14'),('pupr.sekretaris','bernardmaorisimamora@gmail.com','1.3.1.1.1','BERNARD MAORI SIMAMORA,ST,M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.seksipembangunan&pemeliharaandrainasepengelol','ronnisilalahi@gmail.com','1.3.1.1.3.3','RONNI SILALAHI ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.seksipembangunan&pemeliharaanjalan','petrussabunganhfaradjagukguk@gmail.com','1.3.1.1.2.1','PETRUS SABUNGAN H. F. A. RADJAGUKGUK,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.seksipembangunan&pemeliharaanjembatan','bennyhalomoanmarbun@gmail.com','1.3.1.1.2.2','BENNY HALOMOAN MARBUN,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.seksipembangunanirigasi,sungai&rawa','misaelhsimamora@gmail.com','1.3.1.1.3.1','MISAEL H. SIMAMORA,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.seksipemeliharaansaranairigasi,sungai&rawa','paskariapurba@gmail.com','1.3.1.1.3.2','PASKA RIA PURBA,ST,MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.seksipenataanruang','irwanbaktiarsiregar@gmail.com','1.3.1.1.4.1','IRWAN BAKTIAR SIREGAR,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.seksiperalatan,pengujian&laboratorium','pommermorjuthutabarat@gmail.com','1.3.1.1.2.3','POMMER MORJUT HUTABARAT,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.seksiperkotaan&pedesaan','dimpuojakparitonang@gmail.com','1.3.1.1.4.2','DIMPU OJAK P.ARITONANG,ST, M.Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.subbagperencanaan&keuangan','rsahatnelsonsiagian@gmail.com','1.3.1.1.1.2','R SAHAT NELSON SIAGIAN,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('pupr.subbagumum&kepegawaian','hosdianimsitumorang@gmail.com','1.3.1.1.1.1','HOSDIANI.M.SITUMORANG,ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('riris','rirismanik17@gmail.com',NULL,'Riris Manik',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-06-12'),('rirismanik','rirismanik@humbanghasundutankab.go.id','4.3.1.1','Riris Manik, S.Tr.Kom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-07'),('rsud.direktur','netty@gmail.com','1.2.1.2','dr. Netty Iriani Simanjuntak',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('rsud.subbagumum','fernandomtpurba@gmail.com','1.2.1.2.1.1','FERNANDO MT PURBA,SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudbagiantatausaha','heppisurantadepari@gmail.com','1.2.1.2.1','dr. HEPPI SURANTA DEPARI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudbidkeperawatan','meldarialumbantoruan@gmail.com','1.2.1.2.3','MELDARIA LUMBANTORUAN, SKM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudbidpelayananmedik','nettyirianisimanjuntak@gmail.com','1.2.1.2.2','dr. NETTY IRIANI SIMANJUNTAK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudbidpenunjangmedikdansaranaprasarana','hendrikasimamora@gmail.com','1.2.1.2.4','HENDRIKA SIMAMORA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsuddoloksanggul','sugito@gmail.com','1.2.1.2','dr. Sugito Panjaitan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-11'),('rsudseksipelayananrawatjalan','annasiregar@gmail.com','1.2.1.2.2.1','ANNA SIREGAR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudseksipeningkatanmutupelayanan ','henriromulomanalu@gmail.com','1.2.1.2.2.2','dr. HENRI ROMULO MANALU ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudseksiperawatankebidanan','tinesinaga@gmail.com','1.2.1.2.3.2','TINE SINAGA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudseksiperawatanumum','berliansriwatisinamo@gmail.com','1.2.1.2.3.1','BERLIAN SRIWATI SINAMO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudseksisaranaprasarana','niksonmanalu@gmail.com','1.2.1.2.4.2','NIKSON MANALU, SKM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('rsudsubbagkeuangan','darmamanalu@gmail.com','1.2.1.2','DARMA MANALU, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-25'),('satpolpp','rikky@gmail.com','1.5.1.1','JAULIN SIMANULLANG, S.Pd, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.bid.ketentraman&ketertiban','gilberd@gmail.com','1.5.1.1.2','Gilberd Simanjuntak, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.bid.pencegahan&pemadamkebakaran','rickson_m@gmail.com','1.5.1.1.3','Rickson M. Tambunan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.keuangan','k@satpolpp.keuangan','1.5.1.1','Arar M. Purba, SE MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-08-15'),('satpolpp.seksi.inspeksiperalatan&investigasikejadi','oloan@gmail.com','1.5.1.1.3.3','Oloan Pasaribu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.seksi.linmas&peralatan','dian@gmail.com','1.5.1.1.2.3','Dian A.H Pinem',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.seksi.pemadam','anjur@gmail.com','1.5.1.1.3.2','Anjur Situmorang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.seksi.pencegahan','tommy@gmail.com','1.5.1.1.3.1','Tommy Sigalingging',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.seksi.penegakanprodukhukumdaerah&psdm','monang@gmail.com','1.5.1.1.2.2','Monang Folmer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.subbag.perncanaan&keuangan','arar@gmail.com','1.5.1.1.1.2','Arar P.M Purba',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('satpolpp.subbag.umum&kepeg','japosman@gmail.com','1.5.1.1.1.1','Japosman Sihite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('sekwan','parlindungan@gmail.com','4.1.1.4','Drs. Parlindungan Simamora, MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-09'),('sekwan.bag.perencanaan&keuangan','nn@gmail.com','4.1.1.4.3','NN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('sekwan.bag.umum','darma@gmail.com','4.1.1.4.1','Darma Silaban',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('sekwan.bid.persidangan','tarianus@gmail.com','4.1.1.4.2','Tarianus Simatupang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('sekwan.subbag.humas,keprotokolan&dok','imelda@gmail.com','4.1.1.4.1.2','Imelda V. Simanjuntak',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('sekwan.subbag.keuangan','sempurna@gmail.com','4.1.1.4','Sempurna Silaban',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('sekwan.subbag.perencanaan','amran@gmail.com','4.1.1.4.3.1','Amran Simanullang',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('sekwan.subbag.perundang-undangan','rickson@gmail.com','4.1.1.4.2.2','Rickson J. Simamora',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('sekwan.subbag.umum&kepeg','juli@gmail.com','4.1.1.4.1.1','Juli C. Simanjuntak',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab','tonnysihombing@gmail.com','4.1.1.1','Drs. TONNY SIHOMBING, M.IP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.ekbang','jamarlin@gmail.com','4.1.1.1.7','Ir. Jamarlin Siregar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.ekbang.subbag.binapertanian','nurmala@gmail.com','4.1.1.1.7.1','Nurmala M. Sihotang, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.ekbang.subbag.pembangunan','boyke@gmail.com','4.1.1.1.7.3','Boyke Simanjuntak,SE,MM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.ekbang.subbag.perdagangan','parlin@gmail.com','4.1.1.1.7.2','Parlin Siahaan, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.hukum','suhut@gmail.com','4.1.1.1.5','Suhut Silaban, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.hukum.subbag.pelayanandandokumentasihukum','boy@gmail.com','4.1.1.1.5.2','Boy Orlando T.Sirait, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.hukum.subbag.perundang-undangan','sarwono@gmail.com','4.1.1.1.5.1','Sarwono Sihotang, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.kesos','jakkon@gmail.com','4.1.1.1.10','Jakkon H. Marbun, SE, MM ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.kesos.subbag.binakesehatan','rudi@gmail.com','4.1.1.1.10.3','Rudi H.Hutasoit, SKM',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.kesos.subbag.binapendidikan','sabar@gmail.com','4.1.1.1.10.1','Sabar Manullang, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.kesos.subbag.binasosial','edward@gmail.com','4.1.1.1.10.2','Edward Siregar, SE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.organisasi','kamaruddin@gmail.com','4.1.1.1.6','Drs. Kamaruddin Gultom, M.M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.organisasi.subbag.kelembagaan','saul@gmail.com','4.1.1.1.6.1','Saul Halomoan Tua Hutabarat, S.STP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.organisasi.subbag.ketatalaksanaan','ervina@gmail.com','4.1.1.1.6.2','Sri Ervina Hotnida Manalu, SPi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.protokol','lampos@gmail.com','4.1.1.1.8','Sabar Lampos Purba, ST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.protokol.subbag.protokol','irma@gmail.com','4.1.1.1.8.2','Irma A. Simanungkalit, S.STP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.protokol.subbag.tatausahapimpinan','daniel@gmail.com','4.1.1.1.8.1','Daniel Matondang, S.STP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.tapem','naeksinambela@gmail.com','4.1.1.1.4','NAEK M. SINAMBELA, S.Sos, M.M',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.tapem.subbag.binakecamatan&pemdes','astri@gmail.com','4.1.1.1.4.2','Astri J Sianturi, S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.tapem.subbag.otonomidaerah&kerjasama','posma@gmail.com','4.1.1.1.4.1','POSMA ST.SIMANULLANG, SE.AK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.tapem.subbag.penataanwil','hemat@gmail.com','4.1.1.1.4.3','Hemat A. Sitanggang, S.IP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.umum','rommel@gmail.com','4.1.1.1.9','Rommel Silaban, SH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.umum.subbag.keuangan','mariani@gmail.com','4.1.1.1','Mariani F.Sinaga, SS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.umum.subbag.perlengkapan','roland@gmail.com','4.1.1.1.9.3','Roland V. Marbun, S.STP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('setdakab.umum.subbag.tu&rumahtangga','marlina@gmail.com','4.1.1.1.9.1','Marlina Nainggolan, S.Sos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-01-10'),('tester','tester@humbanghasundutankab.go.id',NULL,'Quality Assurance',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-10-07');

/*Table structure for table `sakip_users` */

DROP TABLE IF EXISTS `sakip_users`;

CREATE TABLE `sakip_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sakip_users` */

insert  into `sakip_users`(`UserName`,`Password`,`RoleID`,`IsSuspend`,`LastLogin`,`LastLoginIP`) values ('admin','354259334e3e5522120d50bad3bf44cd',1,0,'2019-11-27 06:04:47','172.22.1.94'),('bappeda','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-10-07 07:44:31','172.22.1.94'),('bappeda.bid.adm.kesra-litbang','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-11 12:37:40','172.22.0.46'),('bappeda.bid.ekonomi-pembangunan','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-14 14:13:55','172.22.0.46'),('bappeda.bid.pemerintahan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bappeda.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-05-13 06:33:29','172.22.1.94'),('bappeda.sekretaris.subbag.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-29 06:18:02','172.22.1.94'),('bappeda.sekretaris.subbag.umu-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.adm-pendidikan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.ekonomi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.kepedundukan.kom.ketertiban-bencana','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.kesehatan.sosial.kb-ketenagakerjaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.kewilayahan-pemberdayaanmasyarakat','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.penelitian-pengembangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.perum.permukiman-lindup','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.pu.tataruang-perhubungan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bappeda.subbid.sekretariat-aparatur','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bkd.bid.pembinaankarir-disiplin','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bkd.bid.pendayagunaan-kesejahteraan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bkd.kaban','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-25 10:09:13','172.22.1.94'),('bkd.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-25 01:57:48','172.22.1.94'),('bkd.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bkd.subbagperencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bkd.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bkd.subbiddata-informasi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bkd.subbidkesejahteraan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bkd.subbidpembinaan-disiplin','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bkd.subbidperencanaan-pendayagunaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpbd.bid.kedaruratan-logistik','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bpbd.bid.pencegahan-kesiapsiagaan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bpbd.bid.rehabilitasi-rekonstruksi','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-11 09:09:15','172.22.0.46'),('bpbd.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-11 09:11:27','172.22.0.46'),('bpbd.seksikesiapsiagaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpbd.seksilogistik','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpbd.seksipenanganankedaruratan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpbd.seksipencegahan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpbd.seksirehabilitasi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpbd.seksirekonstruksi','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-03-26 16:27:44','172.22.1.94'),('bpbd.subbagkeuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-24 06:54:48','172.22.1.94'),('bpbd.subbagprogram-perencanaan ','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-01-11 09:05:49','172.22.0.46'),('bpbd.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpbdhh','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-19 06:34:01','172.22.1.94'),('bpbdsubbagprogramperencanaan','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-03-05 03:00:50','172.22.1.94'),('bpkpad','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-24 01:41:19','172.22.1.94'),('bpkpad.bid.akuntansiperbendaharaan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bpkpad.bid.anggaran','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-16 11:34:24','172.22.0.46'),('bpkpad.bid.asetdaerah','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bpkpad.bid.pendapatan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bpkpad.kasubbagperencanaan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-23 02:12:15','172.22.1.94'),('bpkpad.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('bpkpad.subbagumum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidanggaranadministrasi-kesejahteraanrak','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidanggaranpemerintahan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidanggaranperekonomian-pembangunan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidinventarisasi-pelaporan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidpembukuan-pelaporankeuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidpemeliharaan-pemanfaatan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidpemungutan-pengendalian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidpendataan-penetapan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidpengelolaanPBB-BPHTB','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidpenggajian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidpenilaian-penghapusan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('bpkpad.subbidverifikasi-perbendaharaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('developer','e10adc3949ba59abbe56e057f20f883e',4,0,'2018-10-07 10:46:15','::1'),('dinaspendidikan','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-13 11:10:49','172.22.1.94'),('dinaspendidikan2','e10adc3949ba59abbe56e057f20f883e',4,0,'2018-10-07 10:50:46','::1'),('dinaspendidikan3','e10adc3949ba59abbe56e057f20f883e',5,0,'2018-10-07 11:13:52','::1'),('dinkes','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-25 03:00:50','172.22.1.94'),('dinkes.bid.kesmasyarakat','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinkes.bid.pelayanan-sumberdayakesehatan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinkes.bid.pencegahan-pengendalianpenyakit','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinkes.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinkes.sekretaris.subbag.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-04 02:00:52','172.22.1.94'),('dinkes.sekretaris.subbag.prog-informasikes','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinkes.seksi.kefarmasian-sarpras','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-01-14 08:35:00','172.22.0.46'),('dinkes.seksi.keskeluarga-gizi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinkes.seksi.keslingkungan.keskerja-olahraga','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinkes.seksi.pelayanan-jaminankesehatan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinkes.seksi.pengendalianpenyakitmenular','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinkes.seksi.pengendalianpenyakittidakmenular-kesw','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinkes.seksi.promosi-pemberdayaankesehatan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinkes.seksi.sdm-perizinan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinkes.seksi.surveilans-imunisasi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinp2kb','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-14 07:35:25','172.22.1.94'),('dinp2kb.bid.kb-kk','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinp2kb.bid.pp-pp','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinp2kb.sekretaris','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-21 02:47:55','172.22.1.94'),('dinp2kb.seksi.advokasi.pp-pemb.plkb','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinp2kb.seksi.jaminanber-kb','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinp2kb.seksi.ketahanan-kk','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinp2kb.seksi.pp-ik','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinp2kb.subbag.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinp2kb.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinsos','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-10-18 03:11:04','172.22.1.94'),('dinsos.bid.pelayanan-rehabilitasisosial','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinsos.bid.pemberdayaan-bantuansosial','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinsos.kabid.bidang.pelayanan.rehabilitasi.sosial','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinsos.kabid.pemberdayaan.bantuan.sosial','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dinsos.kasi.bantuan.jaminan.sosial','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinsos.kasi.pelayanan.sosial','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinsos.kasi.pemberdayaan.sosial','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinsos.kasi.rehabilitasi.sosial','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinsos.subbag.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinsos.subbag.perencanaan.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-15 04:25:33','172.22.1.94'),('dinsos.subbag.umum.kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dinsossekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disdik','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-05-13 11:44:06','172.22.1.94'),('disdik.bid.paud.seksi.sarpras','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disdik.bid.pembinaanketenagaan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disdik.bid.pembinaanpaud','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disdik.bid.pembinaanpendidikandasar','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disdik.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-10-10 06:30:03','172.22.1.94'),('disdik.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disdik.sekretaris.subbag.perencanaankeuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-03-05 07:01:16','172.22.1.94'),('disdik.sekretaris.subbag.umum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disdik.seksi.disiplin-etikaprofesi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disdik.seksi.pemb.paud','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disdik.seksi.pemb.paud-pendnonformal','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disdik.seksi.pendayagunaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disdik.seksi.pendidikansd','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-03-05 07:09:48','172.22.1.94'),('disdik.seksi.pendidikansmp','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-03-05 07:17:13','172.22.1.94'),('disdik.seksi.saranaprasarana','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dishub','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-18 04:03:14','172.22.1.94'),('dishub.bid.mrlla','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dishub.bid.prasarana-sarana','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-15 10:33:43','172.22.0.46'),('dishub.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dishub.sekretaris.subbag.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dishub.sekretaris.subbagumum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dishub.seksi.angkutan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dishub.seksi.mrll','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dishub.seksi.pengujiankendaraanbermotor-perbengkel','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dishub.seksi.prasarana-sarana','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('diskominfo','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-27 02:31:39','172.22.1.94'),('diskominfo.bid.informatika','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-09 14:46:49','172.22.0.46'),('diskominfo.bid.informatika.egov','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('diskominfo.bid.informatika.teknologi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('diskominfo.bid.komunikasi','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('diskominfo.bid.komunikasi.pengelolainformasi-komun','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('diskominfo.bid.komunikasi.statistik-persandian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('diskominfo.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('diskominfo.sekretaris.subbag.perencanaankeuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-07-16 03:00:05','::1'),('diskominfo.sekretaris.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnakan','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-25 05:08:18','172.22.1.94'),('disnakan.bid.kesehatanhewan.ikan-masyarakatveterin','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disnakan.bid.perikanan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disnakan.bid.peternakan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disnakan.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-03-26 09:17:43','172.22.1.94'),('disnakan.seksibudidayaperikanan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnakan.seksikesehatanhewan-ikan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnakan.seksikesehatanmasyarakatveteriner','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnakan.seksipenangkapan.pengolahan-pemasaran','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnakan.seksipengelolaan.sarana-pemasaran','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnakan.seksiproduksi-perbibitan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnakan.subbagperencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnakan.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnaker','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-10 09:10:28','172.22.1.94'),('disnaker.bid.hubindustrial','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disnaker.bid.pelatihan-penempatan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disnaker.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('disnaker.sekretaris.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnaker.sekretaris.umum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnaker.seksi.organisasi.persyaratankerja-pengupa','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnaker.seksi.pelatihantenagakerja','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnaker.seksi.penempatantenagakerja','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('disnaker.seksi.penyelesaianperselisihan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispar','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-08-29 11:03:04','172.22.1.94'),('dispar.bid.kebudayaan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dispar.bid.pengembangandestinasi-industripariwisat','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dispar.bid.promosi-kelembagaan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dispar.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dispar.seksiindustripariwisata','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispar.seksikelembagaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispar.seksipengembangan-pelestariansenibudaya','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispar.seksipengembangandestinasi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispar.seksipromosi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispar.seksisejarah-kepurbakalaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispar.subbagperencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispar.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispora','33b3ac2862267abe6957cccdd5aca5c7',3,0,'2019-11-19 04:00:04','172.22.1.94'),('dispora.bid.kepemudaan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dispora.bid.olahraga','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dispora.keuangan','33b3ac2862267abe6957cccdd5aca5c7',6,0,'2019-10-22 02:10:21','172.22.1.94'),('dispora.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dispora.seksiinfrastruktur-kemitraanolahraga','827ccb0eea8a706c4c34a16891f84e7b',5,0,NULL,NULL),('dispora.seksiinfrastruktur-kemitraanpemuda','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispora.seksipemberdayaan-pengembanganpemuda','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispora.seksipembudayaan-peningkatanprestasiolahra','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispora.subbagperencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dispora.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-25 02:59:10','172.22.1.94'),('distan.bid.penyuluhan.prasarana-saranapertanian','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('distan.bid.perkebunan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('distan.bid.tanamanpangan-hortikultura','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('distan.bidperkebunan.seksipengolahan-pemasaran','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.bidperkebunan.seksiperbenihan-perlindungan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.bidperkebunan.seksiproduksi','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('distan.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-24 10:18:04','172.22.1.94'),('distan.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('distan.seksikelembagaan-ketenagaanpenyuluhanpertan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.seksimetode-informasipenyuluhpertanian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.seksipengolahan-pemasaran','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.seksiperbenihan-perlindungan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.seksiprasarana-sarana','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.seksiproduksi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.subbagkeuangan-aset','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.subbagperencanaan-evaluasi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('distan.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dukcatpil','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-19 08:10:15','172.22.1.94'),('dukcatpil.bid.pelayananadministrasipenduduk','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-11 08:44:19','172.22.0.46'),('dukcatpil.bid.piak-pemanfaatandata','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('dukcatpil.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-03-26 13:51:25','172.22.1.94'),('dukcatpil.seksi.kerjasama-inovasipelayanan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dukcatpil.seksi.pelayananpencatatansipil','b5be656a7060dd3525027d6763c33ca0',5,0,'2019-01-16 09:18:31','172.22.0.46'),('dukcatpil.seksi.pelayananpendaftaranpenduduk','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dukcatpil.seksi.pengolahan-penyajiandatakependuduk','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dukcatpil.subbag.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('dukcatpil.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('inspektorat','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-08 12:06:55','172.22.1.94'),('inspektorat.irbanadministrasi-kesejahteraanmasyara','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('inspektorat.irbanpemerintahan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('inspektorat.irbanperekonomian-pembangunan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('inspektorat.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-10-20 17:10:38','172.22.1.94'),('inspektorat.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('inspektorat.subbagperencanaan-pelaporan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('inspektorat.subbagumum-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kesbangpol','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-26 04:43:33','172.22.1.94'),('kesbangpol.bid.penanganankonflik-wasnas','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('kesbangpol.bid.poldagri-ormas','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('kesbangpol.sekretaris','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-28 12:27:39','172.22.1.94'),('kesbangpol.sekretaris.subbag.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kesbangpol.sekretaris.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kesbangpol.subbid.kewaspadaandini','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kesbangpol.subbid.ormas','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kesbangpol.subbid.penanganankonflik','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kesbangpol.subbid.poldagri','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-03-22 07:27:30','172.22.1.94'),('ketapang','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-20 02:57:46','172.22.1.94'),('ketapang.bid.ketersediaan-distribusi','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('ketapang.bid.konsumsi-keamananpangan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('ketapang.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('ketapang.sekretaris.subbag.perencanaan-evaluasi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('ketapang.sekretaris.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('ketapang.seksi.distribusi-kerawananpangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('ketapang.seksi.keamananpangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('ketapang.seksi.ketersediaanpangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('ketapang.seksi.konsumsipangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('keuangan.diskominfo','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-11-27 02:30:20','172.22.1.94'),('keuangan.disnakan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-12 05:48:42','172.22.1.94'),('keuangan.dpmp2tsp','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-28 08:57:55','172.22.1.94'),('keuangan.dukcatpil','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-10-22 04:20:04','172.22.1.94'),('keuangan.ketapang','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-29 01:24:18','172.22.1.94'),('keuangan.ketenagakerjaan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-29 08:32:38','172.22.1.94'),('keuangan.lindup','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-10-23 07:20:03','172.22.1.94'),('keuangan.perkim','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-29 07:38:53','172.22.1.94'),('keuangan.sekwan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-19 07:43:06','172.22.1.94'),('kominfo2','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-06-26 07:20:56','172.22.1.94'),('kopedagin','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-14 13:59:21','172.22.1.94'),('kopedagin.koperasi-umkm','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-10 16:19:02','172.22.0.46'),('kopedagin.koperasi-umkm.subbidkoperasi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kopedagin.koperasi-umkm.subbidumkm','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-01-10 15:01:28','172.22.0.46'),('kopedagin.perdagangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-28 11:02:19','172.22.1.94'),('kopedagin.perdagangan.subbidbinausahaperdagangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kopedagin.perdagangan.subbidpemasaran-perlindungan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kopedagin.perindustrian','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('kopedagin.perindustrian.subbidanekaindusri','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kopedagin.perindustrian.subbidindustriagro','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kopedagin.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-10 13:07:38','36.78.243.37'),('kopedagin.subbagperencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('kopedagin.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-06 08:40:37','172.22.1.94'),('lindup.bid.pelayanankebersihanpengelolaansampah','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup.bid.penataan-kajiandampaklingkungan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('lindup.bid.pengendalianpencemaran-kerusakanlingkun','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('lindup.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('lindup.seksipelayanankebersihan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup.seksipemulihan-kerusakanlingkunganhidup','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup.seksipencemaranlingkunganhidup','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup.seksipengaduan-penyelesaiansengketapenegaka','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup.seksipengelolaansampah','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup.seksiperencanaan.pengendalian-kajiandampakl','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup.subbagkeuangan.kepegawaian-umum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('lindup.subbagprogram.informasi-pelaporan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('p2kb.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-09 13:24:58','172.22.1.94'),('pariwisata.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-29 11:00:56','172.22.1.94'),('partopitao','e10adc3949ba59abbe56e057f20f883e',2,0,'2018-10-07 09:34:05','::1'),('perhubungan.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-10 02:09:54','172.22.1.94'),('perkim','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-10 15:54:49','172.22.1.94'),('perkim.bid.kawasanpermukiman','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('perkim.bid.perumahan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('perkim.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('perkim.sekretaris.subbag.perencanaandankeuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-01-18 16:03:38','36.84.62.69'),('perkim.sekretaris.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perkim.seksi.pembangunan-pemeliharaanperum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perkim.seksi.pembangunaninfrastruktur-utilitaskawa','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perkim.seksi.pembinaan-pengembanganlingkunganhunia','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-01-17 13:31:16','172.22.0.46'),('perkim.seksi.pengawasan-pemeliharaanperum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perkim.seksi.pertanahan-pengembangankawasanpermuki','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perkim.seksi.tatabangunan-lingkungankawasanpermuki','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perpusip','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-23 23:48:30','172.22.1.94'),('perpusip.bid.kearsipan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('perpusip.bid.perpustakaan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('perpusip.sekretaris','827ccb0eea8a706c4c34a16891f84e7b',4,0,NULL,NULL),('perpusip.seksipembinaan-pengembanganperpustakaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perpusip.seksipembinaankearsipan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perpusip.seksipengelolaan-pelestarianarsip','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perpusip.seksipengelolaanperpustakaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perpusip.subbagperencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perpusip.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('perpusipsubbagperencanaankeuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-11 02:23:49','172.22.1.94'),('pmdp2a','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-17 04:26:51','172.22.1.94'),('pmdp2a.bid.administrasipemerintahandesa','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pmdp2a.bid.pemberdayaanmasyarakat-lembagaadat','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pmdp2a.bid.pemberdayaanperempuan-perlindungananak','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pmdp2a.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pmdp2a.seksi.admkeuangandesa','e10adc3949ba59abbe56e057f20f883e',6,0,NULL,NULL),('pmdp2a.seksi.binakehidupan-partisipasimasy','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmdp2a.seksi.evaluasi-pengendalianadministrasipemd','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmdp2a.seksi.kelembagaan-perangkatdesa','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmdp2a.seksi.pelembagaanpengarusutamaangender','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmdp2a.seksi.pendSDA-TTG','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmdp2a.seksi.pengembanganpelayananperlindunganpere','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmdp2a.seksi.usahaekonomidesa','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmdp2a.subbag.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmdp2a.subbag.perencanaan.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-08-23 04:20:02','172.22.1.94'),('pmdp2a.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmp2tsp','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-08-28 09:12:31','172.22.1.94'),('pmp2tsp.bid.BUMD dan ESDM','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pmp2tsp.bid.pelayananterpadusatupintu','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pmp2tsp.bid.penanamanmodal','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pmp2tsp.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pmp2tsp.seksiadministrasi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmp2tsp.seksiBUMD','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmp2tsp.seksiESDM','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmp2tsp.seksipengembanganiklimpenanamanmodal','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmp2tsp.seksipromosi-kerjasama','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmp2tsp.seksiteknis','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmp2tsp.subbagperencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pmp2tsp.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.bid.binamarga','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pupr.bid.penataanruang-bangunangedung','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pupr.bid.sumberdayaair','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('pupr.kadis','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-27 04:51:18','172.22.1.94'),('pupr.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-11-27 05:37:56','172.22.1.94'),('pupr.sekretaris','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-03-14 06:17:26','172.22.1.94'),('pupr.seksipembangunan-pemeliharaandrainasepengelol','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.seksipembangunan-pemeliharaanjalan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.seksipembangunan-pemeliharaanjembatan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.seksipembangunanirigasi.sungai-rawa','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.seksipemeliharaansaranairigasi.sungai-rawa','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.seksipenataanruang','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.seksiperalatan.pengujian-laboratorium','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.seksiperkotaan-pedesaan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.subbagperencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('pupr.subbagumum-kepegawaian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('riris','59e883cd27e229abddca1d6bce1a3cff',1,0,NULL,NULL),('rirismanik','e10adc3949ba59abbe56e057f20f883e',3,0,'2018-10-18 03:41:24','172.22.0.46'),('rsud.direktur','eeafb716f93fa090d7716749a6eefa72',3,0,'2019-09-11 02:45:49','172.22.1.94'),('rsud.subbagumum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('rsudbagiantatausaha','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('rsudbidkeperawatan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('rsudbidpelayananmedik','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('rsudbidpenunjangmedikdansaranaprasarana','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('rsuddoloksanggul','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-04 09:42:53','172.22.1.94'),('rsudseksipelayananrawatjalan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('rsudseksipeningkatanmutupelayanan ','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('rsudseksiperawatankebidanan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('rsudseksiperawatanumum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('rsudseksisaranaprasarana','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('rsudsubbagkeuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-10-24 09:30:04','172.22.1.94'),('satpolpp','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-12 05:14:27','172.22.1.94'),('satpolpp.bid.ketentraman-ketertiban','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('satpolpp.bid.pencegahan-pemadamkebakaran','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('satpolpp.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-09-12 05:04:07','172.22.1.94'),('satpolpp.seksi.inspeksiperalatan-investigasikejadi','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('satpolpp.seksi.linmas-peralatan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('satpolpp.seksi.pemadam','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('satpolpp.seksi.pencegahan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('satpolpp.seksi.penegakanprodukhukumdaerah-psdm','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('satpolpp.subbag.perncanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('satpolpp.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('sekwan','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-10-21 06:11:52','172.22.1.94'),('sekwan.bag.perencanaan-keuangan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('sekwan.bag.umum','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('sekwan.bid.persidangan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('sekwan.subbag.humas.keprotokolan-dok','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('sekwan.subbag.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-03-19 07:40:52','172.22.1.94'),('sekwan.subbag.perencanaan','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-03-05 03:44:12','172.22.1.94'),('sekwan.subbag.perundang-undangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('sekwan.subbag.umum-kepeg','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-11-18 04:54:52','172.22.1.94'),('setdakab.ekbang','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('setdakab.ekbang.subbag.binapertanian','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.ekbang.subbag.pembangunan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.ekbang.subbag.perdagangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.hukum','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-03-26 08:55:45','172.22.1.94'),('setdakab.hukum.subbag.pelayanandandokumentasihukum','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.hukum.subbag.perundang-undangan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.kesos','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('setdakab.kesos.subbag.binakesehatan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.kesos.subbag.binapendidikan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.kesos.subbag.binasosial','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.organisasi','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-01-16 10:17:20','36.84.225.150'),('setdakab.organisasi.subbag.kelembagaan','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-01-11 14:55:29','172.22.0.46'),('setdakab.organisasi.subbag.ketatalaksanaan','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-01-15 15:39:14','172.22.0.46'),('setdakab.protokol','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('setdakab.protokol.subbag.protokol','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.protokol.subbag.tatausahapimpinan','e10adc3949ba59abbe56e057f20f883e',5,0,'2019-08-13 04:24:21','172.22.1.94'),('setdakab.tapem','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('setdakab.tapem.subbag.binakecamatan-pemdes','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.tapem.subbag.otonomidaerah-kerjasama','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.tapem.subbag.penataanwil','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.umum','e10adc3949ba59abbe56e057f20f883e',4,0,'2019-05-15 05:09:07','172.22.1.94'),('setdakab.umum.subbag.keuangan','e10adc3949ba59abbe56e057f20f883e',6,0,'2019-10-28 05:17:24','172.22.1.94'),('setdakab.umum.subbag.perlengkapan','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('setdakab.umum.subbag.tu-rumahtangga','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL),('tester','e10adc3949ba59abbe56e057f20f883e',5,0,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
