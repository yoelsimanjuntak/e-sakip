/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - esakip_new
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`esakip_new` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `sakip_dpa_kegiatan` */

DROP TABLE IF EXISTS `sakip_dpa_kegiatan`;

CREATE TABLE `sakip_dpa_kegiatan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Nm_KegiatanOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Total` double NOT NULL,
  `Kd_SumberDana` varchar(200) NOT NULL,
  `Create_By` varchar(200) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`),
  KEY `FK_Subbid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_DPA_SasaranProgram2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_dpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Subbid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Table structure for table `sakip_dpa_kegiatan_indikator` */

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_indikator`;

CREATE TABLE `sakip_dpa_kegiatan_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorKegiatanOPD` bigint(10) NOT NULL,
  `Nm_IndikatorKegiatanOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`,`Kd_IndikatorKegiatanOPD`),
  CONSTRAINT `FK_DPA_SasaranKeg` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) REFERENCES `sakip_dpa_kegiatan_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`, `Kd_SasaranKegiatanOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Table structure for table `sakip_dpa_kegiatan_sasaran` */

DROP TABLE IF EXISTS `sakip_dpa_kegiatan_sasaran`;

CREATE TABLE `sakip_dpa_kegiatan_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_Subbid` bigint(10) NOT NULL,
  `Kd_KegiatanOPD` bigint(10) NOT NULL,
  `Kd_SasaranKegiatanOPD` bigint(10) NOT NULL,
  `Nm_SasaranKegiatanOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(50) NOT NULL,
  `Target` double NOT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_Subbid`,`Kd_KegiatanOPD`,`Kd_SasaranKegiatanOPD`),
  KEY `FK_DPA_Subbid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`,`Kd_Subbid`),
  CONSTRAINT `FK_DPA_Kegiatan` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) REFERENCES `sakip_dpa_kegiatan` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`, `Kd_Subbid`, `Kd_KegiatanOPD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_DPA_Subbid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) REFERENCES `sakip_msubbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`, `Kd_Subbid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `sakip_dpa_program` */

DROP TABLE IF EXISTS `sakip_dpa_program`;

CREATE TABLE `sakip_dpa_program` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Nm_ProgramOPD` varchar(200) NOT NULL,
  `Is_Renja` tinyint(1) NOT NULL,
  `Create_By` varchar(200) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  `Edit_By` varchar(200) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`),
  KEY `FK_DPA_Bid` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `FK_DPA_Bid` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `sakip_dpa_program_indikator` */

DROP TABLE IF EXISTS `sakip_dpa_program_indikator`;

CREATE TABLE `sakip_dpa_program_indikator` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Kd_IndikatorProgramOPD` bigint(10) NOT NULL,
  `Nm_IndikatorProgramOPD` varchar(200) NOT NULL,
  `Nm_Formula` varchar(200) DEFAULT NULL,
  `Nm_SumberData` varchar(200) DEFAULT NULL,
  `Nm_PenanggungJawab` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`,`Kd_IndikatorProgramOPD`),
  CONSTRAINT `FK_DPA_SasaranProgram` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) REFERENCES `sakip_dpa_program_sasaran` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`, `Kd_SasaranProgramOPD`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Table structure for table `sakip_dpa_program_sasaran` */

DROP TABLE IF EXISTS `sakip_dpa_program_sasaran`;

CREATE TABLE `sakip_dpa_program_sasaran` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Pemda` bigint(10) NOT NULL,
  `Kd_Misi` bigint(10) NOT NULL,
  `Kd_Tujuan` bigint(10) NOT NULL,
  `Kd_IndikatorTujuan` bigint(10) NOT NULL,
  `Kd_Sasaran` bigint(10) NOT NULL,
  `Kd_IndikatorSasaran` bigint(10) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `Kd_Urusan` bigint(10) NOT NULL,
  `Kd_Bidang` bigint(10) NOT NULL,
  `Kd_Unit` bigint(10) NOT NULL,
  `Kd_Sub` bigint(10) NOT NULL,
  `Kd_TujuanOPD` bigint(10) NOT NULL,
  `Kd_IndikatorTujuanOPD` bigint(10) NOT NULL,
  `Kd_SasaranOPD` bigint(10) NOT NULL,
  `Kd_IndikatorSasaranOPD` bigint(10) NOT NULL,
  `Kd_Bid` bigint(10) NOT NULL,
  `Kd_ProgramOPD` bigint(10) NOT NULL,
  `Kd_SasaranProgramOPD` bigint(10) NOT NULL,
  `Nm_SasaranProgramOPD` varchar(200) NOT NULL,
  `Kd_Satuan` varchar(200) NOT NULL,
  `Target` double NOT NULL,
  `Target_TW1` double DEFAULT NULL,
  `Target_TW2` double DEFAULT NULL,
  `Target_TW3` double DEFAULT NULL,
  `Target_TW4` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQUE` (`Kd_Pemda`,`Kd_Misi`,`Kd_Tujuan`,`Kd_IndikatorTujuan`,`Kd_Sasaran`,`Kd_IndikatorSasaran`,`Kd_Tahun`,`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_TujuanOPD`,`Kd_IndikatorTujuanOPD`,`Kd_SasaranOPD`,`Kd_IndikatorSasaranOPD`,`Kd_Bid`,`Kd_ProgramOPD`,`Kd_SasaranProgramOPD`),
  KEY `FK_DPA_Bid2` (`Kd_Urusan`,`Kd_Bidang`,`Kd_Unit`,`Kd_Sub`,`Kd_Bid`),
  CONSTRAINT `DK_DPA_Program2` FOREIGN KEY (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) REFERENCES `sakip_dpa_program` (`Kd_Pemda`, `Kd_Misi`, `Kd_Tujuan`, `Kd_IndikatorTujuan`, `Kd_Sasaran`, `Kd_IndikatorSasaran`, `Kd_Tahun`, `Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_TujuanOPD`, `Kd_IndikatorTujuanOPD`, `Kd_SasaranOPD`, `Kd_IndikatorSasaranOPD`, `Kd_Bid`, `Kd_ProgramOPD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_DPA_Bid2` FOREIGN KEY (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) REFERENCES `sakip_mbid` (`Kd_Urusan`, `Kd_Bidang`, `Kd_Unit`, `Kd_Sub`, `Kd_Bid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
