ALTER TABLE `sakip_mbid_program_indikator` ADD `Kd_Satuan` VARCHAR(200) NULL AFTER `Nm_PenanggungJawab`, ADD `Target` DOUBLE NULL AFTER `Kd_Satuan`, ADD `Awal` DOUBLE NULL AFTER `Target`, ADD `Akhir` DOUBLE NULL AFTER `Awal`;

ALTER TABLE `sakip_msubbid_kegiatan_indikator` ADD `Kd_Satuan` VARCHAR(200) NULL AFTER `Nm_PenanggungJawab`, ADD `Target` DOUBLE NULL AFTER `Kd_Satuan`, ADD `Awal` DOUBLE NULL AFTER `Target`, ADD `Akhir` DOUBLE NULL AFTER `Awal`;

ALTER TABLE `sakip_dpa_kegiatan_indikator` ADD `Kd_Satuan` VARCHAR(200) NULL AFTER `Nm_PenanggungJawab`, ADD `Target` DOUBLE NULL AFTER `Kd_Satuan`, ADD `Target_TW1` DOUBLE NULL AFTER `Target`, ADD `Target_TW2` DOUBLE NULL AFTER `Target_TW1`, ADD `Target_TW3` DOUBLE NULL AFTER `Target_TW2`, ADD `Target_TW4` DOUBLE NULL AFTER `Target_TW3`, ADD `Kinerja_TW1` DOUBLE NULL AFTER `Target_TW4`, ADD `Kinerja_TW2` DOUBLE NULL AFTER `Kinerja_TW1`, ADD `Kinerja_TW3` DOUBLE NULL AFTER `Kinerja_TW2`, ADD `Kinerja_TW4` DOUBLE NULL AFTER `Kinerja_TW3`, ADD `Anggaran_TW1` DOUBLE NULL AFTER `Kinerja_TW4`, ADD `Anggaran_TW2` DOUBLE NULL AFTER `Anggaran_TW1`, ADD `Anggaran_TW3` DOUBLE NULL AFTER `Anggaran_TW2`, ADD `Anggaran_TW4` DOUBLE NULL AFTER `Anggaran_TW3`;

ALTER TABLE `sakip_dpa_program_indikator` ADD `Kd_Satuan` VARCHAR(200) NULL AFTER `Nm_PenanggungJawab`, ADD `Target` DOUBLE NULL AFTER `Kd_Satuan`, ADD `Target_TW1` DOUBLE NULL AFTER `Target`, ADD `Target_TW2` DOUBLE NULL AFTER `Target_TW1`, ADD `Target_TW3` DOUBLE NULL AFTER `Target_TW2`, ADD `Target_TW4` DOUBLE NULL AFTER `Target_TW3`, ADD `Kinerja_TW1` DOUBLE NULL AFTER `Target_TW4`, ADD `Kinerja_TW2` DOUBLE NULL AFTER `Kinerja_TW1`, ADD `Kinerja_TW3` DOUBLE NULL AFTER `Kinerja_TW2`, ADD `Kinerja_TW4` DOUBLE NULL AFTER `Kinerja_TW3`;

ALTER TABLE `sakip_dpa_program_sasaran` CHANGE `Kd_Satuan` `Kd_Satuan` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `Target` `Target` DOUBLE NULL;

ALTER TABLE `sakip_dpa_kegiatan_sasaran` CHANGE `Kd_Satuan` `Kd_Satuan` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `Target` `Target` DOUBLE NULL;

ALTER TABLE `sakip_dpa_kegiatan` ADD `Budget` DOUBLE NULL AFTER `Total`;

ALTER TABLE `sakip_dpa_kegiatan` ADD `Budget_TW1` DOUBLE NULL AFTER `Budget`, ADD `Budget_TW2` DOUBLE NULL AFTER `Budget_TW1`, ADD `Budget_TW3` DOUBLE NULL AFTER `Budget_TW2`, ADD `Budget_TW4` DOUBLE NULL AFTER `Budget_TW3`;

ALTER TABLE `sakip_dpa_kegiatan` ADD `Anggaran_TW1` DOUBLE NULL AFTER `Budget_TW4`, ADD `Anggaran_TW2` DOUBLE NULL AFTER `Anggaran_TW1`, ADD `Anggaran_TW3` DOUBLE NULL AFTER `Anggaran_TW2`, ADD `Anggaran_TW4` DOUBLE NULL AFTER `Anggaran_TW3`;